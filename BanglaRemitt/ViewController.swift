//
//  ViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 24/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
@IBOutlet weak var btnView: UIView!

@IBOutlet weak var sliderview: UIView!

@IBOutlet weak var pageControl: UIPageControl!

// The UIPageViewController
var pageContainer: UIPageViewController!

// The pages it contains
var pages = [UIViewController]()

// Track the current index
var currentIndex: Int?
private var pendingIndex: Int?

var uc = UtilitySoftTechMedia()

let rounderCorner = RoundedCorner()

var tTime: Timer!

    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var signIn: UIButton!
    @IBOutlet weak var signUp: UIButton!

    @IBOutlet weak var btnExchangeRate: UIButtonStyle!
    
    @IBOutlet weak var btnSendMoneyNow: UIButtonStyle!
    
override func viewDidLoad() {
    super.viewDidLoad()
    
    if (UserDefaults.standard.value(forKey: "AllCountriesList") as? String) != nil {
        
    }else{
        
       // self.AllCountryList()
    }
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let page1: UIViewController! = storyboard.instantiateViewController(withIdentifier: "Page1")
    let page2: UIViewController! = storyboard.instantiateViewController(withIdentifier: "Page2")
    let page3: UIViewController! = storyboard.instantiateViewController(withIdentifier: "Page3")
    let page4: UIViewController! = storyboard.instantiateViewController(withIdentifier: "Page4")
    
    pages.append(page1)
    pages.append(page2)
    pages.append(page3)
    pages.append(page4)
    
    // Create the page container
    pageContainer = UIPageViewController()
    pageContainer = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    pageContainer.delegate = self
    pageContainer.dataSource = self
    pageContainer.setViewControllers([page1], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
    currentIndex = 0
    
    
    self.view.addSubview(pageContainer.view)
    
    self.view.bringSubviewToFront(self.pageControl)
    self.view.bringSubviewToFront(self.btnView)
    self.view.bringSubviewToFront(self.btnExchangeRate)
    self.view.bringSubviewToFront(self.btnSendMoneyNow)
    self.view.bringSubviewToFront(self.viewTop)
    pageControl.numberOfPages = pages.count
    pageControl.currentPage = 0
    
    
    tTime = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(changeSlide), userInfo: nil, repeats: true)
    
    if (UserDefaults.standard.value(forKey: "AllCountriesList") as? String) != nil {
        
    }else{
        
        self.AllCountryList()
    }
    // Do any additional setup after loading the view, typically from a nib.
}

override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
}


@IBAction func btnExchangeRateClick(_ sender:Any){
    
    
   self.navigationController?.pushViewController(ExchangeRateViewController(), animated: true)
    
}
@objc func changeSlide() {
    
    if currentIndex == pages.count {
        currentIndex = 0
    }

    if currentIndex == 0 {
        //show buttons
        
        self.view.bringSubviewToFront(self.btnExchangeRate)
        self.view.bringSubviewToFront(self.btnSendMoneyNow)
    }else {
        //hide buttons
        
        self.view.sendSubviewToBack(self.btnExchangeRate)
        self.view.sendSubviewToBack(self.btnSendMoneyNow)
    }
    
    pageContainer.setViewControllers([pages[currentIndex!]], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
    
    pageControl.currentPage = currentIndex!
    currentIndex =  currentIndex! + 1
    
    
}

func viewController(at index: Int) -> UIViewController? {
    let childViewController = storyboard?.instantiateViewController(withIdentifier: "Page\(Int(UInt(index)) + 1)")
    //childViewController?.index = index
    if index == 0 {
        view.backgroundColor = UIColor(red: 0.000 / 255.000, green: 123.000 / 255.000, blue: 203.000 / 255.000, alpha: 1.0)
    } else if index == 1 {
        
    } else {
        view.backgroundColor = UIColor(red: 41.000 / 255.000, green: 61.000 / 255.000, blue: 80.000 / 255.000, alpha: 1.0)
    }
    return childViewController
}
@IBAction func btnSignInCick(_ sender: Any) {
        self.navigationController?.pushViewController(LoginViewController(), animated: true)
}

@IBAction func btnSignUpClick(_ sender: Any) {
    self.navigationController?.pushViewController(SignUp1ViewController(), animated: true)
}

func AllCountryList(){


    let parms =  ["Type":"3"]as [String : Any]


    uc.webServicePosthttp(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading..."){result in

        let AllCountiresResponse = AppUser(JSONString:result)

        if AllCountiresResponse?.myAppResult?.Code == 0 {


            UserDefaults.standard.set(result, forKey: "AllCountriesList")


        }else if AllCountiresResponse?.myAppResult?.Code == 101 {

            self.uc.logout(self)

        }else{

            if(AllCountiresResponse?.myAppResult?.Message == nil){

                self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)

            }else{
                self.uc.errorSuccessAler("Error", (AllCountiresResponse?.myAppResult?.Message)!, self)
            }
        }

    }

}

    @IBAction func btnSendMoney(_ sender: UIButton) {
        self.navigationController?.pushViewController(LoginViewController(), animated: true)
    }
    
    
}

extension ViewController:UIPageViewControllerDelegate,UIPageViewControllerDataSource{
    
    // MARK: - UIPageViewController delegates
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.firstIndex(of:viewController )
        if currentIndex == 0 {
            return  pages[1]
            // return nil
        }
        let previousIndex = abs((currentIndex! - 1) % pages.count)
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.firstIndex(of:viewController )
        if currentIndex == pages.count-1 {
            //return nil
            return pages[0]
        }
        let nextIndex = abs((currentIndex! + 1) % pages.count)
        return pages[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        pendingIndex = pages.firstIndex(of:pendingViewControllers.first!)
        
        print(pendingIndex!)
        //Hide and show buttons
        if pendingIndex == 0 {
            self.view.bringSubviewToFront(self.btnExchangeRate)
            self.view.bringSubviewToFront(self.btnSendMoneyNow)

        }else{
            self.view.sendSubviewToBack(self.btnExchangeRate)
            self.view.sendSubviewToBack(self.btnSendMoneyNow)
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            
            currentIndex = pendingIndex
            if let index = currentIndex {
                pageControl.currentPage = index
                
            }
        }
    }
}
