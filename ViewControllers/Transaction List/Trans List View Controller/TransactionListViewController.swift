//
//  TransactionListViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 08/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class TransactionListViewController: UIViewController {

     @IBOutlet var tranactionListView: TransactionListView!
    
    let uc  = UtilitySoftTechMedia()
    
    let mypopover = Popover()
    
    var TransactionListResponse : TransactionListresult!
    var filterArray = [TransactionListDetail]()
    var strArray = ["ahmad", "aslam","sajid","Danish"]
    var isSearch = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tranactionListView.noTranactionView.isHidden = true
        
        self.tranactionListView.tblTranactions.register(UINib(nibName: "TransListCell", bundle: nil), forCellReuseIdentifier: "TranactionListCell")
        
        self.addDoneButtonOnKeyboard()
    }

    override func viewWillAppear(_ animated: Bool) {
       
        self.GetTransactionList()

    }
    //Button Back Click
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //Button Add Now Click
    @IBAction func btnAddNowClick(_ sender: Any) {
        
        self.navigationController?.pushViewController(CreateTransStep1ViewController(), animated: true)
        
    }
    
    //Button Add New Tranaction Click
    @IBAction func btnAddNewTransClick(_ sender: Any) {
        
        self.navigationController?.pushViewController(CreateTransStep1ViewController(), animated: true)
    }


}

extension TransactionListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let searchString = searchText.trimWhiteSpace()
        if searchString != "", searchString.count > 0 {
            
            filterArray = self.TransactionListResponse.transactionListDetail!.filter {
                isSearch = true
                return (($0.PaymentNumber?.localizedCaseInsensitiveContains(searchText)))!
            }

//            filterArray = self.TransactionListResponse.transactionListDetail!.filter {+
//                isSearch = true
//                return $0.BeneName?.range(of: searchString, options: .caseInsensitive) != nil
//            }
        }else {
            isSearch = false
        }
        self.tranactionListView.tblTranactions.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        filterArray = self.TransactionListResponse.transactionListDetail!
        self.tranactionListView.tblTranactions.reloadData()
    }
    
}

extension String {
    func trimWhiteSpace() -> String {
        let string = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return string
    }
}
