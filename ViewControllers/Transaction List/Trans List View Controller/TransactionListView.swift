//
//  TransactionListView.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 08/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class TransactionListView: UIView {

    //Label Title
    @IBOutlet weak var lblTitle: UILabel!
    
    //Tranaction Table View
    @IBOutlet weak var tblTranactions: UITableView!
    
    //No Tranaction View
    @IBOutlet weak var noTranactionView: UIView!
    
    //Button Back
    @IBOutlet weak var btnBack: UIButton!
    
    //Add More Tranaction View when some Tranaction Already Exist
    @IBOutlet weak var AddTranactionView: UIView!
    
    //Add More Tranaction Button when some Tranaction Exist
    @IBOutlet weak var btnAddTranaction: UIButton!
    
    @IBOutlet weak var searchBar: UISearchBar!

}
