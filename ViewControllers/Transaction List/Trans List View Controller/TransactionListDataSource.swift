//
//  TransactionListDataSource.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 08/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension TransactionListViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let details = self.TransactionListResponse?.transactionListDetail![indexPath.row]
//        let url = self.TransactionResponse!.transactionDetail!.PaymentPageURL!
//        let paymentId = self.TransactionResponse!.transactionDetail!.PaymentID
//        let viewControllerstep4 = CreateTransStep4ViewController()
//        viewControllerstep4.weburl = "\(url)?id=\(String(describing: paymentId!))"
        
        let url = details?.PaymentPageURL
        let paymentId = details?.PaymentID
        
        
        if details?.PaymentStatus == "Canceled" {
            let transactionDetailObject = TransactionDetail()
            transactionDetailObject.transactionListDetail = details
            self.navigationController?.pushViewController(transactionDetailObject, animated: true)
        }else if details?.PaymentStatus == "Incomplete"{
            
            let createTransStep4ViewController = CreateTransStep4ViewController()
            createTransStep4ViewController.weburl = "\(String(describing: url!))?id=\(String(describing: paymentId!))"
            self.navigationController?.pushViewController(createTransStep4ViewController, animated: true)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        if(self.TransactionListResponse != nil){
//
//            if(self.TransactionListResponse?.transactionListDetail != nil){
//
//                return (self.TransactionListResponse?.transactionListDetail?.count)!
//            }
//        }
//        return 0
        if(self.TransactionListResponse != nil){
                   
                   if(self.TransactionListResponse?.transactionListDetail != nil){
                    if isSearch {
                       return filterArray.count
                    }else{
                        return (self.TransactionListResponse?.transactionListDetail?.count)!
                    }
                   }
               }
               return 0

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tranactionListView.tblTranactions.dequeueReusableCell(withIdentifier: "TranactionListCell") as? TransListCell
        
        let details = self.TransactionListResponse?.transactionListDetail![indexPath.row]
        
        
        if isSearch{
            let detailsSearchArray = filterArray[indexPath.row]
            cell?.lblRecipientName.text     = detailsSearchArray.BeneName
            cell?.lblTransPIN.text          = "PIN: \(String(describing: detailsSearchArray.PaymentNumber)) "
            cell?.lblTranactionDate.text    = detailsSearchArray.PaymentDate?.components(separatedBy: "T")[0]
            cell?.lblTransactionStatus.text = detailsSearchArray.PaymentStatus
            cell?.imgTranaction.image       = UIImage(named:"\((details?.ReceivingCountry)!).png")
        }else{
            cell?.lblRecipientName.text = details?.BeneName
            cell?.lblTransPIN.text = "PIN: \((details?.PaymentNumber)!)"
            cell?.lblTranactionDate.text = details?.PaymentDate?.components(separatedBy: "T")[0]
            cell?.lblTransactionStatus.text = details?.PaymentStatus
            cell?.imgTranaction.image = UIImage(named:"\((details?.ReceivingCountry)!).png")
        }

//        self.transactionListResponse.AceTransList![indexPath.row].PaymentStatus
        guard let paymentStatus = self.TransactionListResponse.transactionListDetail![indexPath.row].PaymentStatus else {
            
            return cell!
        }
        
        //color code
        /*
        
        
        */
        
        if(paymentStatus == "Canceled"){
            
            cell?.lblTransactionStatus.textColor = #colorLiteral(red: 0.6, green: 0, blue: 0, alpha: 1)
            cell?.lblTransactionStatus.text = "Cancelled" //"\((paymentStatus))"
            cell?.imgVStatus.image = UIImage(named: "cancelled_icon")
 
            
        }else if(paymentStatus == "Canceling"){
            
            cell?.lblTransactionStatus.textColor = #colorLiteral(red: 0.6, green: 0, blue: 0, alpha: 1)
            cell?.lblTransactionStatus.text = "\((paymentStatus))"
            cell?.imgVStatus.image = UIImage(named: "cancelled_icon")
            
        }else if(paymentStatus == "Completed"){
            
            cell?.lblTransactionStatus.textColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
            cell?.lblTransactionStatus.text = "\((paymentStatus))"
            cell?.imgVStatus.image = UIImage(named: "complete_icon")
            
        }else if(paymentStatus == "In-process"){
            
            cell?.lblTransactionStatus.textColor = #colorLiteral(red: 0, green: 0.5450980392, blue: 0.8, alpha: 1)
            cell?.lblTransactionStatus.text = "\((paymentStatus))"
            cell?.imgVStatus.image = UIImage(named: "in_process_icon")
            
        }else if(paymentStatus == "Incomplete"){
            
            cell?.lblTransactionStatus.textColor = #colorLiteral(red: 0.8, green: 0.4, blue: 0, alpha: 1)
            cell?.lblTransactionStatus.text = "\((paymentStatus))"
            cell?.imgVStatus.image = UIImage(named: "in_complete_icon")
            cell?.lblTransPIN.text = "*****"
            
            
        }else if(paymentStatus == "Ok"){
            
            cell?.lblTransactionStatus.textColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
            cell?.lblTransactionStatus.text = "\((paymentStatus))"
            cell?.imgVStatus.image = UIImage(named: "complete_icon")
            
        }else if(paymentStatus == "Pending"){
            
            cell?.lblTransactionStatus.textColor = #colorLiteral(red: 0, green: 0.5450980392, blue: 0.8, alpha: 1)
            cell?.lblTransactionStatus.text = "\((paymentStatus))"
            cell?.imgVStatus.image = UIImage(named: "in_process_icon")
            
        }else if(paymentStatus == "Paid"){
            
            cell?.lblTransactionStatus.textColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
            cell?.lblTransactionStatus.text = "\((paymentStatus))"
            cell?.imgVStatus.image = UIImage(named: "complete_icon")
        }else{
            
            cell?.lblTransactionStatus.textColor = #colorLiteral(red: 0.8, green: 0.4, blue: 0, alpha: 1)
            cell?.lblTransactionStatus.text = paymentStatus
            cell?.imgVStatus.image = UIImage(named: "in_complete_icon")
        }
        
        
        cell!.btnEdit.addTarget(self, action: #selector(self.ShowPopUP(sender:)), for: .touchUpInside)
        cell!.btnEdit.tag = indexPath.row

        return cell!
    }
    
    //Start PopUp
    
    @objc func ShowPopUP(sender: UIButton){

       // let paymentStatus = self.transactionListResponse.AceTransList![sender.tag].PaymentStatus

        let paymentStatus = self.TransactionListResponse.transactionListDetail![sender.tag].PaymentStatus
//        let buttonPosition = sender.convert(CGPoint.zero, to: self.self.tranactionListView.tblTranactions)

        let buttonPosition = sender.convert(CGPoint.zero, to: self.self.tranactionListView.tblTranactions)

        let indexPath = self.self.tranactionListView.tblTranactions.indexPathForRow(at:buttonPosition)
        let rectOfCellInTableView = self.tranactionListView.tblTranactions.rectForRow(at: indexPath!)
        let rectOfCellInSuperview = self.tranactionListView.tblTranactions.convert(rectOfCellInTableView, to: self.tranactionListView.tblTranactions.superview)
        // print("Y of Cell is: \(rectOfCellInSuperview.origin.y)")

        var height = CGFloat()
        var aView = UIView()
       if(paymentStatus == "Incomplete")
       {

        _ = sender.tag
        _ = CGPoint(x: sender.frame.origin.x+225, y: rectOfCellInSuperview.origin.y+260)
        aView = UIView(frame: CGRect(x: 10, y: 20, width: self.view.frame.width/2, height: 60))

        let View = UIButton() // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        View.setTitle("View Detail", for: .normal)
        View.setTitleColor(UIColor.black, for: .normal)
        View.frame = CGRect(x: aView.frame.origin.x+10, y:10, width:self.view.frame.width/2,height:40)
        View.tag=sender.tag
        View.addTarget(self, action: #selector(self.View(_:)), for: .touchUpInside)
        View.contentHorizontalAlignment = .left

        aView.addSubview(View)
        }
        else
       {

        _ = sender.tag
        _ = CGPoint(x: sender.frame.origin.x+225, y: rectOfCellInSuperview.origin.y+260)
        aView = UIView(frame: CGRect(x: 0, y: 20, width: self.view.frame.width/2, height: 220))

        let View = UIButton() // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        View.setTitle("View Detail", for: .normal)
        View.setTitleColor(UIColor.black, for: .normal)
        View.frame = CGRect(x: aView.frame.origin.x+10, y:10, width:self.view.frame.width/2,height:40)
        View.tag=sender.tag
        View.addTarget(self, action: #selector(self.View(_:)), for: .touchUpInside)
        View.contentHorizontalAlignment = .left
        height = 10 + 50

        let Send = UIButton() // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        Send.setTitle("Send Again", for: .normal)
        Send.setTitleColor(UIColor.black, for: .normal)
        if( paymentStatus == "Canceled" || paymentStatus == "Complete" ){
            Send.frame = CGRect(x: aView.frame.origin.x+10, y:height, width:self.view.frame.width/2,height:40)
        }
        Send.tag=sender.tag
        Send.addTarget(self, action: #selector(self.Send(_:)), for: .touchUpInside)
        Send.contentHorizontalAlignment = .left

        let Complaint = UIButton() // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        Complaint.setTitle("Complaint", for: .normal)
        Complaint.setTitleColor(UIColor.black, for: .normal)
        if( paymentStatus == "In-process" || paymentStatus == "Complete" || paymentStatus == "Incomplete" || paymentStatus == "Ok" || paymentStatus == "ok" ){
            height = height + 50
            Complaint.frame = CGRect(x: aView.frame.origin.x+10, y:height, width:self.view.frame.width/2,height:40)
            aView.addSubview(Complaint)
        }
        Complaint.tag=sender.tag
        Complaint.addTarget(self, action: #selector(self.Complaint(_:)), for: .touchUpInside)
        Complaint.contentHorizontalAlignment = .left

        let Cancel = UIButton() // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        Cancel.setTitle("Cancel Transaction", for: .normal)
        Cancel.setTitleColor(UIColor.black, for: .normal)
        if( paymentStatus == "In-process"  ){
            Cancel.frame = CGRect(x: aView.frame.origin.x+10, y:60, width:self.view.frame.width/2,height:40)
        }
        Cancel.tag=sender.tag
        Cancel.addTarget(self, action: #selector(self.Cancel(_:)), for: .touchUpInside)
        Cancel.contentHorizontalAlignment = .left

        if( paymentStatus == "Canceled" || paymentStatus == "Complete" ){

            aView = UIView(frame: CGRect(x: 0, y: 20, width: self.view.frame.width/2, height: 110))

        }else if( paymentStatus == "In-process"){

            aView = UIView(frame: CGRect(x: 0, y: 20, width: self.view.frame.width/2, height: 160))
            aView.addSubview(Cancel)

        }

        if( paymentStatus != "Canceled" && paymentStatus != "Complete" && paymentStatus != "In-process" && paymentStatus != "Incomplete" )
        {
            aView = UIView(frame: CGRect(x: 0, y: 20, width: self.view.frame.width/2, height: 110))
            aView.addSubview(Send)
        }
        if( paymentStatus == "Canceled" || paymentStatus == "Complete" ){
            aView.addSubview(Send)
        }
        aView.addSubview(View)
        }

        mypopover.isHidden=false

        if((indexPath?.row)! < 2){

            mypopover.popoverType = .down
        }else{

            mypopover.popoverType = .up
        }

        mypopover.show(aView, fromView: sender)
    }
    
    //End PopUp
    
    //start view()
    
    @objc func View(_ sender:UIButton){

        let item = self.TransactionListResponse.transactionListDetail![sender.tag]
        let transactionDetailObject = TransactionDetail()
        transactionDetailObject.transactionListDetail = item
        
       if (self.TransactionListResponse.transactionListDetail![sender.tag].SendingPaymentMethod == "Cash")
           {
//               self.performSegue(withIdentifier: "PayAtLoc", sender: sender.tag)
            self.navigationController?.pushViewController(transactionDetailObject, animated: true)
           }
           else if(self.TransactionListResponse.transactionListDetail![sender.tag].SendingPaymentMethod == "Bank")
           {
//               self.performSegue(withIdentifier: "BankDetail", sender: sender.tag)
            
            
            self.navigationController?.pushViewController(transactionDetailObject, animated: true)
           }
           else
           {

            self.navigationController?.pushViewController(transactionDetailObject, animated: true)
            

//               self.performSegue(withIdentifier: "TransDetail", sender: sender.tag)
           }
           mypopover.dismiss()

       }
       
       @objc func Send(_ sender:UIButton){
           //PaymentSegue
        
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                
                // if Controller is not in navigation then will execute else part.
                if vc.isKind(of: CreateTransStep1ViewController.self)  {
                    print("It is in stack")
                    navigationController?.popToViewController(CreateTransStep1ViewController(), animated: true)
                    mypopover.dismiss()
                    return
                    
                }
            }
        }
        
        self.navigationController?.pushViewController(CreateTransStep1ViewController(), animated: true)
        mypopover.dismiss()
        
//           self.performSegue(withIdentifier: "NewTransaction", sender: sender.tag)
           

       }
    
        @objc func Complaint(_ sender:UIButton){
           self.performSegue(withIdentifier: "Complance", sender: sender.tag)
           mypopover.dismiss()
       }
    
    @objc func Cancel(_ sender:UIButton){

        let alertController = UIAlertController(title: "Cancel Reason", message: "", preferredStyle: .alert)

        let DOne = UIAlertAction(title: "DONE", style: .default) { (aciton) in

            let text = alertController.textFields!.first!.text!

            if text.isEmpty {
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                self.CancelTransactions(row: sender.tag, Reason: text)
            }
        }

        let cancelAction = UIAlertAction(title: "CLOSE", style: .cancel) { (action) in
        }

        alertController.addTextField { (textField) in
            textField.placeholder = "Reason"
            textField.keyboardType = .namePhonePad
            textField.addConstraint(textField.heightAnchor.constraint(equalToConstant: 50))
        }

        alertController.addAction(DOne)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)

        mypopover.dismiss()

    }
    
    func CancelTransactions(row : Int,Reason : String){
        
        let authToken = uc.getAuthToken()
        
        let parms =  ["ID":(authToken?.userId)!,
                      "Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,
                      "CancelReason":"\(Reason)",
                      "PaymentNumber":"\(String(describing: self.TransactionListResponse.transactionListDetail![row].PaymentNumber!))"]as [String : Any]
            
        uc.webServicePosthttp(urlString: ApiUrls.Canceltransaction, params:parms , message: "Loading..."){result in
            
            if(result == "fail")
            {
                self.CancelTransactions(row: row, Reason: Reason)
                return
            }
            
            let responses = CancelRequestResponse(JSONString:result)
            print("\n\n\n\n Cancel request response : \(result)\n\n\n\n")
            if responses?.myAppResult?.Code == 0 {
                
                self.uc.errorSuccessAler("Alert", (responses?.myAppResult?.Message!)!, self)
                self.getUserTransactions()
            }else if responses?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(responses?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (responses?.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    func getUserTransactions(){
            let authToken = uc.getAuthToken()
        
        let parms =  ["ID":(authToken?.userId)!,
                        "Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,
                        "Limit":"0",
                        "PaymentMethod":""]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetTransactionList, params:parms , message: "Loading..."){result in
            
            if(result == "fail")
            {
                self.getUserTransactions()
                return
            }
            
//                self.transactionListResponse = TransactionList(JSONString:result)
            self.TransactionListResponse = TransactionListresult(JSONString: result)
            
        
            if self.TransactionListResponse?.myAppResult?.Code == 0 {
                
                if(self.TransactionListResponse.transactionListDetail != nil && (self.TransactionListResponse.transactionListDetail?.count)! > 0){
                    
                    self.tranactionListView.tblTranactions.isHidden = false
                    self.tranactionListView.noTranactionView.isHidden = true
                    self.tranactionListView.tblTranactions.reloadData()
//                        self.tblTransList.isHidden = false
//                        self.noTransView.isHidden = true
//                        self.tblTransList.reloadData()
                    
                }else{
                    
                    self.tranactionListView.tblTranactions.isHidden = true
                    self.tranactionListView.noTranactionView.isHidden = false
                    
//                       self.tblTransList.isHidden = true
//                       self.noTransView.isHidden = false
                    
                }
                
                
            }else if self.TransactionListResponse?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
//                self.navigationController?.popToRootViewController(animated: true)
                
                
            }else{
                
                self.tranactionListView.tblTranactions.isHidden = true
                self.tranactionListView.noTranactionView.isHidden = false
                
//                    self.tblTransList.isHidden = true
//                    self.noTransView.isHidden = false
                
            }
        }
    }
    
    //End view()
    
}

extension TransactionListViewController{
    
    func GetTransactionList() {
        
        let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"Limit":"0"]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetTransactionList, params:parms , message: "Loading..."){result in
            
            self.TransactionListResponse = TransactionListresult(JSONString:result)
            
            if  self.TransactionListResponse?.myAppResult?.Code == 0 {
                
                print(result)
                
                if(self.TransactionListResponse?.transactionListDetail != nil){
                    
                    //for searching
//                    self.filterArray = (self.TransactionListResponse?.transactionListDetail!)!
                    
                    self.tranactionListView.AddTranactionView.isHidden = false
                    self.tranactionListView.tblTranactions.isHidden = false
                    self.tranactionListView.noTranactionView.isHidden = true
                    self.tranactionListView.tblTranactions.reloadData()
                    
                }else{
                    
                    self.tranactionListView.AddTranactionView.isHidden = true
                    self.tranactionListView.tblTranactions.isHidden = true
                    self.tranactionListView.noTranactionView.isHidden = false
                }

                
            }else if  self.TransactionListResponse?.myAppResult?.Code == 102{
                
                self.tranactionListView.AddTranactionView.isHidden = true
                self.tranactionListView.tblTranactions.isHidden = true
                self.tranactionListView.noTranactionView.isHidden = false
                
            }else{
                
                if( self.TransactionListResponse?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", (self.TransactionListResponse?.myAppResult?.Message)!, self)
                }
            }
        }
    }
}


extension TransactionListViewController{
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonActionSend))
        done.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
//        self.tranactionListView.searchBar.inputAccessoryView = doneToolbar
        self.tranactionListView.searchBar.inputAccessoryView = doneToolbar
        
        
    }
    
    @objc func doneButtonActionSend(){
        self.tranactionListView.searchBar.resignFirstResponder()
    }
}
