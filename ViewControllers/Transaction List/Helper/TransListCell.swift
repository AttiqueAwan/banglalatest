//
//  TransListCell.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 08/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class TransListCell: UITableViewCell {

    @IBOutlet weak var lblRecipientName: UILabel!
    @IBOutlet weak var imgTranaction: UIImageView!
    @IBOutlet weak var lblTransPIN: UILabel!
    @IBOutlet weak var lblTransactionStatus: UILabel!
    @IBOutlet weak var lblTranactionDate: UILabel!
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    @IBOutlet weak var imgVStatus: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
