//
//  PostalCodeCell.swift
//  BanglaRemitt
//
//  Created by Apple on 04/05/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit

class PostalCodeCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblPostalCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
