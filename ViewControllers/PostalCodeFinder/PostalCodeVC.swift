//
//  PostalCodeVC.swift
//  BanglaRemitt
//
//  Created by Apple on 23/04/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit

protocol ClassBVCDelegate: class {
func changeBackgroundColor(_ model: PostCodeFinderDetail2?)
}


class PostalCodeVC: UIViewController {
    
    
    @IBOutlet weak var txtPostalCode: UITextField!
    @IBOutlet weak var tblV: UITableView!
    var postalCode = ""
    let uc = UtilitySoftTechMedia()
    var postCodeFinderresult  : PostCodeFinderresult!
    var postCodeFinderresultDetail  : PostCodeFinderresultDetail!
    var postCodeFindResultSelected : PostCodeFinderDetail! 
    
    var PostCodeDetail = [PostCodeFinderDetail]()

    weak var delegate: ClassBVCDelegate?
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if postalCode != "" {
            print(postalCode)
            self.txtPostalCode.text = postalCode
            self.GetPostalCode()
        }
        
         self.tblV.register(UINib(nibName: "PostalCodeCell", bundle: nil), forCellReuseIdentifier: "PostCodeCell")
        
    }

    @IBAction func btnFindAddress(_ sender: UIButton) {
        
        if(self.txtPostalCode.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Please enter valild post code", self)
            
        }else if((self.txtPostalCode.text?.count)! < 4){
            
            uc.errorSuccessAler("", "Please enter valild post code", self)
            
        }else{
            
            self.txtPostalCode.resignFirstResponder()
            self.GetPostalCode()
            
        }
        
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func enableFindFieldFunction(_ sender: Any) {
        txtPostalCode.becomeFirstResponder()
    }
    
}
