//
//  PostalCodeDataSource.swift
//  BanglaRemitt
//
//  Created by Apple on 23/04/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension PostalCodeVC {
    
    
    func GetPostalCode(){
        
        let authToken = uc.getAuthToken()
        
//        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!]as [String : Any]
        
        let postalCode = self.txtPostalCode.text?.replacingOccurrences(of: " ", with: "")
//        var postalCode = self.txtPostalCode.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        print(postalCode!)
//        let codefirst = postalCode?.dropLast(3)
//        let codelast = postalCode?.dropFirst((codefirst?.count)!)
//        postalCode = "\((codefirst)!) \((codelast)!)"
//        
//        print(postalCode!)
        
//        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
//                      "Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,
//                      "PostCode":(postalCode)!]as [String : Any]
        
        let parms =  ["ID":(authToken?.userId)!,
                      "Token":(authToken?.authToken!)!,"AppID":ApiUrls.AppID,
                      "PostCode":(postalCode)!]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.PostCodeFinder, params:parms , message: "Loading..."){result in
            
            if(result == "fail")
            {
                self.GetPostalCode()
                return
            }
            
            self.postCodeFinderresult = PostCodeFinderresult(JSONString:result)
            
            if self.postCodeFinderresult?.myAppResult?.Code == 0 {
                
              
                self.PostCodeDetail = self.postCodeFinderresult!.PostCodeDetail!
                
                
//                self.noPostCodeView.isHidden = true
                DispatchQueue.main.async {[weak self] in
                    
                    self?.tblV.reloadData()
                }
                
                
            }else if self.postCodeFinderresult?.myAppResult?.Code == 102 {
                
//                self.noPostCodeView.isHidden = false
                
            }else if self.postCodeFinderresult?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(self.postCodeFinderresult?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.postCodeFinderresult?.myAppResult?.Message)!, self)
                }

            }
        }
        
    }
    

    func GetPostalCodeDetail(appResultid: String){
            
            let authToken = uc.getAuthToken()
            
    //        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!]as [String : Any]
            
        let postalCode = self.txtPostalCode.text?.replacingOccurrences(of: " ", with: "")
            
            let parms =  ["ID":(authToken?.userId)!,
                          "Token":(authToken?.authToken!)!,"AppID":ApiUrls.AppID,
                          "ResultId":appResultid,
                          "PostCode":(postalCode)!]as [String : Any]
            
            
            uc.webServicePosthttp(urlString: ApiUrls.PostCodeFinderDetail, params:parms , message: "Loading..."){result in
                
                if(result == "fail")
                {
                    self.GetPostalCode()
                    return
                }
                
                print(result)
                
                self.postCodeFinderresultDetail = PostCodeFinderresultDetail(JSONString:result)
                
                if self.postCodeFinderresultDetail?.myAppResult?.Code == 0 {
                    
                    if let postCodeFinderResult = self.postCodeFinderresultDetail.PostCodeDetail {
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                            self?.delegate?.changeBackgroundColor(postCodeFinderResult[0])
                            self?.navigationController?.popViewController(animated: true)
                        }
                        
                        
                    }
                }else if self.postCodeFinderresultDetail?.myAppResult?.Code == 102 {
                    
    //                self.noPostCodeView.isHidden = false
                    
                }else if self.postCodeFinderresultDetail?.myAppResult?.Code == 101 {
                    
                    self.uc.logout(self)
                    
                }else{
                    
                    if(self.postCodeFinderresultDetail?.myAppResult?.Message == nil){
                        
                        self.uc.errorSuccessAler("Error", result, self)
                        
                    }else{
                        self.uc.errorSuccessAler("Error", (self.postCodeFinderresultDetail?.myAppResult?.Message)!, self)
                    }

                }
            }
            
        }
}

extension PostalCodeVC: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PostCodeDetail.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

//        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")

//        let cell = self.tranactionListView.tblTranactions.dequeueReusableCell(withIdentifier: "TranactionListCell") as? TransListCell

        let cell = self.tblV.dequeueReusableCell(withIdentifier: "PostCodeCell") as? PostalCodeCell

//        cell.textLabel?.text = PostCodeDetail[indexPath.row].Address
        cell?.lblName.text = PostCodeDetail[indexPath.row].Address
        cell?.lblPostalCode.text = PostCodeDetail[indexPath.row].PostCode

        return cell!

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let model = PostCodeDetail[indexPath.row]
        
        postCodeFindResultSelected = PostCodeDetail[indexPath.row]
        
        let result = postCodeFindResultSelected.ResultId
        
        if result != nil {
            self.GetPostalCodeDetail(appResultid: result!)
        }
        
        
       
        
                //delegate?.changeBackgroundColor(model)
//        self.navigationController?.popViewController(animated: true)
        
    }
    
}

