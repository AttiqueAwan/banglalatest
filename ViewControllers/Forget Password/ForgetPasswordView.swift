//
//  ForgetPasswordView.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 25/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class ForgetPasswordView: UIView {

    
    //Button Back
    @IBOutlet weak var btnback: UIButton!

    //Title Label
    @IBOutlet weak var lblTitle: UILabel!
    
    //Email Address Text Field
    @IBOutlet weak var txtEmailAddress: JVFloatLabeledTextField!
    
    
}
