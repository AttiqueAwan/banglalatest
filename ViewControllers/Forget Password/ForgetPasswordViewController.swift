//
//  ForgotPasswordViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 19/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class ForgetPasswordViewController: UIViewController ,UITextFieldDelegate{
    
    @IBOutlet weak var txtOTP: UITextField!
    
    @IBOutlet weak var txtNewPswd: UITextField!
    
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var lbltitleString: UILabel!
    
    @IBOutlet weak var lblEmailOrPhone: UILabel!
    
    @IBOutlet weak var imgEyeTxtNewPassword: UIImageView!
    
    @IBOutlet weak var imgEyeTxtConfirmPassword: UIImageView!
    
    
    @IBOutlet weak var btnEyeTxtConfirmPassword: UIButton!
    @IBOutlet weak var btnEyeTxtNewPassword: UIButton!
    
    @IBOutlet weak var txtEmail: JVFloatLabeledTextField!
//    @IBOutlet weak var txtOTP: SkyFloatingLabelTextField!
//    @IBOutlet weak var txtNewPswd: SkyFloatingLabelTextField!
    @IBOutlet weak var OTPView: UIView!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    let txtFieldSetting = textFieldSetting()
    let round = RoundedCorner()
    let uc = UtilitySoftTechMedia()
    var OTPRes : OTPResponse!
    var globleSender = Int()
    var btnEye = UIButton()
    var btnEyeNewPassword = UIButton()
  
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let image = UIImage(named: "ico_eye.png")
        self.btnEye.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        self.btnEye.imageView?.contentMode = .scaleAspectFit
        self.btnEye.setImage(image, for: .normal)
        self.btnEye.addTarget(self, action: #selector(btnEyeClick(_:)), for: .touchUpInside)
        self.txtConfirmPassword.rightView = self.btnEye
        self.txtConfirmPassword.rightViewMode = .always
        
        let image1 = UIImage(named: "ico_eye.png")
        self.btnEyeNewPassword.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        self.btnEyeNewPassword.imageView?.contentMode = .scaleAspectFit
        self.btnEyeNewPassword.setImage(image1, for: .normal)
        self.btnEyeNewPassword.addTarget(self, action: #selector(btnEyeClick1(_:)), for: .touchUpInside)
        self.txtNewPswd.rightView = self.btnEyeNewPassword
        self.txtNewPswd.rightViewMode = .always
        
        

    }
    
    @IBAction func btnForgotPasswordClick(_ sender: Any) {
        
            if(self.txtEmail.text?.isEmpty)!{
                
                uc.errorSuccessAler("", "Please enter email", self)
                
            }else if !(uc.isValidEmail(testStr: self.txtEmail.text!)){
                
                uc.errorSuccessAler("", "Please enter valid emai", self)
                
            }else{
                Email = self.txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)
                var str = String()
                let btnSender = ((sender as? UIButton)?.tag)!
                globleSender = btnSender
                
                if btnSender == 1 {
                    str = "E"
                }else if btnSender == 2 {
                    str = "T"
                }
                
                self.SendOTP(deliveryTypeValue: str, sender: globleSender)
             
            }
            
        }
    
        @IBAction func Submit(_ sender: UIButton) {
            
            if(self.txtOTP.text?.isEmpty)!{
                
                uc.errorSuccessAler("", "Please enter OTP", self)
                
            }else if(self.txtNewPswd.text?.isEmpty)! {
                
                uc.errorSuccessAler("", "Please enter New Password", self)
            }
            else if(self.txtConfirmPassword.text?.isEmpty)! {
                
                uc.errorSuccessAler("", "Please enter Confirm Password", self)
            }
            else if(self.txtNewPswd.text != self.txtNewPswd.text) {
                
                uc.errorSuccessAler("", "Confirm Password not matched. ", self)
            }
            
            else if(self.txtNewPswd.text!.count < 7) {
                
                uc.errorSuccessAler("", "Minimum 8 characters required for New Password", self)
            }else{
        
                self.VerifyOTP()
                
            }
            
        }

   
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
   func myHandler(alert: UIAlertAction){
            self.OTPView.isHidden = true
            self.navigationController?.popViewController(animated: true)
            
        }
        
    func SendOTP(deliveryTypeValue: String ,sender: Int){
            
            let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
            let parms =  ["AppID":ApiUrls.AppID,"Token":token,"DeliveryType":deliveryTypeValue,"OTPType":4,"Email":Email]as [String : Any]
            
            
            uc.webServicePosthttp(urlString: ApiUrls.SendOTP, params:parms , message: "Loading..."){result in
                
                if(result == "fail")
                {
                    self.SendOTP(deliveryTypeValue: deliveryTypeValue, sender: self.globleSender)
                    return
                }
                
                self.OTPRes = OTPResponse(JSONString:result)
                
                if self.OTPRes != nil && self.OTPRes.myAppResult?.Code == 0{
                    
                    var to = ""
                    if sender == 1 {
                        //email
                        to = self.OTPRes.OTPResp!.To!
                        self.lbltitleString.text = "Verification code has been send on your email address."
                        self.uc.errorSuccessAler("OTP Alert", "OTP has been sent on your \(Email) email address.", self)
                    }else if sender == 2 {
                        
                        to = "+\(String(describing: self.OTPRes.OTPResp!.To!))"
                        
                        self.lbltitleString.text = "Verification code has been send on your phone no."
                        self.uc.errorSuccessAler("OTP Alert", "OTP has been sent on your \(to) Phone no.", self)
                    }
                    
                    self.lblEmailOrPhone.text = to
                    self.OTPView.isHidden = false
                    
                }else{
                    
                    if(self.OTPRes.myAppResult?.Message == nil){
                        
                        self.uc.errorSuccessAler("Error", result, self)
                        
                    }else{
                        
                        self.uc.errorSuccessAler("Error", (self.OTPRes!.myAppResult?.Message)!, self)
                    }
                }
                
            }
            
        }
        
        func VerifyOTP(){
            
           
            let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
            let parms =  ["AppID":ApiUrls.AppID,"Token":token, "OTPType":4,"Email":Email,"OTP":self.txtOTP.text!.trimmingCharacters(in: .whitespacesAndNewlines),"NewPassword":self.txtNewPswd.text!.trimmingCharacters(in: .whitespacesAndNewlines)]as [String : Any]
            print(parms)
            uc.webServicePosthttp(urlString: ApiUrls.VerifyOTP, params:parms , message: "Loading..."){result in
                
                if(result == "fail")
                {
                    self.VerifyOTP()
                    return
                }
                
                let loginResponse = AppUser(JSONString:result)
                
                if loginResponse != nil && loginResponse?.myAppResult?.Code == 0{
                    
                    let alertController = UIAlertController(title: "", message: "Dear customer your password has been updated successfully.", preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .default, handler: self.myHandler)
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }else{
                    
                    if(loginResponse == nil){
                        
                        self.uc.errorSuccessAler("Alert", result, self)
                        
                    }
                    else{
                        
                        self.uc.errorSuccessAler("Alert", (loginResponse!.myAppResult!.Message!), self)
                    }
                    
                }
                
            }
            
        }
    
    
    @IBAction func btnEyeClick(_ sender:Any){
    
    if(self.btnEye.imageView!.image == #imageLiteral(resourceName: "ico_eye")){
        
        self.txtConfirmPassword.isSecureTextEntry = false
        let image = UIImage(named: "ico_password_view.png")
        self.btnEye.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
        self.btnEye.imageView!.contentMode = .scaleAspectFit
        self.btnEye.setImage(image, for: .normal)
    
    }else if(self.btnEye.imageView!.image == #imageLiteral(resourceName: "ico_password_view.png")){
        
        let image = UIImage(named: "ico_eye.png")
       
        self.btnEye.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
        self.btnEye.imageView!.contentMode = .scaleAspectFit
        self.btnEye.setImage(image, for: .normal)
        self.txtConfirmPassword.isSecureTextEntry = true
        
    }
    
    }
    
    @IBAction func btnEyeClick1(_ sender:Any){
       
       if(self.btnEyeNewPassword.imageView!.image == #imageLiteral(resourceName: "ico_eye")){
           
           self.txtNewPswd.isSecureTextEntry = false
           let image = UIImage(named: "ico_password_view.png")
           self.btnEyeNewPassword.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
           self.btnEyeNewPassword.imageView!.contentMode = .scaleAspectFit
           self.btnEyeNewPassword.setImage(image, for: .normal)
       
       }else if(self.btnEyeNewPassword.imageView!.image == #imageLiteral(resourceName: "ico_password_view.png")){
           
           let image = UIImage(named: "ico_eye.png")
          
           self.btnEyeNewPassword.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
           self.btnEyeNewPassword.imageView!.contentMode = .scaleAspectFit
           self.btnEyeNewPassword.setImage(image, for: .normal)
           self.txtNewPswd.isSecureTextEntry = true
           
       }
       
       }
    
    
    
}
