//
//  ProfileDataSource.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 08/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit
import MaterialControls

extension ProfileViewController:MDDatePickerDialogDelegate{
    
    
    func datePickerDialogDidSelect(_ date: Date) {
        
        let formater = DateFormatter()
        formater.dateFormat = "dd-MM-yyyy"
        
        let newDate = formater.string(from: date)
        self.profileView.txtDateofBirth.text = newDate
        
    }
}

extension ProfileViewController{
    
    
    func getUserProfile(){
        
        self.activityView?.isHidden = false
        self.activityView?.startAnimating()
        
        
        
        let authToken = uc.getAuthToken()
        
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetProfile, params:parms , message: "Loading..."){result in
            
            self.ProfileResponse = Dashboardresult(JSONString:result)
            
            if  self.ProfileResponse?.myAppResult?.Code == 0 {
                
                
                
                let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
                
                let AllCountries = CountryListresult(JSONString: result)
                
                
                for i in 0..<(AllCountries?.CountryList?.count)! {
                    
                    self.Nationality.append((AllCountries?.CountryList![i].Nationality)!)
                    
                    if(AllCountries?.CountryList![i].Iso3Code == self.ProfileResponse.UserInfo?.countryIsoCode){
                        
                        if let Country = AllCountries?.CountryList![i].CountryName {
                            
                            self.profileView.txtCountry.text = Country
                            self.profileView.txtPhoneCode.text = "+\((AllCountries?.CountryList![i].DialingCode)!)"
                           
                        }
                       
                    }
                    
                    if(AllCountries?.CountryList![i].Iso3Code == self.ProfileResponse.UserInfo?.nationalityIsoCode){
                        
                        if let Nationality = AllCountries?.CountryList![i].Nationality {
                            
                            self.profileView.txtNationality.text = Nationality
                           
                            
                        }
                        
                    }
                }
                
                self.NationalityDropDown.dataSource = self.Nationality
                self.NationalityDropDown.reloadAllComponents()
                
                    
               
//               self.profileView.txtPhoneNumber.text = self.ProfileResponse.UserInfo?.phone
                
                self.profileView.txtPhoneNumber.text = self.ProfileResponse.UserInfo?.phone.deletingPrefix(self.profileView.txtPhoneCode.text!)
                
                
                 self.profileView.txtDateofBirth.text = "\((self.ProfileResponse.UserInfo?.birthDay)!)-\((self.ProfileResponse.UserInfo?.birthMonth)!)-\((self.ProfileResponse.UserInfo?.birthYear)!)"
                self.profileView.txtHouse.text = self.ProfileResponse.UserInfo?.houseNo
                self.profileView.txtStreet.text = self.ProfileResponse.UserInfo?.address
                self.profileView.txtCity.text = self.ProfileResponse.UserInfo?.city
                self.profileView.txtPostalCode.text = self.ProfileResponse.UserInfo?.postalCode
                self.profileView.txtGender.text = self.ProfileResponse.UserInfo?.gender
                self.profileView.txtBirthPlace.text = self.ProfileResponse.UserInfo?.countryOfBirthIsoCode
                self.profileView.txtOccupation.text = self.ProfileResponse.UserInfo?.occupation
                self.profileView.lblUserName.text = self.ProfileResponse.UserInfo?.fullName
                self.profileView.lblEmail.text = self.ProfileResponse.UserInfo?.email
                self.profileView.lblCustomerID.text = "Customer ID: \((self.ProfileResponse.UserInfo?.customerID)!)"
                
                let firstChar_FN = self.ProfileResponse.UserInfo?.firstName.prefix(1)
                let lastChar_LN = self.ProfileResponse.UserInfo?.lastName.prefix(1)
                let strName = "\(String(describing: firstChar_FN!)) \(String(describing: lastChar_LN!))"
                print(strName)
                self.profileView.lblInsideProfileCircle.text = strName
                
//                AllCountriesList
                
                
                
                if let Imgae1 = self.ProfileResponse.UserInfo?.photo {
                    
                    //self.imgVProfile.sd_setImage(with: URL(string: Imgae1), placeholderImage: #imageLiteral(resourceName: "ico_upload"), options: .continueInBackground, completed: nil)
                    
                    self.imgVProfile.sd_setImage(with: URL(string: Imgae1)) { [weak self] (img, error, chacheType, url) in
                        if (img != nil) {
                            self?.imgVProfile.image = img
                        }
                        self?.activityView!.stopAnimating()
                    }
                }
                
                //Start Code only for extract country image
                if let countryListName = UserDefaults.standard.value(forKey: "AllCountriesList") {
                    print(countryListName)
                
                    for i in 0..<(AllCountries?.CountryList?.count)! {
                        if  (AllCountries?.CountryList![i].Iso3Code == self.ProfileResponse.UserInfo?.countryIsoCode) {
                            
                            let countryName = (AllCountries?.CountryList![i].CountryName)!
                            
                            let countyImage = UIImage(named: "\(countryName+".png")")!
                            
                            self.profileView.imgUser.image = countyImage
                            
                            
                        }
                    }
                } //End Code only for extract country image
                
            }else if  self.ProfileResponse?.myAppResult?.Code == 101 {
                
                
            }else{
                
                if( self.ProfileResponse?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( self.ProfileResponse?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
        
    }

    

    func UpdateProfile() {
        
        let userInfo = self.ProfileResponse.UserInfo
        let DOB = self.profileView.txtDateofBirth.text?.components(separatedBy: "-")
        let authToken = uc.getAuthToken()
        
        let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
        
        let AllCountries = CountryListresult(JSONString: result)
        
        var nationalityISoCode = ""
        for i in 0..<(AllCountries?.CountryList?.count)! {
           
            if(AllCountries?.CountryList![i].Nationality == self.profileView.txtNationality.text){
                
                if let NationalityIsoCOde = AllCountries?.CountryList![i].Iso3Code {
                
                    nationalityISoCode = NationalityIsoCOde
                }
            }
        }
        
        while (self.profileView.txtPhoneNumber.text?.hasPrefix("0"))! {
            self.profileView.txtPhoneNumber.text?.remove(at: (self.profileView.txtPhoneNumber.text?.startIndex)!)
        }
        
        let phoneWithCode = "\(self.profileView.txtPhoneCode.text!)\(self.profileView.txtPhoneNumber.text!)"
        
        
        let parms =  ["ID":authToken!.userId!,
                      "Token":authToken!.authToken!,
                      "FirstName":(userInfo?.firstName)!,
                      "LastName":(userInfo?.lastName)!,
                      "FullName":(self.profileView.lblUserName.text)!,
                      "Email":(self.profileView.lblEmail.text)!,
                      "Phone":phoneWithCode,
                      "Gender":(self.profileView.txtGender.text)!,
                      "CountryIsoCode":(userInfo?.countryIsoCode)!,
                      "CurrencyIsoCode":(userInfo?.currencyIsoCode)!,
                      "NationalityIsoCode":nationalityISoCode,
                      "CountryOfBirthIsoCode":(self.profileView.txtBirthPlace.text)!,
                      "Address":(self.profileView.txtStreet.text)!,
                      "City":(self.profileView.txtCity.text)!,
                      "Occupation":(self.profileView.txtOccupation.text)!,
                      "Employer":(userInfo?.employer)!,
                      "PostalCode":(self.profileView.txtPostalCode.text)!,
                      "CustomerID":(self.profileView.lblCustomerID.text?.components(separatedBy: "Customer ID: ")[1])!,
                      "DeviceType":(userInfo?.deviceType)!,
                      "BirthDay":(DOB?[0])!,
                      "BirthMonth":(DOB?[1])!,
                      "BirthYear":(DOB?[2])!,
                      "HouseNo":(self.profileView.txtHouse.text)!,
                      "AgentUserName":(userInfo?.agentUserName)!,
                      "Password":(userInfo?.password)!,
                      "AgentType":(userInfo?.agentType)!,
                      "AgentBaseCurrency":(userInfo?.agentBaseCurrency)!,
                      "AgentSign":(userInfo?.agentSign)!,
                      "AagentCurPrfxId":(userInfo?.aagentCurPrfxId)!,
                      "TimeZoneHours":(userInfo?.timeZoneHours)!,
                      "TimeZoneMinunts":(userInfo?.timeZoneMinunts)!,
                      "AgentLimitType":(userInfo?.agentLimitType)!,
                      "AgentStatus":(userInfo?.agentStatus)!,
                      "SenderStatus":(userInfo?.senderStatus)!,
                      "AgentLimitAmount":(userInfo?.agentLimitAmount)!,
                      "DomainID":(userInfo?.domainID)!,
                      "ReferralCode":(userInfo?.referralCode)!,
                      "Photo":(userInfo?.photo)!,
                      "AgentPromoCode":(userInfo?.agentPromoCode)!]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.UpdateProfile, params:parms , message: "Loading..."){result in
            
            let ProfileResponseresult = Dashboardresult(JSONString:result)
            
            if  ProfileResponseresult?.myAppResult?.Code == 0 {
                
               self.uc.saveLogininfo(result: result)
                self.navigationController?.popViewController(animated: true)
                
            }else if  ProfileResponseresult?.myAppResult?.Code == 101 {
                
                
            }else{
                
                if( ProfileResponseresult?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( ProfileResponseresult?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
    }

    
    func UpdateProfilePicture(_ url : String){
        
       let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,
                      "PhotoBody":self.imageURL,
                    "ImageData":(self.imgVProfile.image?.jpegData(compressionQuality: 0.7))!] as [String:Any]
        
        
        
        uc.webServicePosthttpUploadPhoto(urlString: url, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
             self.UpdateProfilePicture(url)
                return
            }
            
            self.documentResponce = DocumentListresult(JSONString:result)
            
            if self.documentResponce?.myAppResult?.Code == 0 {
                
                self.uc.errorSuccessAler("", "Profile Picture Updated Successfully", self)

                
            }else if self.documentResponce?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(self.documentResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.documentResponce?.myAppResult?.Message)!, self)
                }
            }
        }
        
    }

}

extension String {
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
}
