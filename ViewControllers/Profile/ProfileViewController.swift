//
//  ProfileViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 08/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import DropDown
import MaterialControls

class ProfileViewController: UIViewController, ClassBVCDelegate {
    
    func changeBackgroundColor(_ model: PostCodeFinderDetail2?) {
        
        var houseOrFlat = ""
        var streetAddress = ""
        
        //1
        if model != nil{
            //2
            if model?.BuildingNumber != nil && model?.BuildingNumber != "" {
                houseOrFlat = model!.BuildingNumber!
                streetAddress = model!.Street!
            }//end 2
            //3
            else if model?.BuildingName != nil && model?.BuildingName != "" {
                
            }//3 end
            //4
            else if model?.Line1 != nil {
                houseOrFlat = model!.Line1!
                streetAddress = model!.Line2!
                
                //4.1
                if model?.Line3 != ""{
                    streetAddress += ", " + model!.Line3!
                }//4.1 end
                //4.2
                if streetAddress != "" && streetAddress.prefix(1) == "," {
        
                    streetAddress = streetAddress.components(separatedBy: ",").last!

                }//4.2 end
            }//4 end
            //5
            if model?.Label != ""{
                
                var fullAddress = model?.Label!.split(separator: "\n")
        
                
                
                //5.1
                if fullAddress!.count > 0 {
                    //5.1.1
                    if model?.SubBuilding != "" {
                        houseOrFlat = String(fullAddress![0])
                        //5.1.1.1
                        if houseOrFlat.contains(model!.Street!) {
                            houseOrFlat = houseOrFlat.replacingOccurrences(of: model!.Street!, with: "", options: .caseInsensitive)
                        }//5.1.1.1 end
                    }//5.1.1 end
                    //5.1.2
                    else {
                        //5.2.1
                        if fullAddress![0].contains(model!.Street!) {
                            houseOrFlat = fullAddress![0].replacingOccurrences(of: model!.Street!, with: "", options: .caseInsensitive)
                            streetAddress = String(fullAddress![0])
                            streetAddress = streetAddress.replacingOccurrences(of: houseOrFlat, with: "", options: .caseInsensitive)
                        }//5.2.1 end
                    }//5.1.2 end
                    
                    
                    let fAddress1:String = String(fullAddress![1])
                    
                    var validCity = ""
                    
                    var validPostalCode = ""
                    
                    if let pCity = model!.City {
                        validCity = pCity
                    }
                    
//                    if(a.caseInsensitiveCompare(b) == .orderedSame){
//                         print("Ok")
//                    }
                    
                    
                    //5.1.3
                    if fAddress1.caseInsensitiveCompare(validCity) != ComparisonResult.orderedSame  {
                        
                        streetAddress = String(fullAddress![1])
                        //5.1.3.1
                        if streetAddress.contains(houseOrFlat){
                            streetAddress = streetAddress.replacingOccurrences(of: houseOrFlat, with: "", options: .caseInsensitive)
                        }//5.1.3.1 end
                    }//5.1.3 end
                    
                    let fAddress2 : String = String(fullAddress![2])
                    
                   
                    if let postalCode = model!.PostalCode {
                        validPostalCode = postalCode
                    }
                    
                    
                    
                    //5.1.4
                    if fAddress2.caseInsensitiveCompare(validCity) != ComparisonResult.orderedSame && fAddress2.caseInsensitiveCompare(validPostalCode) != ComparisonResult.orderedSame {
                        streetAddress += ", " + fullAddress![2]
                    }//5.1.4 end
                    //5.1.5
                    
                    
            
                    if streetAddress.contains(validCity) {
                        streetAddress = streetAddress.replacingOccurrences(of: validCity, with: "" , options: .caseInsensitive)
                    }//5.1.5 end
                    
                }//5.1 end
                
            }//5 end
        }
        
         self.profileView.txtHouse.text = houseOrFlat
        self.profileView.txtStreet.text = streetAddress
        
        
//
//        if model?.BuildingNumber != nil && !(model?.BuildingNumber!.isEmpty)! {
//            houseOrFlat = model!.BuildingNumber!
//
//            streetAddress = model!.Line1!
//
//            if model?.Line2 != nil && !(model?.Line2!.isEmpty)! {
//                streetAddress = streetAddress + ", " + model!.Line2!
//            }
//
//            if streetAddress.isEmpty == false && streetAddress.prefix(1) == "," {
//                streetAddress = streetAddress.replacingOccurrences(of: ", ", with: "")
//            }
//
//
//
//
//
////            if model?.Street != nil && !(model?.Street!.isEmpty)! {
////                streetAddress = model!.Street!
////            }
//        }
////        else if model?.BuildingName != nil && !(model?.BuildingName!.isEmpty)!  {
////
////        }
//        else if model?.Line1 != nil  {
//
//            //!(model?.Line1!.isEmpty)!
//
//            houseOrFlat = model!.Line1!
//            //if model?.Line2 != nil && !(model?.Line2!.isEmpty)! {
//                streetAddress = model!.Line2!
//            //}
//
//            if model?.Line3 != nil && !(model?.Line3!.isEmpty)! {
//                streetAddress = streetAddress + ", " + model!.Line3!
//            }
//
//            if streetAddress.isEmpty == false && streetAddress.prefix(1) == "," {
//                streetAddress = streetAddress.replacingOccurrences(of: ", ", with: "")
//            }
//        }
//
//        if streetAddress.isEmpty {
//            self.profileView.txtHouse.text = ""
//            self.profileView.txtStreet.text = houseOrFlat
//        }else {
//            self.profileView.txtHouse.text = houseOrFlat
//            self.profileView.txtStreet.text = streetAddress
//        }
        
        
    }
    
    @IBOutlet weak var profileView:ProfileView!
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    let txtFieldSetting = textFieldSetting()
    
    let uc = UtilitySoftTechMedia()
    
    var ProfileResponse : Dashboardresult!
    
    var NationalityDropDown = DropDown()
    var GenderDropDown = DropDown()
    
    var Gender = ["Male","Female"]
    
    var Nationality = [String]()
    
    
    //Date Picker
    var datePicker = MDDatePickerDialog()
    
    var checkimage = ""
    var imageURL = ""
    
    @IBOutlet weak var imgVProfile: UIImageView!
    var documentResponce : DocumentListresult!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imgVProfile.layer.cornerRadius = self.imgVProfile.frame.height/2
        self.imgVProfile.layer.masksToBounds = true
        
        self.datePicker.delegate = self
        
        //Setting TextField Bottom Line
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) { [weak self] in
            self?.profileView.txtCountry = self?.txtFieldSetting.textFieldBaseLine((self?.profileView.txtCountry)!,#colorLiteral(red: 0, green: 0.3607843137, blue: 0.1254901961, alpha: 1))
            self?.profileView.txtPhoneCode = self?.txtFieldSetting.textFieldBaseLine((self?.profileView.txtPhoneCode)!,#colorLiteral(red: 0, green: 0.3607843137, blue: 0.1254901961, alpha: 1))
            self?.profileView.txtPhoneNumber = self?.txtFieldSetting.textFieldBaseLine((self?.profileView.txtPhoneNumber)!,#colorLiteral(red: 0, green: 0.3607843137, blue: 0.1254901961, alpha: 1))
            self?.profileView.txtDateofBirth = self?.txtFieldSetting.textFieldBaseLine((self?.profileView.txtDateofBirth)!, #colorLiteral(red: 0, green: 0.3607843137, blue: 0.1254901961, alpha: 1))
            self?.profileView.txtHouse = self?.txtFieldSetting.textFieldBaseLine((self?.profileView.txtHouse)!,#colorLiteral(red: 0, green: 0.3607843137, blue: 0.1254901961, alpha: 1))
            self?.profileView.txtStreet = self?.txtFieldSetting.textFieldBaseLine((self?.profileView.txtStreet)!,#colorLiteral(red: 0, green: 0.3607843137, blue: 0.1254901961, alpha: 1))
            self?.profileView.txtCity = self?.txtFieldSetting.textFieldBaseLine((self?.profileView.txtCity)!,#colorLiteral(red: 0, green: 0.3607843137, blue: 0.1254901961, alpha: 1))
            self?.profileView.txtPostalCode = self?.txtFieldSetting.textFieldBaseLine((self?.profileView.txtPostalCode)!,#colorLiteral(red: 0, green: 0.3607843137, blue: 0.1254901961, alpha: 1))
            self?.profileView.txtNationality = self?.txtFieldSetting.textFieldBaseLine((self?.profileView.txtNationality)!,#colorLiteral(red: 0, green: 0.3607843137, blue: 0.1254901961, alpha: 1))
            self?.profileView.txtGender = self?.txtFieldSetting.textFieldBaseLine((self?.profileView.txtGender)!,#colorLiteral(red: 0, green: 0.3607843137, blue: 0.1254901961, alpha: 1))
            self?.profileView.txtBirthPlace = self?.txtFieldSetting.textFieldBaseLine((self?.profileView.txtBirthPlace)!,#colorLiteral(red: 0, green: 0.3607843137, blue: 0.1254901961, alpha: 1))
            self?.profileView.txtOccupation = self?.txtFieldSetting.textFieldBaseLine((self?.profileView.txtOccupation)!,#colorLiteral(red: 0, green: 0.3607843137, blue: 0.1254901961, alpha: 1))
        }
        
        
        
        //Getting User Profile
        self.getUserProfile()
        
        //setting Country dropdown 
        NationalityDropDown.anchorView = self.profileView.btnNationality
        NationalityDropDown.dataSource = Nationality
        
        
        NationalityDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.profileView.txtNationality.text = item
        }
        
        //setting Gender  dropdown
        GenderDropDown.anchorView = self.profileView.btnGender
        GenderDropDown.dataSource = Gender
        
        
        GenderDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.profileView.txtGender.text = item
            
        }
        
        self.addDoneButtonOnKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(SelectImage(_:)), name: ApiUrls.kNotification, object: nil)
    }
    
    //Button Back Click
    @IBAction func btnbackClick(_ sender:Any){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //Button Nationality Click
    @IBAction func btnNationalityClick(_ sender:Any){
        
        self.NationalityDropDown.show()
        
    }
    
    //BUtton Gender Click
    @IBAction func btnGenderClcick(_ sender:Any){
        
        GenderDropDown.show()
        
    }
    
    //Button Date of Birth Click
    @IBAction func btnDateofBirth(_ sender:Any){
        
        self.view.endEditing(true)
        let calendar = Calendar.current
        var minDateComponent = calendar.dateComponents([.day,.month,.year], from: Date())
        minDateComponent.day = 01
        minDateComponent.month = 01
        minDateComponent.year = 1940

        let minDate = calendar.date(from: minDateComponent)
        print(" min date : \(String(describing: minDate))")

//        var maxDateComponent = calendar.dateComponents([.day,.month,.year], from: Date())
//        maxDateComponent.day = 0
//        maxDateComponent.month = 03 + 1
//        maxDateComponent.year = 2018

//        let maxDate = calendar.date(from: maxDateComponent)
//        print("max date : \(String(describing: maxDate))")

//        picker.minimumDate = minDate! as Date
//        picker.maximumDate =  maxDate! as Date
        
        datePicker.minimumDate = minDate!
    
        datePicker.show()
        
    }
    
    //Button Update Profile Click
    @IBAction func btnUpdateProfileCLick(_ sender:Any){
        
        if(self.profileView.txtCountry.text?.isEmpty)!{
            
            uc.errorSuccessAler("Alert", "Country required!", self)
            
        }else if(self.profileView.txtPhoneCode.text?.isEmpty)!{
            
            self.uc.errorSuccessAler("Alert", "Phone Code required!", self)
            
        }else if(self.profileView.txtPhoneNumber.text?.isEmpty)!{
            
            self.uc.errorSuccessAler("Alert", "Phone Number required!", self)
            
        }else if(self.profileView.txtDateofBirth.text?.isEmpty)!{
            
            self.uc.errorSuccessAler("Alert", "Date of Birth required!", self)
            
        }else if(self.profileView.txtHouse.text?.isEmpty)!{
            
            self.uc.errorSuccessAler("Alert", "House Number required!", self)
            
        }else if(self.profileView.txtStreet.text?.isEmpty)!{
            
            self.uc.errorSuccessAler("Alert", "Street Address required!", self)
            
        }else if(self.profileView.txtCity.text?.isEmpty)!{
            
            self.uc.errorSuccessAler("Alert", "City required!", self)
            
        }else if(self.profileView.txtPostalCode.text?.isEmpty)!{
            
            self.uc.errorSuccessAler("Alert", "Postal Code required!", self)
            
        }else if(self.profileView.txtNationality.text?.isEmpty)!{
            
            self.uc.errorSuccessAler("Alert", "Nationality required!", self)
            
        }else if(self.profileView.txtGender.text?.isEmpty)!{
            
            self.uc.errorSuccessAler("Alert", "Gender required!", self)
            
        }else if(self.profileView.txtBirthPlace.text?.isEmpty)!{
            
            self.uc.errorSuccessAler("Alert", "Birth Place required!", self)
            
        }else if(self.profileView.txtOccupation.text?.isEmpty)!{
            
            self.uc.errorSuccessAler("Alert", "Occupation required!", self)
            
        }else{
            
            //            defaults.set(true, forKey: "isUserFirstTimeCheckDocument")
            
            if UserDefaults.standard.bool(forKey: "isUserFirstTimeCheckDocument")  {
                
                UserDefaults.standard.set(false, forKey: "isUserFirstTimeCheckDocument")
                self.navigationController?.pushViewController(AddDocumentViewController(), animated: true)
            }else {
                self.UpdateProfile()
            }
        }
    }
    
    //keyboard done button
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonActionSend))
        done.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.profileView.txtCountry.inputAccessoryView = doneToolbar
        self.profileView.txtPhoneCode.inputAccessoryView = doneToolbar
        self.profileView.txtPhoneNumber.inputAccessoryView = doneToolbar
        self.profileView.txtBirthPlace.inputAccessoryView = doneToolbar
        self.profileView.txtHouse.inputAccessoryView = doneToolbar
        self.profileView.txtStreet.inputAccessoryView = doneToolbar
        self.profileView.txtPostalCode.inputAccessoryView = doneToolbar
        self.profileView.txtNationality.inputAccessoryView = doneToolbar
        self.profileView.txtDateofBirth.inputAccessoryView = doneToolbar
        self.profileView.txtOccupation.inputAccessoryView = doneToolbar
        self.profileView.txtCity.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonActionSend() {
        self.profileView.txtCountry.resignFirstResponder()
        self.profileView.txtPhoneCode.resignFirstResponder()
        self.profileView.txtPhoneNumber.resignFirstResponder()
        self.profileView.txtBirthPlace.resignFirstResponder()
        self.profileView.txtHouse.resignFirstResponder()
        self.profileView.txtStreet.resignFirstResponder()
        self.profileView.txtPostalCode.resignFirstResponder()
        self.profileView.txtNationality.resignFirstResponder()
        self.profileView.txtDateofBirth.resignFirstResponder()
        self.profileView.txtOccupation.resignFirstResponder()
        self.profileView.txtCity.resignFirstResponder()
    }
    
    @IBAction func btnFindAddress(_ sender: UIButton) {
        if !(self.profileView.txtPostalCode.text!.isEmpty) {
            
            //            if let nav = segue.destination as? UINavigationController, let classBVC = nav.topViewController as? ClassBVC {
            //            classBVC.delegate = self
            //            }
            
            let postalCodeVC = PostalCodeVC()
            postalCodeVC.postalCode = self.profileView.txtPostalCode.text!
            postalCodeVC.delegate = self
            self.navigationController?.pushViewController(postalCodeVC, animated: true)
        }else {
            uc.errorSuccessAler("Alert", "Pleas enter address", self)
        }
        
    }
    
    @IBAction func ImageUpload(_ sender: UIButton) {
        
        let viewcontroller = AddDocumentPopUp()
        checkimage = "img1"
        viewcontroller.checkimage = checkimage
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    @objc  func SelectImage(_ notification:NSNotification) {
        
        let userinfo =  notification.userInfo as? [String:Any]
        let image =  userinfo!["image"] as? UIImage
        checkimage = (userinfo!["checkimage"] as? String)!
        //let imageData = userinfo!["imageData"] as? NSData
        let imageURL = userinfo!["imgURL"] as? String
        
        
        if(self.checkimage == "img1"){
            
            
            self.imgVProfile.image = image
            self.imageURL =  imageURL!
            self.UpdateProfilePicture(ApiUrls.UpdateProfilePicture)
            
        }
        
        
    }
    
    
}

