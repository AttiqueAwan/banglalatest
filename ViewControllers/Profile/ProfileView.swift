//
//  ProfileView.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 08/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class ProfileView: UIView {

    ////////////////// TextFields ///////////////////////
    
    //TextField Country
    @IBOutlet weak var txtCountry:JVFloatLabeledTextField!
    
    //TextField Phone Code
    @IBOutlet weak var txtPhoneCode:JVFloatLabeledTextField!
    
    //TextField Phone Number
    @IBOutlet weak var txtPhoneNumber:JVFloatLabeledTextField!
    
    //textField Date of Birth
    @IBOutlet weak var txtDateofBirth:JVFloatLabeledTextField!
    
    @IBOutlet weak var btnDateofBirth:UIButton!
    
    //TextField House Number
    @IBOutlet weak var txtHouse:JVFloatLabeledTextField!
    
    //TextField Street Number
    @IBOutlet weak var txtStreet:JVFloatLabeledTextField!
    
    //TextField City
    @IBOutlet weak var txtCity:JVFloatLabeledTextField!
    
    //TextField Postal Code
    @IBOutlet weak var txtPostalCode:JVFloatLabeledTextField!
    
    //Text field Nationality
    @IBOutlet weak var txtNationality:JVFloatLabeledTextField!
    
    //TextField Gender
    @IBOutlet weak var txtGender:JVFloatLabeledTextField!
    
    //Button Gender
    @IBOutlet weak var btnGender:UIButton!
    
    //TextField Birth Place
    @IBOutlet weak var txtBirthPlace:JVFloatLabeledTextField!
    
    //TextField Occupation
    @IBOutlet weak var txtOccupation:JVFloatLabeledTextField!
    
    
    ////////////////// Buttons //////////////////
    
    //Button Back
    @IBOutlet weak var btnBack:UIButton!
    
    //Button UpdateProfile
    @IBOutlet weak var btnUpdateProfile:UIButton!
    
    //Button Nationality
    @IBOutlet weak var btnNationality:UIButton!
    
    
    /////////////// Label ////////////////////
    
    //Label User Name
    @IBOutlet weak var lblUserName:UILabel!
    
    //Label Email
    @IBOutlet weak var lblEmail:UILabel!
    
    //Label Customer ID
    @IBOutlet weak var lblCustomerID:UILabel!
    
    
    ////////////////// ImageView ///////////////////
    
    //User ImageView
    @IBOutlet weak var imgUser:UIImageView!
    
    @IBOutlet weak var lblInsideProfileCircle: UILabel!
    

    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
