//
//  ProfileSettingDataSource.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 13/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit

extension ProfileSettingVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblV.dequeueReusableCell(withIdentifier: "ProfileCell") as? ProfileCell
        
        cell?.lblmenu.text = menuName[indexPath.row]
        cell?.imgmenu?.image = menuImages[indexPath.row]
        
        return cell!
 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexName = menuName[indexPath.row]
//        let menuName = ["View Profile","Reset Password","Exchange Rate","Transactions","Inviet Friends"]
        
        switch indexName {
        case "View Profile":
            self.navigationController?.pushViewController(ProfileViewController(), animated: true)
        case "Reset Password":
            self.navigationController?.pushViewController(ForgetPasswordViewController(), animated: true)
        case "Support":
            print("support")
             self.navigationController?.pushViewController(SupportListVC(), animated: true)
        case "Exchange Rate":
            self.navigationController?.pushViewController(ExchangeRateViewController(), animated: true)
        case "Invite Friends":
            self.share_Func()
        default:
            print("default")
        }
    }
    
    //share
    func share_Func() {
        
//        let appUrl = URL(string: "http://www.banglaremit.co.uk/get_the_app.php?click_id=\(self.uc.getUserInfo()?.UserInfo?.agentPromoCode ?? "")")
        
        let info = self.uc.getUserInfo()?.UserInfo?.agentPromoCode.components(separatedBy: " ").last
        let strUrl11 = "http://www.banglaremit.co.uk/get_the_app.php?click_id=\(info!)"
        let appUrl = URL(string: strUrl11) //URL(string: "http://www.banglaremit.co.uk/get_the_app.php?click_id=\(info!)")

        let webUrl = URL(string: "http://www.banglaremit.co.uk")
    
        
        let appUrlWithText = "\nDownload APP: \(String(describing: appUrl!))"
        let webUrlWithText = "\nWebsite Link: \(String(describing: webUrl!))"
        
        let textAndUrl = " I love using Bangla Remit to send money to my loved ones. Use the link below to get the best rates\(appUrlWithText) \(webUrlWithText)"
        let items: [Any] = ["\(textAndUrl))"]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
        
    }
    
}

extension ProfileSettingVC {
    
     func getUserProfile(){
        
        self.activityView?.isHidden = false
        self.activityView?.startAnimating()
            
            let authToken = uc.getAuthToken()
            
            let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!]as [String : Any]
            
            uc.webServicePosthttp(urlString: ApiUrls.GetProfile, params:parms , message: "Loading..."){result in
                
                self.ProfileResponse = Dashboardresult(JSONString:result)
                
                if  self.ProfileResponse?.myAppResult?.Code == 0 {
                    
                    let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
                    
                    let AllCountries = CountryListresult(JSONString: result)
                    
                    
                    for i in 0..<(AllCountries?.CountryList?.count)! {
                        
                        
                        if(AllCountries?.CountryList![i].Iso3Code == self.ProfileResponse.UserInfo?.countryIsoCode){
                            
//                            if let Country = AllCountries?.CountryList![i].CountryName {
//
//                            }
                           
                        }
                        
                        if(AllCountries?.CountryList![i].Iso3Code == self.ProfileResponse.UserInfo?.nationalityIsoCode){
 
                        }
                    }
                    
                    
                    let firstChar_FN = self.ProfileResponse.UserInfo?.firstName.prefix(1)
                    let lastChar_LN = self.ProfileResponse.UserInfo?.lastName.prefix(1)
                    let strName = "\(String(describing: firstChar_FN!)) \(String(describing: lastChar_LN!))"
                    print(strName)
                    self.lblInsideProfileCircle.text = strName
    //                AllCountriesList
                    if let Imgae1 = self.ProfileResponse.UserInfo?.photo {
                        
//                        self.imgVProfile.sd_setImage(with: URL(string: Imgae1), placeholderImage: #imageLiteral(resourceName: "ico_upload"), options: .continueInBackground, completed: nil)
                        
                        
                        self.imgVProfile.sd_setImage(with: URL(string: Imgae1)) { [weak self] (img, error, chacheType, url) in
                            if (img != nil) {
                                self?.imgVProfile.image = img
                            }
                            self?.activityView!.stopAnimating()
                        }
                        
                        
                    }
                    
                    self.lblUserName.text = self.ProfileResponse.UserInfo?.fullName
                    self.lblEmail.text = self.ProfileResponse.UserInfo?.email
                    self.lblCustomerID.text = "Customer ID: \(String(describing: self.ProfileResponse.UserInfo!.customerID!))"
                    
                    //Start Code only for extract country image
                    if let countryListName = UserDefaults.standard.value(forKey: "AllCountriesList") {
                        print(countryListName)
                    
                        for i in 0..<(AllCountries?.CountryList?.count)! {
                            if  (AllCountries?.CountryList![i].Iso3Code == self.ProfileResponse.UserInfo?.countryIsoCode) {
                                
                                let countryName = (AllCountries?.CountryList![i].CountryName)!
                                
                                let countyImage = UIImage(named: "\(countryName+".png")")!
                                
                                self.imgVInsideCircle.image = countyImage
                                
                            }
                        }
                    } //End Code only for extract country image
                    
                }else if  self.ProfileResponse?.myAppResult?.Code == 101 {
                    
                    
                }else{
                    
                    if( self.ProfileResponse?.myAppResult?.Message == nil){
                        
                        self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                        
                    }else{
                        
                        self.uc.errorSuccessAler("Error", ( self.ProfileResponse?.myAppResult?.Message)!, self)
                    }
                    
                }
                
            }
            
            
        }
    
    
    func UpdateProfilePicture(_ url : String){
        
       let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,
                      "PhotoBody":self.imageURL,
                    "ImageData":(self.imgVProfile.image?.jpegData(compressionQuality: 0.7))!] as [String:Any]
        
        
        
        uc.webServicePosthttpUploadPhoto(urlString: url, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
             self.UpdateProfilePicture(url)
                return
            }
            
            self.documentResponce = DocumentListresult(JSONString:result)
            
            if self.documentResponce?.myAppResult?.Code == 0 {
                
                self.uc.errorSuccessAler("", "Profile Picture Updated Successfully", self)

                
            }else if self.documentResponce?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(self.documentResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.documentResponce?.myAppResult?.Message)!, self)
                }
            }
        }
        
    }
}
