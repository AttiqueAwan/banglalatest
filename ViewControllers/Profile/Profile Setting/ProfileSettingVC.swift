//
//  ProfileSettingVC.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 13/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit

class ProfileSettingVC: UIViewController {
    
    @IBOutlet weak var lblUserName:UILabel!
    //Label Email
    @IBOutlet weak var lblEmail:UILabel!
    //Label Customer ID
    @IBOutlet weak var lblCustomerID:UILabel!
    @IBOutlet weak var lblInsideProfileCircle: UILabel!
    @IBOutlet weak var imgVInsideCircle: UIImageView!
    
    @IBOutlet weak var tblV: UITableView!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
   
    let menuName = ["View Profile","Reset Password","Support","Exchange Rate","Invite Friends"]
    let menuImages = [#imageLiteral(resourceName: "profile"),#imageLiteral(resourceName: "ResetPassword"),#imageLiteral(resourceName: "support"),#imageLiteral(resourceName: "ex rate"),#imageLiteral(resourceName: "share")]
    
    var parms = [String:Any]()
    
    let uc = UtilitySoftTechMedia()
    var ProfileResponse : Dashboardresult!
    
    
    var checkimage = ""
    var imageURL = ""
    var documentResponce : DocumentListresult!
    @IBOutlet weak var imgVProfile: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblV.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "ProfileCell")
        
//        let parms = ["userName":self.sideMenuView.lblUserName.text!,"lblCenter":self.sideMenuView.lblInsideProfileCircle.text!,"lblEmail":self.sideMenuView.lblEmail.text!,"lblCustomerID":self.sideMenuView.lblCustomerID.text!,"lblImageInsideProfile":self.sideMenuView.imgVInsideCircle.image!]
        
        self.imgVProfile.layer.cornerRadius = self.imgVProfile.frame.height/2
               self.imgVProfile.layer.masksToBounds = true
        
        if parms["userName"] == nil {
            self.getUserProfile()
        }else {
            if let labelInsideProfile = parms["lblCenter"] as? String {
                     self.lblInsideProfileCircle.text = labelInsideProfile
                 }
                 if let name = parms["userName"] as? String {
                     self.lblUserName.text = name
                 }
                 if let email = parms["lblEmail"] as? String {
                     self.lblEmail.text = email
                 }
                 if let customer = parms["lblCustomerID"] as? String {
                     self.lblCustomerID.text = customer
                 }
                 if let image = parms["lblImageInsideProfile"] as? String {
                     self.imgVInsideCircle.image = UIImage(named: image)
                 }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(SelectImage(_:)), name: ApiUrls.kNotification, object: nil)

    }

    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func ImageUpload(_ sender: UIButton) {

          let viewcontroller = AddDocumentPopUp()
          checkimage = "img1"
          viewcontroller.checkimage = checkimage
          self.navigationController?.pushViewController(viewcontroller, animated: true)
      }
      
      @objc  func SelectImage(_ notification:NSNotification) {
          
          let userinfo =  notification.userInfo as? [String:Any]
          let image =  userinfo!["image"] as? UIImage
          checkimage = (userinfo!["checkimage"] as? String)!
          //let imageData = userinfo!["imageData"] as? NSData
          let imageURL = userinfo!["imgURL"] as? String
          
          
          if(self.checkimage == "img1"){

            
              self.imgVProfile.image = image
              self.imageURL =  imageURL!
              self.UpdateProfilePicture(ApiUrls.UpdateProfilePicture)

          }
          
          
      }
    

}
