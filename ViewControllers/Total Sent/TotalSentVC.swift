//
//  TotalSentVC.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 19/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit

class TotalSentVC: UIViewController {
    
    @IBOutlet weak var lblTotalSendMoney: UILabel!
     @IBOutlet weak var lblTotalTransaction: UILabel!
     @IBOutlet weak var lblCreditLeft: UILabel!
     @IBOutlet weak var lblDate: UILabel!
    
    let uc = UtilitySoftTechMedia()

    override func viewDidLoad() {
        super.viewDidLoad()
        GetTransactionList() 
        // Do any additional setup after loading the view.
    }


    @IBAction func btnClickHere(_ sender: UIButton) {
        self.navigationController?.pushViewController(TransactionListViewController(), animated: true)
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension TotalSentVC {
    

    func GetTransactionList() {
        
        let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"Limit":"0"]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetTransactionList, params:parms , message: "Loading..."){result in
            
            let TransactionListResponse = TransactionListresult(JSONString:result)
            
            if  TransactionListResponse?.myAppResult?.Code == 0 {
                var totalsentAmount = 0.0
                var totalAmountsentIn90Day = 0.0
                print(result)
                
                if(TransactionListResponse?.transactionListDetail != nil &&
                    (TransactionListResponse?.transactionListDetail!.count)! > 0 ){
                    
                    for item in TransactionListResponse!.transactionListDetail! {
                        
                        if(self.isIn90Days(paymentDateString: item.PaymentDate!))
                        {
                            
                            let myDouble = Double(item.PayInAmount!)!
                            
                            totalAmountsentIn90Day += myDouble
                            
                        }
                        
                        let myDoubleValue = Double(item.PayInAmount!)!
                        totalsentAmount += myDoubleValue
                        
                        
                    }
                    
                    let sendingCurrency = TransactionListResponse!.transactionListDetail![0].SendingCurrency!
    
                    self.lblTotalSendMoney.text = "\(totalAmountsentIn90Day) \(sendingCurrency)"
                    
                    self.lblTotalTransaction.text = "\(totalsentAmount) \(sendingCurrency)"
                    

 
                }else{
                    
                    if(TransactionListResponse?.myAppResult?.Message == nil){
                        
                        self.uc.errorSuccessAler("Error", result, self)
                        
                    }else{
                        
                        self.uc.errorSuccessAler("Error", (TransactionListResponse?.myAppResult?.Message)!, self)
                    }
                }
                
                
            }
        }
    }
    
    
    
      func isIn90Days(paymentDateString: String)->Bool {
            

            let date = Date()
            let formater = DateFormatter()
            
            
            let ExactDate = paymentDateString.toDate(format: "yyyy-MM-dd")
            let df = DateFormatter()
            df.dateFormat = "dd-MM-yyyy"
            let date2 = df.string(from: ExactDate!)
            let dattee = date2.toDate(format: "dd-MM-yyyy")
            
            guard let paymentDate = dattee else {
                return false
            }
            print(paymentDate)
            
            formater.dateFormat = "yyyy-MM-dd"
    //        formater.timeStyle = .short
            formater.dateStyle = .short
            let currentDateString = formater.string(from: date)
            print(currentDateString)
            
            let currentDate = formater.date(from: currentDateString)
            
            guard let cd = currentDate else {
                return false
            }

            let dateNintyDays = Calendar.current.date(byAdding: .day, value: -90, to: Date())!

            let currentDateStr = formater.string(from: cd)
            let dateNinDaysStr = formater.string(from: dateNintyDays)
            
            self.lblDate.text = "\(dateNinDaysStr) to \(currentDateStr)"
            
            if  paymentDate < cd && paymentDate > dateNintyDays{
                return true
            }else {
                return false
            }
   
        }
}
