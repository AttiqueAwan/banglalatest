//
//  SendOTPViewController.swift
//  BanglaRemitt
//
//  Created by Asad Zahoor on 26/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit
import BEMCheckBox
import FRHyperLabel

class SendOTPViewController: UIViewController {

    @IBOutlet weak var lblTerms: FRHyperLabel!
    @IBOutlet weak var txtFieldView: UIView!
    @IBOutlet weak var termsAndConditionCheckBox: BEMCheckBox!
    @IBOutlet weak var otpCodeTxtField: UITextFieldX!
    
    @IBOutlet weak var btnEmail: UIButtonStyle!
    @IBOutlet weak var btnSMS: UIButtonStyle!
    let uc = UtilitySoftTechMedia()
    var OTPRes : OTPResponse!
    var isOTPSent  = false
    var deliveryType = String()
    var sendTo = ""
    var timer: Timer?
    var totalTime = 60
    var delegate:ModalViewControllerDelegate!
    var PayerID: Int!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addDoneButtonOnKeyboard()
        
        var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: "I have read and understood the information about fraud and scams, and I am happy to make this transaction.")
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.1960870624, green: 0.2391059399, blue: 0.4983942509, alpha: 1) ,range: NSRange(location:49,length:5))
         
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.1960870624, green: 0.2391059399, blue: 0.4983942509, alpha: 1) ,range: NSRange(location:59,length:5))
     self.lblTerms.attributedText = myMutableString
        // Do any additional setup after loading the view.
        let handler = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            if(substring == "fraud"){
                
                guard let url = URL(string: "https://online.banglaremit.co.uk/en/term-condition.html") else {
                    return //be safe
                }
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
                
            }
            else if(substring == "scams"){
                
                guard let url = URL(string: "https://online.banglaremit.co.uk/en/term-condition.html") else {
                    return //be safe
                }
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
                
            }
            
        }
        self.lblTerms.setLinksForSubstrings(["fraud","scams"], withLinkHandler: handler)
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
    @IBAction func btnViaEmail(_ sender: Any) {
        
        if(termsAndConditionCheckBox.on && self.btnEmail.isEnabled) {
            deliveryType = "E"
            self.SendOTP(deliveryTypeValue: deliveryType)
        }else{
            uc.makeToast(message: "Please select check box above")
        }
    }
    
    @IBAction func BtnViaSMS(_ sender: Any) {
        
        if(termsAndConditionCheckBox.on && self.btnSMS.isEnabled){
            deliveryType = "T"
            self.SendOTP(deliveryTypeValue: deliveryType)
        }else{
            uc.makeToast(message: "Please select check box above")
        }
       }
    
    @IBAction func btnVerifyOtp(_ sender: Any) {
        
        if(self.otpCodeTxtField.text != "")
        {
            self.VerifyOTP(sendTo: sendTo,deliveryType: deliveryType)
        }else{
            uc.makeToast(message: "OTP code is empty")
        }
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    public func startOtpTimer() {
            self.totalTime = 60
            self.isOTPSent = true
            self.txtFieldView.isHidden = false
            self.btnSMS.isEnabled = false
            self.btnEmail.isEnabled = false
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        }

    @objc func updateTimer() {
            
        
        self.btnEmail.titleLabel?.text = self.timeFormatted(self.totalTime) // will show timer
            if totalTime != 0 {
                totalTime -= 1  // decrease counter timer
            } else {
                if let timer = self.timer {
                    timer.invalidate()
                    self.timer = nil
                    self.btnEmail.titleLabel?.text = "SEND OTP VIA EMAIL"
                    self.btnSMS.titleLabel?.text = "SEND OTP VIA SMS"
                    self.btnEmail.isEnabled = true
                    self.btnSMS.isEnabled = true
                }
            }
        }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    
    
    
    
}

protocol ModalViewControllerDelegate
{
    func sendData(isOTPVerified: Bool)
}
