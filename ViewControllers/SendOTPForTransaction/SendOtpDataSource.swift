//
//  SendOtpDataSource.swift
//  BanglaRemitt
//
//  Created by Asad Zahoor on 26/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import Foundation
import UIKit


extension SendOTPViewController {
    
    
    
    func SendOTP(deliveryTypeValue: String ){
        
        let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"AppID":ApiUrls.AppID,"DeliveryType":deliveryTypeValue,"OTPType":1,"Email":Email]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.SendOTP, params:parms , message: "Loading..."){result in
            
            if(result == "fail")
            {
                self.SendOTP(deliveryTypeValue: deliveryTypeValue)
                return
            }
            
            self.OTPRes = OTPResponse(JSONString:result)
            
            if self.OTPRes != nil && self.OTPRes.myAppResult?.Code == 0{
                
                
                self.sendTo = self.OTPRes.OTPResp!.To!
                
                self.uc.makeToast(message: "OTP code sent to \(self.OTPRes.OTPResp?.To ?? "")")
                self.startOtpTimer()
                
            }else{
                
                if(self.OTPRes.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                }else{
                    
                    self.uc.errorSuccessAler("Error", (self.OTPRes!.myAppResult?.Message)!, self)
                }
            }
            
        }
        
    }
    
    func VerifyOTP(sendTo:String , deliveryType:String){
        
        var email = ""
        var phone = ""
        if(sendTo.starts(with: "+"))
        {
            phone = sendTo
        }else{
            email = sendTo
        }
        let authToken = uc.getAuthToken()
        
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"AppID":ApiUrls.AppID,"OTPType":1,"PayerID": self.PayerID!,"Email":email,"PhoneNumber":phone,"OTP":self.otpCodeTxtField.text!.trimmingCharacters(in: .whitespacesAndNewlines)]as [String : Any]
        print(parms)
        uc.webServicePosthttp(urlString: ApiUrls.VerifyOTP, params:parms , message: "Loading..."){result in

            if(result == "fail")
            {
                self.VerifyOTP(sendTo: sendTo,deliveryType: deliveryType)
                return
            }
            
            let loginResponse = AppUser(JSONString:result)
            
            if loginResponse != nil && loginResponse?.myAppResult?.Code == 0{
                
                
                self.dismiss(animated: true, completion: nil)
                
                self.delegate?.sendData(isOTPVerified: true)
                //self.uc.makeToast(message: "Verified")
            }else{
                
                if(loginResponse == nil){
                    self.uc.errorSuccessAler("Alert", result, self)
                    
                }
                else{
                    
                    self.uc.errorSuccessAler("Alert", (loginResponse!.myAppResult!.Message!), self)
                }
                
            }
            
        }
        
    }
    func addDoneButtonOnKeyboard()
          {
              let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:50))
              doneToolbar.barStyle = UIBarStyle.default
              
              let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
              let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonActionSend))
              done.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
              let items = NSMutableArray()
              items.add(flexSpace)
              items.add(done)
              
              doneToolbar.items = items as? [UIBarButtonItem]
              doneToolbar.sizeToFit()
            
            self.otpCodeTxtField.inputAccessoryView = doneToolbar
            self.otpCodeTxtField.inputAccessoryView = doneToolbar

    //          self.editAlertView.txtSendingEditAlert.inputAccessoryView = doneToolbar
    //          self.editAlertView.txtReceivingEditAlert.inputAccessoryView = doneToolbar
            //self.editAlertView.txtReceivingEditAlert.inputAccessoryView = doneToolbar
              
          }
    
    @objc func doneButtonActionSend(){
        
        self.otpCodeTxtField.resignFirstResponder()
        self.otpCodeTxtField.resignFirstResponder()
    }
    
}
