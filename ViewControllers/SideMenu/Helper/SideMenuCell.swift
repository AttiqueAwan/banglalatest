//
//  SideMenuCell.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 08/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    
    @IBOutlet weak var imgmenu:UIImageView!
    @IBOutlet weak var lblmenu:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
