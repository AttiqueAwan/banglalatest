//
//  SideMenuViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 08/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

    let uc = UtilitySoftTechMedia()
    var checkimage = ""
    var imageURL = ""
    var documentResponce : DocumentListresult!
    
    @IBOutlet weak var sideMenuView:SideMenuView!
    var ProfileResponse : Dashboardresult!
    @IBOutlet weak var activityView: UIActivityIndicatorView!

    
//    let menuName = ["Dashboard","My Profile","About Us","Invite Friend","Privacy Policy","Contact Us","FAQ's","Change Password","Logout"]
//    let menuImages = [#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "My Profile"),#imageLiteral(resourceName: "About Us"),#imageLiteral(resourceName: "Invite Friend"),#imageLiteral(resourceName: "Privacy Policy"),#imageLiteral(resourceName: "Contact Us"),#imageLiteral(resourceName: "FAQ's"),#imageLiteral(resourceName: "Change Password"),#imageLiteral(resourceName: "Logout")]
     let menuName = ["Enable auto login","Profile","Document","Recipients","Transactions","Settings","FAQ's","Logout"]
    let menuImages = [#imageLiteral(resourceName: "My Profile"),#imageLiteral(resourceName: "My Profile"),#imageLiteral(resourceName: "document"),#imageLiteral(resourceName: "recipients"),#imageLiteral(resourceName: "ico_trans"),#imageLiteral(resourceName: "menugreen"),#imageLiteral(resourceName: "FAQ's"),#imageLiteral(resourceName: "Logout")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Commit file
        //Commit file ....
        //commit file...
        self.sideMenuView.imgVProfile.layer.cornerRadius = self.sideMenuView.imgVProfile.frame.height/2
        self.sideMenuView.imgVProfile.layer.masksToBounds = true
        self.getUserProfile()
        let userInfo = uc.getUserInfo()
        let details = userInfo?.UserInfo
        
        if let FullName = details?.fullName{
            
            self.sideMenuView.lblUserName.text = FullName
        }
        
        if let Email = details?.email{
            
            self.sideMenuView.lblEmail.text = Email
        }
        
        if let CustomerID = details?.customerID{
            
            self.sideMenuView.lblCustomerID.text = "Customer ID : \(CustomerID)"
        }
        
        
        self.sideMenuView.tblSideMenu.register(UINib(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: "SideMenuCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(SelectImage(_:)), name: ApiUrls.kNotification, object: nil)
        
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func ImageUpload(_ sender: UIButton) {

        let viewcontroller = AddDocumentPopUp()
        checkimage = "img1"
        viewcontroller.checkimage = checkimage
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    @objc  func SelectImage(_ notification:NSNotification) {
        
        let userinfo =  notification.userInfo as? [String:Any]
        let image =  userinfo!["image"] as? UIImage
        checkimage = (userinfo!["checkimage"] as? String)!
        //let imageData = userinfo!["imageData"] as? NSData
        let imageURL = userinfo!["imgURL"] as? String
        
        
        if(self.checkimage == "img1"){

            self.sideMenuView.imgVProfile.image = image
            self.imageURL =  imageURL!
            self.UpdateProfilePicture(ApiUrls.UpdateProfilePicture)

        }
        
        
    }

}
