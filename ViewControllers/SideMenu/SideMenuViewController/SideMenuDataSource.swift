//
//  SideMenuDataSource.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 08/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension SideMenuViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexName = menuName[indexPath.row]
        print(indexName)
        
        switch indexName {
            case "Enable auto login":
            print("enable")
            
            
            
        case "Profile":
//            let  vc =  ProfileViewController(nibName:"ProfileViewController",bundle:Bundle.main)
//            let navigationController = UINavigationController()
//            navigationController.setViewControllers([DashboardViewController(),vc], animated: false)
//            navigationController.navigationBar.isHidden = true
//            navigationController.modalPresentationStyle = .fullScreen
//            self.present(navigationController, animated: false, completion: nil)
           
            self.navigationController?.pushViewController(ProfileViewController(), animated: true)
        case "Document":
            //documents
            GetDocumentList()
//            self.navigationController?.pushViewController(DocumentListViewController(), animated: true)
        case "Recipients":
            getRecipientList()
//            self.navigationController?.pushViewController(RecipientListViewController(), animated: true)
        case "Transactions":
            GetTransactionList()
//            self.navigationController?.pushViewController(TransactionListViewController(), animated: true)
        case "Settings":
            
            print("Settings")
            let objSetting = ProfileSettingVC()
            let parms = ["userName":self.sideMenuView.lblUserName.text!,"lblCenter":self.sideMenuView.lblInsideProfileCircle.text!,"lblEmail":self.sideMenuView.lblEmail.text!,"lblCustomerID":self.sideMenuView.lblCustomerID.text!,"lblImageInsideProfile":self.sideMenuView.imgVInsideCircle.image!] as [String : Any]
            objSetting.parms = parms
            self.navigationController?.pushViewController(objSetting, animated: true)
            
        case "FAQ's":
            if let url = URL(string: "https://online.banglaremit.co.uk/en/faqs-app.html") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        case "Logout":
            let  vc =  LoginViewController(nibName:"LoginViewController",bundle:Bundle.main)
            let navigationController = UINavigationController(nibName: "MainNavigation", bundle: Bundle.main)
            
            let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let mainstoryboardControler = storyBoard.instantiateViewController(withIdentifier: "MainController") as? ViewController
            navigationController.navigationBar.isHidden = true
            navigationController.pushViewController(mainstoryboardControler!, animated: true)
            navigationController.pushViewController(vc, animated: true)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: false, completion: nil)
            
        default:
            print("default")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.menuName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.sideMenuView.tblSideMenu.dequeueReusableCell(withIdentifier: "SideMenuCell") as? SideMenuCell
        
        let indexName = menuName[indexPath.row]
        
        if indexName == "Enable auto login" {
            
            let switchDemo = UISwitch()
            cell?.addSubview(switchDemo)
            switchDemo.translatesAutoresizingMaskIntoConstraints = false
            
            cell?.addConstraint(NSLayoutConstraint(item: switchDemo, attribute: .trailing, relatedBy: .equal, toItem: cell, attribute: .trailing, multiplier: 1, constant: -10))
            
            cell?.addConstraint(NSLayoutConstraint(item: switchDemo, attribute: .centerY, relatedBy: .equal, toItem: cell, attribute: .centerY, multiplier: 1, constant: -2))

            switchDemo.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
            
            if UserDefaults.standard.bool(forKey: "isLoginEnable") {
                switchDemo.setOn(true, animated: false)
            }

            
        }
        
        cell?.lblmenu.text = self.menuName[indexPath.row]
        cell?.imgmenu.image = self.menuImages[indexPath.row]
        cell?.imgmenu.tintColor = UIColor.green
        return cell!
    }

    @objc func switchValueDidChange(_ sender: UISwitch) {
        
        UserDefaults.standard.set(sender.isOn, forKey: "isLoginEnable")
        UserDefaults.standard.synchronize()
        
    }
    
}


extension SideMenuViewController {
    
    func getUserProfile(){
        
        self.activityView?.isHidden = false
        self.activityView?.startAnimating()
            
            let authToken = uc.getAuthToken()
            
            let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!]as [String : Any]
            
            uc.webServicePosthttp(urlString: ApiUrls.GetProfile, params:parms , message: "Loading..."){result in
                
                self.ProfileResponse = Dashboardresult(JSONString:result)
                
                if  self.ProfileResponse?.myAppResult?.Code == 0 {
                    
                    let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
                    
                    let AllCountries = CountryListresult(JSONString: result)
                    
                    
                    for i in 0..<(AllCountries?.CountryList?.count)! {
                        
                        if(AllCountries?.CountryList![i].Iso3Code == self.ProfileResponse.UserInfo?.countryIsoCode){
                            
//                            if let Country = AllCountries?.CountryList![i].CountryName {
//                            }
                           
                        }
                        
                        if(AllCountries?.CountryList![i].Iso3Code == self.ProfileResponse.UserInfo?.nationalityIsoCode){
                            
                            
                        }
                    }
      
                    let firstChar_FN = self.ProfileResponse.UserInfo?.firstName.prefix(1)
                    let lastChar_LN = self.ProfileResponse.UserInfo?.lastName.prefix(1)
                    let strName = "\(String(describing: firstChar_FN!)) \(String(describing: lastChar_LN!))"
                    print(strName)
                    self.sideMenuView.lblInsideProfileCircle.text = strName
                    
                    if let Imgae1 = self.ProfileResponse.UserInfo?.photo {
                        
                        //self.sideMenuView.imgVProfile.sd_setImage(with: URL(string: Imgae1), placeholderImage: #imageLiteral(resourceName: "ico_upload"), options: .continueInBackground, completed: nil)
                        
                        
                        self.sideMenuView.imgVProfile.sd_setImage(with: URL(string: Imgae1)) { [weak self] (img, error, chacheType, url) in
                            if (img != nil) {
                                self?.sideMenuView.imgVProfile.image = img
                            }
                            self?.activityView!.stopAnimating()
                        }
                        
                    }
                    
    //                AllCountriesList
                    
                    //Start Code only for extract country image
                    if let countryListName = UserDefaults.standard.value(forKey: "AllCountriesList") {
                        print(countryListName)
                    
                        for i in 0..<(AllCountries?.CountryList?.count)! {
                            if  (AllCountries?.CountryList![i].Iso3Code == self.ProfileResponse.UserInfo?.countryIsoCode) {
                                
                                let countryName = (AllCountries?.CountryList![i].CountryName)!
                                
                                let countyImage = UIImage(named: "\(countryName+".png")")!
                                
                                self.sideMenuView.imgVInsideCircle.image = countyImage
                                
                            }
                        }
                    } //End Code only for extract country image
                    
                }else if  self.ProfileResponse?.myAppResult?.Code == 101 {
                    
                    
                }else{
                    
                    if( self.ProfileResponse?.myAppResult?.Message == nil){
                        
                        self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                        
                    }else{
                        
                        self.uc.errorSuccessAler("Error", ( self.ProfileResponse?.myAppResult?.Message)!, self)
                    }
                    
                }
            }
        }
    
    func UpdateProfilePicture(_ url : String){
        
       let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,
                      "PhotoBody":self.imageURL,
                    "ImageData":(self.sideMenuView.imgVProfile.image?.jpegData(compressionQuality: 0.7))!] as [String:Any]
        
        
        
        uc.webServicePosthttpUploadPhoto(urlString: url, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
             self.UpdateProfilePicture(url)
                return
            }
            
            self.documentResponce = DocumentListresult(JSONString:result)
            
            if self.documentResponce?.myAppResult?.Code == 0 {
                
                self.uc.errorSuccessAler("", "Profile Picture Updated Successfully", self)

                
            }else if self.documentResponce?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(self.documentResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.documentResponce?.myAppResult?.Message)!, self)
                }
            }
        }
        
    }
    
    //DocoumentList Api
    func GetDocumentList() {
        
        let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.getDocumentList, params:parms , message: "Loading..."){result in
            
            let DocumentList = DocumentListresult(JSONString:result)
            
            if  DocumentList?.myAppResult?.Code == 0 {
                
                if let validDocumentList = DocumentList?.DocumentList, validDocumentList.count > 0 {
                     self.navigationController?.pushViewController(DocumentListViewController(), animated: true)
                }else {
                    let message = "This menu allows you to add and view a range of ID Documents such as Passport, National ID, Driving License... that is used to verify your identity in a secure manner. Please proceed with your 1st Transaction to view this menu"
                    self.uc.errorSuccessAler("Alert", message, self)
                }
                
            }else if  DocumentList?.myAppResult?.Code == 102{
                
 
            }else{
                
                if( DocumentList?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( DocumentList?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
        
    }
    
    //Recipient
    
    func getRecipientList(){
        
        let authToken = uc.getAuthToken()
        
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetRecipientList, params:parms , message: "Loading..."){result in
            
            let RecipientList = RecipientListresult(JSONString:result)
            
            if RecipientList?.myAppResult?.Code == 0 {
                
                if let validRecipientList = RecipientList?.RecipientList, validRecipientList.count > 0 {
                    
                    self.navigationController?.pushViewController(RecipientListViewController(), animated: true)
                    
                }else {
                    let message = "This menu allow you to add and view all beneficiaries. Please proceed with your 1st Transaction to view this menu"
                    self.uc.errorSuccessAler("Alert", message, self)
                }
                
            }else if RecipientList?.myAppResult?.Code == 102{
                
                
            }else{
                
                if( RecipientList?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", (RecipientList?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
        
    }
    
    //Transaction

        func GetTransactionList() {
            
            let authToken = uc.getAuthToken()
            let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"Limit":"0"]as [String : Any]
            
            uc.webServicePosthttp(urlString: ApiUrls.GetTransactionList, params:parms , message: "Loading..."){result in
                
                let TransactionListResponse = TransactionListresult(JSONString:result)
                
                if  TransactionListResponse?.myAppResult?.Code == 0 {
                    
                    if let validTransactionList = TransactionListResponse?.transactionListDetail, validTransactionList.count > 0 {
                        self.navigationController?.pushViewController(TransactionListViewController(), animated: true)
                        
                    }else {
                        let message = "This menu allows you to view all your transaction history, with most recent at the top. Plese proceed with your 1st Transaction to view this menu"
                        self.uc.errorSuccessAler("Alert", message, self)
                    }

                    
                }else if TransactionListResponse?.myAppResult?.Code == 102{
                    
                   let message = "no transaction available for this user"
                    self.uc.errorSuccessAler("Alert", message, self)
                    
                }else{
                    
                    if( TransactionListResponse?.myAppResult?.Message == nil){
                        
                        self.uc.errorSuccessAler("Error", result, self)
                        
                    }else{
                        
                        self.uc.errorSuccessAler("Error", (TransactionListResponse?.myAppResult?.Message)!, self)
                    }
                }
            }
        }
    
}
