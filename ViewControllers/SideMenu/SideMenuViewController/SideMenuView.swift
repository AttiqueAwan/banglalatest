//
//  SideMenuView.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 08/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class SideMenuView: UIView {
    
    @IBOutlet weak var tblSideMenu:UITableView!

    //Label User Name
    @IBOutlet weak var lblUserName:UILabel!
    
    //Label Email
    @IBOutlet weak var lblEmail:UILabel!
    
    //Label Customer ID
    @IBOutlet weak var lblCustomerID:UILabel!
    
    @IBOutlet weak var lblInsideProfileCircle: UILabel!
    @IBOutlet weak var imgVInsideCircle: UIImageView!
    
    @IBOutlet weak var imgVProfile: UIImageView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
