//
//  TransactionDataSource.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 09/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit

extension TransactionDetail {
    
    func fillValues() {
    
        if let status = self.transactionListDetail.PaymentStatus {
            
            if status == "Canceled"{
                self.transactionDetailView.lblStatus.text = "Cancelled"
                self.transactionDetailView.lblStatusInside.text = "Cancelled"
            }else {
                self.transactionDetailView.lblStatus.text = status
                self.transactionDetailView.lblStatusInside.text = status
            }
            
            if status == "Incomplete" {
                self.transactionDetailView.lblPin.text = "*****"
            }else {
                if let pinNumber = self.transactionListDetail.PaymentNumber {
                    self.transactionDetailView.lblPin.text = pinNumber
                }
            }
        }
        
        if let date = self.transactionListDetail.PaymentDate {
            self.transactionDetailView.lblDate.text = date
               }
        
        var receiverName = ""
        if let rName = self.transactionListDetail.BeneName {
            receiverName = rName
        }
        
        if let theyReceive = self.transactionListDetail.PayOutAmount, let receivingCurrency = self.transactionListDetail.ReceivingCurrency {
            
            var payout: Double = Double(theyReceive)!
            let incentive = 0.02 * Double(payout)
            payout = incentive + payout
            
            let append = "\(payout) \(receivingCurrency) "
            
            self.transactionDetailView.lblTheyReceive.text = append
            
            
            self.transactionDetailView.lblTheyReceiveTop.text = "You transfer to \(receiverName) \(append)"
            
               }
        if let payInAmount = self.transactionListDetail.PayInAmount, let serviceCharge = self.transactionListDetail.ServiceCharges {
            
            
            if self.transactionListDetail.SendingCurrency != nil {
                currency = self.transactionListDetail.SendingCurrency!
            }
            
//            let doubleStr = String(format: "%.2f", toDouble!)
            
            let doubleStr = Double(payInAmount)! + (Double(serviceCharge)!)
            let total = String(format: "%.2f", doubleStr)
            
            
            self.transactionDetailView.lblToalPaid.text = "\(total) \(currency)"
        }
        
        
        
        //service charges
        if self.transactionListDetail.ServiceCharges != nil {
            var currency = ""
            if self.transactionListDetail.SendingCurrency != nil {
                currency = self.transactionListDetail.SendingCurrency!
            }
            
            self.transactionDetailView.lblFee.text = "\(self.transactionListDetail!.ServiceCharges!) \(currency)"
        }
    
        
        //ExchangeRate
        if self.transactionListDetail.ExchangeRate != nil {
            
            var reciveCurrency = ""
            
            if self.transactionListDetail.ReceivingCurrency != nil {
                reciveCurrency = self.transactionListDetail.ReceivingCurrency!
            }
            
            self.transactionDetailView.lblExchangeRate.text = "\(self.transactionListDetail!.ExchangeRate!) \(reciveCurrency)"
            
            
            
        }
        
        
        if let receiverPhoneNo = self.transactionListDetail.BenePhone {
            self.transactionDetailView.lblReceiverPhoneNo.text = receiverPhoneNo
               }
        
        if let receiverCountry = self.transactionListDetail.ReceivingCountry {
            self.transactionDetailView.lblReceiverCountry.text = receiverCountry
               }
        
        if let receiverName = self.transactionListDetail.BeneName {
            self.transactionDetailView.lblReceiverName.text = receiverName
               }
        
        if let deliveryMethod = self.transactionListDetail.PaymentMethod {
            self.transactionDetailView.lblDeliveryMethod.text = deliveryMethod
               }
        
        if let paymentMethod = self.transactionListDetail.SendingPaymentMethod {
                self.transactionDetailView.lblPaymentMethod.text = paymentMethod
        
            if paymentMethod == "Debit Card" {
                self.transactionDetailView.viewBankDepositDetails.isHidden = true
                
            }else if paymentMethod == "Bank"{
                self.transactionDetailView.viewBankDepositDetails.isHidden = false
            }
            
               }
        if let cashPayoutBank = self.transactionListDetail.PayerName {
            self.transactionDetailView.lblCashPayoutBank.text = cashPayoutBank
               }
        
        if let payoutBranch = self.transactionListDetail.PayOutBranchName {
            self.transactionDetailView.lblPayoutBranch.text = payoutBranch
               }
        
        if let branchAddress = self.transactionListDetail.PayoutBranchAddress {
            self.transactionDetailView.lblBranchAddress.text = branchAddress
               }
        
        if let senderCountry = self.transactionListDetail.SendingCountry {
        
            self.transactionDetailView.lblSenderCountry.text = senderCountry
               }
        if let sendingReason = self.transactionListDetail.SendingReason {
            self.transactionDetailView.lblSendingReason.text = sendingReason
               }
        
        if let incentive = self.transactionListDetail.IncentiveAmount , self.transactionListDetail.IncentiveAmount != nil  {
            var reciverCurrency = ""
            if self.transactionListDetail.ReceivingCurrency != nil, self.transactionListDetail.ReceivingCurrency != "" {
                reciverCurrency = self.transactionListDetail.ReceivingCurrency!
            }
            
            self.transactionDetailView.lblIncentive.text = "(PLEASE COLLECT YOUR 2% GOVERNMENT INCENTIVE \(incentive) \(reciverCurrency))"
        }

    }
    
    func changeColors() {
        
       if self.transactionListDetail.PaymentStatus != nil {
        let status = self.transactionListDetail.PaymentStatus!

        switch status {
            
        case "Canceled":
            self.transactionDetailView.viewTopBanner.backgroundColor = #colorLiteral(red: 0.6, green: 0, blue: 0, alpha: 1)
             self.transactionDetailView.imgV.image = UIImage(named: "cancelled_icon")
            
        case "Canceling":
            self.transactionDetailView.viewTopBanner.backgroundColor = #colorLiteral(red: 0.6, green: 0, blue: 0, alpha: 1)
            self.transactionDetailView.imgV.image = UIImage(named: "cancelled_icon")
            
        case "Completed":
          self.transactionDetailView.viewTopBanner.backgroundColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
            self.transactionDetailView.imgV.image = UIImage(named: "complete_icon")
            
        case "Incomplete":
            self.transactionDetailView.viewTopBanner.backgroundColor = #colorLiteral(red: 0.8, green: 0.4, blue: 0, alpha: 1)
             self.transactionDetailView.imgV.image = UIImage(named: "in_complete_icon")
            
        case "In-process":
            self.transactionDetailView.viewTopBanner.backgroundColor = #colorLiteral(red: 0, green: 0.5450980392, blue: 0.8, alpha: 1)
            self.transactionDetailView.imgV.image = UIImage(named: "in_process_icon")
            
        case "Ok":
            self.transactionDetailView.viewTopBanner.backgroundColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
            self.transactionDetailView.imgV.image = UIImage(named: "complete_icon")
            
        case "Pending":
            self.transactionDetailView.viewTopBanner.backgroundColor = #colorLiteral(red: 0.8, green: 0.4, blue: 0, alpha: 1)
            self.transactionDetailView.imgV.image = UIImage(named: "in_process_icon")
            
        case "Paid":
            self.transactionDetailView.viewTopBanner.backgroundColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
            self.transactionDetailView.imgV.image = UIImage(named: "complete_icon")
            
        default:
            self.transactionDetailView.viewTopBanner.backgroundColor = #colorLiteral(red: 0.8, green: 0.4, blue: 0, alpha: 1)
            self.transactionDetailView.imgV.image = UIImage(named: "in_process_icon")
        }
        
        }
    }
}
