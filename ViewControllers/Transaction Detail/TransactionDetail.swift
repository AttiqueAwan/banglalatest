//
//  TransactionDetailVC.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 06/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit
import MessageUI

class TransactionDetail: UIViewController,MFMailComposeViewControllerDelegate {
    

    @IBOutlet var transactionDetailView: TransactionDetailView!
    
    var transactionListDetail : TransactionListDetail!
    var currency = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if transactionListDetail != nil {
            self.fillValues()
            self.changeColors()
        }
        
        
    }

    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEmail(_ sender: UIButton) {
        sendEmail()
        
    }
    
    func sendEmail() {
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["support@banglaremit.co.uk"])
            mail.setMessageBody("<p>Add message here</p>", isHTML: true)

            present(mail, animated: true)
        } else {
            // show failure alert
            let alert = UIAlertController(title: "Alert", message: "you are seems not sign In!", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
               self.navigationController?.popViewController(animated: true)
               self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
    @IBAction func btnBankDepositDetails(_ sender: UIButton) {
        
//        UserDefaults.standard.set(bankDepositDetail.sendingAmount, forKey: "bankDepositDetailSendingAmount")
//        UserDefaults.standard.set(bankDepositDetail.referencNumber, forKey: "bankDepositDetailReferencNumber")
        let bankDepositDetailVC = BankDepositDetailVC()

        bankDepositDetailVC.referencNumber = self.transactionDetailView.lblPin.text!
        let total = self.transactionDetailView.lblToalPaid.text!
        let fee = self.transactionDetailView.lblFee.text!
        let totalDigit = total.components(separatedBy: " ")
        let feeDigit = fee.components(separatedBy: " ")
        
//        let totatUp2value = String(format: "%.2f", total)
        
        let caculateValue =  Double(totalDigit[0])! + Double(feeDigit[0])!
        let caculatFormat = String(format: "%.2f",caculateValue)
        
        bankDepositDetailVC.sendingAmount = "\(caculatFormat) \(totalDigit[1])"  //" \(Double(totalDigit[0])! + Double(feeDigit[0])!) "
    
        self.navigationController?.pushViewController(bankDepositDetailVC, animated: true)
    }
}
