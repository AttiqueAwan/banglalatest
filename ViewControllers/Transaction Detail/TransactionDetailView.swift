//
//  TransactionDetailView.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 06/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit

class TransactionDetailView: UIView {

    @IBOutlet weak var viewTopBanner: UIView!
    
    @IBOutlet weak var viewBankDepositDetails: ViewStyle!
    @IBOutlet weak var viewImgViewInside: ViewStyle!
    @IBOutlet weak var lblStatusInside: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblPin: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTheyReceive: UILabel!
    @IBOutlet weak var lblToalPaid: UILabel!
    @IBOutlet weak var lblFee: UILabel!
    @IBOutlet weak var lblExchangeRate: UILabel!
    @IBOutlet weak var lblReceiverPhoneNo: UILabel!
    @IBOutlet weak var lblReceiverCountry: UILabel!
    @IBOutlet weak var lblReceiverName: UILabel!
    @IBOutlet weak var lblDeliveryMethod: UILabel!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    @IBOutlet weak var lblCashPayoutBank: UILabel!
    @IBOutlet weak var lblPayoutBranch: UILabel!
    @IBOutlet weak var lblBranchAddress: UILabel!
    @IBOutlet weak var lblSenderCountry: UILabel!
    @IBOutlet weak var lblSendingReason: UILabel!
    @IBOutlet weak var imgV: UIImageView!
    
    @IBOutlet weak var lblIncentive: UILabel!
    
    
    @IBOutlet weak var lblTheyReceiveTop: UILabel!
}
