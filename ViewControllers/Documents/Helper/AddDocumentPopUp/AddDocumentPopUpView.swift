//
//  AddDocumentPopUpView.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 22/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class AddDocumentPopUpView: UIView {

    @IBOutlet weak var btnGallery: UIButton!
    
    @IBOutlet weak var btnCamera: UIButton!
    
    @IBOutlet weak var btnCancel: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
