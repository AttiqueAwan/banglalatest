//
//  AddDocumentPopUp.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 22/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class AddDocumentPopUp: UIViewController {

    @IBOutlet var addDocumentPopUpView: AddDocumentPopUpView!
    let imagePicker = UIImagePickerController()
    
    var checkimage = ""
    var img1 = UIImageView()
    var image1URL = ""
    var imgData : Data!
    var isDocument = false
    var isDocument2 = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        
        self.addDocumentPopUpView.btnCancel.layer.cornerRadius = 8.0
        self.addDocumentPopUpView.btnCancel.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }

    @IBAction func btnCameraClick(_ sender: Any) {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnGalleryClick(_ sender: Any) {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
        
    }
}


extension AddDocumentPopUp:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        if #available(iOS 11.0, *) {
            
              let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            
            if(picker.sourceType == .camera){
                
             let imageURL = self.saveImage(imageName: "\(arc4random()).png", image)
             self.image1URL = imageURL
            
            }else{
                
                let imageURL = info[UIImagePickerController.InfoKey.imageURL] as! URL
                self.image1URL =  imageURL.absoluteString
            }
            
            self.img1.image = image
            let  imageData = image.jpegData(compressionQuality: 0.7)
            self.imgData = imageData
            
        } else {
            // Fallback on earlier versions
        }
        
        if isDocument {
            
            NotificationCenter.default.post(name: ApiUrls.kDocNotification, object: nil, userInfo: ["image":self.img1.image!,"checkimage":self.checkimage,"imageData":imgData,"imgURL":self.image1URL])
        }else if isDocument2 {
            
            NotificationCenter.default.post(name: ApiUrls.kDocNotification, object: nil, userInfo: ["image":self.img1.image!,"checkimage":self.checkimage,"imageData":imgData,"imgURL":self.image1URL])
        }else {
            NotificationCenter.default.post(name: ApiUrls.kNotification, object: nil, userInfo: ["image":self.img1.image!,"checkimage":self.checkimage,"imageData":imgData,"imgURL":self.image1URL])
        }
        
        
        
        
        dismiss(animated:false, completion: {
        
//             self.dismiss(animated:true, completion: nil)
            self.navigationController?.popViewController(animated: true)
        })
       
    }
    
    func saveImage(imageName: String,_ imageselected:UIImage)-> String{
        //create an instance of the FileManager
        let fileManager = FileManager.default
        //get the image path
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        //get the image we took with camera
        let image = imageselected
        //get the PNG data for this image
        let data = image.pngData()
        //store it in the document directory
        fileManager.createFile(atPath: imagePath as String, contents: data, attributes: nil)
        return imagePath
        
    }
    
    
    
}
