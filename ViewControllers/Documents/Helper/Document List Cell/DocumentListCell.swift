//
//  DocumentListCell.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 08/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class DocumentListCell: UITableViewCell {

    @IBOutlet weak var lblDocumentType: UILabel!
    @IBOutlet weak var lblIssueDate: UILabel!
    @IBOutlet weak var lblExpiryDate: UILabel!
    
    @IBOutlet weak var btnEyeTop: UIButton!
    
    @IBOutlet weak var lblIsVerified: UILabel!
    @IBOutlet weak var btnEyeBottom: UIButton!
    
    @IBOutlet weak var imgDocType: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
