//
//  AddDocumentDataSource.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 07/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import MaterialControls
import UIKit

extension AddDocumentViewController:MDDatePickerDialogDelegate{
    
    
    func datePickerDialogDidSelect(_ date: Date) {
        
        let formater = DateFormatter()
        formater.dateFormat = "yyyy-MM-dd"
        
        let newDate = formater.string(from: date)
        
        if(self.checkpicker == "Issue"){
            
            self.addDocumentView.txtIssueDate.text = newDate
            
        }else if(self.checkpicker == "Expiry"){
            
            self.addDocumentView.txtExpiryDate.text = newDate
            
        }
    }
    
    
    
    
}

extension AddDocumentViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        if #available(iOS 11.0, *) {
            let imageURL = info[UIImagePickerController.InfoKey.imageURL] as! URL
           
            let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            
            if(self.checkimage == "Image1"){
                
                self.addDocumentView.img1.image = image
                 self.image1URL =  imageURL.absoluteString
                
            }else if(self.checkimage == "Image2"){
               
                self.addDocumentView.img2.image = image
                 self.Image2URL =  imageURL.absoluteString
                
            }
           let  imageData = image.jpegData(compressionQuality: 0.7)
            print(imageData!)
           self.imgData = imageData
            
        } else {
            // Fallback on earlier versions
        }
        
         dismiss(animated:true, completion: nil)
    }
    
    
}

extension AddDocumentViewController{
    
    
    func getDocumnetTypeList(){
        
        let authToken = uc.getAuthToken()
        
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"ListType":"DOCUMENTTYPES"]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetLists, params:parms , message: "Loading..."){result in
            
            self.DocumentTypeList = ListingTypesresult(JSONString:result)
            
            if  self.DocumentTypeList?.myAppResult?.Code == 0 {
                
                for i in 0..<(self.DocumentTypeList.ListType?.count)!{
                    
                    self.DocumentType.append(self.DocumentTypeList.ListType![i].Text!)
                    
                }
                
                self.DocumentTypeDropDown.dataSource = self.DocumentType
                self.DocumentTypeDropDown.reloadAllComponents()
                
            }else if  self.DocumentTypeList?.myAppResult?.Code == 102{

            }else{
                
                if( self.DocumentTypeList?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( self.DocumentTypeList?.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    func AddDocument(_ urlString:String){
        
      let authToken = uc.getAuthToken()
        if(self.UpdateRecipient != nil){
            
        }
        
//        self.documentTypeId
        var parms =  ["Token":(authToken?.authToken)!,
                      "ID":(authToken?.userId)!,
                      "DocBody":self.image1URL,
                      "DocTypeName":self.addDocumentView.txtDocumentType.text!,
                      "DocType":self.documentTypeId,
                      "DocIssueDate":self.addDocumentView.txtIssueDate.text!,
                      "DocExpireDate":self.addDocumentView.txtExpiryDate.text!,
                      "DocNumber":self.addDocumentView.txtDocumentNumber.text!,
                      "ImageData":(self.addDocumentView.img1.image?.jpegData(compressionQuality: 0.7)!)!] as [String:Any]
        
        //"ImageData":(self.img1.image?.jpegData(compressionQuality: 0.7))!] as [String:Any]
        
        if( self.addDocumentView.txtDocumentType.text == "Driving Liciense" || self.addDocumentView.txtDocumentType.text == "National ID Card"){
            
            parms["DocBodyBack"] = self.Image2URL
            parms["ImageData2"] = self.addDocumentView.img2.image?.jpegData(compressionQuality: 0.7)
          
        }
        if(self.UpdateRecipientDetail != nil){
            
            parms["DocID"] = "\((self.UpdateRecipientDetail.DocID)!)"
        }
        
        uc.webServicePosthttpUpload(urlString: urlString, params:parms , message: "Loading...", currentController: self){result in
            
            self.UpdateRecipient = DocumentListresult(JSONString:result)
            
            if  self.UpdateRecipient?.myAppResult?.Code == 0 {
                
                self.navigationController?.popViewController(animated: true)
                
            }else if  self.UpdateRecipient?.myAppResult?.Code == 101 {
                
            }else{
                
                if( self.UpdateRecipient?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( self.UpdateRecipient?.myAppResult?.Message)!, self)
                }
            }
        }
    }
}
