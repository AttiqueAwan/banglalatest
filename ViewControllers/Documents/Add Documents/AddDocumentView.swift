//
//  AddDocumentView.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 07/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class AddDocumentView: UIView {
    @IBOutlet weak var documentImagesWidthConstant: NSLayoutConstraint!
    
    @IBOutlet weak var documentButtonWidthConstant: NSLayoutConstraint!
  
    //Label Title
    @IBOutlet weak var lblTitle: UILabel!
    
    
    /// Button ////////////////
    //Button Back
    @IBOutlet weak var btnBack: UIButton!
    
    //Add  Document Button
    @IBOutlet weak var btnAddDocument: UIButton!
    
    //Button Document Type
    @IBOutlet weak var btnDocumentType: UIButton!
    
    //Button Expiry Date
    @IBOutlet weak var btnExpiryDate: UIButton!
    
    //Button Issue Date
    @IBOutlet weak var btnIssueDate: UIButton!
    
    //Button Image 1
    @IBOutlet weak var btnImage1: UIButton!
    
    //Button Image 2
    @IBOutlet weak var btnImage2: UIButton!
    
    //Button Upload Image 1
    @IBOutlet weak var btnUpload1: UIButton!
    
    
    //Button Upload Image 2
    @IBOutlet weak var btnUpload2: UIButton!
    
    ////ImageView ///////////////////
    
    //Image 1
    @IBOutlet weak var img1: UIImageView!
    
    //Image 2
    @IBOutlet weak var img2: UIImageView!
    
    
    /////////// TextFields ///////////////
    
    //TextField DocumentType
    @IBOutlet weak var txtDocumentType: JVFloatLabeledTextField!
    
    //TextField Document Number
    @IBOutlet weak var txtDocumentNumber: JVFloatLabeledTextField!
    
    //TextField Issue Date
    @IBOutlet weak var txtIssueDate: JVFloatLabeledTextField!
    
    //TextField Expiry Date
    @IBOutlet weak var txtExpiryDate: JVFloatLabeledTextField!
    
   //////////========= Views  /////////////
  
    //Upload Buttons View
    @IBOutlet weak var UploadButtonView: UIView!
    
    //Upoads Images View
    @IBOutlet weak var UploadImagesView: UIView!
    
    //Images 1 View
    @IBOutlet weak var image1View: UIView!
    
    //Image 2 View
    @IBOutlet weak var image2View: UIView!
    
    //modify 28/02/2020
    
    
    @IBOutlet weak var viewDocumentDescription: UIView!
    
    @IBOutlet weak var txtDocumentDescription: JVFloatLabeledTextField!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
