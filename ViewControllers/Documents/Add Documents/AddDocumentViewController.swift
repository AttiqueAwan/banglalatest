//
//  AddDocumentViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 07/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import DropDown
import MaterialControls
import SDWebImage
import SVProgressHUD


class AddDocumentViewController: UIViewController {
    
    @IBOutlet var topconstraint: NSLayoutConstraint!
    
    @IBOutlet var addDocumentView: AddDocumentView!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var activityView2: UIActivityIndicatorView!
    
    @IBOutlet weak var lblImage2 : UILabel!
    @IBOutlet weak var lblImage2Details : UILabel!
    
     let txtFieldSetting = textFieldSetting()
    
    let DocumentTypeDropDown = DropDown()
    var DocumentType = [String]()
    var documentTypeId = String()
    
    
    //Date Picker
    var datePicker = MDDatePickerDialog()
    
    var issueDatePicker = MDDatePickerDialog()
    
    let uc = UtilitySoftTechMedia()
    
    var UpdateRecipient : DocumentListresult!
    var UpdateRecipientDetail:DocumentListDetail!
    var DocumentTypeList : ListingTypesresult!
    
     var checkpicker = ""
    var checkimage = ""
    var imgData:Data!
    let imagePicker = UIImagePickerController()
    
    
    var image1URL = ""
    var Image2URL = ""
    
    // alert
    let textView = UITextView(frame: CGRect.zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()

     
        self.addDoneButtonOnKeyboard()
        //Date picker Delegate Setting
        datePicker.delegate = self
        issueDatePicker.delegate = self
        
        //Image Picker Delegate
         imagePicker.delegate = self
        
        
        self.addDocumentView.txtDocumentType = self.txtFieldSetting.textFieldBaseLine(self.addDocumentView.txtDocumentType, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addDocumentView.txtDocumentNumber = self.txtFieldSetting.textFieldBaseLine(self.addDocumentView.txtDocumentNumber, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addDocumentView.txtIssueDate = self.txtFieldSetting.textFieldBaseLine(self.addDocumentView.txtIssueDate, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addDocumentView.txtExpiryDate = self.txtFieldSetting.textFieldBaseLine(self.addDocumentView.txtExpiryDate, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addDocumentView.txtDocumentDescription = self.txtFieldSetting.textFieldBaseLine(self.addDocumentView.txtExpiryDate, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
    
        
        //setting User Type dropdown
        DocumentTypeDropDown.anchorView = self.addDocumentView.btnDocumentType
        DocumentTypeDropDown.dataSource = DocumentType
        
        
        DocumentTypeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
//          documentTypeId
            
            
            let id = self.DocumentTypeList!.ListType![index].ID
            
            
            self.documentTypeId = String(id!)
            
            
            
            if (item == "Driving License"){
                
                self.addDocumentView.image2View.isHidden = false
                    self.addDocumentView.image1View.isHidden = false
                    self.addDocumentView.btnUpload1.isHidden = false
                    self.addDocumentView.btnUpload2.isHidden = false
                    self.lblImage2.isHidden = false
                    self.lblImage2Details.isHidden = false
                
                self.addDocumentView.viewDocumentDescription.isHidden = true
                
                if self.lblImage2.isHidden == true{
                    self.topconstraint.constant = 2
                }else if self.lblImage2.isHidden == false{
                    self.topconstraint.constant = 10
                }
                
//                self.addDocumentView.btnUpload1.frame = CGRect(x: self.addDocumentView.UploadButtonView.frame.origin.x-20, y: self.addDocumentView.btnUpload1.frame.origin.y, width: self.addDocumentView.btnUpload1.frame.width, height: self.addDocumentView.btnUpload1.frame.size.height)
//
//                 self.addDocumentView.image1View.frame = CGRect(x: self.addDocumentView.UploadImagesView.frame.origin.x-50, y: self.addDocumentView.image1View.frame.origin.y, width: self.addDocumentView.image1View.frame.width, height: self.addDocumentView.image1View.frame.size.height)
                
                
            }else if(item == "National ID Card"){
                 
            
            self.addDocumentView.image2View.isHidden = false
            self.addDocumentView.image1View.isHidden = false
            self.addDocumentView.btnUpload1.isHidden = false
            self.addDocumentView.btnUpload2.isHidden = false
            self.lblImage2.isHidden = false
            self.lblImage2Details.isHidden = false
            
            self.addDocumentView.viewDocumentDescription.isHidden = true
                
                if self.lblImage2.isHidden == true{
                    self.topconstraint.constant = 2
                }else if self.lblImage2.isHidden == false{
                    self.topconstraint.constant = 10
                }
                
            }
            else if (item == "Passport" ) {
                
                
                self.addDocumentView.image2View.isHidden = true
                self.addDocumentView.image1View.isHidden = false
                self.addDocumentView.btnUpload1.isHidden = false
                self.addDocumentView.btnUpload2.isHidden = true
                self.addDocumentView.viewDocumentDescription.isHidden = true
                
                self.lblImage2.isHidden = true
                self.lblImage2Details.isHidden = true
                
                if self.lblImage2.isHidden == true{
                    self.topconstraint.constant = 0
                }else if self.lblImage2.isHidden == false{
                    self.topconstraint.constant = 10
                }
                /* old 1
//                self.addDocumentView.image2View.isHidden = true
//                 self.addDocumentView.image1View.isHidden = false
//                 self.addDocumentView.btnUpload1.isHidden = false
//                 self.addDocumentView.btnUpload2.isHidden = true
                self.addDocumentView.viewDocumentDescription.isHidden = true
//
//
//                 self.addDocumentView.btnUpload1.frame = CGRect(x: self.addDocumentView.UploadButtonView.frame.origin.x-20, y: self.addDocumentView.btnUpload1.frame.origin.y, width: self.addDocumentView.btnUpload1.frame.width, height: self.addDocumentView.btnUpload1.frame.size.height)
//
//                  self.addDocumentView.image1View.frame = CGRect(x: self.addDocumentView.UploadImagesView.frame.origin.x-50, y: self.addDocumentView.image1View.frame.origin.y, width: self.addDocumentView.image1View.frame.width, height: self.addDocumentView.image1View.frame.size.height) old 1 */
                 
            }
            else if(item == "Resident Card"){
                
                self.addDocumentView.image2View.isHidden = false
                self.addDocumentView.image1View.isHidden = false
                self.addDocumentView.btnUpload1.isHidden = false
                self.addDocumentView.btnUpload2.isHidden = false
                
                self.addDocumentView.viewDocumentDescription.isHidden = true
                
                self.lblImage2.isHidden = false
                self.lblImage2Details.isHidden = false
                
                if self.lblImage2.isHidden == true{
                    self.topconstraint.constant = 2
                }else if self.lblImage2.isHidden == false{
                    self.topconstraint.constant = 10
                }
                
//                self.addDocumentView.btnUpload1.center.x = self.view.center.x-20
//                self.addDocumentView.image1View.center.x = self.view.center.x-50
                
//                self.addDocumentView.image2View.isHidden = true
//                self.addDocumentView.image1View.isHidden = false
//                self.addDocumentView.btnUpload1.isHidden = false
//                self.addDocumentView.btnUpload2.isHidden = true
//                self.addDocumentView.viewDocumentDescription.isHidden = true
                
            }else if (item == "Bank Statement"){
                
                self.lblImage2.isHidden = false
                self.lblImage2Details.isHidden = false
                
                self.addDocumentView.image2View.isHidden = true
                self.addDocumentView.image1View.isHidden = false
                self.addDocumentView.btnUpload1.isHidden = false
                self.addDocumentView.btnUpload2.isHidden = true
                
                self.addDocumentView.viewDocumentDescription.isHidden = true
                
                   if self.lblImage2.isHidden == true{
                        self.topconstraint.constant = 2
                   }else if self.lblImage2.isHidden == false{
                        self.topconstraint.constant = 10
                }
                
            }else if(item == "Utility Bill"){
                
                self.lblImage2.isHidden = false
                self.lblImage2Details.isHidden = false
                
                self.addDocumentView.image2View.isHidden = true
                self.addDocumentView.image1View.isHidden = false
                self.addDocumentView.btnUpload1.isHidden = false
                self.addDocumentView.btnUpload2.isHidden = true
                
                self.addDocumentView.viewDocumentDescription.isHidden = true
                
                  if self.lblImage2.isHidden == true{
                        self.topconstraint.constant = 2
                    }else if self.lblImage2.isHidden == false{
                        self.topconstraint.constant = 10
                    }
                
            }else if (item == "Credit Card Statement"){
                
                self.lblImage2.isHidden = false
                self.lblImage2Details.isHidden = false
                
                self.addDocumentView.image2View.isHidden = true
                self.addDocumentView.image1View.isHidden = false
                self.addDocumentView.btnUpload1.isHidden = false
                self.addDocumentView.btnUpload2.isHidden = true
                
                self.addDocumentView.viewDocumentDescription.isHidden = true
                
               if self.lblImage2.isHidden == true{
                    self.topconstraint.constant = 2
                }else if self.lblImage2.isHidden == false{
                    self.topconstraint.constant = 10
                }
                
            }else if (item == "Compliance Document"){
                
                self.lblImage2.isHidden = false
                self.lblImage2Details.isHidden = false
                
                self.addDocumentView.image2View.isHidden = false
                self.addDocumentView.image1View.isHidden = false
                self.addDocumentView.btnUpload1.isHidden = false
                self.addDocumentView.btnUpload2.isHidden = false
                
                self.addDocumentView.viewDocumentDescription.isHidden = false
                
                   if self.lblImage2.isHidden == true{
                        self.topconstraint.constant = 2
                    }else if self.lblImage2.isHidden == false{
                        self.topconstraint.constant = 10
                    }
                
            }else if(item == "Others"){
                
                self.lblImage2.isHidden = false
                self.lblImage2Details.isHidden = false
                
                self.addDocumentView.image2View.isHidden = false
                self.addDocumentView.image1View.isHidden = false
                self.addDocumentView.btnUpload1.isHidden = false
                self.addDocumentView.btnUpload2.isHidden = false
                
                self.addDocumentView.viewDocumentDescription.isHidden = true
                
                   if self.lblImage2.isHidden == true{
                        self.topconstraint.constant = 2
                    }else if self.lblImage2.isHidden == false{
                        self.topconstraint.constant = 10
                    }
                
//                self.addDocumentView.btnUpload1.center.x = self.view.center.x-20
//                 self.addDocumentView.image1View.center.x = self.view.center.x-50
                
//                self.addDocumentView.image2View.isHidden = true
//                self.addDocumentView.image1View.isHidden = false
//                self.addDocumentView.btnUpload1.isHidden = false
//                self.addDocumentView.btnUpload2.isHidden = true
//                self.addDocumentView.viewDocumentDescription.isHidden = false
                
            }else{
                
                self.lblImage2.isHidden = false
                self.lblImage2Details.isHidden = false
                
                self.addDocumentView.image2View.isHidden = false
                self.addDocumentView.image1View.isHidden = false
                self.addDocumentView.btnUpload1.isHidden = false
                self.addDocumentView.btnUpload2.isHidden = false
                self.addDocumentView.viewDocumentDescription.isHidden = true
                
                  if self.lblImage2.isHidden == true{
                        self.topconstraint.constant = 2
                    }else if self.lblImage2.isHidden == false{
                        self.topconstraint.constant = 10
                    }
                
            }
            
            self.addDocumentView.txtDocumentType.text = item
        }
        
        if self.UpdateRecipientDetail != nil{
            
            self.FillDocumentValues()
            
        }else{
            
            self.getDocumnetTypeList()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(SelectImage(_:)), name: ApiUrls.kDocNotification, object: nil)
        
        // Do any additional setup after loading the view.
    }

    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonActionSend))
        done.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.textView.inputAccessoryView = doneToolbar
        self.addDocumentView.txtDocumentDescription.inputAccessoryView = doneToolbar
        
    }
    @objc func doneButtonActionSend(){
           
           self.textView.resignFirstResponder()
        self.addDocumentView.txtDocumentDescription.resignFirstResponder()
           
       }
    
    func FillDocumentValues(){
        
        self.addDocumentView.lblTitle.text = "Update Documnet"
        
        self.addDocumentView.btnAddDocument.setTitle("UPDATE", for: .normal)
        
        if let DocumentType = self.UpdateRecipientDetail.DocTypeName{
            
            self.addDocumentView.txtDocumentType.text = DocumentType
            if DocumentType == "Passport"{
                self.addDocumentView.image2View.isHidden = true
                self.addDocumentView.image1View.isHidden = false
                self.addDocumentView.btnUpload1.isHidden = false
                self.addDocumentView.btnUpload2.isHidden = true
                self.addDocumentView.viewDocumentDescription.isHidden = true
                
                self.lblImage2.isHidden = true
                self.lblImage2Details.isHidden = true
                
                if self.lblImage2.isHidden == true{
                    self.topconstraint.constant = 0
                }else if self.lblImage2.isHidden == false{
                    self.topconstraint.constant = 10
                }
            }
        }
        
        if let DocumentNumber = self.UpdateRecipientDetail.DocNumber{
            
            self.addDocumentView.txtDocumentNumber.text = DocumentNumber
        }
        
        if let IssueDate = self.UpdateRecipientDetail.DocIssueDate?.components(separatedBy: "T")[0]{
            
            self.addDocumentView.txtIssueDate.text = IssueDate
        }
        
        if let ExpiryDate = self.UpdateRecipientDetail.DocExpireDate?.components(separatedBy: "T")[0]{
            
            self.addDocumentView.txtExpiryDate.text = ExpiryDate
        }
        
        if let Imgae1 = self.UpdateRecipientDetail.DocBodyURL{
  
//           self.addDocumentView.img1.sd_setImage(with: URL(string: Imgae1), completed: nil)
            SVProgressHUD.show()
            self.addDocumentView.img1.sd_setImage(with: URL(string: Imgae1), placeholderImage: #imageLiteral(resourceName: "ico_upload"), options: .continueInBackground, completed: nil)
            SVProgressHUD.dismiss()
        }
        
        if let image2 = self.UpdateRecipientDetail.DocBodyBackURL{
            SVProgressHUD.show()
            
            self.addDocumentView.img2.sd_setImage(with: URL(string: image2), placeholderImage: #imageLiteral(resourceName: "ico_upload"), options: .continueInBackground, progress: .none) { (img, error, cachType, url) in
                SVProgressHUD.dismiss()
            }
            
            
//            self.addDocumentView.img2.sd_setImage(with: URL(string: image2), placeholderImage: #imageLiteral(resourceName: "ico_upload"), options: .continueInBackground, completed: nil)
        }
        
        
    }

    //Button Back Click
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "bounds"{
            if let rect = (change?[NSKeyValueChangeKey.newKey] as? NSValue)?.cgRectValue {
                let margin: CGFloat = 8
                let xPos = rect.origin.x + margin
                let yPos = rect.origin.y + 54
                let width = rect.width - 2 * margin
                let height: CGFloat = 90

                textView.frame = CGRect.init(x: xPos, y: yPos, width: width, height: height)
            }
        }
    }
    
    
    
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
           if #available(iOS 10.0, *) {
               UIApplication.shared.open(URL)
           } else {
               UIApplication.shared.openURL(URL)
           }
           return false
       }
    
    func share_Func() {
           
           let appUrl = URL(string: "http://www.banglaremit.co.uk/get_the_app.php?click_id=Stm0850")
           let appUrlWithText = "\niOS APP: \(String(describing: appUrl!))"
           let textAndUrl = " I love using Bangla Remit to send money to my loved ones. Use the link below to get the best rates\(appUrlWithText)"
           let items: [Any] = ["\(textAndUrl))", URL(string: "http://www.banglaremit.co.uk")!]
           let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
           present(ac, animated: true)
           
       }
    
    //Button Add Document Click
    @IBAction func btnAddDocumentClick(_ sender: Any) {
        
        if(self.UpdateRecipientDetail != nil){
            
            let message = "If you want to change your document, please contact admin to click on link or add new document support@banglaremit.co.uk."
//            uc.errorSuccessAler("Alert", message, self)
            
            let attributedString = NSMutableAttributedString(string: "Want to learn iOS? You should visit the best source of free iOS tutorials!")
//              attributedString.addAttribute(.link, value: share_Func(), range: NSRange(location: 19, length: 55))
//
            attributedString.addAttribute(.link, value: "https://online.banglaremit.co.uk/en/faqs-app.html", range: NSRange(location: 19, length: 55))
            
            textView.attributedText = attributedString

            let alertController = UIAlertController(title: "Alert \n\n\n\n\n", message: nil, preferredStyle: .alert)

            let cancelAction = UIAlertAction.init(title: "Cancel", style: .default) { (action) in
                alertController.view.removeObserver(self, forKeyPath: "bounds")
            }
            alertController.addAction(cancelAction)

            let saveAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                let enteredText = self.textView.text
                alertController.view.removeObserver(self, forKeyPath: "bounds")
            }
            alertController.addAction(saveAction)

            alertController.view.addObserver(self, forKeyPath: "bounds", options: NSKeyValueObservingOptions.new, context: nil)
            textView.backgroundColor = UIColor.white
            textView.textContainerInset = UIEdgeInsets.init(top: 8, left: 5, bottom: 8, right: 5)
//            textView.text = message
            textView.isEditable = false
            alertController.view.addSubview(self.textView)
            self.present(alertController, animated: true, completion: nil)
            
        }else {
            
            if (addDocumentView.txtDocumentType.text!.isEmpty) {
                uc.errorSuccessAler("", "Please select document type", self)
            }else if (addDocumentView.txtDocumentNumber.text!.isEmpty) {
                uc.errorSuccessAler("", "Please enter document number", self)
            }else if (addDocumentView.txtIssueDate.text!.isEmpty) {
                uc.errorSuccessAler("", "Please select issue date", self)
            }else if(addDocumentView.txtExpiryDate.text!.isEmpty){
                uc.errorSuccessAler("", "Please select expiry date", self)
            }else if(addDocumentView.txtDocumentType.text == "Licence") ||  (addDocumentView.txtDocumentType.text == "ID-Card"){
                
                if (addDocumentView.img1.image == nil) && (addDocumentView.img2.image == nil){
                    uc.errorSuccessAler("", "Upload image document", self)
                }else {
                    self.AddDocument(ApiUrls.AddDocumentList)
                }
                
            }else {
                if (addDocumentView.img1.image == nil){
                    uc.errorSuccessAler("", "Upload image document", self)
                }else {
                    self.AddDocument(ApiUrls.AddDocumentList)
                }
            }
        }
   
    }
    
    //Button Document Type Click
    @IBAction func btnDocumentTypeClick(_ sender: Any) {
        
         self.DocumentTypeDropDown.show()
        
    }
    
    //Button Add Issue Date Click
    @IBAction func btnIssueDateClick(_ sender: Any) {
        self.view.endEditing(true)
         checkpicker = "Issue"
        issueDatePicker.show()
        
    }
    
    //Button Expiry Date Click
    @IBAction func btnExpiryDateClick(_ sender: Any) {
        self.view.endEditing(true)
         checkpicker = "Expiry"
        datePicker.show()
        
    }
    
    //Button Add Image 1 Click
    @IBAction func btnImage1Click(_ sender: Any) {
        
        if(UpdateRecipientDetail != nil)
        {
            if let imag1Url = UpdateRecipientDetail.DocBodyURL {
                
                let webViewController = WebViewViewController()
                webViewController.pageTitle = "Attachment"
                webViewController.webViewUrl = imag1Url
                self.navigationController?.pushViewController(webViewController, animated: true)
            }
            
        }else{
            let viewcontroller = AddDocumentPopUp()
            checkimage = "img1"
            viewcontroller.checkimage = checkimage
            viewcontroller.isDocument = true
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }

    }
    
    
    //Button Add Image 2 Click
    @IBAction func btnImage2Click(_ sender: Any) {
        
        if(UpdateRecipientDetail != nil)
        {
            if let imag2Url = UpdateRecipientDetail.DocBodyBackURL {
                
                let webViewController = WebViewViewController()
                webViewController.pageTitle = "Attachment"
                webViewController.webViewUrl = imag2Url
                self.navigationController?.pushViewController(webViewController, animated: true)
            }
            
        }else{
            let viewcontroller = AddDocumentPopUp()
            checkimage = "img2"
            viewcontroller.checkimage = checkimage
            viewcontroller.isDocument2 = true
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
  
    }
    
    
    //BUtton Upload Image 1 Click
    @IBAction func btnUploadImage1Click(_ sender: Any){
        
        if(UpdateRecipientDetail != nil)
        {
            if let imag1Url = UpdateRecipientDetail.DocBodyURL {
                
                let webViewController = WebViewViewController()
                webViewController.pageTitle = "Attachment"
                webViewController.webViewUrl = imag1Url
                self.navigationController?.pushViewController(webViewController, animated: true)
            }
        }else{
            let viewcontroller = AddDocumentPopUp()
            checkimage = "img1"
            viewcontroller.checkimage = checkimage
            viewcontroller.isDocument = true
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
        
    }
    
    //Button Upload Image 2 Click
    @IBAction func btnUploadImage2Click(_ sender:Any){
        
        if(UpdateRecipientDetail != nil)
        {
            if let imag2Url = UpdateRecipientDetail.DocBodyBackURL {
                
                let webViewController = WebViewViewController()
                webViewController.pageTitle = "Attachment"
                webViewController.webViewUrl = imag2Url
                self.navigationController?.pushViewController(webViewController, animated: true)
            }
            
        }else{
            let viewcontroller = AddDocumentPopUp()
            checkimage = "img2"
            viewcontroller.checkimage = checkimage
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    
        
        
    }
    
    
    @objc  func SelectImage(_ notification:NSNotification) {
        
        let userinfo =  notification.userInfo as? [String:Any]
        let image =  userinfo!["image"] as? UIImage
        checkimage = (userinfo!["checkimage"] as? String)!
        //let imageData = userinfo!["imageData"] as? NSData
        let imageURL = userinfo!["imgURL"] as? String
        
        
        if(self.checkimage == "img1"){

            self.addDocumentView.img1.image = image
            self.image1URL =  imageURL!

        }else if(self.checkimage == "img2"){

            self.addDocumentView.img2.image = image
            self.Image2URL =  imageURL!

        }
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
