//
//  DocumentViewImage.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 11/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit
import SVProgressHUD


class DocumentViewImage: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var imgV: UIImageView!
    var imgUrl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(imgUrl)
        
//        self.DocumentImage.sd_setImage(with: URL(string: self.DocumentURL), placeholderImage: UIImage(named: "ico_camera.png"))
        
        //self.imgV.sd_setImage(with: URL(string: self.imgUrl), placeholderImage: UIImage(named: "ico_camera.png"))
        
        
        SVProgressHUD.show()
        
//        self.imgV.sd_setHighlightedImage(with: URL(string: self.imgUrl), options: .continueInBackground) { (img, error, casheType, url) in
//
//            SVProgressHUD.dismiss()
//
//        }
        
        self.imgV.sd_setImage(with: URL(string: self.imgUrl), placeholderImage: UIImage(named: "ico_camera.png"), options: .lowPriority) { (img, error, cacheTpe, url) in
            
            SVProgressHUD.dismiss()
        }
        
        
        
        
    }


    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
