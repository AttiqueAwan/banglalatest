//
//  DocumentListView.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 07/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class DocumentListView: UIView {

    //Label Title
    @IBOutlet weak var lblTitle: UILabel!
    
    //Document Table View
    @IBOutlet weak var tblDocument: UITableView!
    
    //No Document View
    @IBOutlet weak var noDocumentView: UIView!
    
    //Button Back
    @IBOutlet weak var btnBack: UIButton!
    
    //Add More Document View when some Document Already Exist
    @IBOutlet weak var AddDocumentView: UIView!
    
    //Add More Document Button when some Document Exist
    @IBOutlet weak var btnAddDocument: UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
