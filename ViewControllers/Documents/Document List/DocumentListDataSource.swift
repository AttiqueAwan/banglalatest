//
//  DocumentListDataSource.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 07/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension DocumentListViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let updateVC = AddDocumentViewController()

        updateVC.UpdateRecipientDetail = self.DocumentList.DocumentList![indexPath.row]
        self.navigationController?.pushViewController(updateVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(self.DocumentList != nil){
            
            if(self.DocumentList.DocumentList != nil){
                
                return (self.DocumentList.DocumentList?.count)!
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.documentListView.tblDocument.dequeueReusableCell(withIdentifier: "DocumentListCell") as? DocumentListCell
        
        //apply shadow and corner
        cell?.layer.cornerRadius = 4
          let shadowPath2 = UIBezierPath(rect: cell!.bounds)
         cell?.layer.masksToBounds = false
         cell?.layer.shadowColor = UIColor.gray.cgColor
         cell?.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(1.0))
         cell?.layer.shadowOpacity = 0.2
         cell?.layer.shadowPath = shadowPath2.cgPath
         cell?.btnEyeTop.tag = indexPath.row
        cell?.btnEyeBottom.tag = indexPath.row
//        cell?.btnSend.addTarget(self, action: #selector(btnSend(_:)), for: .touchUpInside)
    
        let details = self.DocumentList.DocumentList![indexPath.row]

        
//        (_:secondParameter:)
        cell!.btnEyeTop.addTarget(self, action: #selector(btnTop), for: .touchUpInside)
        cell!.btnEyeBottom.addTarget(self, action: #selector(btnBottom), for: .touchUpInside)
        
        
//        cell?.btnEyeTop.addTarget(self, action: #selector(btnTop(_:url1:)), for: .touchUpInside)
        
//        cell?.lblDocumentType.text = details.DocType
        
        if let isCheck = details.IsVerified{
            if isCheck == true {
                cell?.lblIsVerified.isHidden = false
                cell?.lblIsVerified.textColor = .green
                cell?.lblIsVerified.text = "Verified"
            }else {
                cell?.lblIsVerified.isHidden = true
                cell?.lblIsVerified.textColor = .red
//                cell?.lblIsVerified.text = "Awaiting"
            }
        }
        
        
         let docType = details.DocType!
        
        switch docType {
        case "1":
           
            cell?.lblDocumentType.text = "Passport"
            cell?.imgDocType.image = UIImage(named: "passport_icon")
            
        case "2":
            cell?.lblDocumentType.text = "Driving License"
            cell?.imgDocType.image = UIImage(named: "driving_licence")
        case "3":
            cell?.lblDocumentType.text = "National ID Card"
            cell?.imgDocType.image = UIImage(named: "id_card")
        case "4":
            cell?.lblDocumentType.text = "Others"
            cell?.imgDocType.image = UIImage(named: "bank_statement")
            
        case "91":
            cell?.lblDocumentType.text = "Resident Card"
            cell?.imgDocType.image = UIImage(named: "resident_card")
            
        case "171":
            cell?.lblDocumentType.text = "Bank Statment"
            cell?.imgDocType.image = UIImage(named: "bank_statement")
           
        case "172":
             cell?.lblDocumentType.text = "Utility Bill"
            cell?.imgDocType.image = UIImage(named: "bank_statement")
        case "173":
            cell?.lblDocumentType.text = "Credit Card Statement"
            cell?.imgDocType.image = UIImage(named: "id_card")
        default:
           cell?.lblDocumentType.text = "Compliance Document"
            cell?.imgDocType.image = UIImage(named: "bank_statement")
        }

        
        cell?.lblIssueDate.text = "Issue: \((details.DocIssueDate?.components(separatedBy: "T")[0])!)"
        cell?.lblExpiryDate.text = "Expiry: \((details.DocExpireDate?.components(separatedBy: "T")[0])!)"
        
        return cell!
    }
    
    
    @objc func btnTop(sender: UIButton) {
        
        print("Btn 1 clicked at index # \(sender.tag)")
        let obj = DocumentViewImage()
        
        if let url = self.DocumentList.DocumentList![sender.tag].DocBodyURL{
                    print(url)
//                   self.performSegue(withIdentifier: "ShowImage2", sender: url)
            obj.imgUrl = url
             self.navigationController?.pushViewController(obj, animated: true)
             }
        
    }
    
        @objc func btnBottom(sender: UIButton) {
            
            print("Btn 1 clicked at index # \(sender.tag)")
            let obj = DocumentViewImage()
            
            if let url = self.DocumentList.DocumentList![sender.tag].DocBodyBackURL{
                        print(url)
    //            self.performSegue(withIdentifier: "ShowImage2", sender: url)
                obj.imgUrl = url
                 self.navigationController?.pushViewController(obj, animated: true)
                 }
            
        }
    
    
    
}

extension DocumentListViewController{
    
    //Get Document List
    func GetDocumentList() {
        
        let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.getDocumentList, params:parms , message: "Loading..."){result in
            
            self.DocumentList = DocumentListresult(JSONString:result)
            
            if  self.DocumentList?.myAppResult?.Code == 0 {
                
                self.documentListView.AddDocumentView.isHidden = false
                self.documentListView.tblDocument.isHidden = false
                self.documentListView.noDocumentView.isHidden = true
                self.documentListView.tblDocument.reloadData()
                
            }else if  self.DocumentList?.myAppResult?.Code == 102{
                
                self.documentListView.AddDocumentView.isHidden = true
                self.documentListView.tblDocument.isHidden = true
                self.documentListView.noDocumentView.isHidden = false
                
            }else{
                
                if( self.DocumentList?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( self.DocumentList?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
        
    }
    
}
