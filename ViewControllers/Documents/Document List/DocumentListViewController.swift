//
//  DocumentListViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 07/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class DocumentListViewController: UIViewController {

    
    @IBOutlet var documentListView: DocumentListView!
    
    let uc = UtilitySoftTechMedia()
    
    var DocumentList : DocumentListresult!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.documentListView.noDocumentView.isHidden = true
        
        self.documentListView.tblDocument.register(UINib(nibName: "DocumentListCell", bundle: nil), forCellReuseIdentifier: "DocumentListCell")
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.GetDocumentList()
    }
    
    //Button Back Click
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //Button Add Now Click
    @IBAction func btnAddNowClick(_ sender: Any) {
        
         self.navigationController?.pushViewController(AddDocumentViewController(), animated: true)
        
    }
    
    //Button Add New Document Click
    @IBAction func btnAddNewRecipientClick(_ sender: Any) {
        
        self.navigationController?.pushViewController(AddDocumentViewController(), animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
