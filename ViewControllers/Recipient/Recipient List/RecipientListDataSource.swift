//
//  RecipientListDataSource.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 07/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension RecipientListViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let UpdateVc = AddRecipientViewController()
        
        UpdateVc.UpdateRecipientDetails = self.RecipientList.RecipientList?[indexPath.row]
        
        UpdateVc.fromtrans = "update"
        self.navigationController?.pushViewController(UpdateVc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.RecipientList != nil{
            
            if let data = self.RecipientList.RecipientList {
                return (data.count)
            }else {return 0}
        }
        
        return 0
    }
    
//    @objc func subscribeTapped(_ sender: UIButton)
    @objc func btnSend(_ sender: UIButton) {
        
      
        let vc = CreateTransStep1ViewController()
        vc.isfromrecipentlist = true
        vc.RecipientDetails = self.RecipientList.RecipientList![(sender.tag)]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        index = indexPath.row
        let cell = self.recipientListView.tblRecipient.dequeueReusableCell(withIdentifier: "RecipientCell") as? RecipientCell
        
//        cell.subscribeButton.addTarget(self, action: #selector(subscribeTapped(_:)), for: .touchUpInside)
        cell?.btnSend.tag = indexPath.row
        cell?.btnSend.addTarget(self, action: #selector(btnSend(_:)), for: .touchUpInside)
       
        //self.RecipientDetails = self.RecipientList.RecipientList![(cell?.btnSend.tag)!]
        //apply shadow and corner
          cell?.layer.cornerRadius = 4
            let shadowPath2 = UIBezierPath(rect: cell!.bounds)
           cell?.layer.masksToBounds = false
           cell?.layer.shadowColor = UIColor.gray.cgColor
           cell?.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(2.0))
           cell?.layer.shadowOpacity = 0.5
           cell?.layer.shadowPath = shadowPath2.cgPath
        
        let details = self.RecipientList.RecipientList![indexPath.row]
        
        cell?.lblRecipientName.text = details.BeneName
  
        if(details.BenePayMethod == 9){
            
            cell?.lblPaymentMethod.text = "Cash Pick-Up"
            cell?.lblRecipientPhone.text = "Ph:\((details.BenePhone)!)"
            
        }else if(details.BenePayMethod == 10){
        
            cell?.lblPaymentMethod.text = "Bank"
            cell?.lblRecipientPhone.text = "A/C NO. \(String(describing: details.BeneAccountNumber!))"
            
        }else if(details.BenePayMethod == 751){
        
            cell?.lblPaymentMethod.text = "Mobile Wallet"
            cell?.lblRecipientPhone.text = "Ph. \(String(describing: details.BenePhone!))"
            
        }

        let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
        
        let AllCountries = CountryListresult(JSONString: result)
        
        
        for i in 0..<(AllCountries?.CountryList?.count)! {
            
            if(AllCountries?.CountryList![i].Iso3Code == details.BeneCountryIsoCode){
                
                if let Country = AllCountries?.CountryList![i].CountryName {
                    
                    cell?.imgRecipientCountry.image = UIImage(named: "\((Country)).png")
                     break
                }
               
            }
        }
       
        return cell!
    }
    
    
    
    
    
}

extension RecipientListViewController{
    
    
    func getRecipientList(){
        
        let authToken = uc.getAuthToken()
        
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetRecipientList, params:parms , message: "Loading..."){result in
            
            self.RecipientList = RecipientListresult(JSONString:result)
            
            if  self.RecipientList?.myAppResult?.Code == 0 {
                
                self.recipientListView.AddRecipientView.isHidden = false
                self.recipientListView.tblRecipient.isHidden = false
                self.recipientListView.noRecipientView.isHidden = true
                self.recipientListView.tblRecipient.reloadData()
                
            }else if  self.RecipientList?.myAppResult?.Code == 102{
                
                self.recipientListView.AddRecipientView.isHidden = true
                self.recipientListView.tblRecipient.isHidden = true
                self.recipientListView.noRecipientView.isHidden = false
                
            }else{
                
                if( self.RecipientList?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( self.RecipientList?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
        
    }
}
