//
//  RecipientListViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 07/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class RecipientListViewController: UIViewController {

    @IBOutlet var recipientListView: RecipientListView!
    
    let uc = UtilitySoftTechMedia()
    var RecipientDetails : RecipientListDetail!
    var RecipientList : RecipientListresult!
    var index : Int?
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.recipientListView.noRecipientView.isHidden = true
        
        self.recipientListView.tblRecipient.register(UINib(nibName: "RecipientCell", bundle: nil), forCellReuseIdentifier: "RecipientCell")
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        
         self.getRecipientList()
        
    }

    //Button Back Click
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
 
    //Button Add Now Click
    @IBAction func btnAddNowClick(_ sender: Any) {
        
        self.navigationController?.pushViewController(AddRecipientViewController(), animated: true)
        
    }
    
    
    //Button Add New Recipient Click
    @IBAction func btnAddNewRecipientClick(_ sender: Any) {
        
        self.navigationController?.pushViewController(AddRecipientViewController(), animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
