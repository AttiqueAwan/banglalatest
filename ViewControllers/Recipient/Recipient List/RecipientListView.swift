//
//  RecipientListView.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 07/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class RecipientListView: UIView {

    //Label Title
    @IBOutlet weak var lblTitle: UILabel!
    
    //Recipient Table View
    @IBOutlet weak var tblRecipient: UITableView!
    
    //No Recipient View
    @IBOutlet weak var noRecipientView: UIView!
    
    //Button Back
    @IBOutlet weak var btnBack: UIButton!
    
    //Add More Recipient View when some Recipient Already Exist
    @IBOutlet weak var AddRecipientView: UIView!
    
    //Add More Recipient Button when some Recipient Exist
    @IBOutlet weak var btnAddRecipient: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
