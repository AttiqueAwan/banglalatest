//
//  AddRecipientBankListCell.swift
//  BanglaRemitt
//
//  Created by Apple on 27/04/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit

class AddRecipientBankListCell: UITableViewCell {

    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
