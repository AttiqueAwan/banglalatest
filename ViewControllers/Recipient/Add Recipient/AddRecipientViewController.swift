//
//  AddRecipientViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 07/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import DropDown
import SVProgressHUD

class AddRecipientViewController: UIViewController {

    @IBOutlet var addRecipientView: AddReipientView!
    
    
    
    let beneCountryDropDown = DropDown()
    let beneReleationShipDropDown = DropDown()
    let bankDropDown = DropDown()
    let mobilenetwork = DropDown()
    
    let uc = UtilitySoftTechMedia()
    
    var UpdateRecipient : RecipientListresult!
    var ReceivingCountryList : CountryListresult!
    var CountryBankList   : BankListresult!
    var BankListDistrict : BankListresult2!
    var BankListBranch   : BankListresult3!
    var GetMobileNetworkList   : NetworkModelResult!
    
    var verify              : verifyAccount!
    var BeneDetails : RecipientListDetail!
    
    var UpdateRecipientDetails : RecipientListDetail!
    
    let txtFieldSetting = textFieldSetting()
    var Countries = [String]()
    var RelationShips = [String]()
    var BankName = [String]()
    var bankCommenDic = [[String:Any]]()
    var mobileNetwork = [String]()
    var transactionparms = [String:Any]()
    var MobilePaymentTypeId: String?
    var PaymentMethod = 10
    var SelectedCountryRow = 0
    var SelectedBankRow = 0
    var isFromTransaction = false
    
    //bankSuperview Outlets
    @IBOutlet var bankSuperView: UIView!
    @IBOutlet weak var addressTop: NSLayoutConstraint!
    
    var bankListApiCounter = 0
    var bankCodeStore : String!
    var districtCode : String!
    var bankNameStore = String()
    var routingNoStroe = String()
    var branchNameStore = String()
    var deliveryOptChoice : Int?
    var fromtrans: String?
    
    @IBOutlet weak var tblVBankSuperView: UITableView!
    @IBOutlet weak var AccountViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitleBankSuperView: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.addRecipientView.mobHeight.constant = 0
        self.addRecipientView.mobilieView.isHidden = true
        mobilenetwork.anchorView = self.addRecipientView.btnMobileNet
        mobilenetwork.dataSource = mobileNetwork
        //bankSuperview
//        self.tranactionListView.tblTranactions.register(UINib(nibName: "TransListCell", bundle: nil), forCellReuseIdentifier: "TranactionListCell")
        self.tblVBankSuperView.register(UINib(nibName: "AddRecipientBankListCell", bundle: nil), forCellReuseIdentifier: "AddRecipientBankListCell")
        
        
        

        self.GetReceivingCountryList()
        
        self.addDoneButtonOnKeyboard()
        
        RelationShips = ["Father","Mother","Husband","Wife","Son","Daughter","Cousin","Brother","Sister","Uncle","Aunt","Friend","Niece","Grandfather","Grandmother","Grnadson","Granddaughter","Nephew","Father-In-Law","Mother-In-Law","Sister-In-Law","Brother-In-Law","Finance","Business Associate","Service Provider","Employee","Self","Not Applicable"]
        
        self.addRecipientView.txtFirstName = self.txtFieldSetting.textFieldBaseLine(self.addRecipientView.txtFirstName, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addRecipientView.txtMiddleName = self.txtFieldSetting.textFieldBaseLine(self.addRecipientView.txtMiddleName, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addRecipientView.txtLastName = self.txtFieldSetting.textFieldBaseLine(self.addRecipientView.txtLastName, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addRecipientView.txtCountry = self.txtFieldSetting.textFieldBaseLine(self.addRecipientView.txtCountry, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addRecipientView.txtPhoneCode = self.txtFieldSetting.textFieldBaseLine(self.addRecipientView.txtPhoneCode, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addRecipientView.txtPhoneNumber = self.txtFieldSetting.textFieldBaseLine(self.addRecipientView.txtPhoneNumber, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addRecipientView.txtAddress = self.txtFieldSetting.textFieldBaseLine(self.addRecipientView.txtAddress, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addRecipientView.txtRealationShip = self.txtFieldSetting.textFieldBaseLine(self.addRecipientView.txtRealationShip, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addRecipientView.txtBankName = self.txtFieldSetting.textFieldBaseLine(self.addRecipientView.txtBankName, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addRecipientView.txtBranchName = self.txtFieldSetting.textFieldBaseLine(self.addRecipientView.txtBranchName, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addRecipientView.txtBranchCode = self.txtFieldSetting.textFieldBaseLine(self.addRecipientView.txtBranchCode, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addRecipientView.txtIBAN = self.txtFieldSetting.textFieldBaseLine(self.addRecipientView.txtIBAN, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addRecipientView.txtAccountCode = self.txtFieldSetting.textFieldBaseLine(self.addRecipientView.txtAccountCode, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        self.addRecipientView.txtAccountNumber = self.txtFieldSetting.textFieldBaseLine(self.addRecipientView.txtAccountNumber, #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        
        
        //setting Country  dropdown
        beneCountryDropDown.anchorView = self.addRecipientView.btnCountry
        beneCountryDropDown.dataSource = Countries
        
        
        beneCountryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.addRecipientView.txtCountry.text = self.Countries[index]
            
            self.addRecipientView.txtPhoneCode.text = "+\((self.ReceivingCountryList.CountryList![index].DialingCode)!)"
            self.addRecipientView.txtAccountCode.text = "+\((self.ReceivingCountryList.CountryList![index].DialingCode)!)"
            self.SelectedCountryRow = index
            self.GetCountryBankList()
            self.GetMobileNetwork()
        }
        
        mobilenetwork.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.addRecipientView.txtmobilie.text = self.mobileNetwork[index]
            
           
        }
        
        //Setting RelationShip DropDown
        beneReleationShipDropDown.anchorView = self.addRecipientView.btnRelationShip
        beneReleationShipDropDown.dataSource = RelationShips
        
        
        beneReleationShipDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.addRecipientView.txtRealationShip.text = self.RelationShips[index]
//            self.UpdateRecipientDetails.BeneRelationWithSender = self.RelationShips[index]
            
        }
        
        
        //Setting RelationShip DropDown
        bankDropDown.anchorView = self.addRecipientView.btnBankName
        bankDropDown.dataSource = BankName
        
        
        bankDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.addRecipientView.txtBankName.text = self.BankName[index]
            self.SelectedBankRow = index
            
        }
        
        if(self.UpdateRecipientDetails != nil){
            
            self.FillUpdateRecipients()
        }
        
        if(self.fromtrans == "dashboard" || self.fromtrans == "update"){
            self.addRecipientView.btnPaymentMethod.isUserInteractionEnabled = false
            self.addRecipientView.btnCountry.isUserInteractionEnabled = false
        }
        
        // Do any additional setup after loading the view.
        
    }

    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonActionSend))
        done.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.addRecipientView.txtPhoneNumber.inputAccessoryView = doneToolbar
        self.addRecipientView.txtBranchCode.inputAccessoryView = doneToolbar
        self.addRecipientView.txtAccountNumber.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonActionSend(){
        
        self.addRecipientView.txtPhoneNumber.resignFirstResponder()
        self.addRecipientView.txtBranchCode.resignFirstResponder()
        self.addRecipientView.txtAccountNumber.resignFirstResponder()
    }
    
    func FillUpdateRecipients() {
        
       
        self.addRecipientView.btnCountry.isUserInteractionEnabled = false
        self.addRecipientView.lblTitle.text = "UPDATE RECIPIENT" //"UpDate Recipient"
        self.addRecipientView.btnAddRecipient.setTitle("UPDATE", for: .normal) //("Update", for: .normal)
        
        if let FirstName = self.UpdateRecipientDetails.BeneFirstName{
            
            self.addRecipientView.txtFirstName.text = FirstName
            
        }
        
        if let MiddleName = self.UpdateRecipientDetails.BeneMiddleName{
            
            self.addRecipientView.txtMiddleName.text = MiddleName
            
        }
        
        if let LastName = self.UpdateRecipientDetails.BeneLastName{
            
            self.addRecipientView.txtLastName.text = LastName
            
        }
        
        if let BenePaymentMethod = self.UpdateRecipientDetails.BenePayMethod{
            
            
            self.PaymentMethod = BenePaymentMethod
            if(self.PaymentMethod == 10){
                self.deliveryOptChoice = 0
                
                self.addRecipientView.btnPaymentMethod.setTitle("", forSegmentAt: 0)
                self.addRecipientView.btnPaymentMethod.setTitle("Bank", forSegmentAt: 1)
                self.addRecipientView.btnPaymentMethod.setTitle("", forSegmentAt: 2)
                self.addRecipientView.btnPaymentMethod.selectedSegmentIndex = 1
                
            }else if(self.PaymentMethod == 9){
                self.deliveryOptChoice = 1
                
                if isFromTransaction {
                    self.addRecipientView.btnPaymentMethod.setTitle("", forSegmentAt: 0)
                    self.addRecipientView.btnPaymentMethod.setTitle("Cash Pick_Up", forSegmentAt: 1)
                    self.addRecipientView.btnPaymentMethod.setTitle("", forSegmentAt: 2)
                    self.addRecipientView.btnPaymentMethod.selectedSegmentIndex = 1
                    //self.addRecipientView.btnPaymentMethod.selectedSegmentIndex = 0
                }else {
                    self.addRecipientView.btnPaymentMethod.setTitle("", forSegmentAt: 0)
                    self.addRecipientView.btnPaymentMethod.setTitle("Cash Pick_Up", forSegmentAt: 1)
                    self.addRecipientView.btnPaymentMethod.setTitle("", forSegmentAt: 2)
                    self.addRecipientView.btnPaymentMethod.selectedSegmentIndex = 1
                    self.addRecipientView.BankView.isHidden = true
                    self.addRecipientView.bankViewHeight.constant = 0
                }
            }
            else if(self.PaymentMethod == 751){
                self.deliveryOptChoice = 2
                
                if isFromTransaction {
                    
                    self.addRecipientView.btnPaymentMethod.setTitle("", forSegmentAt: 0)
                    self.addRecipientView.btnPaymentMethod.setTitle("Mobile Wallet", forSegmentAt: 1)
                    self.addRecipientView.btnPaymentMethod.setTitle("", forSegmentAt: 2)
                    self.addRecipientView.btnPaymentMethod.selectedSegmentIndex = 1
                    self.addRecipientView.btnPaymentMethod.isUserInteractionEnabled = false
                    self.addRecipientView.mobilieView.isHidden = false
                    self.addRecipientView.txtmobilie.text = self.UpdateRecipientDetails.BenemobileCompany
                    self.addRecipientView.mobHeight.constant = 40
                    self.MobilePaymentTypeId = self.UpdateRecipientDetails.BeneMobilePaymentTypeId
                    self.addRecipientView.BankView.isHidden = true
                    self.addRecipientView.bankViewHeight.constant = 0
                }else {
                    self.addRecipientView.btnPaymentMethod.setTitle("", forSegmentAt: 0)
                    self.addRecipientView.btnPaymentMethod.setTitle("Mobile Wallet", forSegmentAt: 1)
                    self.addRecipientView.btnPaymentMethod.setTitle("", forSegmentAt: 2)
                    self.addRecipientView.mobilieView.isHidden = false
                    self.addRecipientView.txtmobilie.text = self.UpdateRecipientDetails.BenemobileCompany
                    self.addRecipientView.mobHeight.constant = 40
                    self.MobilePaymentTypeId = self.UpdateRecipientDetails.BeneMobilePaymentTypeId
                    self.addRecipientView.btnPaymentMethod.selectedSegmentIndex = 1
                    self.addRecipientView.BankView.isHidden = true
                    self.addRecipientView.bankViewHeight.constant = 0
                }
            }
        }
        
        if let CountryIsoCode = self.UpdateRecipientDetails.BeneCountryIsoCode{
            
            
            let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
            
            let AllCountries = CountryListresult(JSONString: result)
            
            
            
            for i in 0..<(AllCountries?.CountryList?.count)! {
                
                if(AllCountries?.CountryList![i].Iso3Code == CountryIsoCode){
                    
                    if let Country = AllCountries?.CountryList![i].CountryName {
                       self.addRecipientView.txtCountry.text = Country
                       self.addRecipientView.txtPhoneCode.text = "+\((AllCountries?.CountryList![i].DialingCode)!)"
                       self.addRecipientView.txtAccountCode.text = "+\((AllCountries?.CountryList![i].DialingCode)!)"
                       break
                    }
                }
            }
        }
        
        if let BenePhone = self.UpdateRecipientDetails.BenePhone{
            
//             self.txtPhoneNo.text = String(PhoneNo.dropFirst((self.txtCountryCode.text?.count)!))
            self.addRecipientView.txtPhoneNumber.text = String(BenePhone.dropFirst(self.addRecipientView.txtPhoneCode.text!.count))
//            self.addRecipientView.txtPhoneNumber.text = BenePhone
            
        }
        
        if let BeneAccountNo = self.UpdateRecipientDetails.MobileAccountNumber{
            
//          self.txtPhoneNo.text = String(PhoneNo.dropFirst((self.txtCountryCode.text?.count)!))
            self.addRecipientView.txtAccountNumber.text = String(BeneAccountNo.dropFirst(self.addRecipientView.txtAccountCode.text!.count))
//            self.addRecipientView.txtPhoneNumber.text = BenePhone
                    
        }
        
        
        
        if let BeneAddress = self.UpdateRecipientDetails.BeneAddress{
            
            self.addRecipientView.txtAddress.text = BeneAddress
            
        }
        
        if let BeneBankName = self.UpdateRecipientDetails.BeneBankName{
            
            self.addRecipientView.txtBankName.text = BeneBankName
            
        }
        
        if let BeneBranchName = self.UpdateRecipientDetails.BeneBranchName{
            
            self.addRecipientView.txtBranchName.text = BeneBranchName
            
        }
        
        if let BeneBankBranchCode = self.UpdateRecipientDetails.BeneBankBranchCode{
            
            self.addRecipientView.txtBranchCode.text = "\((BeneBankBranchCode))"
            
        }
        
        if let BeneIBAN = self.UpdateRecipientDetails.BeneAccountNumber{
            
            self.addRecipientView.txtIBAN.text = BeneIBAN
            
        }
        
        if let BeneRelationWithSender = self.UpdateRecipientDetails.BeneRelationWithSender{
            
            self.addRecipientView.txtRealationShip.text = BeneRelationWithSender
           
            
        }
        
    }

    //Button Back Click
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnMobileNetwork(_ sender: Any) {
        self.mobilenetwork.show()
    }
    
    //button Payment Method Click
    @IBAction func btnPaymentMethodClick(_ sender: Any) {
        
        self.AccountViewHeight.constant = 0
        
        if self.addRecipientView.btnPaymentMethod.selectedSegmentIndex == 0{
            self.deliveryOptChoice = 0
            self.addRecipientView.mobHeight.constant = 0
            self.addRecipientView.bankViewHeight.constant = 95
            self.addressTop.constant = 0
            self.addRecipientView.BankView.isHidden = false
            self.addRecipientView.mobilieView.isHidden = true
            self.addRecipientView.mobileAccountView.isHidden = true
            self.PaymentMethod = 10
//            self.GetCountryBankList()
            
        }else if(self.addRecipientView.btnPaymentMethod.selectedSegmentIndex == 1){
            self.deliveryOptChoice = 1
            self.addRecipientView.mobHeight.constant = 0
            self.addRecipientView.bankViewHeight.constant = 0
            self.addressTop.constant = 0
            self.addRecipientView.BankView.isHidden = true
            self.addRecipientView.mobilieView.isHidden = true
            self.addRecipientView.mobileAccountView.isHidden = true
            self.PaymentMethod = 9
        }else if(self.addRecipientView.btnPaymentMethod.selectedSegmentIndex == 2){
            self.deliveryOptChoice = 2
            self.addRecipientView.bankViewHeight.constant = 0
            self.addressTop.constant = 15
            self.addRecipientView.mobHeight.constant = 40
            self.AccountViewHeight.constant = 40
            self.addRecipientView.BankView.isHidden = true
            self.addRecipientView.mobilieView.isHidden = false
            self.addRecipientView.mobileAccountView.isHidden = false
            self.PaymentMethod = 751
        }
        
    }
    
    //Button Country Click
    @IBAction func btnCountryClick(_ sender: Any) {
        
        self.beneCountryDropDown.show()
    }
    
    //Button Bank Name Click
    @IBAction func btnBankNameClick(_ sender: Any) {
        
//        self.bankDropDown.show()
//        editAlertView.frame = self.view.frame
//        self.createTransStep3View.addSubview(editAlertView)
        
        bankSuperView.frame = self.view.frame
        self.addRecipientView.addSubview(bankSuperView)
        self.lblTitleBankSuperView.text = "SELECT BANK"
        self.GetCountryBankList()
    }
    
    //Button RelationShip Click
    @IBAction func btnRelationshipClick(_ sender: Any) {
        
        self.beneReleationShipDropDown.show()
    }
    
    //Button Add Recipient Click
    @IBAction func btnAddRecipientClick(_ sender: Any) {
        
        if(self.addRecipientView.txtFirstName.text?.isEmpty)!{
            
            uc.errorSuccessAler("Alert", "First Name required!", self)
        }else if(self.addRecipientView.txtLastName.text?.isEmpty)!{
            
            uc.errorSuccessAler("Alert", "Last Name required!", self)
        
        }else if(self.addRecipientView.txtCountry.text?.isEmpty)!{
            
            uc.errorSuccessAler("Alert", "Country required!", self)
            
        }else if(self.addRecipientView.txtPhoneCode.text?.isEmpty)!{
            
            uc.errorSuccessAler("Alert", "Phone Code required!", self)
            
        }else if(self.addRecipientView.txtPhoneNumber.text?.isEmpty)!{
            
            uc.errorSuccessAler("Alert", "Phone Number required!", self)
            
        }else{
            
            if(self.deliveryOptChoice == 0){
                
                if(self.addRecipientView.txtBankName.text?.isEmpty)!{
                    
                    uc.errorSuccessAler("Alert", "Bank Name required!", self)
                    
                }else if(self.addRecipientView.txtBranchName.text?.isEmpty)!{
                    
                    uc.errorSuccessAler("Alert", "Branch Name required!", self)
                    
                }else if(self.addRecipientView.txtBranchCode.text?.isEmpty)!{
                    
                    uc.errorSuccessAler("Alert", "Branch Code required!", self)
                    
                }else if(self.addRecipientView.txtIBAN.text?.isEmpty)!{
                    
                    uc.errorSuccessAler("Alert", "Account No  required!", self)
                    
                }else if(self.addRecipientView.txtRealationShip.text?.isEmpty)!{
                    
                    uc.errorSuccessAler("Alert", "RelationShip With Sender required!", self)
                    
                }else{
                    
                   self.verification()
                }
                
            }else if(self.deliveryOptChoice == 1){
                
                if(self.addRecipientView.txtRealationShip.text?.isEmpty)!{
                    
                    uc.errorSuccessAler("Alert", "RelationShip With Sender required!", self)
                    
                }else{
                    
                    self.verification()
                }
            }
            else if(self.deliveryOptChoice == 2){
                if(self.addRecipientView.txtmobilie.text!.isEmpty){
                    uc.errorSuccessAler("Alert", "Select Mobile Company Name!", self)
                }else if self.addRecipientView.txtAccountCode.text!.isEmpty{
                    uc.errorSuccessAler("Alert", "Select Recipent Country Name!", self)
                }else if addRecipientView.txtAccountNumber.text!.isEmpty{
                    uc.errorSuccessAler("Alert", "Enter Mobile Account Number!", self)
                }else{
                    self.verification()
                }
                
                
                
            }
        }
    }
    
    
    
    func verification(){
        let authToken = uc.getAuthToken()
        var index = self.bankDropDown.indexForSelectedRow
        var bankCode = "0"
        if(index == nil)
        {
           index =  self.SelectedBankRow
            if(self.CountryBankList != nil)
            {
                bankCode = self.CountryBankList.BankList![index!].BankCode!
            }
            
        }
        var countryIndex = self.beneCountryDropDown.indexForSelectedRow
        if(countryIndex == nil )
        {
            countryIndex = self.SelectedCountryRow
        }
                
        var BeneID = 0
        
        if(self.UpdateRecipientDetails != nil){
            
            if let Beneid = self.UpdateRecipientDetails.BeneID{
                
                BeneID = Beneid
            }
        }
        
        var parms =  ["Token":authToken!.authToken!,
                             "ID":authToken!.userId!,
                             "BeneID":BeneID,
                             "PayerId": self.getPayerId(),
                             "DeliveryMethod":self.getPaymentMehtod(),
                             "BenePayMethod":self.PaymentMethod,
                             "BeneCountryIsoCode":"\((self.ReceivingCountryList.CountryList![countryIndex!].Iso3Code)!)"
                         
            
                             
                             ] as [String:Any]
        
        if(self.deliveryOptChoice == 0){
                   
                   parms["BeneBankName"] = "\((self.addRecipientView.txtBankName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)"
                   parms["BeneBranchName"] = "\((self.addRecipientView.txtBranchName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)"
                   parms["AccountNo"] = "\((self.addRecipientView.txtIBAN.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)"
                   
                   parms["BankId"] = bankCode
                   parms["BeneBankBranchCode"] = "\((self.addRecipientView.txtBranchCode.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)"
                 
               }
        if (self.deliveryOptChoice == 2){
                   parms["Network"] = "\(self.addRecipientView.txtmobilie.text!)"
                   parms["PhoneNo"] = "\((self.addRecipientView.txtPhoneCode.text)!)\((self.addRecipientView.txtPhoneNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)"
            parms["MobileAccountNumber"] = "\((self.addRecipientView.txtAccountCode.text)!)\((self.addRecipientView.txtAccountNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)"
                   
                   
               }
        
        uc.webServicePosthttp(urlString: ApiUrls.validateaccount, params:parms , message: "Loading..."){result in
                   
            self.verify = verifyAccount(JSONString:result)
               
                   if  self.verify?.myAppResult?.Code == 0 {
                       
                       //Here .....................
                       //modify

                     if(self.UpdateRecipientDetails != nil){
                         
                         self.AddRecipient(ApiUrls.UpdateRecipient)
                         
                     }else{
                         self.AddRecipient(ApiUrls.AddRecipient)
                     }
                       
                   }else if  self.verify?.myAppResult?.Code == 101 {
                       
                       
                       
                   }else{
                       
                       if( self.verify?.myAppResult?.Message == nil){
                           
                           self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                           
                       }else{
                           
                           self.uc.errorSuccessAler("Error", ( self.verify?.myAppResult?.Message)!, self)
                       }
                       
                   }
                   
               }
    }
    
    //bankSuperView
@IBAction func btnBackBankSuperView(_ sender: UIButton) {
//        self.editAlertView.removeFromSuperview()
    
    
    if self.bankListApiCounter == 3 {
        self.bankListApiCounter = 2
        if self.BankListDistrict?.myAppResult?.Code == 0 {
            self.bankListApiCounter = 2
            self.lblTitleBankSuperView.text = "SELECT DISTRICT"
            self.bankCommenDic.removeAll()
            var myDic = [String:Any]()
            for item in self.BankListDistrict.BankListDistrict! {
                myDic["Code"] = item.DistrictCode!//item.BankCode!
                myDic["Name"] = item.DistrictName
                self.bankCommenDic.append(myDic)
            }
            
            self.tblVBankSuperView.reloadData()
        }
        
    }else if self.bankListApiCounter == 2 {
        self.bankListApiCounter = 1
        
        if self.CountryBankList?.myAppResult?.Code == 0 {
            self.lblTitleBankSuperView.text = "SELECT BANK"
            self.bankListApiCounter = 1
            
            self.bankCommenDic.removeAll()
            var myDic = [String:Any]()
            
            for item in self.CountryBankList.BankList! {
                
                
                myDic["Code"] = item.BankCode!
                myDic["Name"] = item.BankName!
                self.bankCommenDic.append(myDic)
                
            }
            
            DispatchQueue.main.async {
                self.tblVBankSuperView.reloadData()
            }
            

            //Old 1
            for i in 0 ..< (self.CountryBankList?.BankList!.count)!{
                self.BankName.append((self.CountryBankList?.BankList![i].BankName)!)
                if(self.UpdateRecipientDetails != nil){

                    if (self.addRecipientView.txtBankName.text == self.CountryBankList?.BankList![i].BankName){
                        self.SelectedBankRow = i
                         break
                    }
                }
            }
            
            self.bankDropDown.dataSource = self.BankName
            self.bankDropDown.reloadAllComponents()
            //Old 1
            
            
        }
        
        
    }else if self.bankListApiCounter == 1 {
        
        self.bankListApiCounter = 0
        self.bankSuperView.removeFromSuperview()
        
    }
    
    }
}
