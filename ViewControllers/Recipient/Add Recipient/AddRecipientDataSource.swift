//
//  AddRecipientDataSource.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 07/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension AddRecipientViewController{
    
    //Receiving CountryList
    func GetReceivingCountryList(){
        
        
        let parms =  ["Type":"2"]as [String : Any]
        
      
        uc.webServicePosthttp(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading..."){result in
            
            let CountryListingResponce = CountryListresult(JSONString:result)
            
            if CountryListingResponce?.myAppResult?.Code == 0 {
                
                self.ReceivingCountryList =  CountryListresult(JSONString:result)
                
                for i in 0 ..< (self.ReceivingCountryList?.CountryList!.count)!{
                    
                    self.Countries.append((self.ReceivingCountryList?.CountryList![i].CountryName)!)
                    
                    if(self.UpdateRecipientDetails != nil){
                        
                        if (self.addRecipientView.txtCountry.text == self.ReceivingCountryList?.CountryList![i].CountryName){
                            
                            self.SelectedCountryRow = i
                            self.GetMobileNetworkwithIsoCode((self.ReceivingCountryList.CountryList![i].Iso3Code)!)
                             self.GetCountryBankList()
                            break
                        }
                        
                    }
                }
                
                self.beneCountryDropDown.dataSource = self.Countries
                self.beneCountryDropDown.reloadAllComponents()
           
            }else if CountryListingResponce?.myAppResult?.Code == 101 {
                
                
            }else{
                
                if(CountryListingResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", (CountryListingResponce?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
    }
    
    //Mobile Network
      func GetMobileNetwork(){
        
           let index = self.beneCountryDropDown.indexForSelectedRow
        
        let parms =  ["CountryIso3Code": self.ReceivingCountryList.CountryList![index! ?? self.SelectedCountryRow].Iso3Code! ,"PaymentMethod":"Wallet"]as [String : Any]
          
        uc.webServicePosthttp(urlString: ApiUrls.mobileNetworkapi, params:parms , message: "Loading..."){result in
              
              let networkModelrespo = NetworkModelResult(JSONString:result)
              
              if networkModelrespo?.myAppResult?.Code == 0 {
                  
                  self.GetMobileNetworkList =  NetworkModelResult(JSONString:result)
                  
                for i in 0 ..< (self.GetMobileNetworkList?.networkModel!.count)!{
                      
                    //self.Countries.append((self.ReceivingCountryList?.CountryList![i].CountryName)!)
                    self.mobileNetwork.append((self.GetMobileNetworkList?.networkModel![i].MobileCompanyName)!)
                    self.MobilePaymentTypeId = (self.GetMobileNetworkList?.networkModel![i].MobilePaymentTypeId)!
                      
                  }
                  
                  self.mobilenetwork.dataSource = self.mobileNetwork
                  self.mobilenetwork.reloadAllComponents()
             
              }else if networkModelrespo?.myAppResult?.Code == 101 {
                  
                  
              }else{
                  
                  if(networkModelrespo?.myAppResult?.Message == nil){
                      
                      self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                      
                  }else{
                      
                      self.uc.errorSuccessAler("Error", (networkModelrespo?.myAppResult?.Message)!, self)
                  }
                  
              }
              
          }
          
      }
    func GetMobileNetworkwithIsoCode(_ Iso: String?){
           
              //let index = self.beneCountryDropDown.indexForSelectedRow
           
           let parms =  ["CountryIso3Code": Iso! ,"PaymentMethod":"Wallet"]as [String : Any]
             
           uc.webServicePosthttp(urlString: ApiUrls.mobileNetworkapi, params:parms , message: "Loading..."){result in
                 
                 let networkModelrespo = NetworkModelResult(JSONString:result)
                 
                 if networkModelrespo?.myAppResult?.Code == 0 {
                     
                     self.GetMobileNetworkList =  NetworkModelResult(JSONString:result)
                     
                   for i in 0 ..< (self.GetMobileNetworkList?.networkModel!.count)!{
                         
                    self.mobileNetwork.append((self.GetMobileNetworkList?.networkModel![i].MobileCompanyName)!)
                        if(self.GetMobileNetworkList?.networkModel![i].MobileCompanyName)! == self.UpdateRecipientDetails.BenemobileCompany{
                            self.MobilePaymentTypeId = (self.GetMobileNetworkList?.networkModel![i].MobilePaymentTypeId)!
                        }
                    }
                    if self.MobilePaymentTypeId == nil{
                        self.MobilePaymentTypeId = (self.GetMobileNetworkList?.networkModel![0].MobilePaymentTypeId)!
                        self.addRecipientView.txtmobilie.text = (self.GetMobileNetworkList?.networkModel![0].MobileCompanyName)!
                    }
                     
                    self.mobilenetwork.dataSource = self.mobileNetwork
                    self.mobilenetwork.reloadAllComponents()
                
                 }else if networkModelrespo?.myAppResult?.Code == 101 {
                     
                     
                 }else{
                     
                     if(networkModelrespo?.myAppResult?.Message == nil){
                         
                         self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                         
                     }else{
                         
                         self.uc.errorSuccessAler("Error", (networkModelrespo?.myAppResult?.Message)!, self)
                     }
                     
                 }
                 
             }
             
         }
    
    
    func getPayerId() -> Int{
       
        if (self.deliveryOptChoice == 1){
            return 2
        }else if (self.deliveryOptChoice == 0){
            return 3
        }else if (self.deliveryOptChoice == 2){
            return 10
        }
        return 0
    }
    func getPaymentMehtod() -> String{
       
        if (self.deliveryOptChoice == 0){
            return "Bank"
        }else if (self.deliveryOptChoice == 1){
            return "Cash"
        }else if (self.deliveryOptChoice == 2){
            return "Wallet"
        }
        return ""
    }
    
    
    func AddRecipient(_ apiURL:String) {
        
        var index = self.bankDropDown.indexForSelectedRow
        var bankCode = "0"
        if(index == nil)
        {
           index =  self.SelectedBankRow
            if(self.CountryBankList != nil)
            {
                bankCode = self.CountryBankList.BankList![index!].BankCode!
            }
            
        }
        var countryIndex = self.beneCountryDropDown.indexForSelectedRow
        if(countryIndex == nil )
        {
            countryIndex = self.SelectedCountryRow
        }
        let authToken = uc.getAuthToken()
        
        var BeneID = 0
        
        if(self.UpdateRecipientDetails != nil){
            
            if let Beneid = self.UpdateRecipientDetails.BeneID{
                
                BeneID = Beneid
            }
        }
        
        var parms =  ["Token":authToken!.authToken!,
                      "UserID":"",
                      "ID":authToken!.userId!,
                      "BeneID":BeneID,
                      "PayerId": self.getPayerId(),
                      "BenePayMethod":self.PaymentMethod,
                      "BeneName":"\((self.addRecipientView.txtFirstName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!) \((self.addRecipientView.txtMiddleName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!) \((self.addRecipientView.txtLastName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)",
                      "BeneFirstName":"\((self.addRecipientView.txtFirstName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)",
                        "BeneMiddleName":"\((self.addRecipientView.txtMiddleName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)",
                      "BeneLastName":"\((self.addRecipientView.txtLastName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)",
                      "BeneAddress":"\((self.addRecipientView.txtAddress.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)",
                      "BeneCity":"",
                      "BenePostCode":"",
                      "BeneCountryIsoCode":"\((self.ReceivingCountryList.CountryList![countryIndex!].Iso3Code)!)",
                      "BenePhone":"\((self.addRecipientView.txtPhoneCode.text)!)\((self.addRecipientView.txtPhoneNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)",
                      "BeneRelationWithSender":"\((self.addRecipientView.txtRealationShip.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)"] as [String:Any]
        
        if(self.PaymentMethod == 10){
            
            parms["BeneBankName"] = "\((self.addRecipientView.txtBankName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)"
            parms["BeneBranchName"] = "\((self.addRecipientView.txtBranchName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)"
            parms["BeneAccountNumber"] = "\((self.addRecipientView.txtIBAN.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)"
            parms["BeneIBAN"] = ""
            parms["BeneBankCode"] = bankCode
            parms["BeneBankBranchCode"] = "\((self.addRecipientView.txtBranchCode.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)"
          
        }
        if (self.PaymentMethod == 751){
            parms["MobileCompanyName"] = "\(self.addRecipientView.txtmobilie.text!)"
            parms["MobilePaymentTypeId"] = "\(self.MobilePaymentTypeId!)"
            parms["MobileAccountNumber"] = "\((self.addRecipientView.txtAccountCode.text)!)\((self.addRecipientView.txtAccountNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)"
        }
        
        uc.webServicePosthttp(urlString: apiURL, params:parms , message: "Loading..."){result in
            
            self.UpdateRecipient = RecipientListresult(JSONString:result)
        
            
            if  self.UpdateRecipient?.myAppResult?.Code == 0 {
                
                //Here .....................
                //modify
                if self.isFromTransaction{
                    self.transactionparms["MobileCompanyName"] = "\(self.addRecipientView.txtmobilie.text!)"
                   
                    let step3 = CreateTransStep3ViewController()
                    step3.parms = self.transactionparms
                    step3.BeneDetails = self.BeneDetails
                    self.navigationController?.pushViewController(step3, animated: true)
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
                

              
                
            }else if  self.UpdateRecipient?.myAppResult?.Code == 101 {
                
                
                
            }else{
                
                if( self.UpdateRecipient?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( self.UpdateRecipient?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
    }
    
    
    //Bank list 1
    func GetCountryBankList() {
        
        let index = self.beneCountryDropDown.indexForSelectedRow
        let authToken = uc.getAuthToken()
        
        let parms =  ["Token":authToken!.authToken!,
                      "ID":authToken!.userId!,
                      "ReceivingCountryIso3Code":self.ReceivingCountryList.CountryList![index ?? self.SelectedCountryRow].Iso3Code!]as [String : Any]
        print(parms)
        
        
        uc.webServicePosthttp(urlString: ApiUrls.CountryBankList, params:parms , message: "Loading..."){result in
            
            self.CountryBankList = BankListresult(JSONString:result)
            
            if self.CountryBankList?.myAppResult?.Code == 0 {
                self.lblTitleBankSuperView.text = "SELECT BANK"
                self.bankListApiCounter = 1
                
                self.bankCommenDic.removeAll()
                var myDic = [String:Any]()
                
                for item in self.CountryBankList.BankList! {
                    
                    
                    myDic["Code"] = item.BankCode!
                    myDic["Name"] = item.BankName!
                    self.bankCommenDic.append(myDic)
                    
                }
                
                DispatchQueue.main.async {
                    self.tblVBankSuperView.reloadData()
                }
                

                //Old 1
                for i in 0 ..< (self.CountryBankList?.BankList!.count)!{
                    self.BankName.append((self.CountryBankList?.BankList![i].BankName)!)
                    if(self.UpdateRecipientDetails != nil){

                        if (self.addRecipientView.txtBankName.text == self.CountryBankList?.BankList![i].BankName){

                            self.SelectedBankRow = i

                             break
                        }
                    }
                }
                
                self.bankDropDown.dataSource = self.BankName
                self.bankDropDown.reloadAllComponents()
                //Old 1
                
                
            }else if self.CountryBankList?.myAppResult?.Code == 101 {
                
                
            }else{
                
                if(self.CountryBankList?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", (self.CountryBankList?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
        
    }
    
    //Bank list 1
    func getBankListDistrict(bankCode:String) {
            
            let index = self.beneCountryDropDown.indexForSelectedRow
            let authToken = uc.getAuthToken()
            
            let parms =  ["Token":authToken!.authToken!,
                          "ID":authToken!.userId!,
                          "BankCode":bankCode,
                          "ReceivingCountryIso3Code":self.ReceivingCountryList.CountryList![index ?? self.SelectedCountryRow].Iso3Code!]as [String : Any]
            print(parms)
            
    //        http://api.banglaremit.co.uk/api/utils/countrybanklist
            
    //        ["Token": "OTTiYXBV220s8YXf7Uau3hmCD0iDcnK7rSfTPuF+yuE=", "ID": "92", "ReceivingCountryIso3Code": "BGD"]
            
            
            uc.webServicePosthttp(urlString: ApiUrls.bankdistrictlist, params:parms , message: "Loading..."){result in
                
                self.BankListDistrict = BankListresult2(JSONString:result)
                
                if self.BankListDistrict?.myAppResult?.Code == 0 {
                    self.bankListApiCounter = 2
                    self.lblTitleBankSuperView.text = "SELECT DISTRICT"
                    self.bankCommenDic.removeAll()
                    var myDic = [String:Any]()
                    
                    for item in self.BankListDistrict.BankListDistrict! {
                        
                        
                        myDic["Code"] = item.DistrictCode!//item.BankCode!
                        myDic["Name"] = item.DistrictName
                        self.bankCommenDic.append(myDic)
                        
                    }
                    
                    self.tblVBankSuperView.reloadData()
                
                }else if self.BankListDistrict?.myAppResult?.Code == 101 {
                    
                    
                }else{
                    
                    if(self.BankListDistrict?.myAppResult?.Message == nil){
                        
                        self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                        
                    }else{
                        
                        self.uc.errorSuccessAler("Error", (self.CountryBankList?.myAppResult?.Message)!, self)
                    }
                    
                }
                
            }
            
            
        }
    
    //branch
    //Bank list 3
    func getBankListBranch(bankCode:String,districtCode:String) {
            
            let index = self.beneCountryDropDown.indexForSelectedRow
            let authToken = uc.getAuthToken()
            
            let parms =  ["Token":authToken!.authToken!,
                          "ID":authToken!.userId!,
                          "BankCode":bankCode,
                          "DistrictCode":districtCode,
                          "ReceivingCountryIso3Code":self.ReceivingCountryList.CountryList![index ?? self.SelectedCountryRow].Iso3Code!]as [String : Any]
            print(parms)
            
    //        http://api.banglaremit.co.uk/api/utils/countrybanklist
            
    //        ["Token": "OTTiYXBV220s8YXf7Uau3hmCD0iDcnK7rSfTPuF+yuE=", "ID": "92", "ReceivingCountryIso3Code": "BGD"]
            
            
            uc.webServicePosthttp(urlString: ApiUrls.bankbranchlist, params:parms , message: "Loading..."){result in
                
                self.BankListBranch = BankListresult3(JSONString:result)
                
                if self.BankListBranch?.myAppResult?.Code == 0 {
                    self.bankListApiCounter = 3
                    self.lblTitleBankSuperView.text = "SELECT BRANCH"
                    self.bankCommenDic.removeAll()
                    var myDic = [String:Any]()
                    
                    for item in self.BankListBranch.BankListBranch! {
                        
                        
                        myDic["Code"] = item.BranchCode//item.BankCode!
                        myDic["Name"] = item.BranchName
                        myDic["SubTitle"] = item.BankNameSubtitle
                        myDic["RoutingNo"] = item.RoutingNo
                        self.bankCommenDic.append(myDic)
                        
                    }
                    
                    self.tblVBankSuperView.reloadData()
                
                }else if self.BankListBranch?.myAppResult?.Code == 101 {
                    
                    
                }else{
                    
                    if(self.BankListBranch?.myAppResult?.Message == nil){
                        
                        self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                        
                    }else{
                        
                        self.uc.errorSuccessAler("Error", (self.CountryBankList?.myAppResult?.Message)!, self)
                    }
                    
                }
                
            }
            
            
        }
    
    
}

extension AddRecipientViewController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bankCommenDic.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//         let cell = self.tranactionListView.tblTranactions.dequeueReusableCell(withIdentifier: "TranactionListCell") as? TransListCell
        
        let cell = self.tblVBankSuperView.dequeueReusableCell(withIdentifier: "AddRecipientBankListCell") as? AddRecipientBankListCell
        
        let model = bankCommenDic[indexPath.row]
        
        cell?.lblCode.text = model["Code"] as? String
        cell?.lblBankName.text = model["Name"] as? String
        
        if self.bankListApiCounter == 3 {
            cell?.lblSubtitle.isHidden = false
            cell?.lblSubtitle.text = model["SubTitle"] as? String
        }else {
            cell?.lblSubtitle.isHidden = true
        }
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let model = bankCommenDic[indexPath.row]
        
//        var bankNameStore = String()
//        var routingNoStroe = String()
//        var branchNameStore = String()
        
        
        if self.bankListApiCounter == 1 {
            
            self.bankCodeStore = model["Code"] as? String
            self.bankNameStore = (model["Name"] as? String)!
            self.getBankListDistrict(bankCode: self.bankCodeStore)
            
        }else if self.bankListApiCounter == 2 {
            
            self.districtCode = model["Code"] as? String
            
            
            self.getBankListBranch(bankCode: self.bankCodeStore, districtCode: self.districtCode)
        
            
        }else if self.bankListApiCounter == 3 {
            
            self.routingNoStroe = (model["RoutingNo"] as? String)!
            self.branchNameStore = (model["Name"] as? String)!
            self.addRecipientView.txtBankName.text = self.bankNameStore
            self.addRecipientView.txtBranchName.text = self.branchNameStore
            self.addRecipientView.txtBranchCode.text = self.routingNoStroe
            
            self.bankListApiCounter = 0
            bankSuperView.removeFromSuperview()
            self.bankCommenDic.removeAll()
            tableView.reloadData()
        }
    }
    
    
}
