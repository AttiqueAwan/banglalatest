//
//  AddReipientView.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 07/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class AddReipientView: UIView {
    
    //Label Title
    @IBOutlet weak var lblTitle: UILabel!

    //Button Back
    @IBOutlet weak var btnBack: UIButton!
    
    //Add  Recipient Button 
    @IBOutlet weak var btnAddRecipient: UIButton!
    
    //TextField First Name
    @IBOutlet weak var txtFirstName: JVFloatLabeledTextField!
    
    //TextField Middle Name
    @IBOutlet weak var txtMiddleName: JVFloatLabeledTextField!
    
    //TextField Last Name
    @IBOutlet weak var txtLastName: JVFloatLabeledTextField!
    
    //Segmented Control Payment Method
    @IBOutlet weak var btnPaymentMethod: UISegmentedControl!
    
    
    //TextField Country Name
    @IBOutlet weak var txtCountry: JVFloatLabeledTextField!
    
    //Button Country
    @IBOutlet weak var btnCountry: UIButton!
    
    @IBOutlet weak var btnMobileNet: UIButton!
    
    //TextField Phone Code
    @IBOutlet weak var txtPhoneCode: JVFloatLabeledTextField!
    
    //TextField Phone Number
    @IBOutlet weak var txtPhoneNumber: JVFloatLabeledTextField!
    
    //TextField Address
    @IBOutlet weak var txtAddress: JVFloatLabeledTextField!
    
    //TextField RelationShip
    @IBOutlet weak var txtRealationShip: JVFloatLabeledTextField!
    
    @IBOutlet weak var btnRelationShip: UIButton!
    
    @IBOutlet weak var mobilieView: UIView!
    @IBOutlet weak var txtmobilie: JVFloatLabeledTextField!
    @IBOutlet weak var mobHeight: NSLayoutConstraint!
    
    
    ///////Mobile Account Number///////
    
    @IBOutlet weak var txtAccountCode: JVFloatLabeledTextField!
    @IBOutlet weak var txtAccountNumber: JVFloatLabeledTextField!
    @IBOutlet weak var mobileAccountView: UIView!
    ///////////Bank Details////////////////
  
    //Bank View Height
    @IBOutlet weak var bankViewHeight: NSLayoutConstraint!
    
    
    //Bank View
    @IBOutlet weak var BankView: UIView!
    
      //TextField Bank Name
    @IBOutlet weak var txtBankName: JVFloatLabeledTextField!
    
    //Button Bank Name
    @IBOutlet weak var btnBankName: UIButton!
    
    //TextField Branch Name
    @IBOutlet weak var txtBranchName: JVFloatLabeledTextField!
    
    //TextField Branch Code
    @IBOutlet weak var txtBranchCode: JVFloatLabeledTextField!
    
    //TextField IBAN Number
    @IBOutlet weak var txtIBAN: JVFloatLabeledTextField!
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
