//
//  WebViewViewController.swift
//  BanglaRemitt
//
//  Created by Asad Zahoor on 30/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class WebViewViewController: UIViewController {
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var lblPageTitle: UILabel!
    var webViewUrl = String()
    var pageTitle = String()
    let uc = UtilitySoftTechMedia()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblPageTitle.text = pageTitle
       // webView = WKWebView(frame: self.view.frame)
        webView.frame = self.view.frame
        if self.webViewUrl != "" {
            self.webView.loadRequest(URLRequest(url: URL(string: self.webViewUrl)!))
        }else {
            uc.makeToast(message: "Image does not exist")
        }
        
               
        self.webView.delegate = self
            
       SVProgressHUD.setStatus("Loading....")
    }

    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension WebViewViewController:UIWebViewDelegate{
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        
        SVProgressHUD.show()
        return true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        SVProgressHUD.dismiss()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        SVProgressHUD.dismiss()
    }
    
}
