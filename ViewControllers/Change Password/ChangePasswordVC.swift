//
//  ChangePasswordVC.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 12/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    let uc = UtilitySoftTechMedia()
    var btnEye = UIButton()
    var btnEyeNewPassword = UIButton()
   
    override func viewDidLoad() {
        super.viewDidLoad()

        self.eyeShowAndHide()
        
    }

    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnReset(_ sender: UIButton) {
        
        if(txtOldPassword.text == "")
        {
            self.uc.errorSuccessAler("", "Current password can't be null", self)
        }
        else if(txtNewPassword.text == "")
        {
            self.uc.errorSuccessAler("", "New password can't be null", self)
        }
        else if(txtConfirmPassword.text == "")
        {
            self.uc.errorSuccessAler("", "Confirm password can't be null", self)
        }
        else if(txtNewPassword.text != txtConfirmPassword.text)
        {
            self.uc.errorSuccessAler("", "New and confirm password not matched.", self)
        }
        else
        {
           ResetPassword()
        }

    }
}
