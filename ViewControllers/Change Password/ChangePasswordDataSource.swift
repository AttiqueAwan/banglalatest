//
//  ChangePasswordDataSource.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 12/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit

extension ChangePasswordVC {
    
    func eyeShowAndHide() {
        
        let image = UIImage(named: "ico_eye.png")
        self.btnEye.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        self.btnEye.imageView?.contentMode = .scaleAspectFit
        self.btnEye.setImage(image, for: .normal)
        self.btnEye.addTarget(self, action: #selector(btnEyeClick(_:)), for: .touchUpInside)
        self.txtNewPassword.rightView = self.btnEye
        self.txtNewPassword.rightViewMode = .always
        
        let image1 = UIImage(named: "ico_eye.png")
        self.btnEyeNewPassword.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        self.btnEyeNewPassword.imageView?.contentMode = .scaleAspectFit
        self.btnEyeNewPassword.setImage(image1, for: .normal)
        self.btnEyeNewPassword.addTarget(self, action: #selector(btnEyeClick1(_:)), for: .touchUpInside)
        self.txtConfirmPassword.rightView = self.btnEyeNewPassword
        self.txtConfirmPassword.rightViewMode = .always
    }
    
    @IBAction func btnEyeClick(_ sender:Any){
       
       if(self.btnEye.imageView!.image == #imageLiteral(resourceName: "ico_eye")){
           
           self.txtNewPassword.isSecureTextEntry = false
           let image = UIImage(named: "ico_password_view.png")
           self.btnEye.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
           self.btnEye.imageView!.contentMode = .scaleAspectFit
           self.btnEye.setImage(image, for: .normal)
       
       }else if(self.btnEye.imageView!.image == #imageLiteral(resourceName: "ico_password_view.png")){
           
           let image = UIImage(named: "ico_eye.png")
          
           self.btnEye.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
           self.btnEye.imageView!.contentMode = .scaleAspectFit
           self.btnEye.setImage(image, for: .normal)
           self.txtNewPassword.isSecureTextEntry = true
           
       }
       
       }
       
       @IBAction func btnEyeClick1(_ sender:Any){
          
          if(self.btnEyeNewPassword.imageView!.image == #imageLiteral(resourceName: "ico_eye")){
              
              self.txtConfirmPassword.isSecureTextEntry = false
              let image = UIImage(named: "ico_password_view.png")
              self.btnEyeNewPassword.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
              self.btnEyeNewPassword.imageView!.contentMode = .scaleAspectFit
              self.btnEyeNewPassword.setImage(image, for: .normal)
          
          }else if(self.btnEyeNewPassword.imageView!.image == #imageLiteral(resourceName: "ico_password_view.png")){
              
              let image = UIImage(named: "ico_eye.png")
             
              self.btnEyeNewPassword.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
              self.btnEyeNewPassword.imageView!.contentMode = .scaleAspectFit
              self.btnEyeNewPassword.setImage(image, for: .normal)
              self.txtConfirmPassword.isSecureTextEntry = true
              
          }
          
          }
    
    
    func ResetPassword(){
       
        let authToken = uc.getAuthToken()
        
//        let parms =  ["NewPassword":self.txtNewPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines),"OldPassword":self.txtOldPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines),"Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,"ID":(uc.getAuthToken()?.AuthToken?.user_id)!]as [String : Any]
        
        let parms =  ["NewPassword":self.txtNewPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines),"OldPassword":self.txtOldPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines),"Token":(authToken?.authToken)!,"ID":(authToken?.userId)!]as [String : Any]

        uc.webServicePosthttp(urlString: ApiUrls.ChangePassword, params:parms , message: "Loading..."){result in
            
            if(result == "fail")
            {
                self.ResetPassword()
                return
            }
            let resultsss = AppUser(JSONString: result)
            print(result)
            if(result.contains("Successfully done"))
            {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "", message: "\(resultsss!.Data!)", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                       
                        self.navigationController?.popToRootViewController(animated: true)
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else if(result.contains("length between 6 and 25"))
            {
                self.uc.errorSuccessAler("", "Password must have a length between 6 and 25", self)
            }
            else if(result.contains("Invalid old password."))
            {
                self.uc.errorSuccessAler("", "Invalid old password.", self)
            }else {
                self.uc.errorSuccessAler("", "000000 Pattern is not allowed", self)
            }
            
        }
        
    }
    
    
}
