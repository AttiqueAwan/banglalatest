//
//  LoginViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 25/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

import LocalAuthentication


class LoginViewController: UIViewController {

    //Login View Outlet 
    @IBOutlet var loginView: LoginView!
    
    @IBOutlet weak var imgFaceTouhId: UIImageView!
    
    let uc = UtilitySoftTechMedia()
    
    var loginResponse:LoginResult!
    var email = ""
    var password = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
        
        let image = UIImage(named: "ico_password_view.png")
        self.loginView.btnEye.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
        self.loginView.btnEye.imageView!.contentMode = .scaleAspectFit
        self.loginView.btnEye.setImage(image, for: .normal)
        self.loginView.btnEye.addTarget(self, action: #selector(btnEyeClick(_:)), for: .touchUpInside)
        self.loginView.txtPassword.rightView = self.loginView.btnEye
        self.loginView.txtPassword.rightViewMode = .always
        
        if let UserName = UserDefaults.standard.value(forKey: "Email"){
            
            self.loginView.txtUserName.text = UserName as? String
        }
        
        if let Password = UserDefaults.standard.value(forKey: "Password"){
            
            self.loginView.txtPassword.text = Password as? String
            
        }
        
        // Do any additional setup after loading the view.
        
    }

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let currentType = LAContext().biometricType
        
        if currentType == LAContext.BiometricType.faceID{
            //image set
            print("faceidImage")
            self.imgFaceTouhId.image = UIImage(named: "faceIdBangla")
                
        }else if currentType == LAContext.BiometricType.touchID{
            //finger Image
            print("touchidImage")
            self.imgFaceTouhId.image = UIImage(named: "finger_print")
        }else {
            //remove Image
            print("remove image")
            self.loginView.lblOneTouch.isHidden = true
            self.loginView.viewOneTouch.isHidden = true
            self.loginView.viewOR.isHidden = true
//            self.imgFaceTouhId.image = UIImage(named: "")
        }
    }
    
    //Back Button Click
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //Button Login Click
    @IBAction func btnLoginClick(_ sender: Any) {
        
        if(self.loginView.txtUserName.text?.isEmpty)!{
            
            uc.errorSuccessAler("Alert", "Email Address Is required!", self)
            
        }else if !(uc.isValidEmail(testStr: (self.loginView.txtUserName.text)!)){
            
            uc.errorSuccessAler("Alert", "valid Email Address Is required!", self)
            
        }else if(self.loginView.txtPassword.text?.isEmpty)!{
            
             uc.errorSuccessAler("Alert", "Password Is required!", self)
    
        }else if((self.loginView.txtPassword.text?.count)! < 6){
            
            uc.errorSuccessAler("Alert", "Password must have a length between 6 and 25!", self)
            
        }else{
    
            self.UserLogin()
        }
    }
    
    //Forget Password Click
    @IBAction func btnForgetPasswordClick(_ sender: Any) {
        
        self.navigationController?.pushViewController(ForgetPasswordViewController(), animated: true)
        
    }
    
    //Touch ID Button Click
    @IBAction func btnTouchIDClick(_ sender: Any) {
        
        guard let _ = UserDefaults.standard.value(forKey: "Email") as? String else {
            if(self.loginView.txtUserName.text?.isEmpty)!{
                
                uc.errorSuccessAler("Alert", "Email Address required!", self)
                
            }else if !(uc.isValidEmail(testStr:self.loginView.txtUserName.text!)){
                
                uc.errorSuccessAler("Alert", "Valid Email required!", self)
                
            }else{
                guard let _ = UserDefaults.standard.value(forKey:"Password") as? String else {
                    if(self.loginView.txtPassword.text?.isEmpty)!{
                        
                        uc.errorSuccessAler("Alert", "Password required!", self)
                        
                    }else{
                        self.authenticationWithTouchID()
                    }
                    return
                }
                self.authenticationWithTouchID()
            }
            return
        }
        
        guard let _ = UserDefaults.standard.value(forKey:"Password") as? String else {
            if(self.loginView.txtPassword.text?.isEmpty)!{
                
                uc.errorSuccessAler("Alert", "Password required!", self)
                
            }else{
                self.authenticationWithTouchID()
            }
            return
        }
        
        self.authenticationWithTouchID()
        
    }
    
    
    //Sign Up Butotn Click
    @IBAction func btnSignUpClick(_ sender: Any) {
        
        self.navigationController?.pushViewController(SignUp1ViewController(), animated: true)
    }
    
    
    //Button Eye Click
    @IBAction func btnEyeClick(_ sender:Any){
    
    if(self.loginView.btnEye.imageView!.image == #imageLiteral(resourceName: "ico_password_view.png")){
        
        self.loginView.txtPassword.isSecureTextEntry = false
        let image = UIImage(named: "ico_eye.png")
        self.loginView.btnEye.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
        self.loginView.btnEye.imageView!.contentMode = .scaleAspectFit
        self.loginView.btnEye.setImage(image, for: .normal)
    
    }else if(self.loginView.btnEye.imageView!.image == #imageLiteral(resourceName: "ico_eye")){
        
        let image = UIImage(named: "ico_password_view.png")
        self.loginView.btnEye.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
        self.loginView.btnEye.imageView!.contentMode = .scaleAspectFit
        self.loginView.btnEye.setImage(image, for: .normal)
        self.loginView.txtPassword.isSecureTextEntry = true
        
    }
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LAContext {
    enum BiometricType: String {
        case none
        case touchID
        case faceID
    }

    var biometricType: BiometricType {
        var error: NSError?

        guard self.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            // Capture these recoverable error thru Crashlytics
            return .none
        }

        if #available(iOS 11.0, *) {
            switch self.biometryType {
            case .none:
                return .none
            case .touchID:
                return .touchID
            case .faceID:
                return .faceID
            
            @unknown default:
                return .none
            }
        } else {
            return  self.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touchID : .none
        }
    }
}
