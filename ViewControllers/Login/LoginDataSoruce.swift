//
//  LoginDataSoruce.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 25/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

import LocalAuthentication
extension LoginViewController{
    
    
    func UserLogin(){
      
//
        
        email = self.loginView.txtUserName.text!
        password = self.loginView.txtPassword.text!
        
        if email == "" || password == "" {
            if let email1 = UserDefaults.standard.value(forKey: "Email") as? String {
                email = email1
            }
            if let password1 = UserDefaults.standard.value(forKey: "Password") as? String {
                password = password1
            }
        }
        
        if email == "" || password == "" {
            uc.errorSuccessAler("Alert", "Please sign in first", self)
            return
        }
        
//        let parms =  ["Password":self.loginView.txtPassword .text!.trimmingCharacters(in: .whitespacesAndNewlines),"Email":self.loginView.txtUserName.text!.trimmingCharacters(in: .whitespacesAndNewlines)]as [String : Any]
        
        let parms =  ["Password":self.password.trimmingCharacters(in: .whitespacesAndNewlines),"Email":self.email.trimmingCharacters(in: .whitespacesAndNewlines)]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.LoginUser, params:parms , message: "Loading..."){result in
            
            self.loginResponse = LoginResult(JSONString:result)
            
            if self.loginResponse != nil  && self.loginResponse.myAppResult?.Code == 0{
                
                if self.loginView.txtUserName.text != nil && self.loginView.txtUserName.text != "" {
                    UserDefaults.standard.set(self.loginView.txtUserName.text, forKey: "Email")
                }
                if self.loginView.txtPassword.text != nil && self.loginView.txtPassword.text != "" {
                    UserDefaults.standard.set(self.loginView.txtPassword.text, forKey: "Password")
                }
                
//                UserDefaults.standard.set(self.loginView.txtUserName.text, forKey: "Email")
//                UserDefaults.standard.set(self.loginView.txtPassword.text, forKey: "Password")
                self.uc.saveAuthToken(result: result)
                self.navigationController?.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(DashboardViewController(), animated: true)
                
            }else{
                
                if(self.loginResponse == nil){
                    
                    self.uc.errorSuccessAler("Error", "User Name Or Password Is Invalid", self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", (self.loginResponse.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    func authenticationWithTouchID() {
        
        
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "Use Passcode"
        
        var authError: NSError?
        let reasonString = "To access the secure data"
        
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString) { success, evaluateError in
                
                if success {
                    
                    DispatchQueue.main.async {[weak self] in 
                        self?.UserLogin()
                    }
                    
                    //TODO: User authenticated successfully, take appropriate action
                    
                } else {
                    //TODO: User did not authenticate successfully, look at error and take appropriate action
                    guard let error = evaluateError else {
                        return
                    }
                    
                    print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code))
                    
                    //TODO: If you have choosen the 'Fallback authentication mechanism selected' (LAError.userFallback). Handle gracefully
                    
                }
            }
        } else {
            
            guard let error = authError else {
                return
            }
            //TODO: Show appropriate alert if biometry/TouchID/FaceID is lockout or not enrolled
            print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
        }
 
    
       //Create a context
     /*  let authenticationContext = LAContext()
       var error:NSError?

       //Check if device have Biometric sensor
       let isValidSensor : Bool = authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)

       if isValidSensor {
           //Device have BiometricSensor
           //It Supports TouchID

           authenticationContext.evaluatePolicy(
               .deviceOwnerAuthenticationWithBiometrics,
               localizedReason: "Touch / Face ID authentication",
               reply: { [unowned self] (success, error) -> Void in

                   if(success) {
                       // Touch / Face ID recognized success here
                      // completion(true)
                    print("sucess")
                   } else {
                       //If not recognized then
                       if let error = error {
                           let strMessage = self.errorMessage(errorCode: error._code)
                           if strMessage != ""{
                               self.showAlertWithTitle(title: "Error", message: strMessage)
                           }
                       }
                       //completion(false)
                    print("failure not recognised")
                   }
           })
       } else {

           let strMessage = self.errorMessage(errorCode: (error?._code)!)
           if strMessage != ""{
               self.showAlertWithTitle(title: "Error", message: strMessage)
           }
       }*/
        
    }
    
//    func errorMessage(errorCode:Int) -> String{
//
//        var strMessage = ""
//
//        switch errorCode {
//
//        case LAError.Code.authenticationFailed.rawValue:
//            strMessage = "Authentication Failed"
//
//        case LAError.Code.userCancel.rawValue:
//            strMessage = "User Cancel"
//
//        case LAError.Code.systemCancel.rawValue:
//            strMessage = "System Cancel"
//
//        case LAError.Code.passcodeNotSet.rawValue:
//            strMessage = "Please goto the Settings & Turn On Passcode"
//
//        case LAError.Code.touchIDNotAvailable.rawValue:
//            strMessage = "TouchI or FaceID DNot Available"
//
//        case LAError.Code.touchIDNotEnrolled.rawValue:
//            strMessage = "TouchID or FaceID Not Enrolled"
//
//        case LAError.Code.touchIDLockout.rawValue:
//            strMessage = "TouchID or FaceID Lockout Please goto the Settings & Turn On Passcode"
//
//        case LAError.Code.appCancel.rawValue:
//            strMessage = "App Cancel"
//
//        case LAError.Code.invalidContext.rawValue:
//            strMessage = "Invalid Context"
//
//        default:
//            strMessage = ""
//
//        }
//        return strMessage
//    }
//    //MARK: Show Alert
//    func showAlertWithTitle( title:String, message:String ) {
//
//        DispatchQueue.main.async {
//             let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//
//                   let actionOk = UIAlertAction(title: "OK", style: .default, handler: nil)
//                   alert.addAction(actionOk)
//                   self.present(alert, animated: true, completion: nil)
//        }
//
//    }
    
    func evaluatePolicyFailErrorMessageForLA(errorCode: Int) -> String {
        var message = ""
        if #available(iOS 11.0, macOS 10.13, *) {
            switch errorCode {
            case LAError.biometryNotAvailable.rawValue:
                message = "Authentication could not start because the device does not support biometric authentication."
                
            case LAError.biometryLockout.rawValue:
                message = "Authentication could not continue because the user has been locked out of biometric authentication, due to failing authentication too many times."
                
            case LAError.biometryNotEnrolled.rawValue:
                message = "Authentication could not start because the user has not enrolled in biometric authentication."
                
            default:
                message = "Did not find error code on LAError object"
            }
        } else {
            switch errorCode {
            case LAError.touchIDLockout.rawValue:
                message = "Too many failed attempts."
                
            case LAError.touchIDNotAvailable.rawValue:
                message = "TouchID is not available on the device"
                
            case LAError.touchIDNotEnrolled.rawValue:
                message = "TouchID is not enrolled on the device"
                
            default:
                message = "Did not find error code on LAError object"
            }
        }
        
        return message;
    }
    
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int) -> String {
        
        var message = ""
        
        switch errorCode {
            
        case LAError.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
            
        case LAError.appCancel.rawValue:
            message = "Authentication was cancelled by application"
            
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.notInteractive.rawValue:
            message = "Not interactive"
            
        case LAError.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
            
        case LAError.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
            
        case LAError.userCancel.rawValue:
            message = "The user did cancel"
            
        case LAError.userFallback.rawValue:
            message = "The user chose to use the fallback"
            
        default:
            message = evaluatePolicyFailErrorMessageForLA(errorCode: errorCode)
        }
        
        return message
    }
}

