//
//  LoginView.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 25/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import BEMCheckBox

class LoginView: UIView {

    //Back Button
    @IBOutlet weak var btnBack: UIButton!
    
    //User Name TextField
    @IBOutlet weak var txtUserName: JVFloatLabeledTextField!
    
    //Password TextField
    @IBOutlet weak var txtPassword: JVFloatLabeledTextField!
    
    //Remember Me Check Box
    @IBOutlet weak var RememberMeCheckBox: BEMCheckBox!
    
    //Forget Password Button
    @IBOutlet weak var btnForgetPassword: UIButton!
    
    
    @IBOutlet var passwordView: UIView!
    @IBOutlet var useremailView: UIView!
    //Login Button
    @IBOutlet weak var btnLogin: UIButton!
    
    
    // Touch ID Button
    @IBOutlet weak var btnTouchID: UIButton!
    
    //Sign Up Button
    @IBOutlet weak var btnSignUp: UIButton!
    
    
    //Eye Button
    var btnEye = UIButton()
    
    @IBOutlet weak var viewOR: UIView!
    @IBOutlet weak var lblOneTouch: UILabel!
    
    @IBOutlet weak var viewOneTouch: UIView!
}
