//
//  ExchangeRateDataSource.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 26/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension ExchangeRateViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        return 50.0
//    
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(self.PublicExchangeRate != nil){
            
            if(self.PublicExchangeRate.ExchangeRateDetail != nil){
                
                return self.PublicExchangeRate.ExchangeRateDetail?.count ?? 0
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.exchangeRateView.tblExchangeRate.dequeueReusableCell(withIdentifier: "ExchangeRateCell") as? ExchangeRateCell
        
        let details = self.PublicExchangeRate .ExchangeRateDetail![indexPath.row]
        cell?.lblBankName.text = details.PayerName
        //details.CreationDate
//        if (indexPath.row) == 2 {
//            self.exchangeRateView.lblLastUpdated.text = "Last updated: \(String(describing: details.CreationDate!))"
//        }
        
        
                let date = Date()
                let formatter = DateFormatter()
                formatter.locale = Locale(identifier: "nl_NL")
                formatter.setLocalizedDateFormatFromTemplate("dd-MM-yyyy")
        
                let datetime = formatter.string(from: date)
                print(datetime)
        
        self.exchangeRateView.lblLastUpdated.text = "Last updated: \(datetime)"
        

//        // Output: 11-03-2020
        
        cell?.lblExchangeRate.text = "\((details.ExchangeRate)!) \((details.SendingCurrencyISOCode)!)"
        
        //cell?.imgBank.image = self.GetImage(details)
        
        return cell!
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.indicatorStyle = .black
//        scrollView.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.003921568627, blue: 0.03921568627, alpha: 1)
//        let verticalIndicator = scrollView.subviews.last as? UIImageView
//        verticalIndicator?.backgroundColor = UIColor.green
    }
    
    
    
    func GetImage(_ details:PublicExchangeRateDetail) -> UIImage {
        
        
        if(details.PayerName == "Al-Arafah Islami Bank Ltd"){
            
            return #imageLiteral(resourceName: "Al-Arafah Islami Bank Ltd")
            
        }else if(details.PayerName == "BANK TRANSFER-BANGLADESH"){
            
             return #imageLiteral(resourceName: "bank_logo.png")
            
        }else if(details.PayerName == "Uttara Bank Ltd"){
            
             return #imageLiteral(resourceName: "Uttara_Bank_Ltd")
            
        }else if(details.PayerName == "Social Islami Bank Ltd"){
           
            return UIImage(named: "Social_Islami_Bank_Ltd.png")!
            
        }else if(details.PayerName == "Premier Bank Ltd"){
            
            
            return UIImage(named:"Premier_Bank_Ltd.png")!
            
        }else if(details.PayerName == "South East Bank Ltd"){
            
            
             return UIImage(named:"South_East_Bank_Ltd.png")!
            
        }else if(details.PayerName == "South Bangla Agricultural and Commerce Bank Ltd "){
            
            
             return UIImage(named:"South_Bangla_Agricultural_and_Commerce_Bank_Ltd.png")!
            
            
        }else if(details.PayerName == "National Credit & Commerce Bank Ltd "){
            
            
             return UIImage(named:"National_Credit_&_Commerce_Bank_Ltd.png")!
            
            
        }else if(details.PayerName == "Jamuna Bank Ltd"){
            
             return UIImage(named:"Jamuna_Bank_Ltd.png")!
            
        }else{
            
            return UIImage(named: "bank_logo.png")!
        }
        
    }
    
    func countryName(from countryCode: String) -> String {
        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
            // Country name was found
            return name
        } else {
            // Country name cannot be found
            return countryCode
        }
    }
    
}


extension ExchangeRateViewController{
    
   
    func GetExchangeRate(){
       
        var iso3Code = ""
      
        let englishLocale : NSLocale = NSLocale.init(localeIdentifier :  "en_US")
        
        // get the current locale
        let currentLocale = NSLocale.current
        
        let theEnglishName : String? = englishLocale.displayName(forKey: NSLocale.Key.identifier, value: currentLocale.identifier)
        if let theEnglishName = theEnglishName
        {
            print("the localized country name is \(theEnglishName)")
            iso3Code = theEnglishName
            iso3Code = iso3Code.components(separatedBy: "(")[1]
            iso3Code = iso3Code.components(separatedBy: ")")[0]
        }
        
        
            for i in  0..<ApiUrls.countries.count{
                
                if(ApiUrls.countries[i] == iso3Code){
                    
                    iso3Code = ApiUrls.countriesIso3Code[i]
                    break
                    
                }
            }
        
        let parms =  ["SendingCountryIso3Code":iso3Code,"ReceivingCountryIso3Code":"BGD"]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetPublicExchangeRate, params:parms , message: "Loading..."){result in
            
            self.PublicExchangeRate = PublicExchangeRateresult(JSONString:result)
            
            if self.PublicExchangeRate?.myAppResult?.Code == 0 {
                
                var CashTop = self.PublicExchangeRate.ExchangeRateDetail![0]
                var BankTop = self.PublicExchangeRate.ExchangeRateDetail![0]
                var removingIndexCash = 0,removngIndexBank = 0
                
                for i in 0..<(self.PublicExchangeRate.ExchangeRateDetail?.count)!{
                    
                    if(self.PublicExchangeRate.ExchangeRateDetail![i].PaymentMethod == "Cash"){
                        
                        let testCash = self.PublicExchangeRate.ExchangeRateDetail![i]
                        if(testCash.ExchangeRate! > CashTop.ExchangeRate!){
                            
                            CashTop = self.PublicExchangeRate.ExchangeRateDetail![i]
                            removingIndexCash = i
                        }
                        
                        
                        
                    }else if(self.PublicExchangeRate.ExchangeRateDetail![i].PaymentMethod == "Bank"){
                        
                        if(BankTop.PaymentMethod == "Cash"){
                            
                            BankTop = self.PublicExchangeRate.ExchangeRateDetail![i]
                            removngIndexBank = i
                        }else{
                            
                            let testBank = self.PublicExchangeRate.ExchangeRateDetail![i]
                            
                            if(testBank.ExchangeRate! > BankTop.ExchangeRate!){
                                
                                CashTop = self.PublicExchangeRate.ExchangeRateDetail![i]
                                removngIndexBank = i
                            }
                            
                        }
                        
                    }
                    
                }
                
                self.PublicExchangeRate.ExchangeRateDetail?.remove(at: removingIndexCash)
                
                if(removngIndexBank != 0){
                    
                     self.PublicExchangeRate.ExchangeRateDetail?.remove(at: removngIndexBank-1)
                }else{
                    
                     self.PublicExchangeRate.ExchangeRateDetail?.remove(at: removngIndexBank)
                }
               
                
                //self.exchangeRateView.lblBankName.text = BankTop.PayerName
              //  self.exchangeRateView.lblBankExchangeRate.text = "\((BankTop.ExchangeRate)!) \((BankTop.SendingCurrencyISOCode)!)"
              //  self.exchangeRateView.imgBank.image = self.GetImage(BankTop)
                
                self.exchangeRateView.lblCashName.text = CashTop.PayerName
                self.exchangeRateView.lblCashExchangeRate.text = "\((CashTop.ExchangeRate)!) \((CashTop.SendingCurrencyISOCode)!)"
                self.exchangeRateView.iconCash.image = self.GetImage(CashTop)
                
//                self.exchangeRateView.lblLastUpdated.text = "(Last Updated \((CashTop.CreationDate?.components(separatedBy:"T")[0])!) )"
                
                self.exchangeRateView.tblExchangeRate.reloadData()
                
            }else if self.PublicExchangeRate?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
                
            }else{
                
                if(self.PublicExchangeRate.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.PublicExchangeRate.myAppResult?.Message)!, self)
                }
            }
            
        }
    }

}


