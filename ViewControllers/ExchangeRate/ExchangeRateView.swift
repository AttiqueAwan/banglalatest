//
//  ExchangeRateView.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 26/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class ExchangeRateView: UIView {
   
    //Label Last Updated
    @IBOutlet weak var lblLastUpdated: UILabel!
    
    //Bank Icon
    @IBOutlet weak var imgBank: UIImageView!
    
    //Label Bank Name
    @IBOutlet weak var lblBankName: UILabel!
    
    //Label Bank Exchange Rate
    @IBOutlet weak var lblBankExchangeRate: UILabel!
    
    //Cash icon
    @IBOutlet weak var iconCash: UIImageView!
    
    //Label Cash Payer Name
    @IBOutlet weak var lblCashName: UILabel!
    
    //Label Cash Exchange Rate
    @IBOutlet weak var lblCashExchangeRate: UILabel!
    
    // Back Button
    @IBOutlet weak var btnBack: UIButton!
    
    
    // Table View Exchange Rate
    @IBOutlet weak var tblExchangeRate: UITableView!
   
    
    //Login Button
    @IBOutlet weak var btnLogin: UIButton!
    
    
    //SignUp Button
    @IBOutlet weak var btnSignUp: UIButton!
    
    //SignUp Sign IN View
    @IBOutlet weak var signUpSignInView: UIView!
    
    
    //Back Button Bottom
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
