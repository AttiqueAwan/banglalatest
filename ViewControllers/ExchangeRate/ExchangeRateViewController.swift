//
//  ExchangeRateViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 26/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class ExchangeRateViewController: UIViewController {

    //Exchange Rate View
    @IBOutlet var exchangeRateView: ExchangeRateView!
    
    var PublicExchangeRate : PublicExchangeRateresult!
    
    let uc = UtilitySoftTechMedia()
    
    var isSignupLoginToHide = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isSignupLoginToHide {
            //contier hide
            self.exchangeRateView.signUpSignInView.isHidden = true
        }

        let nibName = UINib(nibName: "ExchangeRateCell", bundle:nil)
        
        self.exchangeRateView.tblExchangeRate.register(nibName, forCellReuseIdentifier: "ExchangeRateCell")
    
          self.GetExchangeRate()
        
        // Do any additional setup after loading the view.
    }


    //Button Back CLick
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //Button Login Click
    @IBAction func btnLoginClick(_ sender: Any) {
        
         self.navigationController?.pushViewController(LoginViewController(), animated: true)
        
    }
    
    
    //Button SignUp Click
    @IBAction func btnSignUpClick(_ sender: Any) {
        
         self.navigationController?.pushViewController(SignUp1ViewController(), animated: true)
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
