//
//  ExchangeRateCell.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 26/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class ExchangeRateCell: UITableViewCell {
    
    //Bank Logo Image
    @IBOutlet weak var imgBank: UIImageView!
    
    //Bank Name Label
    @IBOutlet weak var lblBankName: UILabel!
    
    
    //Exchange Rate Label
    @IBOutlet weak var lblExchangeRate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
