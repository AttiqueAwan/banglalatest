//
//  SignUp1ViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 25/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

var Email = ""

class SignUp1ViewController: UIViewController {

    //Sign Up View
    @IBOutlet var signUp1View: SignUp1View!
    
    let uc = UtilitySoftTechMedia()
    var parms = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let frame =  CGRect(x: 0, y: 0, width: 20 , height: 20)
        let image = UIImage(named: "ico_password_view.png")
        self.signUp1View.btnPasswordEye.frame = frame
        self.signUp1View.btnPasswordEye.imageView!.contentMode = .scaleAspectFit
        self.signUp1View.btnPasswordEye.setImage(image, for: .normal)
        self.signUp1View.btnPasswordEye.addTarget(self, action: #selector(btnPasswordClick(_:)), for: .touchUpInside)
        self.signUp1View.txtPassword.rightView = self.signUp1View.btnPasswordEye
        self.signUp1View.txtPassword.rightViewMode = .always
        
        let frame2 =  CGRect(x: 0, y: 0, width: 20 , height: 20)
        let image2 = UIImage(named: "ico_password_view.png")

        self.signUp1View.btnConfirmPassowrdEye.frame = frame2
        self.signUp1View.btnConfirmPassowrdEye.imageView!.contentMode = .scaleAspectFit
        self.signUp1View.btnConfirmPassowrdEye.setImage(image2, for: .normal)
        self.signUp1View.btnConfirmPassowrdEye.addTarget(self, action: #selector(btnConfirmPasswordClick(_:)), for: .touchUpInside)
        self.signUp1View.txtReTypePassword.rightView = self.signUp1View.btnConfirmPassowrdEye
        self.signUp1View.txtReTypePassword.rightViewMode = .always
        
        // Do any additional setup after loading the view.
        
      
    }


    //Back Button Click
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //Button Next Click
    @IBAction func btnNextClick(_ sender: Any) {
        
        if(self.signUp1View.txtFirstName.text?.isEmpty)!{
            
            uc.errorSuccessAler("Alert", "First Name is required!", self)
            
        }else if(isValidInput(Input: self.signUp1View.txtFirstName.text!) == false){
            
             uc.errorSuccessAler("Alert", "Enter only Alphabets", self)
            
        }else if((self.signUp1View.txtFirstName.text?.count)! < 3){
            
            uc.errorSuccessAler("Alert", "Password must have a length between 6 and 25!", self)
            
        }else if(self.signUp1View.txtLastName.text?.isEmpty)!{
            
             uc.errorSuccessAler("Alert", "Last Name is required!", self)
            
        }else if(isValidInput(Input: self.signUp1View.txtLastName.text!) == false){
            
             uc.errorSuccessAler("Alert", "Enter only Alphabets", self)
            
        }else if((self.signUp1View.txtLastName.text?.count)! < 3){
            
            uc.errorSuccessAler("Alert", "Password must have a length between 6 and 25!", self)
            
        }else if(self.signUp1View.txtEmail.text?.isEmpty)!{
            
             uc.errorSuccessAler("Alert", "Email Address is required!", self)
            
        }else if !(uc.isValidEmail(testStr: self.signUp1View.txtEmail.text!)){
            
            uc.errorSuccessAler("Alert", "Valid Email Address is required!", self)
            
        }else if(self.signUp1View.txtPassword.text?.isEmpty)!{
            
             uc.errorSuccessAler("Alert", "Password is required!", self)
            
        }else if((self.signUp1View.txtPassword.text?.count)! < 6){
            
            uc.errorSuccessAler("Alert", "Password must have a length between 6 and 25!", self)
            
        }else if(self.signUp1View.txtReTypePassword.text?.isEmpty)!{
            
             uc.errorSuccessAler("Alert", "Re-Type Password is required!", self)
            
        }else if(self.signUp1View.txtPassword.text != self.signUp1View.txtPassword.text){
            
             uc.errorSuccessAler("Alert", "Password and Re-Type Password Must be Same!", self)
            
        }else{
            parms.append(["FirstName":"\(self.signUp1View.txtFirstName.text!.trimmingCharacters(in: .whitespacesAndNewlines))"])
            parms.append(["MiddleName":"\(self.signUp1View.txtMiddleName.text!.trimmingCharacters(in: .whitespacesAndNewlines))"])
            parms.append(["LastName":"\(self.signUp1View.txtLastName.text!.trimmingCharacters(in: .whitespacesAndNewlines))"])
            parms.append(["Email":"\(self.signUp1View.txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines))"])
            parms.append(["Password":"\(self.signUp1View.txtPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines))"])

            //
            
            self.isEmailAlreadyExist(email: self.signUp1View.txtEmail.text!)
        }
    }
    func isValidInput(Input:String) -> Bool {
        let myCharSet=CharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        let output: String = Input.trimmingCharacters(in: myCharSet.inverted)
        let isValid: Bool = (Input == output)
        print("\(isValid)")

        return isValid
    }
    //Button Eye Click
    @IBAction func btnPasswordClick(_ sender:Any){
        
        if(self.signUp1View.btnPasswordEye.imageView!.image == #imageLiteral(resourceName: "ico_password_view.png")){
       
//            ico_password_view
            
            let image = UIImage(named: "ico_eye.png")
            self.signUp1View.btnPasswordEye.setImage(image, for: .normal)
            self.signUp1View.txtPassword.rightView = self.signUp1View.btnPasswordEye
            self.signUp1View.txtPassword.rightViewMode = .always
            self.signUp1View.txtPassword.isSecureTextEntry = false
            
        }else if(self.signUp1View.btnPasswordEye.imageView!.image == #imageLiteral(resourceName: "ico_eye")){
            
            let image = UIImage(named: "ico_password_view.png")
            self.signUp1View.btnPasswordEye.setImage(image, for: .normal)
            self.signUp1View.txtPassword.rightView = self.signUp1View.btnPasswordEye
            self.signUp1View.txtPassword.rightViewMode = .always
            self.signUp1View.txtPassword.isSecureTextEntry = true
            
        }
    }
    
    
    //Button Eye Click
    @IBAction func btnConfirmPasswordClick(_ sender:Any){

        if(self.signUp1View.btnConfirmPassowrdEye.imageView!.image == #imageLiteral(resourceName: "ico_password_view.png") ){

           
            let image = UIImage(named: "ico_eye.png")
            self.signUp1View.btnConfirmPassowrdEye.setImage(image, for: .normal)
            self.signUp1View.txtReTypePassword.rightView = self.signUp1View.btnConfirmPassowrdEye
            self.signUp1View.txtReTypePassword.rightViewMode = .always
            self.signUp1View.txtReTypePassword.isSecureTextEntry = false
            
        }else if(self.signUp1View.btnConfirmPassowrdEye.imageView!.image == #imageLiteral(resourceName: "ico_eye") ){

            let image = UIImage(named: "ico_password_view.png")
            self.signUp1View.btnConfirmPassowrdEye.setImage(image, for: .normal)
            self.signUp1View.txtReTypePassword.rightView = self.signUp1View.btnConfirmPassowrdEye
            self.signUp1View.txtReTypePassword.rightViewMode = .always
            self.signUp1View.txtReTypePassword.isSecureTextEntry = true

        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func btnTerms(_ sender: UIButton) {
         //https://online.banglaremit.co.uk/en/term-condition.html
        
        if let url = URL(string: "https://online.banglaremit.co.uk/en/term-condition.html") {
                       if #available(iOS 10.0, *) {
                           UIApplication.shared.open(url)
                       } else {
                           UIApplication.shared.openURL(url)
                       }
    }
    
    }
  
    
    
    func isEmailAlreadyExist(email : String){
           
                let parms =  ["Email":email]as [String : Any]
                
                
                uc.webServicePosthttp(urlString: ApiUrls.CheckUserEmail, params:parms , message: "Loading..."){result in
                    
                    //let stringresult = "\(result.components(separatedBy: "}}")[0])}}"
                    
                    
                    let emailResponse = AppUser(JSONString:result)
                    
                    if emailResponse != nil {
                        
                        if(emailResponse?.myAppResult?.Code == 0 )
                        {
                            let vc = SignUp2ViewController()
                            vc.parms = self.parms
                            self.navigationController?.pushViewController(vc, animated: true)
                        
                        }else if(emailResponse?.myAppResult?.Code == 200){
                            
                            self.uc.errorSuccessAler("Error", "Email already exist", self)
                        }else if(emailResponse?.myAppResult?.Code == 223){
                            
                            self.uc.errorSuccessAler("Error", (emailResponse?.myAppResult!.Message)!, self)
                        }else{
                            self.uc.errorSuccessAler("Error", (emailResponse?.myAppResult!.Message)!, self)
                        }
                        
                    }else{
                        
                        if(emailResponse == nil){

                            self.uc.errorSuccessAler("Error", "Email doesn't exist", self)
        //                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                            
                        }else{
                            
                            self.uc.errorSuccessAler("Error", (emailResponse!.myAppResult?.Message)!, self)
                        }
                    }
                }
       }
}


