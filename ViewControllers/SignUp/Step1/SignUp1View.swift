//
//  SignUp1View.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 25/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import BEMCheckBox
import FRHyperLabel

class SignUp1View: UIView {

    
    //Back Button
    @IBOutlet weak var btnBack: UIButton!
    
    //First Name TextField
    @IBOutlet weak var txtFirstName: JVFloatLabeledTextField!
    
    
    //Middle Name Text Field
    @IBOutlet weak var txtMiddleName: JVFloatLabeledTextField!
    
    //Last Name Text Field
    @IBOutlet weak var txtLastName: JVFloatLabeledTextField!
    
    //Email Text Field
    @IBOutlet weak var txtEmail: JVFloatLabeledTextField!
    
    // Password Text Field
    @IBOutlet weak var txtPassword: JVFloatLabeledTextField!
    
    //Re-Type Password Text Field
    @IBOutlet weak var txtReTypePassword: JVFloatLabeledTextField!
    
    
    //Check Box Agreeing
    @IBOutlet weak var agreeCheckBox: BEMCheckBox!
    
    //Hyper Label
    @IBOutlet weak var lblAgrement: FRHyperLabel!
    
    //Button Next
    @IBOutlet weak var btnNext: UIButton!
    
    //Eye Button
    var btnPasswordEye = UIButton()
    
    //Eye Button
    var btnConfirmPassowrdEye = UIButton()
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
