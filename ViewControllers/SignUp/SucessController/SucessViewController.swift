//
//  SucessViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 14/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import UserNotificationsUI

class SucessViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func btnOkClick(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: ApiUrls.SucessMessage), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
