//
//  SignUp2ViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 25/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import DropDown


class SignUp2ViewController: UIViewController {
    
    //Sign Up 2 View
    @IBOutlet var signUp2View: SignUp2View!
    
    var parms = [[String:String]]()
    //Side Menu Presentation
   
    
    let uc = UtilitySoftTechMedia()
    
    var loginResponse:AppUser!
    var CountryListingResponce :CountryListresult!
    
    let CountryDropDown = DropDown()
    let GenderDropDown = DropDown()
    var Gender = [String]()
    var Countries = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         NotificationCenter.default.addObserver(self, selector: #selector(SucessFulllyLogin(_:)), name: Notification.Name(rawValue: ApiUrls.SucessMessage), object: nil)
        
        self.GetSendingCountryList()
        
        
        //Adding Done Button On Keyboard
        self.addDoneButtonOnKeyboard()
        
        
        Gender = ["Male","FeMale"]

        //setting Country dropdown
        CountryDropDown.anchorView = self.signUp2View.btnCountry
        CountryDropDown.dataSource = Countries
        
        
        CountryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.signUp2View.txtCountry.text = item
            self.signUp2View.txtCode.text = "+\((self.CountryListingResponce.CountryList![index].DialingCode)!)"
        }
        
        //setting Gender  dropdown
        GenderDropDown.anchorView = self.signUp2View.btnGender
        GenderDropDown.dataSource = Gender
        
        
        GenderDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.signUp2View.txtGender.text = item
           
        }
        
        // Do any additional setup after loading the view.
        
       
    }

    @objc  func SucessFulllyLogin(_ notification:NSNotification) {
        
        let  vc =  LoginViewController(nibName:"LoginViewController",bundle:Bundle.main)
        let navigationController = UINavigationController(nibName: "MainNavigation", bundle: Bundle.main)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let mainstoryboardControler = storyBoard.instantiateViewController(withIdentifier: "MainController") as? ViewController
        navigationController.navigationBar.isHidden = true
        navigationController.pushViewController(mainstoryboardControler!, animated: true)
        navigationController.pushViewController(vc, animated: true)
        
        
        DispatchQueue.main.async {
            
              self.present(navigationController, animated: false, completion: nil)
        }
      
        
    }
    
    //Back Button Click
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //Gender Button Click
    @IBAction func btnGenderClick(_ sender: Any) {
        
        self.GenderDropDown.show()
    }
    
    //Country Button Click
    @IBAction func btnCountryClick(_ sender: Any) {
        
        self.CountryDropDown.show()
    }
    
    // Done Button Click
    @IBAction func btnDoneClick(_ sender: Any) {
        
        
        if(self.signUp2View.txtGender.text?.isEmpty)!{
            
            uc.errorSuccessAler("Alert", "Please Select Gender", self)
            
        }else if(self.signUp2View.txtCountry.text?.isEmpty)!{
            
            uc.errorSuccessAler("Alert", "Please Select Country", self)
            
        }else if(self.signUp2View.txtCode.text?.isEmpty)!{
            
            uc.errorSuccessAler("Alert", "Please Enter Country Code", self)
            
        }else if(self.signUp2View.txtContactNumber.text?.isEmpty)!{
            
            uc.errorSuccessAler("Alert", "Please Enter Contact Number", self)
            
        }else if(self.signUp2View.txtContactNumber.text!.count < 10 ){
            
            uc.errorSuccessAler("Alert", "Please Enter Valid Contact Number", self)
            
        }else{
            
            self.CreateAccount()
            
            
        }
   
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}




