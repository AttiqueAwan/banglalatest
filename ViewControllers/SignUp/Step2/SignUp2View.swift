//
//  SignUp2View.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 25/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import FRHyperLabel
import BEMCheckBox

class SignUp2View: UIView {
    
    // Gender TextField
    @IBOutlet weak var txtGender: JVFloatLabeledTextField!
    
    //Gender Button
    @IBOutlet weak var btnGender: UIButton!
    
    //Country TextField
    @IBOutlet weak var txtCountry: JVFloatLabeledTextField!
    
    
    //Country Button
    @IBOutlet weak var btnCountry: UIButton!
    
    //Country Code Text Field
    @IBOutlet weak var txtCode: JVFloatLabeledTextField!
    
    //Contact Number Text Field
    @IBOutlet weak var txtContactNumber: JVFloatLabeledTextField!
    
    //Referal Code TextField
    @IBOutlet weak var txtreferalCode: JVFloatLabeledTextField!
    
    
    //Check Box Agrement
    @IBOutlet weak var agrementCheckBox: BEMCheckBox!
    
    
    //Agrement Hyper Label
    @IBOutlet weak var lblAgrement: FRHyperLabel!
    
    //Done Button
    @IBOutlet weak var btnDone: UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
