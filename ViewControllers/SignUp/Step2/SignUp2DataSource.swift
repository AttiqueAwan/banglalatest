//
//  SignUp2DataSource.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 25/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit


extension SignUp2ViewController{
    
    
    //SignUp Function Call
    func CreateAccount() {
        
        var myvalues = [String]()
        for i in 0...4{
            
            let lazyMapCollection = self.parms[i].values
            
            let stringArray = Array(lazyMapCollection.map { String($0) })
            // let stringArray = Array(lazyMapCollection).map { String($0) } // also works
            print(stringArray)
            myvalues.append(stringArray[0])

        }
            
        
//        while (self.ammountTextField.text?.hasPrefix("0"))! {
//           self.ammountTextField.text?.remove(at: (self.ammountTextField.text?.startIndex)!)
//        }
        
        
        while (self.signUp2View.txtContactNumber.text?.hasPrefix("0"))! {
            self.signUp2View.txtContactNumber.text?.remove(at: (self.signUp2View.txtContactNumber.text?.startIndex)!)
        }
        
        
        
        
        let parms =  ["Email":myvalues[3],"Password":myvalues[4],"FirstName":myvalues[0],"MiddleName":myvalues[1],"LastName":myvalues[2],"Phone":"\((self.signUp2View.txtCode.text)!)\((self.signUp2View.txtContactNumber.text)!)","CountryIsoCode":"GBR"]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.RegisterUser, params:parms , message: "Loading..."){result in
            
//            let stringresult = "\(result.components(separatedBy: "}}")[0])}}"
            
            
            let loginResponse = AppUser(JSONString:result)

            if loginResponse != nil {
                
                if loginResponse?.myAppResult?.Code == 0{
                    UserDefaults.standard.set(myvalues[3], forKey: "Email")
                    UserDefaults.standard.set(myvalues[4], forKey: "Password")
                    self.navigationController?.pushViewController(DashboardViewController(), animated: true)
                }else if loginResponse?.myAppResult?.Code == 200{
                    self.uc.errorSuccessAler("",(loginResponse?.myAppResult!.Message)! , self)
                }else if loginResponse?.myAppResult?.Code == 223 {
                    self.uc.errorSuccessAler("",(loginResponse?.myAppResult!.Message)! , self)
                }else if loginResponse?.myAppResult?.Code == 104{
                    self.uc.errorSuccessAler("Alert", "Please provide valid phone number with full country dialing code.", self)
                }else if loginResponse?.myAppResult?.Code == 219{
                    self.uc.errorSuccessAler("Alert", "This Phone # is already associated with another account.", self)
                }
            }
            else{
                
                if(loginResponse == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
//                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", (loginResponse!.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    
    
    func GetSendingCountryList(){
        
        let parms =  ["Type":"1"]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading..."){result in
            
            self.CountryListingResponce = CountryListresult(JSONString:result)
            
            if self.CountryListingResponce?.myAppResult?.Code == 0 {
                
                
                for i in 0 ..< (self.CountryListingResponce?.CountryList!.count)! {
                    
                    self.Countries.append((self.CountryListingResponce?.CountryList![i].CountryName)!)
                   
                    
                }
                self.CountryDropDown.dataSource = self.Countries
                self.CountryDropDown.reloadAllComponents()
                
                
            }else if self.CountryListingResponce?.myAppResult?.Code == 101 {
                
                
                
            }else{
                
                if(self.CountryListingResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.CountryListingResponce?.myAppResult?.Message)!, self)
                }
            }
            
        }
        
        
    }
    
    
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonActionSend))
        done.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.signUp2View.txtContactNumber.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonActionSend(){
        
        self.signUp2View.txtContactNumber.resignFirstResponder()
    }
}
