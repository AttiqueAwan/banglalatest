//
//  CreateTransStep3DataSource.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 05/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension CreateTransStep3ViewController{
    
    
    func GetSendingPurpose(listType:String){
        
        let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"AppID":ApiUrls.AppID,"ListType":listType]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetLists, params:parms , message: "Loading..."){result in
            
            if(result == "fail")
            {
                self.GetSendingPurpose(listType: listType)
                return
            }
            
            self.SendingPurposeList = ListingTypesresult(JSONString:result)
            
            
            if  self.SendingPurposeList?.myAppResult?.Code == 0 {
                
                for i in 0..<(self.SendingPurposeList.ListType?.count)!{
                    if(listType == "SOURCEOFFUND")
                    {
                        self.arraySourceOfFunds.append(self.SendingPurposeList.ListType![i].Text!)
                        
                    }else {
                        self.arrayPurposeOfRemittance.append(self.SendingPurposeList.ListType![i].Text!)
                    }
                    
                }
                
                if(listType == "SOURCEOFFUND" && self.arraySourceOfFunds.count > 0){
//                    self.createTransStep3View.lblSourceofFund.text = self.arraySourceOfFunds[0]
                    self.dropDownSourcOfFunds.dataSource = self.arraySourceOfFunds
                    self.dropDownSourcOfFunds.reloadAllComponents()
                    self.GetSendingPurpose(listType: "SENDINGPURPOSE")
                    
                }else if(self.arrayPurposeOfRemittance.count > 0){
                    
//                    self.createTransStep3View.lblPurposeofRemitance.text = self.arrayPurposeOfRemittance[0]
                    self.dropDownPuroseOfRemittance.dataSource = self.arrayPurposeOfRemittance
                    self.dropDownPuroseOfRemittance.reloadAllComponents()
                }
                
               
                
            }else if  self.SendingPurposeList?.myAppResult?.Code == 102{
                
                self.uc.errorSuccessAler("", "Sending purpose not found", self)
                
            }else{
                
                if( self.SendingPurposeList?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( self.SendingPurposeList?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
        
    }
    
    func createTransaction(_ apiURL:String){
        var digitString1 = String()
        var payin = String()
        let serviceDigit = self.createTransStep3View.lblReceivingAmount!.text!.components(separatedBy: " ")
        let payDigit = self.createTransStep3View.lblSendingAmount!.text!.components(separatedBy: " ")
                                        if serviceDigit.count > 0 {
                                            if serviceDigit.count == 4{
                                                digitString1 = serviceDigit[1]
                                                
                                            }else{
                                                digitString1 = serviceDigit[0]
                                               
                                            }
                                        }
                                      if payDigit.count > 0 {
                                            if payDigit.count == 4{
                                                payin = payDigit[1]
                                                
                                            }else{
                                                payin = payDigit[0]
                                               
                                            }
                                        }
        
       let authToken = uc.getAuthToken()
        let parms =  ["Token": authToken!.authToken!,
                      "ID":authToken!.userId!,
                      "BeneID":BeneDetails.BeneID!,
                      "DeviceType":ApiUrls.DeviceType,
                      "SendingReason":"Family/Remitance",
                      "PaymentMethod":(self.parms["PaymentMethod"] as? String)!,
//                      "SendingPaymentMethod":"Debit Card",
                     "SendingPaymentMethod":self.createTransStep3View.txtPaymentMethod.text!,
                      "PayOutBranchCode":(self.parms["PayOutBranchCode"] as? Int)!,
                      "PayOutCurrencyCode":(self.parms["ReceivingCurrencyCode"] as? String)!,
                      "PayInCurrencyCode":(self.parms["SendingCurrencyCode"] as? String)!,
                      "PromoCode": self.parms["PromoCode"] as! String,
                      "PayerID" : self.parms["PayerID"] as! Int,
                      "ServiceCharges" : self.parms["Service_Fee"] as! String,
                      "PayInAmount": Int(payin)! ,
                      "Total_Amount": self.createTransStep3View.lblTotalAmount!.text!,
                      "PayOutAmount": self.parms["PayOutAmount"]!//receivingAmount
                      
            ] as [String:Any]
        
       
        uc.webServicePosthttp(urlString: apiURL, params:parms , message: "Loading..."){result in
            
            self.TransactionResponse = TransactionResult(JSONString:result)
            
            if  self.TransactionResponse?.myAppResult?.Code == 0 {
                
                if (self.createTransStep3View.txtPaymentMethod.text! == "Bank") {
                    
                    let bankDepositDetail = BankDepositDetailVC()
                    
                    let myDouble = Double(self.TransactionResponse!.transactionDetail!.PayInAmount!)
                    let serviceCharging = Double(self.TransactionResponse!.transactionDetail!.ServiceCharges!)
                    let countryCurrency = self.TransactionResponse!.transactionDetail!.SendingCurrency!
                    let total = myDouble! + serviceCharging!
                    
//                    let doubleStr = String(format: "%.2f", toDouble!)
                    
                    let totatUp2value = String(format: "%.2f", total)
                    
                    bankDepositDetail.sendingAmount = "\(totatUp2value) \(countryCurrency)"
                    bankDepositDetail.referencNumber = self.TransactionResponse!.transactionDetail!.PaymentNumber!
                    
                    bankDepositDetail.parms = self.parms
                    
                    
                    self.navigationController?.pushViewController(bankDepositDetail, animated: true)
                    
                    
                }else {
                    let url = self.TransactionResponse!.transactionDetail!.PaymentPageURL!
                    let paymentId = self.TransactionResponse!.transactionDetail!.PaymentID
                    let viewControllerstep4 = CreateTransStep4ViewController()
                    viewControllerstep4.weburl = "\(url)?id=\(String(describing: paymentId!))"
                    
                    self.navigationController?.pushViewController(viewControllerstep4, animated: true)
                }
                
               
                //self.navigationController?.popViewController(animated: true)
                
            }else if  self.TransactionResponse?.myAppResult?.Code == 101 {

            }else{
                
                if( self.TransactionResponse?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( self.TransactionResponse?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
    }
    
    func getSourceRemitance(){
        
        let authToken = uc.getAuthToken()
        
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"ListType":"DOCUMENTTYPES"]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetLists, params:parms , message: "Loading..."){result in
            
            self.TypeList = ListingTypesresult(JSONString:result)
            
            if  self.TypeList?.myAppResult?.Code == 0 {
                
//                for i in 0..<(self.DocumentTypeList.ListType?.count)!{
//
//                    self.DocumentType.append(self.DocumentTypeList.ListType![i].Text!)
//                }
//
//                self.DocumentTypeDropDown.dataSource = self.DocumentType
//                self.DocumentTypeDropDown.reloadAllComponents()
                
            }else if  self.TypeList?.myAppResult?.Code == 102{
                
                
                
            }else{
                
                if( self.TypeList?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( self.TypeList?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
        
    }
    
    func getSendingMethod(){
            
        let authToken = uc.getAuthToken()
        let parms =  ["Token":authToken!.authToken!,"ID":(authToken?.userId)!,"CountryIso3Code":uc.getUserInfo()!.UserInfo!.countryIsoCode!]as [String : Any]
            
            uc.webServicePosthttp(urlString: ApiUrls.GetSendingMethod, params:parms , message: "Loading..."){result in
                
                print(result)
                self.SendingMethodResultType = SendingMethodResult(JSONString:result)
                
                if  self.SendingMethodResultType?.myAppResult?.Code == 0 {
                    
                    if(self.SendingMethodResultType.SendingMehodList!.count == 0)
                    {
                        return
                    }
                    for i in 0..<(self.SendingMethodResultType.SendingMehodList!.count){
                    
                        self.arrayPaymentMethod.append(self.SendingMethodResultType.SendingMehodList![i].PaymentMethodCode!)
                    }
    
                    
                    if (self.arrayPaymentMethod.count > 0 ) {
//                        self.createTransStep3View.txtPaymentMethod.text = self.arrayPaymentMethod[0]
                    }
                    
                    self.dropDownPaymentMethod.dataSource = self.arrayPaymentMethod
                    self.dropDownPaymentMethod.reloadAllComponents()
                    
                }else if  self.SendingMethodResultType?.myAppResult?.Code == 102{
                    
                    
                    
                }else{
                    
                    if( self.SendingMethodResultType?.myAppResult?.Message == nil){
                        
                        self.uc.errorSuccessAler("Error", result, self)
                        
                    }else{
                        
                        self.uc.errorSuccessAler("Error", ( self.SendingMethodResultType?.myAppResult?.Message)!, self)
                    }
                    
                }
                
            }
            
            
        }
    
    //Label/image animation and view round
    func lblAnimation(){
        UIView.animate(withDuration: 1, delay: 0, options: [.repeat,.autoreverse], animations: {
            self.createTransStep3View.lblIncentive.alpha = 0.0
        }, completion: nil)
        //eye image
        UIView.animate(withDuration: 1, delay: 0, options: [.repeat,.autoreverse], animations: {
            self.createTransStep3View.imgEye.alpha = 0.0
        }, completion: nil)
        

    }
    
    //Alert on Edit button click
    
    func promptForAnswer() {
        let ac = UIAlertController(title: "Enter answer", message: nil, preferredStyle: .alert)
        ac.addTextField()

        let submitAction = UIAlertAction(title: "Submit", style: .default) { [unowned ac] _ in
            _ = ac.textFields![0]
            // do something interesting with "answer" here
        }

        ac.addAction(submitAction)

        present(ac, animated: true)
    }
    
    func GetServiceCharges(_ PayerId:Int){
        
        let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"PayerID":self.parms["PayerID"]!,"Amount":self.createTransStep3View.txtSendingEditAlert.text!]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetServiceCharges, params:parms , message: "Loading..."){result in
            
            self.ServiceChargesType = ServiceChargesResult(JSONString:result)
            
            if self.ServiceChargesType.myAppResult?.Code == 0 {
                
                self.createTransStep3View.lblTransferFee.text = " \(Double(self.ServiceChargesType.ServiceCharges!.Charges!))  \((self.uc.getUserInfo()?.UserInfo?.currencyIsoCode)!)"
                
                self.parms["Service_Fee"] = self.createTransStep3View.lblTransferFee.text!
                
            }else if self.ServiceChargesType.myAppResult?.Code == 101 {
                self.uc.logout(self)
            }else {
                if self.ServiceChargesType.myAppResult?.Message == nil {
                     self.uc.errorSuccessAler("Error", result, self)
                }else {
                    self.uc.errorSuccessAler("Error", (self.ServiceChargesType.myAppResult?.Message)!, self)
                }
            }
            
            
            
//            if self.PayerBranchList?.myAppResult?.Code == 0 {
//
////                self.createTransStep3View.txtReceivingEditAlert.text =
//
//            }else if self.ServiceCharges?.myAppResult?.Code == 101 {
//
//                self.uc.logout(self)
//
//            }else{
//
//                if(self.ServiceCharges.myAppResult?.Message == nil){
//
//                    self.uc.errorSuccessAler("Error", result, self)
//
//                }else{
//                    self.uc.errorSuccessAler("Error", (self.ServiceCharges.myAppResult?.Message)!, self)
//                }
//            }
            
        }
    }
    
    func GetServiceChargesofpromo(_ PayerId : Int,_ promoCode : String ){
            self.editAlertView.isHidden = true
            let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"PayerID":self.parms["PayerID"]!,"Amount":self.createTransStep3View.txtSendingEditAlert.text!,"PromoCode" : promoCode]as [String : Any]
            
            uc.webServicePosthttp(urlString: ApiUrls.GetServiceCharges, params:parms , message: "Applying Discount Code..."){result in
                
                self.ServiceChargesType = ServiceChargesResult(JSONString:result)
                
                if self.ServiceChargesType.myAppResult?.Code == 0 {
                    //self.createTransStep3View.txtSendingEditAlert.text
                    
                    if let sendingAmount = self.createTransStep3View.lblSendingAmount.text,let serviceCharges = self.parms["Service_Fee"] as? String {
                                var digitString = String()
                                var countryCode = String()
                                var sendam = String()
                        
                        
                        
                                 let sendamount = sendingAmount.components(separatedBy: " ")
                                sendam = sendamount[0]
                                let serviceDigit = serviceCharges.components(separatedBy: " ")
                                if serviceDigit.count > 0 {
                                    if serviceDigit.count == 4{
                                        digitString = serviceDigit[1]
                                        countryCode = serviceDigit[3]
                                    }else{
                                        digitString = serviceDigit[0]
                                        countryCode = serviceDigit[1]
                                    }
                                }
                        let totalAmount = "\(Double(sendam)! + Double(digitString)! - Double(self.ServiceChargesType.ServiceCharges!.DiscountAmount!))"
                          
                             
                               
                        
                                let toDouble = Double(totalAmount)
                                let doubleStr = String(format: "%.2f", toDouble!)
                                
                                self.createTransStep3View.lblTotalAmount.text = "\(doubleStr) \(countryCode)"
                                //            self.createTransStep3View.lblTotalAmount.text = "\(doubleStr) \(serviceDigit[0])" //totalAmount
                         //self.parms["Service_Fee"] = "\(self.ServiceChargesType.ServiceCharges!.Charges!) GBP"
                                
                            }
                    
                    self.createTransStep3View.lblTransferFee!.text! = "\(self.ServiceChargesType.ServiceCharges!.Charges!) GBP"
                    self.parms["PromoCode"] = promoCode
                    
                    
                    
                }else if self.ServiceChargesType.myAppResult?.Code == 101 {
                    self.uc.logout(self)
                }else {
                    self.editAlertView.isHidden = true
                    if self.ServiceChargesType.myAppResult?.Message == nil {
                         self.uc.errorSuccessAler("Error", result, self)
                    }else {
                        self.uc.errorSuccessAler("Error", (self.ServiceChargesType.myAppResult?.Message)!, self)
                    }
                }
                
                
                
    
            }
        }
    
    func getRecipientList(){
        
        let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetRecipientList, params:parms , message: "Loading..."){result in
            
            let RecipientList:RecipientListresult  = RecipientListresult(JSONString:result)!
            
            if  RecipientList.myAppResult?.Code == 0 {
                
                if((self.parms["BeneId"] != nil) )
                {
                    for item in RecipientList.RecipientList! {
                    
                        if((self.parms["BeneId"] as! Int) == item.BeneID)
                        {
                            self.BeneDetails = item
                            break
                        }
                    }
                    self.fillTransactionDetails()
                    
                }
            }
        }
        
    }
}


extension CreateTransStep3ViewController:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    @objc func AddingValues(_ textField:UITextField){
        
        if textField == self.createTransStep3View.txtSendingEditAlert {
            
            let checkamount = Double(textField.text!)!
            let maxamount =    Double(String(format:"%0.2f", self.MaximumAmount/self.ExchangeRates))!
            
            if (checkamount <= maxamount){
                let cacValue = String(format:"%0.2f",(Double(self.createTransStep3View.txtSendingEditAlert.text!)!*self.ExchangeRates))
                let cacValue1 = cacValue.components(separatedBy: ".")
                
                self.createTransStep3View.txtReceivingEditAlert.text = cacValue1[0]
                
            }else{
                
            }
            
            if !(self.createTransStep3View.txtSendingEditAlert.text?.isEmpty)!{
                
            }
            
        }else if textField == self.createTransStep3View.txtReceivingEditAlert {
            
            if ((Double(textField.text!)!) <=  self.MaximumAmount){
                
                self.createTransStep3View.txtSendingEditAlert.text = String(format:"%0.2f", (Double( self.createTransStep3View.txtReceivingEditAlert.text!)! / self.ExchangeRates))
            }
            
            
            if !(self.createTransStep3View.txtSendingEditAlert.text?.isEmpty)!{
            }
            
            
        }
        self.GetServiceCharges(self.parms["PayerID"] as! Int)
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var checkval = 0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            
            if !((textField.text?.isEmpty)!){

                if(checkval == 0){
                    
                    self.AddingValues(textField)
                }
            }
        })
        
        if(string == ""){
            
            if(textField.text?.count == 1){
                
                self.createTransStep3View.txtReceivingEditAlert.text = ""
                self.createTransStep3View.txtSendingEditAlert.text = ""
                
            }else{
                // self.GetServiceCharges()
            }
            
        }else{
            
            if textField == self.createTransStep3View.txtSendingEditAlert {
                
                let checkamount = Double(textField.text!+string)!
                let maxamount =  Double(String(format:"%0.2f", self.MaximumAmount/self.ExchangeRates))!
                if !(checkamount <= maxamount){
                    checkval = 1
                    return false
                }
            }else if textField == self.createTransStep3View.txtReceivingEditAlert {
                
                if textField.text == nil || textField.text == "" {
                    return false
                }
                
                if !((Double(textField.text!+string)!) <=  self.MaximumAmount){
                    checkval = 1
                    return false
                }
            }
        }
        
        return true
    }
}
