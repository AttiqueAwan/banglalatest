//
//  CreateTransStep3View.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 05/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class CreateTransStep3View: UIView {

    //Button Back
    @IBOutlet weak var btnBack: UIButton!
    
    //TopView Slider
    @IBOutlet weak var TopView: UIView!
    
    //Button Next
    @IBOutlet weak var btnNext: UIButton!
    
    //Source of Funds View
    @IBOutlet weak var SourceofFundView: UIView!
    
    //Purpose of Funds View
    @IBOutlet weak var purposeOfFundView: UIView!
    
    //Label Sending Amount
    @IBOutlet weak var lblSendingAmount: UILabel!
    
    //Label Receiving Amount
    @IBOutlet weak var lblReceivingAmount: UILabel!
    
    //Label Receiver Name
    @IBOutlet weak var lblReceiverName: UILabel!
    
    //Btn Receiver Name
    @IBOutlet weak var btnReceiverName: UIButton!
    
    //Label Payer Name
    @IBOutlet weak var lblPayerName: UILabel!
    
    //Label Bank Name / Payer Name
    @IBOutlet weak var lblBankName: UILabel!
    
    
    
    //Label Payout Branch Name
    @IBOutlet weak var lblPayoutBranchName: UILabel!
    
    //Label Branch Name / Payout Branch Name
    @IBOutlet weak var lblBranchName: UILabel!
    
    //Label Phone Number
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    //Label Account Number / Phone Number
    @IBOutlet weak var lblAccountNumber: UILabel!
    
    //Label Payout Branch Code
    @IBOutlet weak var lblPayoutBranchCode: UILabel!
    
    
    //Label Bank Branch Code / Payout Branch Code
    @IBOutlet weak var lblBranchCode: UILabel!
    
    //Label Transfer Fee
    @IBOutlet weak var lblTransferFee: UILabel!
    
    //Label Total Amount
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    //Btn Source of Fund
    @IBOutlet weak var btnSourceofFund: UIButton!
    
    //Label Source of Fund
    @IBOutlet weak var lblSourceofFund: UITextField!
    
    
    //Btn Purpose of Remitance
    @IBOutlet weak var btnPurposeofRemitance: UIButton!
    
    
    @IBOutlet var lblPromoCode: JVFloatLabeledTextField!
    
    //Label Purpose of Remitance
    @IBOutlet weak var lblPurposeofRemitance: UITextField!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    // modify
    
//    @IBOutlet weak var lblIncentiveIncluded: UILabel!
    
    @IBOutlet weak var imgEye: UIImageView!
    
    @IBOutlet weak var btnPaymentMethod: UIButton!
    @IBOutlet weak var viewPaymentMethod: UIView!
    
    @IBOutlet weak var txtPaymentMethod: UITextField!
    
    //Edid Alert view outlets
    
    @IBOutlet weak var txtSendingEditAlert: UITextField!
    
    @IBOutlet weak var txtReceivingEditAlert: UITextField!
    
    //Start ViewSummaryAlert outlets
    //1
    @IBOutlet weak var imgViewViewSummaryAlert: UIImageView!

    @IBOutlet weak var lblCountryViewSummaryAlert: UILabel!
    
    //2
    @IBOutlet weak var lblReceiverNameViewSummaryAlert: UILabel!
    
    //3
    
    @IBOutlet weak var lblPaymentMethodViewSummaryAlert: UILabel!
    
    //4
    @IBOutlet weak var lblPhoneViewSummaryAlert: UILabel!
    
    //5
    @IBOutlet weak var lblPayoutBankViewSummaryAlert: UILabel!
    
    //6
    @IBOutlet weak var lblPayoutBranchViewSummaryAlert: UILabel!
    
    //7
    @IBOutlet weak var lblRelationWithSenderViewSummaryAlert: UILabel!
    

    
    @IBOutlet weak var lblAccountNoViewSummaryAlert: UILabel!
    
    @IBOutlet weak var viewAccountNoViewSummaryAlert: UIView!
    @IBOutlet weak var payoutBranckheading: UILabel!
    @IBOutlet weak var viewSummaryAlertPayoutBank: UIView!
    
    @IBOutlet weak var lblExchangeRate: UILabel!
 
    //End ViewSummaryAlert outlets
    
    @IBOutlet weak var lblPaymentMethod: UILabel!
    
    @IBOutlet weak var lblIncentive: UILabel!
    
    
    
    @IBOutlet weak var txtViewDepenPaymentMethod: UITextView!
}
