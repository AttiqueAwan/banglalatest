//
//  CreateTransStep3ViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 05/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

//step2.parms = ["PayerID":self.PayerId,
//               "MaxAmountLimit": self.MaximumAmount,
//               "PaymentMethod":self.Payments,
//               "BranchCode":self.BranchCode,
//               "BranchName":self.BranchName,
//               "SendingAmount":self.createTransactionView.txtSending.text!,"ReceivingAmount":self.createTransactionView.txtReceiving.text!,
//               "Service_Fee":(self.createTransactionView.lblSendingFee.text?.components(separatedBy: "Sending Fee: ")[1])!,
//               "Exchange_Rate":self.ExchangeRates,
//               "Total_Amount":(self.createTransactionView.lblTotalAmount.titleLabel?.text)!,
//               "Receiving_Country":(self.createTransactionView.lblCountry.text)!,
//               "SendingCurrencyCode":(self.createTransactionView.lblSendingCurrencyCode.text)!,
//               "ReceivingCurrencyCode":(self.createTransactionView.lblReceivingCurrencyCode.text)!]



import UIKit
import DropDown

class CreateTransStep3ViewController: UIViewController,ModalViewControllerDelegate {
    
    
    
    @IBOutlet var createTransStep3View: CreateTransStep3View!
    
    @IBOutlet var editAlertView: CreateTransStep3View!
    
    @IBOutlet var viewSummaryAlert: CreateTransStep3View!
    
    var Topmenu = TopViewController()
    let roundedCorner = RoundedCorner()
    
    var parms = [String:Any]()
    var BeneDetails : RecipientListDetail!
    
    let uc = UtilitySoftTechMedia()
    
    var TransactionResponse : TransactionResult!
    var TypeList : ListingTypesresult!
    var SendingMethodResultType : SendingMethodResult!
    
    //source of funds dropdown
    let dropDownSourcOfFunds = DropDown()
    var arraySourceOfFunds = [String]()
    var arrayPurposeOfRemittance = [String]()
    
    //purose of remittance dropdown
    let dropDownPuroseOfRemittance = DropDown()
    var SendingPurposeList:ListingTypesresult!
    
    //Payment methods
    let dropDownPaymentMethod = DropDown()
    var arrayPaymentMethod = [String]()
    var transfer_fee : String?
    var MaximumAmount = 0.0
    var MinimumAmount = 0.0
    var ExchangeRates = 0.0
    var PayerId = 0
    var ServiceChargesType : ServiceChargesResult!
    var PayerBranchList : BranchListresult!
    var isFirsTime = true
    var isOTPVerified = false
    
    var getValueFromselftxtReceivingEditAlert = 0.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createTransStep3View.purposeOfFundView = roundedCorner.RoundViews(self.createTransStep3View.purposeOfFundView)
        self.createTransStep3View.SourceofFundView = roundedCorner.RoundViews(self.createTransStep3View.SourceofFundView)
        self.createTransStep3View.viewPaymentMethod = roundedCorner.RoundViews(self.createTransStep3View.viewPaymentMethod)
        
        
        //Dropdown
        //source of Funds dropdown
        self.GetSendingPurpose(listType: "SOURCEOFFUND")
        self.getSendingMethod() // dropDown payment 
        //        self.GetSendingPurpose(listType: "SENDINGPURPOSE")
        
        self.dropDownSourcOfFunds.anchorView = self.createTransStep3View.btnSourceofFund
        self.dropDownSourcOfFunds.dataSource = self.arraySourceOfFunds
        dropDownSourcOfFunds.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            //            PaymentMEthodCOde = (self.PaymentMethodListResponse?.PaymentMethodList?[index].PaymentMethodCode)!
            self.createTransStep3View.lblSourceofFund.text = item
            
        }//end source of Funds dropdown
        
        //Purpose of Remittance
        self.dropDownPuroseOfRemittance.anchorView = self.createTransStep3View.btnPurposeofRemitance
        self.dropDownPuroseOfRemittance.dataSource = self.arraySourceOfFunds
        
        dropDownPuroseOfRemittance.selectionAction = {[unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.createTransStep3View.lblPurposeofRemitance.text = item
            
        } //end Purpose of Remittance
        
        //DropDown payment method
        dropDownPaymentMethod.anchorView = self.createTransStep3View.btnPaymentMethod
        self.dropDownPaymentMethod.dataSource = self.arrayPaymentMethod
        
        dropDownPaymentMethod.selectionAction = {[unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.createTransStep3View.txtPaymentMethod.text = item
            
            if item == "Debit Card" || item == "Credit Card" {
                self.createTransStep3View.txtViewDepenPaymentMethod.text = "Payments made by Debit / Credit card after 6 p.m. will be processed next day."
            }else if item == "Bank" {
               self.createTransStep3View.txtViewDepenPaymentMethod.text = "Your payment to our UK bank account must be received within 12 hours from the time of your transaction however any type of payments received after 6 pm will be processed next working day."
            }
            
        }
        
        self.addDoneButtonOnKeyboard()
        
        
    }
    
    
   
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonActionSend))
        done.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.createTransStep3View.txtSendingEditAlert.inputAccessoryView = doneToolbar
        self.createTransStep3View.txtReceivingEditAlert.inputAccessoryView = doneToolbar
        
        
    }
    
    @objc func doneButtonActionSend(){
        
        self.createTransStep3View.txtSendingEditAlert.resignFirstResponder()
        self.createTransStep3View.txtReceivingEditAlert.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if BeneDetails != nil {
            
            print(BeneDetails.BeneRelationWithSender!)
        }
        
        self.lblAnimation()
        Topmenu.view.frame = CGRect(x: 0, y: 0, width: self.createTransStep3View.TopView.frame.width, height: Topmenu.view.frame.height)
        self.createTransStep3View.TopView.addSubview(Topmenu.view)
        self.getRecipientList()
        
        //        self.fillTransactionDetails()
        
    }
    
    //Button Back Click
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    // Button on promo code apply
    @IBAction func btnPromoApply(_ sender: Any) {
        
        if createTransStep3View.lblPromoCode.text != ""{
            self.GetServiceChargesofpromo(self.parms["PayerID"] as! Int,createTransStep3View.lblPromoCode!.text!)
        }else {
            self.editAlertView.isHidden = true
            uc.errorSuccessAler("Alert", "Please Enter a valid promo code", self)
        }
        
        
         
    }
    
    
    
    
    
    //Button Next Click
    @IBAction func btnNextClick(_ sender: Any) {
        
        if (self.createTransStep3View.txtPaymentMethod.text == "Bank") {
            
        }else {
            
        }
        
        if self.createTransStep3View.lblSourceofFund.text!.isEmpty {
            uc.errorSuccessAler("Alert", "Please select Source of Funds", self)
        }else if self.createTransStep3View.lblPurposeofRemitance.text!.isEmpty {
            uc.errorSuccessAler("Alert", "Please select Purpose of Remittance", self)
        }else if self.createTransStep3View.txtPaymentMethod.text!.isEmpty {
            uc.errorSuccessAler("Alert", "Please select Choose Payment Method", self)
        }else {
            
            let OTPViewController = SendOTPViewController()
            
            OTPViewController.delegate = self
            OTPViewController.PayerID = self.parms["PayerID"] as! Int
            
            self.present(OTPViewController, animated: true, completion: nil)
        }
        
    }
    
    func fillTransactionDetails(){
        
        self.ExchangeRates = self.parms["Exchange_Rate"] as! Double
        self.MaximumAmount = self.parms["MaxAmountLimit"] as! Double
        
        if let paymentMethod = self.parms["PaymentMethod"] as? String {
            self.createTransStep3View.lblPaymentMethod.text = paymentMethod
            self.createTransStep3View.lblPaymentMethodViewSummaryAlert.text = paymentMethod
            
        }
        
        
        if let exchangeRate = self.parms["Exchange_Rate"] as? Double {
            var receivingCurrency = ""
            if let receivingCurr = self.parms["ReceivingCurrencyCode"] as? String {
                receivingCurrency = receivingCurr
            }
            
            self.createTransStep3View.lblExchangeRate.text = "\(exchangeRate) \(receivingCurrency)"
        }
        
        if let SendingAmount = self.parms["SendingAmount"] as? String , let ReceivingAmount = self.parms["ReceivingAmount"] as? String{
            let receivingAmount = ReceivingAmount
            var incentiveAmount = 0.0
            var totalWithIncentive = 0.0
            
            if let incentive = UserDefaults.standard.value(forKey: "incentiveMaximumAmount") as? Double {
                
                if Double(SendingAmount)! > incentive {
                    
                    self.createTransStep3View.lblIncentive.isHidden = true
                    totalWithIncentive = Double(receivingAmount)!
                }else {
                    self.createTransStep3View.lblIncentive.isHidden = false
                    incentiveAmount =  0.02 * Double(receivingAmount)!
                    totalWithIncentive = incentiveAmount + Double(receivingAmount)!
                    self.parms["IncentiveAmount"] = incentiveAmount
                    
                }
            }
            self.createTransStep3View.lblReceivingAmount.text = "\(Int(round(totalWithIncentive))) \((self.parms["ReceivingCurrencyCode"] as? String)!)"
            self.createTransStep3View.lblSendingAmount.text = "\((SendingAmount)) \((self.parms["SendingCurrencyCode"] as? String)!)"
            
        }
        
        
        
        if let chargesFee = self.parms["Service_Fee"] as? String {
            self.createTransStep3View.lblTransferFee.text = chargesFee
        }
        
        guard let Payment_Method = self.parms["PaymentMethod"] as? String else{
            
            return
        }
        
        if(Payment_Method == "Cash"){
            
            self.createTransStep3View.viewAccountNoViewSummaryAlert.isHidden = true
            
            //            if let paymentMethod = self.parms["PaymentMethod"] as? String {
            //                self.createTransStep3View.lblPaymentMethodViewSummaryAlert.text = paymentMethod
            //            }
            if let bankName = self.parms["PayoutLocation"] as? String {
                self.createTransStep3View.lblPayoutBankViewSummaryAlert.text = bankName
            }
            if let BranchName = self.parms["BranchName"] as? String{
                
                //                self.createTransStep3View.lblBranchName.text = BranchName
                self.createTransStep3View.lblPayoutBranchViewSummaryAlert.text = BranchName
            }
            
        }else if(Payment_Method == "Bank"){
            
            if let AccountNumber = BeneDetails.BeneAccountNumber{
                
                self.createTransStep3View.lblAccountNoViewSummaryAlert.text = AccountNumber
            }
            
            self.createTransStep3View.viewAccountNoViewSummaryAlert.isHidden = false
            
            if let bankName = BeneDetails.BeneBankName {
                self.createTransStep3View.lblPayoutBankViewSummaryAlert.text = bankName
            }
            
            if let BranchName = BeneDetails.BeneBranchName{
                
                //self.createTransStep3View.lblBranchName.text = BranchName
                self.createTransStep3View.lblPayoutBranchViewSummaryAlert.text = BranchName
                
            }
            
            //7 done
            
        }else  if(Payment_Method == "Wallet"){
            
            self.createTransStep3View.viewAccountNoViewSummaryAlert.isHidden = true
            self.createTransStep3View.viewSummaryAlertPayoutBank.isHidden = true
            self.createTransStep3View.payoutBranckheading.text = "Company Name"
            
            
            //            if let paymentMethod = self.parms["PaymentMethod"] as? String {
            //                self.createTransStep3View.lblPaymentMethodViewSummaryAlert.text = paymentMethod
            //            }
            
            if let companyName = self.parms["MobileCompanyName"] as? String{
                self.createTransStep3View.lblPayoutBranchViewSummaryAlert.text = companyName
            }
            
            
            
            
        }
        
        //2 missing name
        if let Benename = self.BeneDetails.BeneName  {
            //                self.createTransStep3View.lblBankName.text = UserName
            self.createTransStep3View.lblReceiverNameViewSummaryAlert.text = Benename
        }
        //1 missing Photo and country
        if let photo = self.parms["Receiving_Country"] as? String {
            self.createTransStep3View.imgViewViewSummaryAlert.image = UIImage(named: photo)
            self.createTransStep3View.lblCountryViewSummaryAlert.text = photo //country name
        }
        
        //7 missing relation
        if let relation = self.BeneDetails.BeneRelationWithSender {
            self.createTransStep3View.lblRelationWithSenderViewSummaryAlert.text = relation
        }
        if let PhoneNumber = self.BeneDetails.BenePhone  {
            
            //                self.createTransStep3View.lblAccountNumber.text = PhoneNumber
            self.createTransStep3View.lblPhoneViewSummaryAlert.text = PhoneNumber
        }
        
        
        if let Transfer_Fee = self.parms["Service_Fee"] as? String{
            
            self.createTransStep3View.lblTransferFee.text = Transfer_Fee
        }
        
        if let sendingAmont = self.parms["SendingAmount"] as? String,  let serviceCharges = parms["Service_Fee"] as? String {
            
            var digitString = String()
            var countryCode = String()
            let serviceDigit = serviceCharges.components(separatedBy: " ")
            //            var digitArray = [String]()
            if serviceDigit.count > 0 {
                digitString = serviceDigit[0]
                
                if serviceDigit.count > 1 {
                    countryCode = serviceDigit[1]
                }else {
                    countryCode = "GBP"
                }
            }
            
            let totalAmount = "\(Double(sendingAmont)! + Double(digitString)!)"
            let toDouble = Double(totalAmount)
            let doubleStr = String(format: "%.2f", toDouble!)
            
            self.createTransStep3View.lblTotalAmount.text = "\(doubleStr) \(countryCode)"
            //            self.createTransStep3View.lblTotalAmount.text = "\(doubleStr) \(serviceDigit[0])" //totalAmount
            
        }
        
    }
    
    // Dropdown buttons actions
    
    
    @IBAction func btnSourceOfFund_Action(_ sender: UIButton) {
        self.dropDownSourcOfFunds.show()
        print("dropDownSourcOfFunds Clicked")
    }
    
    @IBAction func btnPurposeOfRemittance_Action(_ sender: UIButton) {
        self.dropDownPuroseOfRemittance.show()
    }
    
    
    @IBAction func btnPaymentMethod_Action(_ sender: UIButton) {
        self.dropDownPaymentMethod.direction = .top
        self.dropDownPaymentMethod.show()
    }
    
    @IBAction func btnViewSummary(_ sender: UIButton) {
        
        self.createTransStep3View.addSubview(viewSummaryAlert)
        //        self.createTransStep3View.lblCountryViewSummaryAlert.text = ""
        
    }
    
    @IBAction func btnEdit(_ sender: UIButton) {
        
        //        self.editAlertView.isHidden = false
        self.editAlertView.isHidden = false
        editAlertView.frame = self.view.frame
        self.createTransStep3View.addSubview(editAlertView)
        self.addDoneButtonOnKeyboard()
        
        if isFirsTime {
            self.createTransStep3View.txtSendingEditAlert.text = parms["SendingAmount"] as? String
            self.createTransStep3View.txtReceivingEditAlert.text = parms["ReceivingAmount"] as? String
            isFirsTime = false
        }else {
            let sendingValidAmount = self.createTransStep3View.lblSendingAmount.text!.components(separatedBy: " ").first
            self.createTransStep3View.txtSendingEditAlert.text = sendingValidAmount
            
            
            let ReceivingAmount = self.createTransStep3View.lblReceivingAmount.text!
            
            
            let splitValue = ReceivingAmount.components(separatedBy: " ")
            let spiltDigit = splitValue[0]
            //            let countryCode = splitValue[1]
            
            
            let calculation = (Double(spiltDigit)! - (Double(spiltDigit)! * 0.02))
            // let MyStringValue = round(Double(self.createTransStep3View.lblReceivingAmount.text!)! - (Double(self.createTransStep3View.lblReceivingAmount.text!)! * 0.02))
            
            self.createTransStep3View.txtReceivingEditAlert.text = "\(Int(round(calculation)))" //self.createTransStep3View.lblReceivingAmount.text
            
        }
        
    }
    
    //Edit Alert Actions
    
    @IBAction func btnContinueEditAlert(_ sender: UIButton) {
        
        if self.createTransStep3View.txtSendingEditAlert.text == nil || self.createTransStep3View.txtReceivingEditAlert.text == "" {
            uc.makeToast(message: "Sending amount should not be empty")
            return
        }
        
        
        //Take Previous value Section
        self.editAlertView.removeFromSuperview()
        self.createTransStep3View.lblSendingAmount.text = "\(String(describing: self.createTransStep3View.txtSendingEditAlert.text!)) \(String(describing: self.parms["SendingCurrencyCode"]!))"
        
        //\(String(describing: self.parms["SendingCurrencyCode"]!)
        
        self.createTransStep3View.lblReceivingAmount.text = self.createTransStep3View.txtReceivingEditAlert.text
        
        let srtReceivingAmount =  self.createTransStep3View.lblReceivingAmount.text
        
        getValueFromselftxtReceivingEditAlert = Double(self.createTransStep3View.txtReceivingEditAlert.text!)!
        
        //Value calculation section
        if(srtReceivingAmount != nil && srtReceivingAmount != "")
        {
            if  let strsendingamount = self.createTransStep3View.txtSendingEditAlert!.text! as? String {
            var receivingAmount = Double(getValueFromselftxtReceivingEditAlert)
            self.parms["PayOutAmount"] = receivingAmount
                
            if let incentive = UserDefaults.standard.value(forKey: "incentiveMaximumAmount") as? Double {
                if Double(strsendingamount)! < incentive {
                    self.createTransStep3View.lblIncentive.isHidden = false
                    let incentiveAmount =  0.02 * receivingAmount
                    self.parms["IncentiveAmount"] = incentiveAmount
                    let totalWithIncentive = incentiveAmount + receivingAmount
                    self.createTransStep3View.lblReceivingAmount.text  = "\(Int(round(totalWithIncentive))) \((self.parms["ReceivingCurrencyCode"] as? String)!)"
                }else{
                    self.createTransStep3View.lblIncentive.isHidden = true
                    self.createTransStep3View.lblReceivingAmount.text  = "\(Int(round(getValueFromselftxtReceivingEditAlert))) \((self.parms["ReceivingCurrencyCode"] as? String)!)"
                }
            }
            }
            
        }
        
        self.GetServiceCharges(self.parms["PayerID"] as! Int)
        
        if let sendingAmont = self.createTransStep3View.txtSendingEditAlert.text, let serviceCharges = self.createTransStep3View.lblTransferFee.text as? String {
            
            var digitString = String()
            var countryCode = String()
            var service = String()
            let serviceDigit = serviceCharges.components(separatedBy: " ")
            //            var digitArray = [String]()
            if serviceDigit.count > 0 {
                if serviceDigit.count == 4{
                    digitString = serviceDigit[1]
                    countryCode = serviceDigit[3]
                }else{
                digitString = serviceDigit[0]
                countryCode = serviceDigit[1]
                }
            }
            let sendingAmountSeparat = sendingAmont.components(separatedBy: " ")
            let sendingDigit = sendingAmountSeparat[0]
            
            
                let totalAmount = "\(Double(sendingDigit)! + Double(digitString)!)"
                let toDouble = Double(totalAmount)
                let doubleStr = String(format: "%.2f", toDouble!)
                self.createTransStep3View.lblTotalAmount.text = "\(doubleStr) \(countryCode)"
            
            if self.createTransStep3View.lblPromoCode.text != ""{
                self.GetServiceChargesofpromo(self.parms["PayerID"] as! Int, self.createTransStep3View.lblPromoCode.text!)
            }
            
        }
        
        let sending = self.createTransStep3View.txtSendingEditAlert.text!
        let receiving = self.createTransStep3View.txtReceivingEditAlert.text!
        
        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo: ["sending":sending, "receiving":receiving])
        
        //        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo: ["Renish":"Dadhaniya"])
        
        
    }
    
    //Start ViewSummaryAlert Actions
    
    @IBAction func btnCloseViewSummaryAlert(_ sender: UIButton) {
        self.viewSummaryAlert.removeFromSuperview()
        
    }
    
    @IBAction func btnEditViewSummaryAlert(_ sender: UIButton) {
        
        
        
        //        self.navigationController?.pushViewController(addRecipientViewController, animated: true)
        
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                
                // if Controller is not in navigation then will execute else part.
                if vc.isKind(of: AddRecipientViewController.self)  {
                    print("It is in stack")
                    //Your Process
                    let addRecipientViewController = vc as? AddRecipientViewController
                    addRecipientViewController?.UpdateRecipientDetails = BeneDetails
                    addRecipientViewController?.addRecipientView.btnPaymentMethod.isUserInteractionEnabled = false
                    addRecipientViewController?.addRecipientView.btnPaymentMethod.isEnabled = false
    //                    self.navigationController?.pushViewController(vc, animated: true)
                    navigationController?.popToViewController(addRecipientViewController!, animated: true)
                    
                    self.viewSummaryAlert.removeFromSuperview()
                    return
                    
                }
            }
        }
        
        let addRecipientViewController = AddRecipientViewController()
        addRecipientViewController.fromtrans = "dashboard"
        addRecipientViewController.UpdateRecipientDetails = BeneDetails
        navigationController?.pushViewController(addRecipientViewController, animated: true)
        
        self.viewSummaryAlert.removeFromSuperview()
        
    }
    
    func sendData(isOTPVerified: Bool) {
        if(isOTPVerified)
        {
            self.createTransaction(ApiUrls.CreateTransaction)
        }
    }
}
