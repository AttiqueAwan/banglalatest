//
//  CreateTransStep1View.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 05/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit
import JVFloatLabeledTextField

class  CreateTransStep1View: UIView {
    
    @IBOutlet var CashPayoutHeight: NSLayoutConstraint!
    @IBOutlet var ViewStyleHeight: NSLayoutConstraint!
    
    //Top Statrus View
      @IBOutlet weak var topView: UIView!
    
    //PayerList CollectionView
    @IBOutlet weak var PayerListCollectionView:UICollectionView!
    
    //Button Back
    @IBOutlet weak var btnBack: UIButton!
    
    //Label Country
    @IBOutlet weak var lblCountry:UILabel!
    
    //Button Country
    @IBOutlet weak var btnCountry:UIButton!
    
    //Image Country
    @IBOutlet weak var imgCOuntry:UIImageView!
    
    //Label Tite
    @IBOutlet weak var lbklTitle: UILabel!
    
    //Sending Amount Textfield
    @IBOutlet weak var txtSending: JVFloatLabeledTextField!
   
    //Receiving Amount TextField
    @IBOutlet weak var txtReceiving: JVFloatLabeledTextField!
    
    //Button Sending Country
    @IBOutlet weak var btnSendingCountry: UIButton!
    
    //Button Receiving Country
    @IBOutlet weak var btnReceivingCountry: UIButton!
    
    //Image Sending Country
    @IBOutlet weak var imgSendingCountry: UIImageView!
    
    
    //Label Sending Currency
    @IBOutlet weak var lblSendingCurrencyCode: UILabel!
    
    //Image Receiving Country
    @IBOutlet weak var imgReceivingCountry: UIImageView!
    
    //Label Receiving Currency COde
    @IBOutlet weak var lblReceivingCurrencyCode: UILabel!
    
    
    //Label Exchange Rate
    @IBOutlet weak var lblExchangeRate: UILabel!
    
    //Button Cash
    @IBOutlet weak var btnCash: UIButton!
    
    //Button Account
    @IBOutlet weak var btnAccount: UIButton!
    
    //button wallet
    @IBOutlet weak var btnWallet: UIButtonStyle!
    
    //Label Sending Amount
    @IBOutlet weak var lblSendingAmount: UILabel!
    
    //Label Sending Fee
    @IBOutlet weak var lblSendingFee: UILabel!
    
    //Label Total Amount
    @IBOutlet weak var lblTotalAmount: UIButton!
    
    //Button Select Recipient
    @IBOutlet weak var btnSelectRecipient: UIButton!
    
    //Modify 26 feb 2020
    // Cash pay out bank sections
    
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var imgOnClickToSelectBank: UIImageView!
    @IBOutlet weak var btnShowTableView: UIButton!
    
    //TableView PopUp
    
    @IBOutlet weak var tblViewPopUp: UITableView!
    
    @IBOutlet weak var stackViewSendAndReceive: UIStackView!
    
    @IBOutlet weak var viewSending: UIView!
    
    @IBOutlet weak var viewReceive: UIView!
    
    //change tableview controller from there to here
    
    
    @IBOutlet weak var tblViewAsPopUp: UITableView!
    
    @IBOutlet weak var viewTableView: UIView!
    
    @IBOutlet weak var tblView3rdone: UITableView!
    
    
    @IBOutlet weak var viewImageFade: ViewStyle!
    
    @IBOutlet weak var viewDame: UIView!
    
    @IBOutlet weak var lblIncentive: UILabel!
    
    @IBOutlet weak var lblCashPayoutCaption: UILabel!
    
    @IBOutlet weak var lblClickToSelectBank: UILableX!
    
}
