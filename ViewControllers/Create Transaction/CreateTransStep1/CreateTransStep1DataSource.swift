//
//  CreateTransStep1DataSource.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 05/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension CreateTransStep1ViewController: protocolBankName {
    func bankName(bankName: String) {
    
        self.createTransactionView.lblBankName.text = bankName
    }
}

extension CreateTransStep1ViewController: UITableViewDataSource,UITableViewDelegate{
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.CountryPayerListResponce != nil){
            
            if self.CountryPayerListResponce.PayerList != nil{
                
                return (self.CountryPayerListResponce.PayerList?.count)!
            }
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BankListCell", for: indexPath) as? BankListCell

       
        
        
        cell?.lblBankNameM.text = "\((self.CountryPayerListResponce.PayerList![indexPath.row].PayerName)!)"
        cell?.lblRateM.text = "\((self.CountryPayerListResponce.PayerList![indexPath.row].ExchangeRate)!)"
        cell?.lblReceivingCountryIsoCode.text = "\((self.CountryPayerListResponce.PayerList![indexPath.row].CurrencyIsoCode)!)"

        if(indexPath.row == self.SelectedRow){

        }else{

            
        }
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let bankName = "\((self.CountryPayerListResponce.PayerList![indexPath.row].PayerName)!)"
        let payerId = (self.CountryPayerListResponce.PayerList![indexPath.row].PayerId)!
        self.PayerBranchList(payerId)
        self.PayerName = bankName
        self.createTransactionView.lblBankName.text = bankName
        self.createTransactionView.viewTableView.isHidden = true
//        self.createTransactionView.alpha = 1.0

    }
    

    func GetPayerImage(_ details:PayerListDetail) -> UIImage {
        
        
        if(details.PayerName == "Al-Arafah Islami Bank Ltd"){
            
            return #imageLiteral(resourceName: "Al-Arafah Islami Bank Ltd")
            
        }else if(details.PayerName == "BANK TRANSFER-BANGLADESH"){
            
            return #imageLiteral(resourceName: "bank_logo.png")
            
        }else if(details.PayerName == "Uttara Bank Ltd"){
            
            return #imageLiteral(resourceName: "Uttara_Bank_Ltd")
            
        }else if(details.PayerName == "Social Islami Bank Ltd"){
            
            return UIImage(named: "Social_Islami_Bank_Ltd.png")!
            
        }else if(details.PayerName == "Premier Bank Ltd"){
            
            
            return UIImage(named:"Premier_Bank_Ltd.png")!
            
        }else if(details.PayerName == "South East Bank Ltd"){
            
            
            return UIImage(named:"South_East_Bank_Ltd.png")!
            
        }else if(details.PayerName == "South Bangla Agricultural and Commerce Bank Ltd "){
            
            
            return UIImage(named:"South_Bangla_Agricultural_and_Commerce_Bank_Ltd.png")!
            
            
        }else if(details.PayerName == "National Credit & Commerce Bank Ltd "){
            
            
            return UIImage(named:"National_Credit_&_Commerce_Bank_Ltd.png")!
            
            
        }else if(details.PayerName == "Jamuna Bank Ltd"){
            
            return UIImage(named:"Jamuna_Bank_Ltd.png")!
            
        }else{
            
            return UIImage(named: "bank_logo.png")!
        }
        
    }
    
    
}

extension CreateTransStep1ViewController{
    
//Receiving CountryList
func GetReceivingCountryList(){
    
    let parms =  ["Type":"2"]as [String : Any]
    
    uc.webServicePosthttp(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading..."){result in
        
        let CountryListingResponce = CountryListresult(JSONString:result)
        
        if CountryListingResponce?.myAppResult?.Code == 0 {
            
            self.ReceivingCountryList =  CountryListresult(JSONString:result)
            
            for i in 0 ..< (self.ReceivingCountryList?.CountryList!.count)!{
                
                self.Countries.append((self.ReceivingCountryList?.CountryList![i].CountryName)!)
                self.CountriesISO3Code.append((self.ReceivingCountryList?.CountryList![i].CurrencyIsoCode)!)
            }
            
            self.createTransactionView.imgCOuntry.image = UIImage(named: "\((self.ReceivingCountryList?.CountryList![0].CountryName)!).png")
            self.createTransactionView.lblCountry.text = (self.ReceivingCountryList?.CountryList![0].CountryName)!
            self.createTransactionView.lblReceivingCurrencyCode.text = self.ReceivingCountryList.CountryList![0].CurrencyIsoCode
            self.createTransactionView.imgReceivingCountry.image = UIImage(named: "\(self.ReceivingCountryList.CountryList![0].CountryName!).png")
            self.countryindex = 0
            
            self.receivingCountryDropDown.dataSource = self.Countries
            
            self.receivingCountryDropDown.reloadAllComponents()
            
            if(self.ReceivingCountryList != nil){
                
                if((self.ReceivingCountryList.CountryList?.count)! > 0){
                    
                    DispatchQueue.main.async {
                       
                        self.getCountryPayerList(self.Payments)
                        print(self.Payments)
                    }
                    
                }
            }
            

        }else if CountryListingResponce?.myAppResult?.Code == 101 {
            
            
        }else{
            
            if(CountryListingResponce?.myAppResult?.Message == nil){
                
                self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                
            }else{
                
                self.uc.errorSuccessAler("Error", (CountryListingResponce?.myAppResult?.Message)!, self)
            }
            
        }
    }
}



func getCountryPayerList(_ PaymentType:String){
    
    
    let authToken = uc.getAuthToken()
    let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"ReceivingCountryIso3Code":(self.ReceivingCountryList.CountryList![self.countryindex].Iso3Code)!,
                  "PaymentMethod":PaymentType]as [String : Any]
    
    uc.webServicePosthttp(urlString: ApiUrls.GetCountryPayerList, params:parms , message: "Loading..."){result in
        
        self.CountryPayerListResponce = PayerListresult(JSONString:result)
        
        if self.CountryPayerListResponce?.myAppResult?.Code == 0 {
            
            
            
            
            var k = 0 , j = 0
            
            DispatchQueue.main.async {
                
                for i in 0..<(self.CountryPayerListResponce.PayerList?.count)!{
                    
                    if self.CountryPayerListResponce.PayerList?.count == 1{
                        self.createTransactionView.lblClickToSelectBank.text = self.CountryPayerListResponce.PayerList![i].PayerName
                        self.createTransactionView.btnShowTableView.isEnabled = false
                    }else {
                        self.createTransactionView.btnShowTableView.isEnabled = true
                        self.createTransactionView.lblClickToSelectBank.text = "CLICK TO SELECT BANK"
                    }
  
                    if(k==1 && j == 1){
                        
                        break
                    }
                    
                    if (i == 0) {
                        self.createTransactionView.lblBankName.text = self.CountryPayerListResponce.PayerList![i].PayerName
                        
                    }
                    
                    if(self.CountryPayerListResponce.PayerList![i].PaymentMethod == "Cash" && k == 0){
                        
                        self.createTransactionView.btnCash.setTitle("Cash \((self.CountryPayerListResponce.PayerList![i].ExchangeRate)!) ", for: .normal)
                        
                        self.selectedcashRow = i
                        
                        self.MaximumAmount = self.CountryPayerListResponce.PayerList![i].MaximumAmount!
                        self.MinimumAmount = self.CountryPayerListResponce.PayerList![i].MinimumAmount!
                        
                        self.ExchangeRates = self.CountryPayerListResponce.PayerList![i].ExchangeRate ?? 0.0
                        
                        k = k + 1
                        self.PayerBranchList((self.CountryPayerListResponce.PayerList?[i].PayerId)!)
                        self.PayerId = (self.CountryPayerListResponce.PayerList?[i].PayerId)!
                        print(self.PayerId)
                        self.PayerName = (self.CountryPayerListResponce.PayerList?[i].PayerName)!
                        
                    }else if(self.CountryPayerListResponce.PayerList![i].PaymentMethod == "Bank" && j == 0){
                        
                        if (j == 0) {
                            self.createTransactionView.lblBankName.text = self.CountryPayerListResponce.PayerList![j].PayerName
                        }
                        
                        self.createTransactionView.btnAccount.setTitle("Account \((self.CountryPayerListResponce.PayerList![i].ExchangeRate)!) ", for: .normal)
                        self.ExchangeRates = self.CountryPayerListResponce.PayerList![i].ExchangeRate ?? 0.0
                        j = j + 1
                        self.selectedAccountRow = i
                        
                        self.MaximumAmount = self.CountryPayerListResponce.PayerList![i].MaximumAmount!
                        self.MinimumAmount = self.CountryPayerListResponce.PayerList![i].MinimumAmount!
                        self.PayerId = (self.CountryPayerListResponce.PayerList?[i].PayerId)!
                        //addedd
                        self.PayerBranchList((self.CountryPayerListResponce.PayerList?[i].PayerId)!)
                        self.PayerName = (self.CountryPayerListResponce.PayerList?[i].PayerName)!
                        
                        print(self.PayerId)
                        if(PaymentType == ""){
                            
                            self.CountryPayerListResponce.PayerList?.remove(at: i)
                        }
                    }else if(self.CountryPayerListResponce.PayerList![i].PaymentMethod == "Wallet" ){
                        
                        if (j == 0) {
                            self.createTransactionView.lblBankName.text = self.CountryPayerListResponce.PayerList![j].PayerName
                        }
                        
                        self.createTransactionView.btnWallet.setTitle("Wallet \((self.CountryPayerListResponce.PayerList![i].ExchangeRate)!) ", for: .normal)
                        self.ExchangeRates = self.CountryPayerListResponce.PayerList![i].ExchangeRate ?? 0.0
                        j = j + 1
                        self.selectedAccountRow = i
                        
                        self.MaximumAmount = self.CountryPayerListResponce.PayerList![i].MaximumAmount!
                        self.MinimumAmount = self.CountryPayerListResponce.PayerList![i].MinimumAmount!
                        self.PayerId = (self.CountryPayerListResponce.PayerList?[i].PayerId)!
                        //addedd
                        self.PayerBranchList((self.CountryPayerListResponce.PayerList?[i].PayerId)!)
                        self.PayerName = (self.CountryPayerListResponce.PayerList?[i].PayerName)!
                        
                        print(self.PayerId)
                        if(PaymentType == ""){
                            
                            self.CountryPayerListResponce.PayerList?.remove(at: i)
                        }
                    }
                    
                }
//                 self.createTransactionView.PayerListCollectionView.reloadData()
                self.createTransactionView.txtSending.delegate = self
                if let firstValue = self.recivedParam["SendingAmount"] as? String {
                    if firstValue != "" {
                        
                      
                        let recevingValue = self.recivedParam["ReceivingAmount"] as? String
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            
                            let isReceivingTextFieldAmount = self.recivedParam["isReceivingTextFieldAmount"] as? Bool
                            
                            if (isReceivingTextFieldAmount!) {
                                if recevingValue != nil && recevingValue != "" {
                                    self.createTransactionView.txtReceiving.text = recevingValue!
                                    self.AddingValues(self.createTransactionView.txtReceiving)
                                }
                               
                            }else {
                                self.createTransactionView.txtSending.text = firstValue
                                self.AddingValues(self.createTransactionView.txtSending)
                            }
                            
                           
                        }
 
                    }
                    
                }
                
            }
            
            
            
        }else if self.CountryPayerListResponce?.myAppResult?.Code == 101 {
            
            self.uc.logout(self)
            
            
        }else{
            
            if(self.CountryPayerListResponce.myAppResult?.Message == nil){
                
                self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                
            }else{
                self.uc.errorSuccessAler("Error", (self.CountryPayerListResponce.myAppResult?.Message)!, self)
            }
        }
        
    }
}

    func PayerBranchList(_ PayerId:Int){
        
        print(PayerId)
        let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"PayerID":PayerId]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetPayerBranchList, params:parms , message: "Loading..."){result in
            
            print(result)
            
            self.PayerBranchList = BranchListresult(JSONString:result)
            
            if self.PayerBranchList?.myAppResult?.Code == 0 {
                self.PayerId = PayerId
                self.BranchCode = self.PayerBranchList.BranchList?[0].BranchCode ?? 0
                self.BranchName = self.PayerBranchList.BranchList?[0].BranchName ?? ""
                
              
                
            }else if self.PayerBranchList?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(self.PayerBranchList.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.PayerBranchList.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    func GetServiceCharges(_ PayerId:Int){
        let authToken = uc.getAuthToken()
        
        print(PayerId)
        let sendingValue = self.createTransactionView.txtSending.text!.components(separatedBy: " ").first
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"PayerID":PayerId,"Amount":sendingValue!]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetServiceCharges, params:parms , message: "Loading..."){result in
            
            self.ServiceCharges = ServiceChargesResult(JSONString:result)
            
//            if self.PayerBranchList?.myAppResult?.Code == 0 {
            if self.ServiceCharges?.myAppResult?.Code == 0 {
                
                if self.createTransactionView.txtSending.text != "" {
                    
                
                    if let ReceivingAmount = self.createTransactionView.txtReceiving.text {
                        
                        var roundValue : Int?
                        
//                        IncentiveMaximumAmount
                        
                        var incentiveLimit = 0.0 //self.recivedParam["incentiveMaximumAmount"] as? Double
                        
//                        UserDefaults.standard.set(self.incentiveMaximumAmount, forKey: "incentiveMaximumAmount")
                        
                        if let isIncentiveMaximumAmount = UserDefaults.standard.value(forKey: "incentiveMaximumAmount") as? Double{
                            incentiveLimit = isIncentiveMaximumAmount
                        }
                        
                        let txtSending = self.createTransactionView.txtSending.text?.components(separatedBy: " ").first
                        
                        if ( Double(txtSending!)! > incentiveLimit) {
                            
                            self.createTransactionView.lblIncentive.isHidden = true
                            roundValue = Int(ReceivingAmount)
                            
                        }else {
                            self.createTransactionView.lblIncentive.isHidden = false
                            let receivingAmount = ReceivingAmount
                            let incentiveAmount =  0.02 * Double(receivingAmount)!
                            let totalWithIncentive = incentiveAmount + Double(receivingAmount)!
                            roundValue = Int(totalWithIncentive.rounded())
                        }
                        
                        self.createTransactionView.lblSendingAmount.text = "Receiving Amount: \(String(describing: (roundValue!))) BDT"
                        self.createTransactionView.lblSendingFee.text! = "Sending Fee: \(Double(self.ServiceCharges.ServiceCharges!.Charges!)) \((self.uc.getUserInfo()?.UserInfo?.currencyIsoCode)!)"
                    }
                }

                let sendingAmont = (self.createTransactionView.txtSending.text!).components(separatedBy: " ").first!
                let digitString = (self.ServiceCharges.ServiceCharges?.Charges!)
                
                if sendingAmont != "" {
                    let totalAmount = "\(Double(sendingAmont)! + Double(digitString!))"
                    
                    let toDouble = Double(totalAmount)
                    
                    let doubleStr = String(format: "%.2f", toDouble!)
                    
                    self.createTransactionView.lblTotalAmount.setTitle("  \(doubleStr)  \((self.uc.getUserInfo()?.UserInfo?.currencyIsoCode)!)   ", for: .normal)
                }
                   
            }else if self.ServiceCharges?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(self.ServiceCharges.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
//                    self.uc.errorSuccessAler("Error", (self.ServiceCharges.myAppResult?.Message)!, self)
//                    GetServiceCharges(<#T##PayerId: Int##Int#>)
                }
            }
            
        }
    }

}


extension CreateTransStep1ViewController:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
    }
    
    @objc func AddingValues(_ textField:UITextField){
        
        if textField == self.createTransactionView.txtSending {
            
            
            
            if let checkamount = Double(textField.text!) {
                let maxamount =    Double(String(format:"%0.2f", self.MaximumAmount/self.ExchangeRates))!
                
                if (checkamount <= maxamount){
                    
                    let cacValue = String(format:"%0.2f",(Double(self.createTransactionView.txtSending.text!)!*self.ExchangeRates))
                    
                    let cacValue1 = cacValue.components(separatedBy: ".")
                    
                    self.createTransactionView.txtReceiving.text = cacValue1[0]
                    
                    
                }else{
                                    
                }

                if !(self.createTransactionView.txtSending.text?.isEmpty)!{
                    
                }
            }
        
        }else if textField == self.createTransactionView.txtReceiving {
            
            if ((Double(textField.text!)!) <=  self.MaximumAmount){
                
                self.createTransactionView.txtSending.text = String(format:"%0.2f", (Double( self.createTransactionView.txtReceiving.text!)! / self.ExchangeRates))
            }
            
            if !(self.createTransactionView.txtSending.text?.isEmpty)!{
            }
        }
        
        self.GetServiceCharges(self.PayerId)
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var checkval = 0
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            
            if !((textField.text?.isEmpty)!){
                
                
                if(checkval == 0){
                    
                    self.AddingValues(textField)
                }

            }
            
        })
        
        if(string == ""){
            
            if(textField.text?.count == 1){
                
                self.createTransactionView.txtReceiving.text = ""
                self.createTransactionView.txtSending.text = ""
                
                
            }else{
                
                // self.GetServiceCharges()
            }
            
        }else{
            
            
            if textField == self.createTransactionView.txtSending {
                
                let checkamount = Double(textField.text!+string)!
                let maxamount =  Double(String(format:"%0.2f", self.MaximumAmount/self.ExchangeRates))!
                if !(checkamount <= maxamount){
                    checkval = 1
                    return false
//                    return true
                }
            }else if textField == self.createTransactionView.txtReceiving {
                
                if !((Double(textField.text!+string)!) <=  self.MaximumAmount){
                    checkval = 1
                    return false
//                    return true
                }
            }
            
        }
        
        return true
    }
    func verification(){
            

               let authToken = uc.getAuthToken()
               
               var parms =  ["Token":authToken!.authToken!,
                             "ID":authToken!.userId!,
                             "PayerId": self.parms["PayerID"]!,
                             "DeliveryMethod": self.parms["PaymentMethod"]! ] as [String:Any]
               
            if(self.parms["PaymentMethod"]! as? String == "Bank"){
                parms["AccountNo"] = self.RecipientDetails.BeneAccountNumber
                          parms["BankId"] = self.RecipientDetails.BeneBankCode
                          }
              else if (self.parms["PaymentMethod"]! as? String == "Wallet"){
                  parms["Network"] = self.RecipientDetails.BenemobileCompany
                  parms["PhoneNo"] = self.RecipientDetails.BenePhone
                  
                  
              }
               
               uc.webServicePosthttp(urlString: ApiUrls.validateaccount, params:parms , message: "Loading..."){result in
                          
                self.verify = verifyAccount(JSONString:result)
                      
                          if  self.verify?.myAppResult?.Code == 0 {
                              
                              //Here .....................
                              //modify
                            let step3 = CreateTransStep3ViewController()
                            self.parms["BeneId"] = self.RecipientDetails.BeneID
                            self.parms["AccountNumber"] = self.RecipientDetails.BeneAccountNumber
                            self.parms["MobileCompanyName"] = self.RecipientDetails.BenemobileCompany
                            self.parms["MobilePaymentTypeId"] = self.RecipientDetails.BeneMobilePaymentTypeId
                            step3.parms = self.parms
                            step3.BeneDetails = self.RecipientDetails
                            self.navigationController?.pushViewController(step3, animated: true)
    //                        if(self.UpdateRecipientDetails != nil){
    //
    //                            self.AddRecipient(ApiUrls.UpdateRecipient)
    //
    //                        }else{
    //                            self.AddRecipient(ApiUrls.AddRecipient)
    //                        }
                              
                          }else if  self.verify?.myAppResult?.Code == 226 {
                            let strMessage = "Please update missing information"
                            self.uc.makeToast(message: strMessage)
                            let addRecipientViewController = AddRecipientViewController()
                             self.parms["BeneId"] = self.RecipientDetails.BeneID
                           self.parms["AccountNumber"] = self.RecipientDetails.BeneAccountNumber
                           self.parms["MobileCompanyName"] = self.RecipientDetails.BenemobileCompany
                           self.parms["MobilePaymentTypeId"] = self.RecipientDetails.BeneMobilePaymentTypeId
                            //self.parms["selectedIndex"] = index
                            addRecipientViewController.isFromTransaction = true
                            addRecipientViewController.transactionparms = self.parms
                            addRecipientViewController.BeneDetails = self.RecipientDetails
                            addRecipientViewController.UpdateRecipientDetails = self.RecipientDetails
                            self.navigationController?.pushViewController(addRecipientViewController, animated: true)
                              
                              
                          }else{
                              
                              if( self.verify?.myAppResult?.Message == nil){
                                  
                                  self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                                  
                              }else{
                                  
                                  self.uc.errorSuccessAler("Error", ( self.verify?.myAppResult?.Message)!, self)
                              }
                              
                          }
                          
                      }
           }
        
    func swipSendAndReciveMoney()  {
        if swipButtonIsSelected{
        createTransactionView.stackViewSendAndReceive.insertArrangedSubview(createTransactionView.viewSending, at: 1)
        }else{
            createTransactionView.stackViewSendAndReceive.insertArrangedSubview(createTransactionView.viewSending, at: 0)
            
        }
    }
}
