//
//  CreateTransStep1ViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 05/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import DropDown


class CreateTransStep1ViewController: UIViewController {

    @IBOutlet var createTransactionView: CreateTransStep1View!
    @IBOutlet weak var viewTableViewHide: UIView!

    
    var Topmenu = TopViewController()
    
    var rounderCorner = RoundedCorner()
    
    var parms : [String:Any]!
    
    let bankImages = [#imageLiteral(resourceName: "allied"),#imageLiteral(resourceName: "ubl"),#imageLiteral(resourceName: "hbl"),#imageLiteral(resourceName: "bop"),#imageLiteral(resourceName: "bankalfalah"),#imageLiteral(resourceName: "jsbank"),#imageLiteral(resourceName: "mcb")]
    
    let uc = UtilitySoftTechMedia()
    
    let receivingCountryDropDown = DropDown()
    
    var ReceivingCountryList : CountryListresult!
    var CountryPayerListResponce : PayerListresult!
    var PayerBranchList : BranchListresult!
    var ServiceCharges : ServiceChargesResult!
    var verify        : verifyAccount!
    
    var BranchCode = 0
    var BranchName = ""
    
    
    var Countries = [String]()
    var CountriesISO3Code = [String]()
    var Payments = "Bank"
    var countryindex = 0
    var ExchangeRates = 0.0
    var MaximumAmount = 0.0
    var MinimumAmount = 0.0
    var selectedcashRow = 0
    var selectedAccountRow = 0
    var PayerId = 0
    var PayerName = ""
    var SelectedRow = 0
    
    var recivedParam = [String:Any]()
    
    //Modify
    var swipButtonIsSelected = false
    let bankOBJ = BankListVC()
    var incentiveMaximumAmount = 0.0
    var isfromrecipentlist : Bool = false
    var RecipientDetails : RecipientListDetail!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        UIView.animate(withDuration: 0.9, delay: 0, options: [.repeat,.autoreverse], animations: {
            self.createTransactionView.viewImageFade.alpha = 0.0
        }, completion: nil)

        self.createTransactionView.viewTableView.isHidden = true
        
        self.createTransactionView.tblView3rdone.register(UINib(nibName: "BankListCell", bundle: nil), forCellReuseIdentifier: "BankListCell")
        
        //Getting All Receiving Countries
        self.GetReceivingCountryList()
        
        //Adding Done Button On Decimal Keyboard
        self.addDoneButtonOnKeyboard()
        
        //setting User Type dropdown
        receivingCountryDropDown.anchorView = self.createTransactionView.btnCountry
        receivingCountryDropDown.dataSource = Countries
        
        receivingCountryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.createTransactionView.lblReceivingCurrencyCode.text = self.CountriesISO3Code[index]
            self.createTransactionView.imgReceivingCountry.image = UIImage(named: "\(item).png")
             self.createTransactionView.imgCOuntry.image = UIImage(named: "\(item).png")
             self.createTransactionView.lblCountry.text = item
            self.countryindex = index
            
            if let payment = self.recivedParam["PaymentMethod"] as? String {
                self.getCountryPayerList(payment)
            }
            self.createTransactionView.tblView3rdone.reloadData()
        }
        
        let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
        
        let AllCountries = CountryListresult(JSONString: result)
        let userinfo = uc.getUserInfo()
        
        for i in 0..<(AllCountries?.CountryList?.count)! {
            
            if(AllCountries?.CountryList![i].CurrencyIsoCode == userinfo?.UserInfo?.currencyIsoCode){
                
                if let Country = AllCountries?.CountryList![i].CountryName {
                    
                    self.createTransactionView.imgSendingCountry.image = UIImage(named: "\(Country).png")
                    self.createTransactionView.lblSendingCurrencyCode.text = AllCountries?.CountryList![i].CurrencyIsoCode
                    
                }
                break
            }
        }
        // Do any additional setup after loading the view.
        filldetails()
    }



    @objc func methodOfReceivedNotification(notification: Notification) {

        if let data = notification.userInfo as? [String: Any]
        {
            for (name, score) in data
            {
                print("\(name) scored \(score) points!")
                
                if name == "sending" {
                    self.createTransactionView.txtSending.text = score as? String
                }else if name == "receiving" {
                    self.createTransactionView.txtReceiving.text = score as? String
                }
                
            }
            //
            if let sending = self.createTransactionView.txtSending.text {
                if sending != "" {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                             self.AddingValues(self.createTransactionView.txtSending)
                    }
                   
                }
            }
        }
        
    }
    
    func filldetails(){
        
        if self.RecipientDetails != nil{
            self.createTransactionView.btnCash.isUserInteractionEnabled = false
            self.createTransactionView.btnWallet.isUserInteractionEnabled = false
            self.createTransactionView.btnAccount.isUserInteractionEnabled = false
        if let payment = self.RecipientDetails.BenePayMethod as? Int {
            
               if payment == 9 {
                   self.createTransactionView.btnAccount.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
                   self.createTransactionView.btnWallet.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
                   self.createTransactionView.btnCash.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.01176470588, blue: 0.04705882353, alpha: 1)
                   //recivedParam["PaymentMethod"] = payment
                   self.Payments = "Cash"
                   self.createTransactionView.lblCashPayoutCaption.text = "CASH PAY OUT"
                
                self.createTransactionView.lblClickToSelectBank.isHidden = false
                self.createTransactionView.imgOnClickToSelectBank.isHidden = false
                self.createTransactionView.lblCashPayoutCaption.textColor = .black
                
                
               }else if payment == 751{
                   self.createTransactionView.btnWallet.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.01176470588, blue: 0.04705882353, alpha: 1)
                   self.createTransactionView.btnCash.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
                   self.createTransactionView.btnAccount.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
                   //recivedParam["PaymentMethod"] = payment
                   self.Payments = "Wallet"
                   self.createTransactionView.lblCashPayoutCaption.text = "Mobile Wallet"
                   self.createTransactionView.lblClickToSelectBank.isHidden = true
                   self.createTransactionView.imgOnClickToSelectBank.isHidden = true
                   self.createTransactionView.CashPayoutHeight.constant = 0
                   self.createTransactionView.ViewStyleHeight.constant = 0
                   self.createTransactionView.viewImageFade.isHidden = true
                   self.createTransactionView.lblCashPayoutCaption.textColor = .white
                
               }
               else {
                   self.createTransactionView.btnAccount.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.01176470588, blue: 0.04705882353, alpha: 1)
                   self.createTransactionView.btnCash.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
                   self.createTransactionView.btnWallet.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
                   //recivedParam["PaymentMethod"] = payment
                   self.Payments = "Bank"
                   self.createTransactionView.lblCashPayoutCaption.text = "BANK TRANSFER"
                
                if self.createTransactionView.lblCashPayoutCaption.text == "BANK TRANSFER"{
                    self.createTransactionView.lblClickToSelectBank.isHidden = true
                    self.createTransactionView.imgOnClickToSelectBank.isHidden = true
                    self.createTransactionView.CashPayoutHeight.constant = 0
                    self.createTransactionView.ViewStyleHeight.constant = 0
                    self.createTransactionView.viewImageFade.isHidden = true
                    self.createTransactionView.lblCashPayoutCaption.textColor = .white
                }else{
                    self.createTransactionView.lblClickToSelectBank.isHidden = false
                    self.createTransactionView.imgOnClickToSelectBank.isHidden = false
                    self.createTransactionView.CashPayoutHeight.constant = 100
                    self.createTransactionView.ViewStyleHeight.constant = 70
                    self.createTransactionView.viewImageFade.isHidden = false
                    self.createTransactionView.lblCashPayoutCaption.textColor = .black
                }


               }
        }
        }
    }
    //After View Did Load Function Call
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)

        
        Topmenu.view.frame = CGRect(x: 0, y: 0, width: self.createTransactionView.topView.frame.width, height: Topmenu.view.frame.height)
        self.createTransactionView.topView.addSubview(Topmenu.view)
        
        //1
        if let cashE = recivedParam["cashExchangeRate"] as? Double {
            
            self.createTransactionView.btnCash.setTitle("Cash \(cashE)", for: .normal)
        }
        if let bankE = recivedParam["bankExchangeRate"] as? Double {
            print(bankE)
            self.createTransactionView.btnAccount.setTitle("Account \(bankE)", for: .normal)
            
        }
        if let wallet = recivedParam["walletExchangeRate"] as? Double {
            
            self.createTransactionView.btnWallet.setTitle("Wallet \(wallet)", for: .normal)
            
        }
        
        //2
        if let payment = recivedParam["PaymentMethod"] as? String {
               if payment == "Cash" {
                   self.createTransactionView.btnAccount.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
                   self.createTransactionView.btnWallet.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
                   self.createTransactionView.btnCash.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.01176470588, blue: 0.04705882353, alpha: 1)
                   recivedParam["PaymentMethod"] = payment
                   self.Payments = payment
                   self.createTransactionView.lblCashPayoutCaption.text = "CASH PAY OUT"
                
                self.createTransactionView.lblClickToSelectBank.isHidden = false
                self.createTransactionView.imgOnClickToSelectBank.isHidden = false
                self.createTransactionView.lblCashPayoutCaption.textColor = .black
                
                
               }else if payment == "Wallet"{
                   self.createTransactionView.btnWallet.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.01176470588, blue: 0.04705882353, alpha: 1)
                   self.createTransactionView.btnCash.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
                   self.createTransactionView.btnAccount.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
                   recivedParam["PaymentMethod"] = payment
                   self.Payments = payment
                   self.createTransactionView.lblCashPayoutCaption.text = "Mobile Wallet"
                   self.createTransactionView.lblClickToSelectBank.isHidden = true
                   self.createTransactionView.imgOnClickToSelectBank.isHidden = true
                   self.createTransactionView.CashPayoutHeight.constant = 0
                   self.createTransactionView.ViewStyleHeight.constant = 0
                   self.createTransactionView.viewImageFade.isHidden = true
                   self.createTransactionView.lblCashPayoutCaption.textColor = .white
                
               }
               else {
                   self.createTransactionView.btnAccount.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.01176470588, blue: 0.04705882353, alpha: 1)
                   self.createTransactionView.btnCash.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
                   self.createTransactionView.btnWallet.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
                   recivedParam["PaymentMethod"] = payment
                   self.Payments = payment
                   self.createTransactionView.lblCashPayoutCaption.text = "BANK TRANSFER"
                
                if self.createTransactionView.lblCashPayoutCaption.text == "BANK TRANSFER"{
                    self.createTransactionView.lblClickToSelectBank.isHidden = true
                    self.createTransactionView.imgOnClickToSelectBank.isHidden = true
                    self.createTransactionView.CashPayoutHeight.constant = 0
                    self.createTransactionView.ViewStyleHeight.constant = 0
                    self.createTransactionView.viewImageFade.isHidden = true
                    self.createTransactionView.lblCashPayoutCaption.textColor = .white
                }else{
                    self.createTransactionView.lblClickToSelectBank.isHidden = false
                    self.createTransactionView.imgOnClickToSelectBank.isHidden = false
                    self.createTransactionView.CashPayoutHeight.constant = 100
                    self.createTransactionView.ViewStyleHeight.constant = 70
                    self.createTransactionView.viewImageFade.isHidden = false
                    self.createTransactionView.lblCashPayoutCaption.textColor = .black
                }


               }
        }
//        else {
//            //from transaction list screen
//            self.createTransactionView.btnAccount.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.01176470588, blue: 0.04705882353, alpha: 1)
//            self.createTransactionView.btnCash.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
//            recivedParam["PaymentMethod"] = "Bank"
//            self.Payments = "Bank"
//            self.createTransactionView.lblCashPayoutCaption.text = "BANK TRANSFER"
//
//            if self.createTransactionView.lblCashPayoutCaption.text == "BANK TRANSFER"{
//                self.createTransactionView.lblClickToSelectBank.isHidden = true
//                self.createTransactionView.imgOnClickToSelectBank.isHidden = true
//                self.createTransactionView.CashPayoutHeight.constant = 0
//                self.createTransactionView.ViewStyleHeight.constant = 0
//                self.createTransactionView.viewImageFade.isHidden = true
//                self.createTransactionView.lblCashPayoutCaption.textColor = .white
//            }else{
//                self.createTransactionView.lblClickToSelectBank.isHidden = false
//                self.createTransactionView.imgOnClickToSelectBank.isHidden = false
//                self.createTransactionView.CashPayoutHeight.constant = 100
//                self.createTransactionView.ViewStyleHeight.constant = 70
//                self.createTransactionView.viewImageFade.isHidden = false
//                self.createTransactionView.lblCashPayoutCaption.textColor = .black
//            }
//
//        }

        self.GetReceivingCountryList()
        
        filldetails()
        
    }
    
    
    //Button Back Click
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func btnSelectCountryClick(_ sender: Any) {
        
        self.receivingCountryDropDown.show()
    }
    
    //Button Sending Country Click
    @IBAction func btnSendingCountryClick(_ sender: Any) {
    }
    
    //Button Receiving Country Click
    @IBAction func btnReceivingCountryClick(_ sender: Any) {
    }
    
    //Button Cash Click
    @IBAction func btnCashClick(_ sender: Any) {
        
        self.Payments = "Cash"
        self.createTransactionView.lblCashPayoutCaption.text = "CASH PAY OUT"
        self.createTransactionView.btnAccount.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
        self.createTransactionView.btnCash.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.01176470588, blue: 0.04705882353, alpha: 1)
        self.createTransactionView.btnWallet.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
        self.createTransactionView.lblClickToSelectBank.isHidden = false
        self.createTransactionView.imgOnClickToSelectBank.isHidden = false
        self.createTransactionView.CashPayoutHeight.constant = 100
        self.createTransactionView.ViewStyleHeight.constant = 70
        self.createTransactionView.viewImageFade.isHidden = false
        self.createTransactionView.lblCashPayoutCaption.textColor = .black
        if let cashE = recivedParam["cashExchangeRate"] as? Double {
            
            self.ExchangeRates = cashE
            
        }
        

        
        self.MaximumAmount = self.CountryPayerListResponce.PayerList![self.selectedcashRow].MaximumAmount!
        self.MinimumAmount = self.CountryPayerListResponce.PayerList![self.selectedcashRow].MinimumAmount!
        
        if (self.Payments == "Cash"){
             self.Payments = "Cash"
            recivedParam["PaymentMethod"] = self.Payments
            self.getCountryPayerList("Cash")
        }
        
    }
    
    //Button Account Click
    @IBAction func btnAccountClick(_ sender: Any) {
        
    
        self.Payments = "Bank"
        self.createTransactionView.lblCashPayoutCaption.text = "BANK TRANSFER"
        self.createTransactionView.btnAccount.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.01176470588, blue: 0.04705882353, alpha: 1)
        self.createTransactionView.btnCash.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
        self.createTransactionView.btnWallet.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
        if self.createTransactionView.lblCashPayoutCaption.text == "BANK TRANSFER"{
            self.createTransactionView.lblClickToSelectBank.isHidden = true
            self.createTransactionView.imgOnClickToSelectBank.isHidden = true
            self.createTransactionView.CashPayoutHeight.constant = 0
            self.createTransactionView.ViewStyleHeight.constant = 0
            self.createTransactionView.viewImageFade.isHidden = true
            self.createTransactionView.lblCashPayoutCaption.textColor = .white
        }
        else{
            self.createTransactionView.lblClickToSelectBank.isHidden = false
            self.createTransactionView.imgOnClickToSelectBank.isHidden = false
            self.createTransactionView.CashPayoutHeight.constant = 100
            self.createTransactionView.ViewStyleHeight.constant = 70
            self.createTransactionView.viewImageFade.isHidden = false
            self.createTransactionView.lblCashPayoutCaption.textColor = .black
        }
        
        if let bankE = recivedParam["bankExchangeRate"] as? Double {
             self.ExchangeRates = bankE
        }
        
        
        
        
        if (self.Payments == "Bank"){
             self.Payments = "Bank"
            recivedParam["PaymentMethod"] = self.Payments
            self.getCountryPayerList("Bank")
        }
        
        self.MaximumAmount = self.CountryPayerListResponce.PayerList![self.selectedAccountRow].MaximumAmount!
        self.MinimumAmount = self.CountryPayerListResponce.PayerList![self.selectedAccountRow].MinimumAmount!
        
    }
    
    @IBAction func btnWalletClick(_ sender: UIButton) {
        
         self.Payments = "Wallet"
                self.createTransactionView.lblCashPayoutCaption.text = "Mobile Wallet"
                self.createTransactionView.btnAccount.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
                self.createTransactionView.btnCash.backgroundColor = #colorLiteral(red: 0.1019607843, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
                self.createTransactionView.btnWallet.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.01176470588, blue: 0.04705882353, alpha: 1)
                if self.createTransactionView.lblCashPayoutCaption.text == "Mobile Wallet"{
                    self.createTransactionView.lblClickToSelectBank.isHidden = true
                    self.createTransactionView.imgOnClickToSelectBank.isHidden = true
                    self.createTransactionView.CashPayoutHeight.constant = 0
                    self.createTransactionView.ViewStyleHeight.constant = 0
                    self.createTransactionView.viewImageFade.isHidden = true
                    self.createTransactionView.lblCashPayoutCaption.textColor = .white
                }
                else{
                    self.createTransactionView.lblClickToSelectBank.isHidden = false
                    self.createTransactionView.imgOnClickToSelectBank.isHidden = false
                    self.createTransactionView.CashPayoutHeight.constant = 100
                    self.createTransactionView.ViewStyleHeight.constant = 70
                    self.createTransactionView.viewImageFade.isHidden = false
                    self.createTransactionView.lblCashPayoutCaption.textColor = .black
                }
                
                if let wallet = recivedParam["walletExchangeRate"] as? Double {
                     self.ExchangeRates = wallet
                }
                
                
                
                
                if (self.Payments == "Wallet"){
                     self.Payments = "Wallet"
                    recivedParam["PaymentMethod"] = self.Payments
                    self.getCountryPayerList("Wallet")
                }
                
                self.MaximumAmount = self.CountryPayerListResponce.PayerList![self.selectedAccountRow].MaximumAmount!
                self.MinimumAmount = self.CountryPayerListResponce.PayerList![self.selectedAccountRow].MinimumAmount!
                
        
        
        
    }
    
    
    //Button Select Recipient Click
    @IBAction func btnSelectRecipentCLick(_ sender:Any){
        
        if (createTransactionView.txtSending.text!.isEmpty) || (createTransactionView.txtReceiving.text!.isEmpty) {
            uc.errorSuccessAler("Alert", "Please enter sending amount", self)
            return
           
        }
      
        let step2 = CreateTransStep2ViewController()
        
        parms = [
                       "PromoCode":"",
                       "PayerID":self.PayerId,
                       "MaxAmountLimit": self.MaximumAmount,
                       "PaymentMethod":self.Payments,
                       "BranchName":self.BranchName,
                       "PayoutLocation":self.PayerName,
                       "SendingAmount":self.createTransactionView.txtSending.text!,
                       "ReceivingAmount":self.createTransactionView.txtReceiving.text!,
                       "Service_Fee":(self.createTransactionView.lblSendingFee.text?.components(separatedBy: "Sending Fee: ")[1])!,
                       "Exchange_Rate":self.ExchangeRates,
                       "Total_Amount":(self.createTransactionView.lblTotalAmount.titleLabel?.text)!,
                       "Receiving_Country":(self.createTransactionView.lblCountry.text)!,
                       "SendingCurrencyCode":(self.createTransactionView.lblSendingCurrencyCode.text)!,
                       "PayOutAmount" : self.createTransactionView.txtReceiving.text!,
                       "ReceivingCurrencyCode":(self.createTransactionView.lblReceivingCurrencyCode.text)!,
                       "PayOutBranchCode" : self.BranchCode
        ]
        
        if !isfromrecipentlist{
            step2.parms = self.parms
            self.navigationController?.pushViewController(step2, animated: true)
        }else{
            verification()
        }
        
    }
 

    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonActionSend))
        done.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.createTransactionView.txtSending.inputAccessoryView = doneToolbar
        self.createTransactionView.txtReceiving.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonActionSend(){
        
        self.createTransactionView.txtSending.resignFirstResponder()
        self.createTransactionView.txtReceiving.resignFirstResponder()
    }
    
    
    @IBAction func btnShowTableView(_ sender: UIButton) {

        self.createTransactionView.viewDame.alpha = 0.5
        self.createTransactionView.viewTableView.isHidden = false
        self.createTransactionView.tblView3rdone.reloadData()
    }


    @IBAction func btnExchangeSwipe(_ sender: UIButton) {
        swipButtonIsSelected = !swipButtonIsSelected
        swipSendAndReciveMoney()
        
    }
    
    //popup table view cancel button
    
    @IBAction func btnCancel(_ sender: UIButton) {
        self.createTransactionView.viewDame.alpha = 0
         self.createTransactionView.viewTableView.isHidden = true
//        self.createTransactionView.alpha = 1.0
//        self.bankListAsPopUpM.removeFromSuperview()
        
        
//        UIView.animate(withDuration: 0.1, animations: {
//                   self.willMove(toParent: nil)
//                   self.view.removeFromSuperview()
//                   self.removeFromParent()
//               })
        
    }
    
    
}


