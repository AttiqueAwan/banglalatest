//
//  BankDepositDetailVC.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 20/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit

class BankDepositDetailVC: UIViewController {
    
    
    @IBOutlet weak var lblAccount: UILabel!
    @IBOutlet weak var lblSortCode: UILabel!
    @IBOutlet weak var lblAccountNumber: UILabel!
    
    @IBOutlet weak var lblAmountToSend: UILabel!
    
    @IBOutlet weak var lblReference: UILabel!
    
    @IBOutlet weak var lblAccountTitile: UILabel!
    
    var sendingAmount : String?
    var referencNumber : String?
    var parms = [String:Any]()
    let uc = UtilitySoftTechMedia()
    
    var bankDetailObj : BankDetail!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if sendingAmount != nil, sendingAmount != "" {
            self.lblAmountToSend.text = "  \(String(describing: sendingAmount!))"
        }
        if referencNumber != nil, referencNumber != "" {
            self.lblReference.text = "  \(String(describing: referencNumber!))"
        }
//
//        if let accountNumber = self.parms["AccountNumber"] as? String {
//            self.lblAccountNumber.text = accountNumber
//        }
        
    
        self.getBankDetail()
        
        
        
    }
    
    
    @IBAction func btnBack(_ sender: UIButton) {
        
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                
                if vc.isKind(of: DashboardViewController.self) {
                    print("It is in stack")
                    //Your Process
                    //                    self.navigationController?.pushViewController(vc, animated: true)
                    navigationController?.popToViewController(vc, animated: true)
                    
                }
            }
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func btnClose(_ sender: UIButton) {
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                
                if vc.isKind(of: DashboardViewController.self) {
                    
                    navigationController?.popToViewController(vc, animated: true)
                    
                }
            }
        }
    }
    

}

extension BankDepositDetailVC {
    

        func getBankDetail(){
            
            let authToken = uc.getAuthToken()
            
//            ,
//            "PayInCurrencyCode":(self.parms["SendingCurrencyCode"] as? String)!
            
    
    
            let parms =  ["ID":(authToken?.userId)!,
                          "Token":(authToken?.authToken!)!,"CountryIso3Code":
                            (uc.getUserInfo()?.UserInfo?.countryIsoCode!)!]as [String : Any]
            
            
            uc.webServicePosthttp(urlString: ApiUrls.depositbanklist, params:parms , message: "Loading..."){result in
                
                if(result == "fail")
                {
                    self.getBankDetail()
                    return
                }
                
//                self.postCodeFinderresult = PostCodeFinderresult(JSONString:result)
                
                self.bankDetailObj = BankDetail(JSONString:result)
                
                if self.bankDetailObj != nil {
                    if self.bankDetailObj?.myAppResult?.Code == 0 {
                        
                        if let accountTitle = self.bankDetailObj?.bankDetailList![0].AccountTitle {
                            self.lblAccountTitile.text = "  \(accountTitle)"
                        }
                        
                    
                        
                        if let accountNumber = self.bankDetailObj?.bankDetailList![0].AccountNumber {
                            
                                self.lblAccountNumber.text = "  \(accountNumber)"
                            
                            
                        }
                        if let sordCode = self.bankDetailObj?.bankDetailList![0].SortCode {
                            self.lblSortCode.text = "  \(sordCode)"
                        }
                        
                        
                    }else if self.bankDetailObj?.myAppResult?.Code == 102 {
                                    
                    //                self.noPostCodeView.isHidden = false
                                    
                                }else if self.bankDetailObj?.myAppResult?.Code == 101 {
                                    
                                    self.uc.logout(self)
                                    
                                }else{
                                    
                                    if(self.bankDetailObj?.myAppResult?.Message == nil){
                                        
                                        self.uc.errorSuccessAler("Error", result, self)
                                        
                                    }else{
                                        self.uc.errorSuccessAler("Error", (self.bankDetailObj?.myAppResult?.Message)!, self)
                                    }

                                }
                }else {
                    self.uc.errorSuccessAler("Error", "Data no found", self)
                }
                
            
                
            }
            
        }
}
