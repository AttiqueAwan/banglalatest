//
//  CreateTransStep4ViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 05/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import SVProgressHUD

class CreateTransStep4ViewController: UIViewController {

    @IBOutlet var createTransStep4View: CreateTransStep4View!
    
    let uc = UtilitySoftTechMedia()
    
    var weburl = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(self.weburl)
        self.createTransStep4View.TransWebView.loadRequest(URLRequest(url: URL(string: self.weburl)!))
        
        self.createTransStep4View.TransWebView.delegate=self;
        
        SVProgressHUD.setStatus("Loading....")
        // Do any additional setup after loading the view.
    }


    @IBAction func btnBackClick(_ sender: Any) {
        
        //self.navigationController?.pushViewController(CreateTransStep3ViewController(), animated: true)
        
//        self.navigationController?.popViewController(animated: true)
        
//        self.navigationController?.pushViewController(DashboardViewController(), animated: true)
        
        if let viewControllers = self.navigationController?.viewControllers {
              for vc in viewControllers {

                if vc.isKind(of: DashboardViewController.self) {
                        print("It is in stack")
                        //Your Process
//                    self.navigationController?.pushViewController(vc, animated: true)
                    navigationController?.popToViewController(vc, animated: true)

                   }
              }
        }
//        if let viewControllers = self.navigationController?.viewControllers {
//                     for vc in viewControllers {
//                          if vc.isKind(of: YourViewController.classForCoder()) {
//                               print("It is in stack")
//                               //Your Process
//                          }
//                     }
//               }
        
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CreateTransStep4ViewController:UIWebViewDelegate{
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        
        SVProgressHUD.show()
        return true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        SVProgressHUD.dismiss()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        SVProgressHUD.dismiss()
    }
    
}
