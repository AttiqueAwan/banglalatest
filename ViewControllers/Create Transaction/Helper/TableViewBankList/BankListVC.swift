//
//  BankListVC.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 26/02/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit

protocol protocolBankName {
    func bankName(bankName: String)
}


class BankListVC: UIViewController {
    
    internal var dummyArray = ["BANK1","BANK2","BANK3","BANK4","BANK5"]

    @IBOutlet var bankListAsPopUp: BankListView!
    var CountryPayerListResponce : PayerListresult!

    var delegate : protocolBankName?
    
     var recivedParam = [String:Any]()
    
    let uc = UtilitySoftTechMedia()
//    let objCreateTransStep1VC = CreateTransStep1ViewController()
    
    var Payments = "Cash"
    var ReceivingCountryList : CountryListresult!
    var ExchangeRates = 0.0
    var PayerBranchList : BranchListresult!
    var BranchCode = 0
    var BranchName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
//        objCreateTransStep1VC.getCountryPayerList(self.Payments)
        
        
        let nib = UINib(nibName: "BankListCell", bundle: nil)
        self.bankListAsPopUp.tblViewBankList.register(nib, forCellReuseIdentifier: "Cell")

        
        if CountryPayerListResponce != nil {
            print(CountryPayerListResponce!)
        }
        
        self.bankListAsPopUp.tblViewBankList.reloadData()
        
    }
    @IBAction func btnCancel(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.1, animations: {
            self.willMove(toParent: nil)
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
    }
}



