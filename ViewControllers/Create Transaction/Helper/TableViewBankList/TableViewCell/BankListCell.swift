//
//  BankListCell.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 26/02/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit

class BankListCell: UITableViewCell {

    @IBOutlet weak var lblBankNameM: UILabel!
    @IBOutlet weak var imgBankIconM: UIImageView!
    @IBOutlet weak var lblRateM: UILabel!
    
    @IBOutlet weak var lblReceivingCountryIsoCode: UILabel!
    
    @IBOutlet weak var viewOfLable: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
