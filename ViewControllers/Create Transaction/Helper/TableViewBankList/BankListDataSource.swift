//
//  BankListDataSource.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 26/02/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit


extension BankListVC : UITableViewDataSource,UITableViewDelegate {

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.CountryPayerListResponce != nil){
            
            if self.CountryPayerListResponce.PayerList != nil{
                
                return (self.CountryPayerListResponce.PayerList?.count)!
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? BankListCell
//        cell?.lblBankNameM.text = dummyArray[indexPath.row]
//        return cell!
        
        
        let cell = self.bankListAsPopUp.tblViewBankList.dequeueReusableCell(withIdentifier: "BankListCell", for: indexPath) as? BankListCell
        
        cell?.lblBankNameM.text = "\((self.CountryPayerListResponce.PayerList![indexPath.row].ExchangeRate)!)"
        
        
//                let cell = self.createTransactionView.PayerListCollectionView.dequeueReusableCell(withReuseIdentifier: "PayerListCell", for: indexPath) as? PayerListCell
//
//                cell?.payerbackView = rounderCorner.CircleView((cell?.payerbackView)!)
//                cell?.lblPayerRate = rounderCorner.RoundLabel((cell?.lblPayerRate)!)
//                cell?.imgPayer.image = self.GetPayerImage(self.CountryPayerListResponce.PayerList![indexPath.row])
//                cell?.lblPayerRate.text = "\((self.CountryPayerListResponce.PayerList![indexPath.row].ExchangeRate)!)"
//                if(indexPath.row == self.SelectedRow){
//
//                    cell?.lblPayerRate.backgroundColor = #colorLiteral(red: 0.8599639535, green: 0.124634333, blue: 0.03709618747, alpha: 1)
//                    cell?.payerbackView.layer.borderWidth = 1
//                    cell?.payerbackView.layer.borderColor =  #colorLiteral(red: 0.8599639535, green: 0.124634333, blue: 0.03709618747, alpha: 1)
//
//                }else{
//
//                    cell?.lblPayerRate.backgroundColor = #colorLiteral(red: 0, green: 0.3607843137, blue: 0.1254901961, alpha: 1)
//                     cell?.payerbackView.layer.borderWidth = 0
//                    cell?.payerbackView.layer.borderColor =  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//
//                }
                return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(dummyArray[indexPath.row])
        let bName = dummyArray[indexPath.row]
        delegate?.bankName(bankName: bName)
        
        UIView.animate(withDuration: 0.1, animations: {
            self.willMove(toParent: nil)
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
    }
}

//extension BankListVC {
//    
//    func getCountryPayerList(_ PaymentType:String){
//        
//         let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
//                         "Token":(uc.getAuthToken()?.AuthToken?.auth_token)!,
//                       "ReceivingCountryIso3Code":"BGD"/*(self.ReceivingCountryList.CountryList![self.countryindex].Iso3Code)!*/,
//                         "PaymentMethod":PaymentType]as [String : Any]
//        
//        uc.webServicePosthttp(urlString: ApiUrls.GetCountryPayerList, params:parms , message: "Loading..."){result in
//            
//            self.CountryPayerListResponce = PayerListresult(JSONString:result)
//            
//            if self.CountryPayerListResponce?.myAppResult?.Code == 0 {
//                
//                var k = 0 , j = 0
//                
//                DispatchQueue.main.async {
//                    
//                    
//                    for i in 0..<(self.CountryPayerListResponce.PayerList?.count)!{
//                        
//                        
//                        
////                        if(k==1 && j == 1){
////
////                            break
////                        }
//                        
//                        if(self.CountryPayerListResponce.PayerList![i].PaymentMethod == "Cash" && k == 0){
//                            
////                            self.createTransactionView.btnCash.setTitle("Cash \((self.CountryPayerListResponce.PayerList![i].ExchangeRate)!) ", for: .normal)
////
////                            self.selectedcashRow = i
////
////                            self.MaximumAmount = self.CountryPayerListResponce.PayerList![i].MaximumAmount!
////                            self.MinimumAmount = self.CountryPayerListResponce.PayerList![i].MinimumAmount!
//                            
//                            
//                            self.ExchangeRates = self.CountryPayerListResponce.PayerList![i].ExchangeRate ?? 0.0
//                            
//                            k = k + 1
//                            self.PayerBranchList((self.CountryPayerListResponce.PayerList?[i].PayerId)!)
//                            self.PayerId = (self.CountryPayerListResponce.PayerList?[i].PayerId)!
//                            
//                        }else if(self.CountryPayerListResponce.PayerList![i].PaymentMethod == "Bank" && j == 0){
//                            
//                            self.createTransactionView.btnAccount.setTitle("Account \((self.CountryPayerListResponce.PayerList![i].ExchangeRate)!) ", for: .normal)
//                            self.ExchangeRates = self.CountryPayerListResponce.PayerList![i].ExchangeRate ?? 0.0
//                            j = j + 1
//                            self.selectedAccountRow = i
//                            
//                            if(PaymentType == ""){
//                                
//                                self.CountryPayerListResponce.PayerList?.remove(at: i)
//                            }
//                        }
//                        
//                    }
//                    
//    //                 self.createTransactionView.PayerListCollectionView.reloadData()
//                }
//                
//               
//                
//            }else if self.CountryPayerListResponce?.myAppResult?.Code == 101 {
//                
//                self.uc.logout(self)
//                
//                
//            }else{
//                
//                if(self.CountryPayerListResponce.myAppResult?.Message == nil){
//                    
//                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
//                    
//                }else{
//                    self.uc.errorSuccessAler("Error", (self.CountryPayerListResponce.myAppResult?.Message)!, self)
//                }
//            }
//            
//        }
//    }
//    
//    func PayerBranchList(_ PayerId:Int){

//let authToken = uc.getAuthToken()
//          let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"PayerID":PayerId]as [String : Any]
//          
//          uc.webServicePosthttp(urlString: ApiUrls.GetPayerBranchList, params:parms , message: "Loading..."){result in
//              
//              self.PayerBranchList = BranchListresult(JSONString:result)
//              
//              if self.PayerBranchList?.myAppResult?.Code == 0 {
//                  
//                  self.BranchCode = self.PayerBranchList.BranchList?[0].BranchCode ?? 0
//                  self.BranchName = self.PayerBranchList.BranchList?[0].BranchName ?? ""
//                  
//              }else if self.PayerBranchList?.myAppResult?.Code == 101 {
//                  
//                  self.uc.logout(self)
//                  
//                  
//              }else{
//                  
//                  if(self.PayerBranchList.myAppResult?.Message == nil){
//                      
//                      self.uc.errorSuccessAler("Error", result, self)
//                      
//                  }else{
//                      self.uc.errorSuccessAler("Error", (self.PayerBranchList.myAppResult?.Message)!, self)
//                  }
//              }
//              
//          }
//      }
//}
