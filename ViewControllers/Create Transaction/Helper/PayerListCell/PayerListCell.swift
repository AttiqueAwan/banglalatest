//
//  PayerListCell.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 06/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class PayerListCell: UICollectionViewCell {

    @IBOutlet weak var payerbackView: UIView!
    @IBOutlet weak var imgPayer: UIImageView!
    @IBOutlet weak var lblPayerRate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
