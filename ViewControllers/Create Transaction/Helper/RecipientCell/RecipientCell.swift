//
//  RecipientCell.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 06/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

protocol CellDelegate: class {
    func cellButtonTapepd(tag: Int)
    
}


class RecipientCell: UITableViewCell {

    
    @IBOutlet weak var lblRecipientName: UILabel!
    @IBOutlet weak var imgRecipientCountry: UIImageView!
    @IBOutlet weak var lblRecipientPhone: UILabel!
    @IBOutlet weak var lblRecipientPayMenthod: UILabel!
    
    @IBOutlet weak var lblPaymentMethod: UILabel!
  
    
    @IBOutlet weak var btnSend: UIButton!

    weak var delegate : CellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    @IBAction func SendButton(_ sender: UIButton) {
        delegate?.cellButtonTapepd(tag: sender.tag)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  
}
