//
//  CreateTransStep2DataSource.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 05/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension CreateTransStep2ViewController:UITableViewDelegate,UITableViewDataSource,CellDelegate{
    func cellButtonTapepd(tag: Int) {
        let paymentMethod = self.RecipientList.RecipientList![tag].BenePayMethod!
        let accountNo = self.RecipientList.RecipientList![tag].BeneAccountNumber
        let bankName = self.RecipientList.RecipientList![tag].BeneBankName
        let branchName = self.RecipientList.RecipientList![tag].BeneBranchName
        guard let paymentMethodFromButton = self.parms["PaymentMethod"] as? String else{
            return
        }
        
        if paymentMethodFromButton == "Cash" {
            if self.RecipientList.RecipientList!.count > 0 {
                
                let myIndexPathRow = tag
                UserDefaults.standard.set(myIndexPathRow, forKey: "myIndexPathRow")
                
                let step3 = CreateTransStep3ViewController()
                self.parms["BeneId"] = self.RecipientList.RecipientList![tag].BeneID
                step3.parms = self.parms
                step3.BeneDetails = self.RecipientList.RecipientList![tag]
                self.parms["selectedIndex"] = tag
                print(self.RecipientList.RecipientList![tag])
                
                self.navigationController?.pushViewController(step3, animated: true)
            }
            
            
        }else if paymentMethodFromButton == "Bank" {
            if self.RecipientList.RecipientList!.count > 0 {
                
                if paymentMethod == 9 {
                    
                    
                    let strMessage = "Please update missing bank information"
                    uc.makeToast(message: strMessage)
                    
                    let addRecipientViewController = AddRecipientViewController()
                    addRecipientViewController.isFromTransaction = true
                    addRecipientViewController.UpdateRecipientDetails = self.RecipientList.RecipientList![tag]
                    
                    self.navigationController?.pushViewController(addRecipientViewController, animated: true)
                    
                }else if paymentMethod == 10 {
                    
                    if accountNo == nil || accountNo == "", bankName == nil || bankName == "", branchName == nil || branchName == ""{
                        
                        let strMessage = "Please update missing bank information"
                        uc.makeToast(message: strMessage)
                        
                        let addRecipientViewController = AddRecipientViewController()
                        addRecipientViewController.isFromTransaction = true
                        addRecipientViewController.UpdateRecipientDetails = self.RecipientList.RecipientList![tag]
                        self.navigationController?.pushViewController(addRecipientViewController, animated: true)
                    }else {
                        
                        let myIndexPathRow = tag
                        UserDefaults.standard.set(myIndexPathRow, forKey: "myIndexPathRow")
                        
                        let step3 = CreateTransStep3ViewController()
                        self.parms["BeneId"] = self.RecipientList.RecipientList![tag].BeneID
                        self.parms["AccountNumber"] = self.RecipientList.RecipientList![tag].BeneAccountNumber
                        step3.parms = self.parms
                        step3.BeneDetails = self.RecipientList.RecipientList![tag]
                        self.parms["selectedIndex"] = tag
                        print(self.RecipientList.RecipientList![tag])
                        
                        self.navigationController?.pushViewController(step3, animated: true)
                    }
                }
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let paymentMethod = self.RecipientList.RecipientList![indexPath.row].BenePayMethod!
        let accountNo = self.RecipientList.RecipientList![indexPath.row].BeneAccountNumber
        let bankName = self.RecipientList.RecipientList![indexPath.row].BeneBankName
        let branchName = self.RecipientList.RecipientList![indexPath.row].BeneBranchName
        let mobileaccountNo = self.RecipientList.RecipientList![indexPath.row].MobileAccountNumber
        guard let paymentMethodFromButton = self.parms["PaymentMethod"] as? String else{
            return
        }
        
        if paymentMethodFromButton == "Cash" {
            if self.RecipientList.RecipientList!.count > 0 {
                
                let myIndexPathRow = indexPath.row
                UserDefaults.standard.set(myIndexPathRow, forKey: "myIndexPathRow")
                self.verification(indexPath.row)
                
            }
            
            
        }else if paymentMethodFromButton == "Bank" {
            if self.RecipientList.RecipientList!.count > 0 {
                
                
                
                if paymentMethod == 10 {
                    
                    if accountNo == nil || accountNo == "", bankName == nil || bankName == "", branchName == nil || branchName == ""{
                        
                        let strMessage = "Please update missing bank information"
                        uc.makeToast(message: strMessage)
                        let addRecipientViewController = AddRecipientViewController()
                        addRecipientViewController.isFromTransaction = true
                        addRecipientViewController.UpdateRecipientDetails = self.RecipientList.RecipientList![indexPath.row]
                        self.navigationController?.pushViewController(addRecipientViewController, animated: true)
                    }else {
                        
                        let myIndexPathRow = indexPath.row
                        UserDefaults.standard.set(myIndexPathRow, forKey: "myIndexPathRow")
                        
                        self.verification(indexPath.row)
                        
                        
                    }
                }
            }
        }else if paymentMethodFromButton == "Wallet"{
            if mobileaccountNo == ""{
                let strMessage = "Please update missing Wallet information"
                uc.makeToast(message: strMessage)
                let addRecipientViewController = AddRecipientViewController()
                addRecipientViewController.isFromTransaction = true
                self.parms["BeneId"] = self.RecipientList.RecipientList![indexPath.row].BeneID
                self.parms["AccountNumber"] = self.RecipientList.RecipientList![indexPath.row].BeneAccountNumber
                self.parms["MobileCompanyName"] = self.RecipientList.RecipientList![indexPath.row].BenemobileCompany
                self.parms["MobilePaymentTypeId"] = self.RecipientList.RecipientList![indexPath.row].BeneMobilePaymentTypeId
                self.parms["selectedIndex"] = indexPath.row
                addRecipientViewController.UpdateRecipientDetails = self.RecipientList.RecipientList![indexPath.row]
                addRecipientViewController.transactionparms = self.parms
                addRecipientViewController.BeneDetails = self.RecipientList.RecipientList![indexPath.row]
                addRecipientViewController.UpdateRecipientDetails = self.RecipientList.RecipientList![indexPath.row]
                self.navigationController?.pushViewController(addRecipientViewController, animated: true)
            }else{
                let myIndexPathRow = indexPath.row
                UserDefaults.standard.set(myIndexPathRow, forKey: "myIndexPathRow")
                               
                self.verification(indexPath.row)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if  self.payment == self.benepay{
                        return 60
                       
                    }else{
                        return 0
                    }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.RecipientList != nil{
            
            if(self.RecipientList.RecipientList != nil){
                
                return (self.RecipientList.RecipientList?.count)!
            }
            
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.createTransactionStep2View.tblRecipient.dequeueReusableCell(withIdentifier: "RecipientCell") as? RecipientCell
        
        
        //apply shadow and corner
        cell?.layer.cornerRadius = 4
        let shadowPath2 = UIBezierPath(rect: cell!.bounds)
        cell?.layer.masksToBounds = false
        cell?.layer.shadowColor = UIColor.gray.cgColor
        cell?.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(2.0))
        cell?.layer.shadowOpacity = 0.5
        cell?.layer.shadowPath = shadowPath2.cgPath
       
        cell?.btnSend.tag = indexPath.row
       
        cell?.delegate = self
        cell?.btnSend.tag = indexPath.row
        // add border and color
//        cell?.backgroundColor = UIColor.darkGray
//        cell?.layer.borderColor = UIColor.black.cgColor
//        cell?.layer.borderWidth = 1
//        cell?.layer.cornerRadius = 8
//        cell?.clipsToBounds = true
        if self.parms["PaymentMethod"] as! String == "Cash"{
            payment = 9
        }else if self.parms["PaymentMethod"] as! String == "Bank"{
            payment = 10
        }
        else if self.parms["PaymentMethod"] as! String == "Wallet"{
            payment = 751
        }
        let details = self.RecipientList.RecipientList![indexPath.row]
        
        self.benepay = details.BenePayMethod
        
        if(details.BenePayMethod == 9){
            cell?.lblRecipientName.text = details.BeneName
            cell?.lblPaymentMethod.text = "Cash Pick-Up"
            cell?.lblRecipientPhone.text = "Ph:\((details.BenePhone)!)"

        }
        if(details.BenePayMethod == 10){
            cell?.lblRecipientName.text = details.BeneName
            cell?.lblPaymentMethod.text = "Bank"
            cell?.lblRecipientPhone.text = "A/C NO. \(String(describing: details.BeneAccountNumber!))"
            
        }
        
        if(details.BenePayMethod == 751){
            cell?.lblRecipientName.text = details.BeneName
            cell?.lblPaymentMethod.text = "Wallet"
            cell?.lblRecipientPhone.text = "Ph. \(String(describing: details.BenePhone!))"
            
        }
        
        let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
        
        let AllCountries = CountryListresult(JSONString: result)
        
        
        for i in 0..<(AllCountries?.CountryList?.count)! {
            
            if(AllCountries?.CountryList![i].Iso3Code == details.BeneCountryIsoCode){
                
                if let Country = AllCountries?.CountryList![i].CountryName {
                    
                    cell?.imgRecipientCountry.image = UIImage(named: "\((Country)).png")
                    break
                }
                
            }
        }
        if details.BenePayMethod == payment{
            cell?.isHidden = false
            
        }else{
            cell?.isHidden = true
            
        }
        
        return cell!
    }

    
}

extension CreateTransStep2ViewController{
    
    func verification(_ index: Int){
        
           let mobileCampony = self.RecipientList!.RecipientList![index].BenemobileCompany
//        let payerId = self.RecipientList!.RecipientList![index].BenePhone
           let authToken = uc.getAuthToken()
           
           var parms =  ["Token":authToken!.authToken!,
                         "ID":authToken!.userId!,
                         "PayerId": self.parms["PayerID"]!,
                         "DeliveryMethod": self.parms["PaymentMethod"]! ] as [String:Any]
           
        if(self.parms["PaymentMethod"]! as? String == "Bank"){
                      parms["AccountNo"] = self.RecipientList!.RecipientList![index].BeneAccountNumber
                      parms["BankId"] = self.RecipientList!.RecipientList![index].BeneBankCode
                      }
          else if (self.parms["PaymentMethod"]! as? String == "Wallet"){
              parms["Network"] = mobileCampony
              parms["PhoneNo"] = self.RecipientList!.RecipientList![index].BenePhone
              
              
          }
           
           uc.webServicePosthttp(urlString: ApiUrls.validateaccount, params:parms , message: "Loading..."){result in
                      
               self.verify = verifyAccount(JSONString:result)
                  
                      if  self.verify?.myAppResult?.Code == 0 {
                          
                          //Here .....................
                          //modify
                        let step3 = CreateTransStep3ViewController()
                        self.parms["BeneId"] = self.RecipientList.RecipientList![index].BeneID
                        self.parms["AccountNumber"] = self.RecipientList.RecipientList![index].BeneAccountNumber
                        self.parms["MobileCompanyName"] = self.RecipientList.RecipientList![index].BenemobileCompany
                        self.parms["MobilePaymentTypeId"] = self.RecipientList.RecipientList![index].BeneMobilePaymentTypeId
                        self.parms["selectedIndex"] = index
                        step3.parms = self.parms
                        step3.BeneDetails = self.RecipientList.RecipientList![index]
                        
                        print(self.RecipientList.RecipientList![index])
                        
                        self.navigationController?.pushViewController(step3, animated: true)
//                        if(self.UpdateRecipientDetails != nil){
//
//                            self.AddRecipient(ApiUrls.UpdateRecipient)
//
//                        }else{
//                            self.AddRecipient(ApiUrls.AddRecipient)
//                        }
                          
                      }else if  self.verify?.myAppResult?.Code == 226 {
                        let strMessage = "Please update missing information"
                        self.uc.makeToast(message: strMessage)
                        let addRecipientViewController = AddRecipientViewController()
                        self.parms["BeneId"] = self.RecipientList.RecipientList![index].BeneID
                        self.parms["AccountNumber"] = self.RecipientList.RecipientList![index].BeneAccountNumber
                        self.parms["MobileCompanyName"] = self.RecipientList.RecipientList![index].BenemobileCompany
                        self.parms["MobilePaymentTypeId"] = self.RecipientList.RecipientList![index].BeneMobilePaymentTypeId
                        self.parms["selectedIndex"] = index
                        addRecipientViewController.isFromTransaction = true
                        addRecipientViewController.transactionparms = self.parms
                        addRecipientViewController.BeneDetails = self.RecipientList.RecipientList![index]
                        addRecipientViewController.UpdateRecipientDetails = self.RecipientList.RecipientList![index]
                        self.navigationController?.pushViewController(addRecipientViewController, animated: true)
                          
                          
                      }else{
                          
                          if( self.verify?.myAppResult?.Message == nil){
                              
                              self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                              
                          }else{
                              
                              self.uc.errorSuccessAler("Error", ( self.verify?.myAppResult?.Message)!, self)
                          }
                          
                      }
                      
                  }
       }
    
    
    func getRecipientList(){
        var payment : Int!
        if self.parms["PaymentMethod"] as! String == "Cash"{
            payment = 9
        }else if self.parms["PaymentMethod"] as! String == "Bank"{
            payment = 10
        }
        let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetRecipientList, params:parms , message: "Loading..."){result in
            
            self.RecipientList = RecipientListresult(JSONString:result)
            
            if  self.RecipientList?.myAppResult?.Code == 0 {
                
                if((self.parms["BeneId"] != nil) )
                {
                    var counter = -1
                    
                    for item in self.RecipientList.RecipientList! {
                        
                        counter += 1
                        if((self.parms["BeneId"] as! Int) == item.BeneID)
                        {
                            if let row = UserDefaults.standard.value(forKey: "myIndexPathRow") as? Int {
                                let indexPath = IndexPath(row: row, section: 0)
                                let cell = self.createTransactionStep2View.tblRecipient.cellForRow(at: indexPath) as? RecipientCell
                                cell?.btnSend.setImage(UIImage(named: "tick.png"), for: .normal)
//                                let cell = self.createTransactionStep2View.tblRecipient.cellForRow(at: indexP) as! RecipientCell
//                                cell.btnSend.setImage(UIImage(named: "tick.png"), for: .normal)
                            }
                            // Set tick image meaning you have beneficary data
                            
                        }else {
                            
                            let indexPath = IndexPath(row: counter, section: 0)
                            let cell = self.createTransactionStep2View.tblRecipient.cellForRow(at: indexPath) as? RecipientCell
                            cell?.btnSend.setImage(UIImage(named: "send_green.png"), for: .normal)
                        
                           
                        }
                    }
                    
                }
                
                if(self.RecipientList.RecipientList != nil){
//                    var counter = -1
//                    for item in self.RecipientList.RecipientList! {
//                        counter += 1
//                    if let row = UserDefaults.standard.value(forKey: "myIndexPathRow") as? Int {
//                        let indexPath = IndexPath(row: row, section: 0)
//                        let cell = self.createTransactionStep2View.tblRecipient.cellForRow(at: indexPath) as? RecipientCell
//                            if (payment) == item.BenePayMethod{
//                                let cell = self.createTransactionStep2View.tblRecipient.cellForRow(at: indexPath) as? RecipientCell
//                                    cell?.isHidden = false
//                            }else{
//                                cell?.isHidden = true
//                        }
//                    }else {
//
//
//
//
//                    }
//                }
//
                    
                    
                    self.createTransactionStep2View.AddRecipientView.isHidden = false
                    self.createTransactionStep2View.tblRecipient.isHidden = false
                    self.createTransactionStep2View.noRecipientView.isHidden = true
                    self.createTransactionStep2View.tblRecipient.reloadData()
                
                }else{
                    
                    self.createTransactionStep2View.AddRecipientView.isHidden = true
                    self.createTransactionStep2View.tblRecipient.isHidden = true
                    self.createTransactionStep2View.noRecipientView.isHidden = false
                    
                }
                
                
            }else if  self.RecipientList?.myAppResult?.Code == 102{
                
                self.createTransactionStep2View.AddRecipientView.isHidden = true
                self.createTransactionStep2View.tblRecipient.isHidden = true
                self.createTransactionStep2View.noRecipientView.isHidden = false
                
            }else{
                
                if( self.RecipientList?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( self.RecipientList?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
        
    }
    
}
