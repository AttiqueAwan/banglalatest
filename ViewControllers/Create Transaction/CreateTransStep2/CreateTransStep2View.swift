//
//  CreateTransStep2View.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 05/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class CreateTransStep2View: UIView {

    //Label Title
    @IBOutlet weak var lblTitle: UILabel!
    
    //Top View Slider
    @IBOutlet weak var topView: UIView!
    
    //Recipient Table View
     @IBOutlet weak var tblRecipient: UITableView!
    
    //No Recipient View
     @IBOutlet weak var noRecipientView: UIView!
    
    //Button Back
    @IBOutlet weak var btnBack: UIButton!
    
    //Button Next
    @IBOutlet weak var btnNext: UIButton!
    
    //Add More Recipient View when some Recipient Already Exist
    @IBOutlet weak var AddRecipientView: UIView!
    
    //Add More Recipien Button when some Recipient Exist
    @IBOutlet weak var btnAddRecipient: UIButton!
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
