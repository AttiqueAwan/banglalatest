//
//  CreateTransStep2ViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 05/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

var myIndexPath : IndexPath?
class CreateTransStep2ViewController: UIViewController {

     @IBOutlet var createTransactionStep2View: CreateTransStep2View!
    
    var Topmenu = TopViewController()
    
    let uc = UtilitySoftTechMedia()
    
    var verify              : verifyAccount!
     var RecipientList : RecipientListresult!
    
    var parms = [String:Any]()
    var isPaymentMethod = false
    var payment : Int!
    var benepay : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createTransactionStep2View.noRecipientView.isHidden = true
        
        self.createTransactionStep2View.tblRecipient.register(UINib(nibName: "RecipientCell", bundle: nil), forCellReuseIdentifier: "RecipientCell")
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        
        Topmenu.view.frame = CGRect(x: 0, y: 0, width: self.createTransactionStep2View.topView.frame.width, height: Topmenu.view.frame.height)
        self.createTransactionStep2View.topView.addSubview(Topmenu.view)
        
        self.getRecipientList()
        self.createTransactionStep2View.tblRecipient.reloadData()
        
    }
    
    //Button Back Click
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    
    }
    
    //Button Add Now Click
    @IBAction func btnAddNowClick(_ sender: Any) {
        
        self.navigationController?.pushViewController(AddRecipientViewController(), animated: true)
    }
    
    //Button Next Click
    @IBAction func btnNextClick(_ sender: Any) {
        
        
        if(self.parms["BeneId"] != nil &&  (self.parms["BeneId"] as! Int) != 0)
        {
            let step3 = CreateTransStep3ViewController()
            step3.parms = self.parms
            
            for bene in RecipientList.RecipientList! {
                if((self.parms["BeneId"] as! Int) == bene.BeneID)
                {
                    step3.BeneDetails = bene
                    break
                }
            }
            
            if let data = RecipientList.RecipientList {
                print(data)
            }else {return}
            self.navigationController?.pushViewController(step3, animated: true)
        }else{
            uc.errorSuccessAler("Alert", "Please select", self)
        }

    
        
      
//        if self.RecipientList.RecipientList!.count > 0 {
//            step3.BeneDetails = self.RecipientList.RecipientList![0]
             
//        }
    }
    
    //Button Add New Recipient Click
    @IBAction func btnAddNewRecipientClick(_ sender: Any) {
        
        self.navigationController?.pushViewController(AddRecipientViewController(), animated: true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
