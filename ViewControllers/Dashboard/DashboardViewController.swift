//
//  DashboardViewController.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 25/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import DropDown

class DashboardViewController: UIViewController {

    @IBOutlet var dashboardView: DashboardView!
    
    @IBOutlet var firstTimeAlertView: DashboardView!
    
    
    @IBOutlet weak var topHeightViewHeightConstraint: NSLayoutConstraint!
    
    
    let roundcorner = RoundedCorner()
    let uc = UtilitySoftTechMedia()
    
    var Countries = [String]()
    var CountriesISO3Code = [String]()
    var Payments = "Cash"
    var countryindex = 0
    var ExchangeRates = 0.0
    var MaximumAmount = 0.0
    var MinimumAmount = 0.0
    var selectedcashRow = 0
    var selectedAccountRow = 0
    var selectedWalletRow = 0
    
    var cashExchangeRate = 0.0
    var bankExchangeRate = 0.0
    var WalletExchangeRate = 0.0
    
    let receivingCountryDropDown = DropDown()
    
    
    var dashboardResponse : Dashboardresult!
    var ReceivingCountryList : CountryListresult!
    var CountryPayerListResponce : PayerListresult!
    
    //modify
    var swipButtonIsSelected = false
    
    var isReceivingTextFieldAmount = false
    var incentiveMaximumAmount = 0.0
    
    var parms = [String: Any]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.dashboardView.txtSendingMoney.text = ""
        self.dashboardView.txtReceivingMoney.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dashboardView.SendView = roundcorner.CircleView(self.dashboardView.SendView)
        self.dashboardView.totalTransView = roundcorner.RoundCornerView(self.dashboardView.totalTransView)
        self.dashboardView.SendMoneyView = roundcorner.RoundCornerView(self.dashboardView.SendMoneyView)
        self.dashboardView.inCompleteTransView = roundcorner.RoundCornerView(self.dashboardView.inCompleteTransView)
        self.dashboardView.ComopleteTransView = roundcorner.RoundCornerView(self.dashboardView.ComopleteTransView)
        
        //print("Top Bar Height :: \(self.view.safeAreaInsets.top)")
        
        
        
        
        if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")

            case 1334:
                print("iPhone 6/6S/7/8")

            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")

            case 2436:
                print("iPhone X/XS/11 Pro")
            topHeightViewHeightConstraint.constant = 44

            case 2688:
                print("iPhone XS Max/11 Pro Max")
            topHeightViewHeightConstraint.constant = 48

            case 1792:
                print("iPhone XR/ 11 ")

            default:
                print("Unknown")
            }
        }
        
        
        
        
        //Getting Dashbaord Values
        self.GetDashboardApi()
        self.getWalletPayerList("Wallet")
        //Getting All Receiving Countries
//        self.GetReceivingCountryList()
        
        //Adding Done Button On Decimal Keyboard
        self.addDoneButtonOnKeyboard()
        
        //setting User Type dropdown
        receivingCountryDropDown.anchorView = self.dashboardView.btnReceivingCountry
        receivingCountryDropDown.dataSource = Countries

        receivingCountryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.dashboardView.lblReceivingCurrency.text = self.CountriesISO3Code[index]
            self.dashboardView.imgReceivingCountry.image = UIImage(named: "\(item).png")
            
            self.countryindex = index
            
            self.getCountryPayerList("Bank")
            
        }
    }
    
    //Menu Button Click
    @IBAction func btnMenuClick(_ sender: Any) {

        self.navigationController?.pushViewController(SideMenuViewController(), animated: true)

    }
    
    //Button Cash Click
    @IBAction func btnCashClick(_ sender: Any) {
        
        self.Payments = "Cash"
        self.parms["PaymentMethod"] = self.Payments
        self.dashboardView.btnAccount.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        self.dashboardView.btnCash.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.01176470588, blue: 0.04705882353, alpha: 1)
        self.dashboardView.btnWallet.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {[weak self] in
            self?.ExchangeRates = (self?.CountryPayerListResponce.PayerList![self!.selectedcashRow].ExchangeRate!)!
            
            self?.MaximumAmount = (self?.CountryPayerListResponce.PayerList![self!.selectedcashRow].MaximumAmount!)!
            
            self?.MinimumAmount = (self?.CountryPayerListResponce.PayerList![self!.selectedcashRow].MinimumAmount!)!
        }
    }
    
    //Buttton Account Click
    @IBAction func btnAccountClick(_ sender: Any) {
        self.Payments = "Bank"
        self.parms["PaymentMethod"] = self.Payments
        self.dashboardView.btnAccount.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.01176470588, blue: 0.04705882353, alpha: 1)
        self.dashboardView.btnCash.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        self.dashboardView.btnWallet.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {[weak self] in
            self?.ExchangeRates = (self?.CountryPayerListResponce.PayerList![self!.selectedAccountRow].ExchangeRate!)!
            
            self?.MaximumAmount = (self?.CountryPayerListResponce.PayerList![self!.selectedAccountRow].MaximumAmount!)!
            self?.MinimumAmount = (self?.CountryPayerListResponce.PayerList![self!.selectedAccountRow].MinimumAmount!)!
        }
    }
    //button wallet check
    
    @IBAction func btnWallet(_ sender: UIButton) {
        self.Payments = "Wallet"
        self.parms["PaymentMethod"] = self.Payments
        
        self.dashboardView.btnAccount.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        self.dashboardView.btnWallet.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.01176470588, blue: 0.04705882353, alpha: 1)
        self.dashboardView.btnCash.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {[weak self] in
            self?.ExchangeRates = (self?.CountryPayerListResponce.PayerList![self!.selectedWalletRow].ExchangeRate!)!
            
            self?.MaximumAmount = (self?.CountryPayerListResponce.PayerList![self!.selectedWalletRow].MaximumAmount!)!
            
            self?.MinimumAmount = (self?.CountryPayerListResponce.PayerList![self!.selectedWalletRow].MinimumAmount!)!
        }
    }
    //Button Sending CountryClick
    @IBAction func btnSendingCountryClick(_ sender: Any) {
        
    }
    
    //Button Receiving COuntry Click
    @IBAction func btnReceivingCountryClick(_ sender: Any) {
        
        self.receivingCountryDropDown.show()
        
    }
    
    //Button Send Money CLick
    @IBAction func btnSendClick(_ sender: Any) {
        
        let vc = CreateTransStep1ViewController()
        let vcOfBankListVC = BankListVC()
        
        parms = [    "SendingAmount":(self.dashboardView.txtSendingMoney.text)!,
                     "ReceivingAmount":(self.dashboardView.txtReceivingMoney.text)!,
                     "ExchangeRate":self.ExchangeRates,
                     "MaximumAmount":self.MaximumAmount,
                     "MinimumAmount":self.MinimumAmount,
                     "PaymentMethod":self.Payments,
                     "cashExchangeRate":self.cashExchangeRate,
                     "walletExchangeRate":self.WalletExchangeRate,
                     "incentiveMaximumAmount":self.incentiveMaximumAmount,
                     "bankExchangeRate":self.bankExchangeRate,
                     "isReceivingTextFieldAmount":self.isReceivingTextFieldAmount,
                     "ReceivingCountry":(self.dashboardView.lblReceivingCurrency.text)!] as [String : Any]
        
        vc.recivedParam = parms
        vcOfBankListVC.recivedParam = parms
        vcOfBankListVC.CountryPayerListResponce = self.CountryPayerListResponce
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //Button Dashboard Click
    @IBAction func btnDashboardClick(_ sender: Any) {
        if let url = URL(string: "https://online.banglaremit.co.uk/en/faqs-app.html") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
            
        }
    }
    
    
    //Button Transaction CLick
    @IBAction func btnTransClick(_ sender: Any) {
        
            GetTransactionList()
//          self.navigationController?.pushViewController(TransactionListViewController(), animated: true)
    }
    
    //Button Document Click
    @IBAction func btnDocumentClick(_ sender: Any) {
        
        self.navigationController?.pushViewController(DocumentListViewController(), animated: true)
    }
    
    //Button Recipient Click
    @IBAction func btnRecipientClick(_ sender: Any) {
        
        self.navigationController?.pushViewController(RecipientListViewController(), animated: true)
        
    }
    
    @IBAction func btnSetting(_ sender: UIButton) {
        self.navigationController?.pushViewController(ProfileSettingVC(), animated: true)
    }
    
    @IBAction func btnSupport(_ sender: UIButton) {
        //Customer support
        
       self.navigationController?.pushViewController(SupportListVC(), animated: true)
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonActionSend))
        done.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.dashboardView.txtSendingMoney.inputAccessoryView = doneToolbar
        self.dashboardView.txtReceivingMoney.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonActionSend(){
        
        self.dashboardView.txtSendingMoney.resignFirstResponder()
        self.dashboardView.txtReceivingMoney.resignFirstResponder()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btnSwipSendAndReciveMoney(_ sender: UIButton) {
        swipButtonIsSelected = !swipButtonIsSelected
        swipSendAndReciveMoney()
    }
    

    @IBAction func btnExchangeM(_ sender: UIButton) {
        
        let exchangeRateViewController = ExchangeRateViewController()
        exchangeRateViewController.isSignupLoginToHide = true
        self.navigationController?.pushViewController(exchangeRateViewController, animated: true)
        
    }
    
    
    @IBAction func btnTotalSent(_ sender: UIButton) {
        
        let totalSentVC = TotalSentVC()
        
        self.navigationController?.pushViewController(totalSentVC, animated: true)
    }
    
    @IBAction func btnCreditLimit(_ sender: UIButton) {
        
        self.navigationController?.pushViewController(SupportListVC(), animated: true)
        
        
//        let creditLimtVC = CreditLimtVC()
//        self.navigationController?.pushViewController(creditLimtVC, animated: true)
        
    }
    
    
    //FirstTime Alert
    
    @IBAction func btnCanelFirstTimeAlert(_ sender: UIButton) {
//        self.editAlertView.removeFromSuperview()
        self.firstTimeAlertView.removeFromSuperview()
    }
    
    
    @IBAction func btnEditFirstTimeAlert(_ sender: UIButton) {
        self.firstTimeAlertView.removeFromSuperview()
        self.navigationController?.pushViewController(ProfileViewController(), animated: true)
    }
    
}

