//
//  DashboardView.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 25/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class DashboardView: UIView {

    //Title Label
    @IBOutlet weak var lblTitle: UILabel!
    
    //Button Menu
    @IBOutlet weak var btnMenu: UIButton!
    
    //Button Cash
    @IBOutlet weak var btnCash: UIButton!
    
    //Button Account
    @IBOutlet weak var btnAccount: UIButton!
    //button Mobile Wallet
    @IBOutlet weak var btnWallet: UIButtonStyle!
    
    //Sending Money TextFeild
    @IBOutlet weak var txtSendingMoney: JVFloatLabeledTextField!
    
    //Buttin Sending Money
    @IBOutlet weak var btnSendingMoney: UIButton!
    
    //Image Sending Country
    @IBOutlet weak var imgSendingCountry: UIImageView!
    
    
    //Label Sending Currency
    @IBOutlet weak var lblSendignCurrency: UILabel!
    
    //Image Sending Arrow
    @IBOutlet weak var imgSendingarrow: UIImageView!
    
    
    //Receiving Money TextField
    @IBOutlet weak var txtReceivingMoney: JVFloatLabeledTextField!
    
    
    //Button Receiving Country
    @IBOutlet weak var btnReceivingCountry: UIButton!
    
    //Image Receiving Country
    @IBOutlet weak var imgReceivingCountry: UIImageView!
    
    //Label Receiving Currency
    @IBOutlet weak var lblReceivingCurrency: UILabel!
    
    //Image Receiving arrow
    @IBOutlet weak var imgReceivingarrow: UIImageView!
    
    //Label Exchange Rate
    @IBOutlet weak var lblExchangeRate: UILabel!
    
    //UIView Send
    @IBOutlet weak var SendView: UIView!
    
    //Image Send
    @IBOutlet weak var imgSend: UIImageView!
    
    //Button Send
    @IBOutlet weak var btnSend: UIButton!
    
    //Label Total Transactions
    @IBOutlet weak var lblTotalTrans: UILabel!
    
    //Total Trans View
    @IBOutlet weak var totalTransView: UIView!
    
    //Label Total Send Money
    @IBOutlet weak var lblTotalSendMoney: UILabel!
    
    //Total Send Money View
    @IBOutlet weak var SendMoneyView: UIView!
    
    //InComplete Trans View
    @IBOutlet weak var inCompleteTransView: UIView!
    
    //Label InComplete Trans
    @IBOutlet weak var lblinCompleteTrans: UILabel!
    
    //Label CompleteTrans
    @IBOutlet weak var lblCompleteTrans: UILabel!
    
    //Incomplete Trans View
    @IBOutlet weak var ComopleteTransView: UIView!
    
    //Button Dashbaord
    @IBOutlet weak var btnDashboard: UIButton!
    
    //Button Transactions
    @IBOutlet weak var btnTrans: UIButton!
    
    //Button Document
    @IBOutlet weak var btnDocument: UIButton!
    
    //Button Recipient
    @IBOutlet weak var btnRecipient: UIButton!
    
    //Modify
    //stack view of sending and receiving money
    @IBOutlet weak var stackVSendingAndReceiving: UIStackView!
    @IBOutlet weak var btnSwipSendAndReceivigMoney: UIButton!
    @IBOutlet weak var btnExchangeM: UIButton!
  
    @IBOutlet weak var viewSendMoneyM: UIView!
    @IBOutlet weak var viewReceivingMoneyM: UIView!
    
    //FirstTimeAlert
    @IBOutlet weak var lblNameFirstTimeAlert: UILabel!
    @IBOutlet weak var lblEmailFirstTimeAlert: UILabel!
    @IBOutlet weak var lblPhoneFirstTimeAlert: UILabel!
    @IBOutlet weak var lblCountryFirstTimeAlert: UILabel!
    
}
