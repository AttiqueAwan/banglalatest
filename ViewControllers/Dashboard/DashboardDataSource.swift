//
//  DashboardDataSource.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 25/10/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension DashboardViewController{
    
    //UserInfo
    

    func getUserProfile(){
        
        let authToken = uc.getAuthToken()
        
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetProfile, params:parms , message: "Loading..."){result in
            
            let ProfileResponse = Dashboardresult(JSONString:result)
            
            if  ProfileResponse?.myAppResult?.Code == 0 {
                
                
                if let userName = ProfileResponse?.UserInfo?.fullName {
                    self.dashboardView.lblNameFirstTimeAlert.text = userName
                }
                
                if let email = ProfileResponse?.UserInfo?.email {
                    self.dashboardView.lblEmailFirstTimeAlert.text = email
                }
                
                if let phone = ProfileResponse?.UserInfo?.phone {
                    self.dashboardView.lblPhoneFirstTimeAlert.text = phone
                }
              
                if let country = ProfileResponse?.UserInfo?.countryIsoCode {
                    self.dashboardView.lblCountryFirstTimeAlert.text = country
                }
                
            }else if  ProfileResponse?.myAppResult?.Code == 101 {
                
                
            }else{
                
                if( ProfileResponse?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", (ProfileResponse?.myAppResult?.Message)!, self)
                }
                
            }
        }
    }
    
    
    
    //TransactionList
    func GetTransactionList() {
        
        let authToken = uc.getAuthToken()
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"Limit":"0"]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetTransactionList, params:parms , message: "Loading..."){result in
            
            let TransactionListResponse = TransactionListresult(JSONString:result)
            
            if  TransactionListResponse?.myAppResult?.Code == 0 {
                
                if let validTransactionList = TransactionListResponse?.transactionListDetail, validTransactionList.count > 0 {
                    self.navigationController?.pushViewController(TransactionListViewController(), animated: true)
                    
                }else {
                    let message = "This menu allows you to view all your transaction history, with most recent at the top. Plese proceed with your 1st Transaction to view this menu"
                    self.uc.errorSuccessAler("Alert", message, self)
                }

                
            }else if TransactionListResponse?.myAppResult?.Code == 102{
                
               let message = "no transaction available for this user"
                self.uc.errorSuccessAler("Alert", message, self)
                
            }else{
                
                if( TransactionListResponse?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", (TransactionListResponse?.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    
    func GetDashboardApi(){
        
        let authToken = uc.getAuthToken() 
        if(authToken == nil){
            
            self.uc.errorSuccessAler("Erorr!", "Server Can't give right User ID!", self)
            
        }else{
            
            let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetDashbaord, params:parms , message: "Loading..."){result in
            
            let stringresult = "\(result.components(separatedBy: "}}")[0])}}"
            
            self.dashboardResponse = Dashboardresult(JSONString:stringresult)
            
            print(result)
            
            if self.dashboardResponse != nil {
 
              
            
                //Start First Time Alert
                if self.dashboardResponse.UserInfo?.houseNo == "" || self.dashboardResponse.UserInfo?.gender == "" || self.dashboardResponse.UserInfo?.address == ""{
                    
                   
                    let defaults = UserDefaults.standard
                    if defaults.bool(forKey: "isUserFirstTime") {
                        print("if")
                    }else {
                        
                        defaults.set(true, forKey: "isUserFirstTime")
                        defaults.set(true, forKey: "isUserFirstTimeCheckDocument")
                        self.getUserProfile()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {[weak self] in
                            self?.firstTimeAlertView.frame = (self?.view.frame)!
                            self?.dashboardView.addSubview(self!.firstTimeAlertView)
                        }
                    }
                    
                } //End First Time Alert
                
                
                
                
                self.incentiveMaximumAmount = 4400
                UserDefaults.standard.set(self.incentiveMaximumAmount, forKey: "incentiveMaximumAmount")
                
                if let agentLimit = self.dashboardResponse.dashboard?.AvailableCreditLimit90Days {
                    self.dashboardView.lblCompleteTrans.text = "\(agentLimit)"
                    
                }else {
                    self.dashboardView.lblCompleteTrans.text = "0"
                }
                
                
                    
                
//                self.dashboardView.lblCompleteTrans.text = "\((self.dashboardResponse.dashboard?.CompletedTransections)!)"
                self.dashboardView.lblinCompleteTrans.text = "\((self.dashboardResponse.dashboard?.InCompletedTransections)!)"
                self.dashboardView.lblTotalTrans.text = "\((self.dashboardResponse.dashboard?.TotalTransections)!)"
                self.dashboardView.lblTotalSendMoney.text = "\((self.dashboardResponse.dashboard?.TotalAmount)!)"
                self.uc.saveLogininfo(result: result)
               
                let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
                
                let AllCountries = CountryListresult(JSONString: result)
                
                for i in 0..<(AllCountries?.CountryList?.count)! {
                
                    if(AllCountries?.CountryList![i].Iso3Code == self.dashboardResponse.UserInfo?.countryIsoCode){
                        
                    if let Country = AllCountries?.CountryList![i].CountryName {
                        
                        self.dashboardView.imgSendingCountry.image = UIImage(named: "\(Country).png")
                        self.dashboardView.lblSendignCurrency.text = AllCountries?.CountryList![i].CurrencyIsoCode
                        
                    }
                        break
                    }
                }
                
                self.GetReceivingCountryList()
                
            }else{
                
                if(self.dashboardResponse == nil){
                    
                    self.uc.errorSuccessAler("Server Error", "Can't get User Details", self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", (self.dashboardResponse.myAppResult?.Message)!, self)
                }
            }
            
        }
        
        }
        
    }
    
    
    //Receiving CountryList
    func GetReceivingCountryList(){
        
        let parms =  ["Type":"2"]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading..."){result in
            
            let CountryListingResponce = CountryListresult(JSONString:result)
            
           if CountryListingResponce?.myAppResult?.Code == 0 {
            
                self.ReceivingCountryList =  CountryListresult(JSONString:result)
            
                for i in 0 ..< (self.ReceivingCountryList?.CountryList!.count)!{
                    
                    self.Countries.append((self.ReceivingCountryList?.CountryList![i].CountryName)!)
                    self.CountriesISO3Code.append((self.ReceivingCountryList?.CountryList![i].CurrencyIsoCode)!)
                    
                    
                }
            
                self.dashboardView.lblReceivingCurrency.text = self.ReceivingCountryList.CountryList![0].CurrencyIsoCode
            
                self.dashboardView.imgReceivingCountry.image = UIImage(named: "\(self.ReceivingCountryList.CountryList![0].CountryName!).png")
            
                self.countryindex = 0
            
                self.receivingCountryDropDown.dataSource = self.Countries
            
                self.receivingCountryDropDown.reloadAllComponents()
            
                if(self.ReceivingCountryList != nil){
                    
                    if((self.ReceivingCountryList.CountryList?.count)! > 0){
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {[weak self] in
                            
                            self?.getCountryPayerList(self!.Payments)
                        }
                    }
                }
            
                }else if CountryListingResponce?.myAppResult?.Code == 101 {
            
            
                }else{
            
                    if(CountryListingResponce?.myAppResult?.Message == nil){
                    
                        self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                    }else{
                    
                        self.uc.errorSuccessAler("Error", (CountryListingResponce?.myAppResult?.Message)!, self)
                    }
            }
        }
    }
    
    
    
    func getCountryPayerList(_ PaymentType:String){
        
        let parms =  ["ID":(uc.getAuthToken()?.userId)!,"Token":(uc.getAuthToken()?.authToken)!,"ReceivingCountryIso3Code":(self.ReceivingCountryList.CountryList![self.countryindex].Iso3Code)!,"PaymentMethod":PaymentType]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetCountryPayerList, params:parms , message: "Loading..."){result in
            
            self.CountryPayerListResponce = PayerListresult(JSONString:result)
            print(result)
            
            if self.CountryPayerListResponce?.myAppResult?.Code == 0 {
                
                 var k = 0 , j = 0
                
                DispatchQueue.main.async {
                    
                
                for i in 0..<(self.CountryPayerListResponce.PayerList?.count)!{
                    
                   
                    
                    if(k==1 && j == 1){
                        
                        break
                    }
                    
                    if(self.CountryPayerListResponce.PayerList![i].PaymentMethod == "Cash" && k == 0){
                    
                        self.dashboardView.btnCash.setTitle("Cash \((self.CountryPayerListResponce.PayerList![i].ExchangeRate)!) ", for: .normal)
                        
//                          self.dashboardView.lblExchangeRate.text = "EXCHANGE RATE : \((self.CountryPayerListResponce.PayerList![i].ExchangeRate)!)"
                        
                        self.selectedcashRow = i
                        
                        self.MaximumAmount = self.CountryPayerListResponce.PayerList![i].MaximumAmount!
                        self.MinimumAmount = self.CountryPayerListResponce.PayerList![i].MinimumAmount!
                        
                        
                        self.ExchangeRates = self.CountryPayerListResponce.PayerList![i].ExchangeRate ?? 0.0
                        
                        self.cashExchangeRate = self.ExchangeRates
                        
                        k = k + 1
                        if(k  == 1)
                        {
                            self.getCountryPayerList("Bank")
                        }
                    }else if(self.CountryPayerListResponce.PayerList![i].PaymentMethod == "Bank" && j == 0){
                    
                        
                        self.dashboardView.btnAccount.setTitle("Account \((self.CountryPayerListResponce.PayerList![i].ExchangeRate)!) ", for: .normal)
                         //self.ExchangeRates = self.CountryPayerListResponce.PayerList![i].ExchangeRate ?? 0.0
                        self.bankExchangeRate = (self.CountryPayerListResponce.PayerList![i].ExchangeRate)! //self.ExchangeRates
                         j = j + 1
                         self.selectedAccountRow = i
                        
                        self.btnAccountClick(self.dashboardView.btnAccount)
                    }
                
                    

                }
                
                }
            }else if self.CountryPayerListResponce?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
                
            }else{
                
                if(self.CountryPayerListResponce.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.CountryPayerListResponce.myAppResult?.Message)!, self)
                }
            }
            
        }
    }
    
    func getWalletPayerList(_ PaymentType:String){
        
        let parms =  ["ID":(uc.getAuthToken()?.userId)!,"Token":(uc.getAuthToken()?.authToken)!,"ReceivingCountryIso3Code":"BGD","PaymentMethod":PaymentType]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetCountryPayerList, params:parms , message: "Loading..."){result in
            
            self.CountryPayerListResponce = PayerListresult(JSONString:result)
            print(result)
            
            if self.CountryPayerListResponce?.myAppResult?.Code == 0 {
                
                DispatchQueue.main.async {
                    
                for i in 0..<(self.CountryPayerListResponce.PayerList?.count)!{
                    
                    if(self.CountryPayerListResponce.PayerList![i].PaymentMethod == "Wallet"){
                        self.MaximumAmount = self.CountryPayerListResponce.PayerList![i].MaximumAmount!
                        self.MinimumAmount = self.CountryPayerListResponce.PayerList![i].MinimumAmount!
                        self.dashboardView.btnWallet.setTitle("Wallet \((self.CountryPayerListResponce.PayerList![i].ExchangeRate)!) ", for: .normal)
                       
                        self.WalletExchangeRate = self.CountryPayerListResponce.PayerList![i].ExchangeRate ?? 0.0
                    }else{
                        self.dashboardView.btnWallet.isHidden = true
                    }
                    }
                }
    }else if self.CountryPayerListResponce?.myAppResult?.Code == 101 {                    self.uc.logout(self)
                
    }else{
        if(self.CountryPayerListResponce.myAppResult?.Message == nil){
            self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
        }else{
            self.uc.errorSuccessAler("Error", (self.CountryPayerListResponce.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    
    func swipSendAndReciveMoney()  {
        if swipButtonIsSelected{
            dashboardView.stackVSendingAndReceiving.insertArrangedSubview(dashboardView.viewSendMoneyM, at: 1)
        }else{dashboardView.stackVSendingAndReceiving.insertArrangedSubview(dashboardView.viewSendMoneyM, at: 0)}
    }
    
}

extension DashboardViewController:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
    }
    
    @objc func AddingValues(_ textField:UITextField){
        
        
        if textField == self.dashboardView.txtReceivingMoney {
            
            self.isReceivingTextFieldAmount = true
            
        }
        
        
        if textField == self.dashboardView.txtSendingMoney {
            
            
            
            let checkamount = Double(textField.text!)!
            let maxamount =    Double(String(format:"%0.2f", self.MaximumAmount/self.ExchangeRates))!
            
            if (checkamount <= maxamount){
                let cacValue = String(format:"%0.2f",(Double(self.dashboardView.txtSendingMoney.text!)!*self.ExchangeRates))
                let myDoublValue = (Double(cacValue))?.rounded()
                
//                let cacValue1 = cacValue.components(separatedBy: ".")
                self.dashboardView.txtReceivingMoney.text = "\(Int(myDoublValue!))" //cacValue1[0]
            }else{
            }
            
            if !(self.dashboardView.txtSendingMoney.text?.isEmpty)!{
                
            }
            
        }else if textField == self.dashboardView.txtReceivingMoney {
            
            if ((Double(textField.text!)!) <=  self.MaximumAmount){
                
                self.dashboardView.txtSendingMoney.text = String(format:"%0.2f", (Double( self.dashboardView.txtReceivingMoney.text!)! / self.ExchangeRates))
            }
            if !(self.dashboardView.txtSendingMoney.text?.isEmpty)!{
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var checkval = 0
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            
            if !((textField.text?.isEmpty)!){
                
                if(checkval == 0){
                    
                    self.AddingValues(textField)
                }
            }
        })
        
        if(string == ""){
            
            if(textField.text?.count == 1){
                
                self.dashboardView.txtReceivingMoney.text = ""
                self.dashboardView.txtSendingMoney.text = ""
                
                
            }else{
                
                // self.GetServiceCharges()
            }
            
        }else{
            
            
            if textField == self.dashboardView.txtSendingMoney {
                
                if string != "." {
                    let checkamount = Double(textField.text!+string)!
                    let maxamount =  Double(String(format:"%0.2f", self.MaximumAmount/self.ExchangeRates))!
                    if !(checkamount <= maxamount){
                        checkval = 1
                        return false
                    }
                }
                
            }else if textField == self.dashboardView.txtReceivingMoney {
                
                if !((Double(textField.text!+string)!) <=  self.MaximumAmount){
                    checkval = 1
                    return false
                }
            }
        }
        return true
    }
}
