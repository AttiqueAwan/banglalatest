//
//  ChatCell.swift
//  Bakhter Money
//
//  Created by Mac on 30/05/2019.
//  Copyright © 2019 BakhterMoney. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {

    
    @IBOutlet weak var lblMessage:UILabel!
    @IBOutlet weak var LblUserTime: UILabel!
    
    @IBOutlet weak var imgVUser: UIImageView!
    @IBOutlet weak var LblAdminTime: UILabel!
    @IBOutlet weak var lblMessage1: UILabel!
    @IBOutlet weak var lblAdmin:UILabel!
    @IBOutlet weak var lblUser:UILabel!
 
    @IBOutlet weak var imgVAdmin: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
