//
//  ChatDataSource.swift
//  Bakhter Money
//
//  Created by Mac on 30/05/2019.
//  Copyright © 2019 BakhterMoney. All rights reserved.
//

import Foundation
import UIKit

extension ChattingVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(self.complanceresult != nil){
            if(self.complanceresult.ComplanceDetail != nil){
                return (self.complanceresult.ComplanceDetail?.count)!
            }
        }
          return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let messageDetails = self.complanceresult.ComplanceDetail![indexPath.row]
        
        print(messageDetails.SenderUserType!)
        
        
        if(messageDetails.SenderUserType == "0"){
            
            let cell = self.tblMessage.dequeueReusableCell(withIdentifier: "MyCell1") as? ChatCell
            
            if let message = messageDetails.MessageBody{
                cell?.lblMessage?.text =  "\(message)"
//                cell?.lblMessage.frame.size.height = (cell?.lblMessage.intrinsicContentSize.height)! + 30
            }
            
            
            
            cell?.lblMessage.layer.cornerRadius = 2.0
            cell?.lblMessage.layer.masksToBounds = true
            
            let dateformatter = DateFormatter()
            if( Calendar.current.locale?.identifier == "en_GB")
            {
                dateformatter.locale = Locale(identifier:"en_GB_POSIX")
            }
            else
            {
                dateformatter.locale = Locale(identifier:"en_US_POSIX")
            }
            dateformatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            let Date = dateformatter.date(from: messageDetails.AddedDate!)
            dateformatter.dateFormat = "MMM,dd yyyy hh:mm a"
            let ComplainDAte = dateformatter.string(from: Date!)
            
           cell?.LblUserTime.text = "\(ComplainDAte)"
//            cell?.lblUser.text = "Me"
            
            return cell!
            
           
        }else{
          
            let cell = self.tblMessage.dequeueReusableCell(withIdentifier: "ChatCellLeft") as? ChatCell
            
            if let message = messageDetails.MessageBody{
                cell?.lblMessage1?.text = "\(message)"
            }
            
            cell?.imgVAdmin.image = UIImage(named: "support")
            
            cell?.lblMessage1.layer.cornerRadius = 2.0
            cell?.lblMessage1.layer.masksToBounds = true
            
            let dateformatter = DateFormatter()
            if( Calendar.current.locale?.identifier == "en_GB")
            {
                dateformatter.locale = Locale(identifier:"en_GB_POSIX")
            }
            else
            {
                dateformatter.locale = Locale(identifier:"en_US_POSIX")
            }
            dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            print(messageDetails.AddedDate!)
            let Date = dateformatter.date(from: messageDetails.AddedDate!)
            dateformatter.dateFormat = "MMM,dd yyyy hh:mm a"
            if(Date != nil)
            {
               let ComplainDAte = dateformatter.string(from: Date!)
               cell?.LblAdminTime.text = "\(ComplainDAte)"
            }
            else
            {
                cell?.LblAdminTime.text = "Now"
            }
//            cell?.lblAdmin.text = "System Support"
            
            return cell!
        }
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return 85
//
//    }

}

extension UITableView {

   func setOffsetToBottom(animated: Bool) {
        self.setContentOffset(CGPoint(x:0, y:self.contentSize.height - self.frame.size.height), animated: true)
    }

   func scrollToLastRow(animated: Bool) {
        if self.numberOfRows(inSection: 0) > 0 {
            self.scrollToRow(at: NSIndexPath(row: self.numberOfRows(inSection: 0) - 1, section: 0) as IndexPath, at: .bottom, animated: animated)
        }
    }
}


extension ChattingVC{
    
    
    func GetComplance(){
        
//        //        let userinfo = uc.getUserInfo()
         let authToken = uc.getAuthToken()
        
        let parms =  ["ID":(uc.getAuthToken()?.userId)!,
                      "Token":(authToken?.authToken)!,"AppID":ApiUrls.AppID,
                             "ComplaintId":SupportModelDATA.ComplaintId!]as [String : Any]
               
        
        uc.webServicePosthttp(urlString: ApiUrls.GetComplaintMsglist, params:parms , message: "Loading..."){result in
            if(result == "fail")
            {
                self.GetComplance()
                return
            }
            self.complanceresult = Complanceresult(JSONString:result)
            
            
            if self.complanceresult?.myAppResult?.Code == 0 {
                 let detail = ComplanceDetail(ComplaintId: SupportModelDATA.ComplaintId!, MessageId: 0, SenderUserId: 0, SenderUserType: "0", AddedDate: SupportModelDATA.AddedDate!, MessageBody: SupportModelDATA.Body!)
                DispatchQueue.main.async {
                    if(self.complanceresult.ComplanceDetail == nil)
                    {
                        var DetailArray = [ComplanceDetail]()
                        DetailArray.append(detail)
                        self.complanceresult.ComplanceDetail = DetailArray
                    }
                    else
                    {
                      self.complanceresult.ComplanceDetail!.insert(detail, at: 0)
                    }
                
                    self.tblMessage.reloadData()
                    self.tblMessage.scrollToLastRow(animated: true)
                }
               
                
            }else if self.complanceresult?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(self.complanceresult?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.complanceresult?.myAppResult?.Message)!, self)
                }
            }
        }
        
    }
    
    
    
    func SaveComplance(){
        
         let authToken = uc.getAuthToken()
//
//        //        let userinfo = uc.getUserInfo()
//
//                let parms =  ["ID":(authToken?.userId)!,
//                "Token":(authToken?.authToken)!,"AppID":ApiUrls.AppID]as [String : Any]
        
        
        let parms =  ["ID":(uc.getAuthToken()?.userId)!,
                      "Token":(authToken?.authToken)!,"AppID":ApiUrls.AppID,
                             "ComplaintId":SupportModelDATA.ComplaintId!,
                             "MessageBody":(self.txtMessage.text)!,
                             "SenderUserType":"0"]as [String : Any]
        
//        let parms =  ["ID":(uc.getAuthToken()?.userId)!,
//                      "Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,
//                      "ComplaintId":SupportModelDATA.ComplaintId!,
//                      "MessageBody":(self.txtMessage.text)!,
//                      "SenderUserType":"0"]as [String : Any]

        
        uc.webServicePosthttp(urlString: ApiUrls.InsertComplaintMsg, params:parms , message: "Loading..."){result in
            if(result == "fail")
            {
                self.SaveComplance()
                return
            }
            self.complanceresult = Complanceresult(JSONString:result)
            
            if self.complanceresult?.myAppResult?.Code == 0 {
                
                self.txtMessage.text = ""
                self.GetComplance()
                
                
            }else if self.complanceresult?.myAppResult?.Code == 10505 {
                self.txtMessage.text = ""
                self.GetComplance()
                
            }else if self.complanceresult?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(self.complanceresult?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.complanceresult?.myAppResult?.Message)!, self)
                }
            }
        }
        
    }
    
}

extension ChattingVC {
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonActionSend))
        done.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.txtMessage.inputAccessoryView = doneToolbar
    
        
    }
    
    @objc func doneButtonActionSend(){
        
        self.txtMessage.resignFirstResponder()
        
    }

}
