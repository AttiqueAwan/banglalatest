//
//  ChattingVC.swift
//  Bakhter Money
//
//  Created by Mac on 30/05/2019.
//  Copyright © 2019 BakhterMoney. All rights reserved.
//

import UIKit
 var IsComplaintRead = false
 var SupportModelDATA : SupportModel!

class ChattingVC: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblMessage: UITableView!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var btnSend: UIButton!
    
    @IBOutlet weak var viewTableView: UIView!
    
    let uc = UtilitySoftTechMedia()
    
    var complanceresult : Complanceresult!
    
    var chatID = 0
    var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblMessage.delegate = self
        tblMessage.dataSource = self
      
        self.tblMessage.register(UINib(nibName: "ChatCell", bundle: nil), forCellReuseIdentifier: "MyCell1")
        
         self.tblMessage.register(UINib(nibName: "ChatCell2", bundle: nil), forCellReuseIdentifier: "ChatCellLeft")
        
//        self.tblMessage.register(UINib(nibName: "ChatCell2", bundle: nil), forCellReuseIdentifier: "MyCell2")
        
        
//        self.tranactionListView.tblTranactions.register(UINib(nibName: "TransListCell", bundle: nil), forCellReuseIdentifier: "TranactionListCell")
        
//        self.tblMessage.estimatedRowHeight = UITableView.automaticDimension
//        self.viewTableView.addSubview(tblMessage)
//        self.view.addSubview(self.tblMessage)
        
        self.tblMessage.rowHeight = UITableView.automaticDimension
        self.tblMessage.estimatedRowHeight = 85
        addDoneButtonOnKeyboard()
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.lblTitle.text = SupportModelDATA.ComplaintType!
        self.GetComplance()
        IsComplaintRead = true
        NotificationCenter.default.addObserver(self, selector: #selector(Reload(_:)), name: ApiUrls.ComplaintNotification, object: nil)
    }
    
    
    @objc  func Reload(_ notification:NSNotification) {
        DispatchQueue.main.async {
            self.GetComplance()
        }
        
        IsComplaintRead = true
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                if vc.isKind(of: ChattingVC.classForCoder()) {
                    print("It is in stack")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnSendClick(_ sender: Any) {
        
        if(self.txtMessage.text.isEmpty){
            
            uc.errorSuccessAler("", "Please type message to send", self)
            
        }else{
            
            self.view.endEditing(true)
            self.SaveComplance()
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    

}
