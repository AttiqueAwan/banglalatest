//
//  SupportLogCell.swift
//  Bakhter Money
//
//  Created by Mac on 30/05/2019.
//  Copyright © 2019 BakhterMoney. All rights reserved.
//

import UIKit

class SupportLogCell: UITableViewCell {

    @IBOutlet weak var LblSupportType: UILabel!
    @IBOutlet weak var LblComplaintDate: UILabel!
    @IBOutlet weak var LblComplaintBody: UILabel!
    @IBOutlet weak var LblComplaintStatus: UILabel!
    @IBOutlet weak var NewView: UIView!
    @IBOutlet weak var PrefixOfUserName: UILableX!
    
    @IBOutlet weak var lblPaymentNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
