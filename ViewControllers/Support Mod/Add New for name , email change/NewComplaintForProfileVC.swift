//
//  NewComplaintForProfileVC.swift
//  Sangarwal
//
//  Created by Mac on 18/07/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
import MaterialControls

class NewComplaintForProfileVC: UIViewController,UITextViewDelegate  {

    
    @IBOutlet weak var TxtComplaintDescription: TextViewX!
    @IBOutlet weak var TextFieldsView: UIView!
    @IBOutlet weak var TxtFirstName: UITextField!
    @IBOutlet weak var TxtLastName: UITextField!
    @IBOutlet weak var TxtEmailAddress: UITextField!
    @IBOutlet weak var PhoneNumber: UITextField!
    
    var Toast = MDToast()
    let uc = UtilitySoftTechMedia()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.TxtComplaintDescription.delegate = self
        self.TxtComplaintDescription.text = "Description*"
        self.TxtComplaintDescription.textColor = UIColor.lightGray
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Show the Navigation Bar
        
        let userinfo = uc.getUserInfo()
        
        if let FirstName = userinfo?.UserInfo?.firstName
        {
            self.TxtFirstName.text = FirstName
        }
        if let LastName = userinfo?.UserInfo?.lastName
        {
            self.TxtLastName.text = LastName
        }
        if let Phone = userinfo?.UserInfo?.phone
        {
            self.PhoneNumber.text = Phone
        }
        if let Email = userinfo?.UserInfo?.email
        {
            self.TxtEmailAddress.text = Email
        }
        
        
    }
    
   
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Description*"
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func BackBtnFunc(_ sender: UIButton) {
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                if vc.isKind(of: NewComplaintForProfileVC.classForCoder()) {
                    print("It is in stack")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func AddNewComplaintFunc(_ sender: UIButton) {
        let userinfo = uc.getUserInfo()
        let FirstName = (userinfo?.UserInfo?.firstName)!
       
        let LastName = (userinfo?.UserInfo?.lastName)!
       
        let Phone = (userinfo?.UserInfo?.phone)!
       
        let Email = (userinfo?.UserInfo?.email)!
        
        
        if (self.TxtFirstName.text?.isEmpty)!
        {
            self.uc.errorSuccessAler("", "Enter your first name", self)
        }
        else if (self.TxtLastName.text?.isEmpty)!
        {
            self.uc.errorSuccessAler("", "Enter your last name", self)
        }
        else if (self.PhoneNumber.text?.isEmpty)!
        {
            self.uc.errorSuccessAler("", "Enter your phone number", self)
        }
        else if (self.TxtEmailAddress.text?.isEmpty)!
        {
            self.uc.errorSuccessAler("", "Enter your email address", self)
        }
        else if(self.TxtFirstName.text == FirstName && self.TxtLastName.text == LastName && self.PhoneNumber.text == Phone && self.TxtEmailAddress.text == Email)
        {
            self.uc.errorSuccessAler("", "Please update something from the given fields for submitting your request to admin.", self)
        }
        else
        {
            addnewComplaint()
        }
    }
    
    func addnewComplaint()
    {
        let userinfo = uc.getUserInfo()
        let PaymentID = ""
        var ComplaintDescription = ""
        var txt = ""
        if(self.TxtComplaintDescription.text! == "Description*" || self.TxtComplaintDescription.text! == "")
        {
            txt = ""
            ComplaintDescription = "Information changed to : \nFirst Name : \(self.TxtFirstName.text!) \nLast Name : \(self.TxtLastName.text!) \nEmail : \(self.TxtEmailAddress.text!) \nPhone Number : \(self.PhoneNumber.text!)"
           
        }
        else
        {
            txt = self.TxtComplaintDescription.text!
            ComplaintDescription = "Information changed to : \nFirst Name : \(self.TxtFirstName.text!) \nLast Name : \(self.TxtLastName.text!) \nEmail : \(self.TxtEmailAddress.text!) \nPhone Number : \(self.PhoneNumber.text!) \nDescription : \(txt)"
            
        }
     
        let parms =  ["ID":userinfo?.AuthToken?.user_id!,
        "Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,
        "SenderUserType":"0",
        "SenderUserName":userinfo!.UserInfo!.fullName!,
        "ComplaintType":"Name-Email-Phone Change",
        "Body":ComplaintDescription,
        "PaymentId":PaymentID]as [String : Any]
        
//        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
//                      "Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,
//                      "SenderUserType":"0",
//                      "SenderUserName":userinfo!.UserInfo!.FullName!,
//                      "ComplaintType":"Name-Email-Phone Change",
//                      "Body":ComplaintDescription,
//                      "PaymentId":PaymentID]as [String : Any]
        
        print(parms)
        
        uc.webServicePosthttp(urlString: ApiUrls.InsertComplaint, params:parms , message: "Loading..."){result in
            if(result == "fail")
            {
                self.addnewComplaint()
                return
            }
            let NewCompResp = AppUser(JSONString:result)
            
            if NewCompResp?.myAppResult?.Code == 0 {
                
                
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "", message: "Your request has been submitted successfully. One of our support team member will contact you shortly at your support page in application setting menu.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                       self.navigationController?.popViewController(animated: true)
                       self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                
                
            }else if NewCompResp?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(NewCompResp?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (NewCompResp?.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
