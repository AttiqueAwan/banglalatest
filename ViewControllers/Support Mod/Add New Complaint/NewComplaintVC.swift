//
//  NewComplaintVC.swift
//  Bakhter Money
//
//  Created by Mac on 30/05/2019.
//  Copyright © 2019 BakhterMoney. All rights reserved.
//

import UIKit
import DropDown
import MaterialControls

class NewComplaintVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var TransferIDbtn: UIButton!
    @IBOutlet weak var DropdownBtn: UIButton!
    @IBOutlet weak var TxtComplaintType: UITextField!
    @IBOutlet weak var TxtPaymentID: UITextField!
    @IBOutlet weak var DescriptionTopConstraintWithCOmplaintType: NSLayoutConstraint!
    @IBOutlet weak var TxtComplaintDescription: TextViewX!
    @IBOutlet weak var PaymentView: ViewStyle!
    
    
    
    var PaymentId = 0
    var Toast = MDToast()
    let uc = UtilitySoftTechMedia()
    var transactionListResponse : TransactionList!
    var DocumentTypeList : ListingTypesresult!
    var ComplaintTypeDrop = DropDown()
    var CompType = [String]()
    var isTransactionAvailable = false
    var DocumentType = [String]()
    var DocumentTypeName = [String]()
    var Docindex = Int()
    
    var PaymentIDDrop = DropDown()
    var PaymentNumArray = [String]()
    var Selectedindex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.TxtComplaintDescription.delegate = self
        self.TxtComplaintDescription.text = "Description*"
        self.TxtComplaintDescription.textColor = UIColor.lightGray
//        DropDown.appearance().textFont = UIFont.systemFont(ofSize: 12)
       
        //setting User Type dropdown
        ComplaintTypeDrop.anchorView = self.DropdownBtn
        ComplaintTypeDrop.dataSource = DocumentTypeName
        
        ComplaintTypeDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.TxtComplaintType.text = item
            if(item == "Remarks" || item == "Others")
            {
                self.PaymentView.isHidden = true
//                self.DescriptionTopConstraintWithCOmplaintType.constant = 10
            }
            else
            {
                self.PaymentView.isHidden = false
//                self.DescriptionTopConstraintWithCOmplaintType.constant = 80
            }
           
        }
        
        //setting User Type dropdown
        PaymentIDDrop.anchorView = self.TransferIDbtn
        PaymentIDDrop.dataSource = self.PaymentNumArray
        PaymentIDDrop.sizeToFit()
        PaymentIDDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.TxtPaymentID.text = item
            self.Selectedindex = index
        }
        
        // Do any additional setup after loading the view.
        addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonActionSend))
        done.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.TxtComplaintDescription.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonActionSend(){
        
        self.TxtComplaintDescription.resignFirstResponder()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Show the Navigation Bar
//        self.DescriptionTopConstraintWithCOmplaintType.constant = 10
        self.getUserTransactions()
       
    }
    
 
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Description*"
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    @IBAction func BackBtnFunc(_ sender: UIButton) {
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                if vc.isKind(of: NewComplaintVC.classForCoder()) {
                    print("It is in stack")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func SelectComplaintTypeFunc(_ sender: UIButton) {
        ComplaintTypeDrop.show()
    }
    
    @IBAction func SelectPaymentID(_ sender: UIButton) {
        if(self.transactionListResponse.AceTransList != nil)
        {
           self.PaymentIDDrop.show()
        }
    }
    
    @IBAction func AddNewComplaintFunc(_ sender: UIButton) {
        
     
        if (self.TxtComplaintType.text?.isEmpty)!
        {
            self.uc.errorSuccessAler("", "Please select complaint type", self)
        }
        else if((self.TxtComplaintDescription.text?.isEmpty)! || self.TxtComplaintDescription.text == "Description*")
        {
             self.uc.errorSuccessAler("", "Please write some description for complaint type", self)
        }
        else if(self.TxtComplaintType.text! != "Others" && self.TxtComplaintType.text! != "Remarks" && self.TxtComplaintType.text != "Name-Email-Phone Change" )
        {
            if(self.TxtPaymentID.text?.isEmpty)!
            {
                self.uc.errorSuccessAler("", "Please enter payment id", self)
            }
            else
            {
                addnewComplaint()
            }
        }
        else
        {
            addnewComplaint()
        }
    }
    
    func addnewComplaint()
    {
        let userinfo = uc.getUserInfo()
        let authToken = uc.getAuthToken()
        var PaymentID = ""
        
        if(self.transactionListResponse.AceTransList != nil)
        {
//            PaymentID = self.transactionListResponse.AceTransList![Selectedindex].PaymentNumber!
//            PaymentID = self.transactionListResponse.AceTransList![Selectedindex].PaymentNumber
            
        }
        else
        {
            PaymentID = ""
        }
        
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"AppID":ApiUrls.AppID,
                             "SenderUserType":"0",
                             "SenderUserName":userinfo!.UserInfo!.fullName!,
                             "ComplaintType":self.TxtComplaintType.text!,
                             "Body":self.TxtComplaintDescription.text!,
                             "PaymentId":PaymentID]as [String : Any]
        
//        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
//                      "Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,
//                      "SenderUserType":"0",
//                      "SenderUserName":userinfo!.UserInfo!.FullName!,
//                      "ComplaintType":self.TxtComplaintType.text!,
//                      "Body":self.TxtComplaintDescription.text!,
//                      "PaymentId":PaymentID]as [String : Any]
        
        print(parms)
        
        uc.webServicePosthttp(urlString: ApiUrls.InsertComplaint, params:parms , message: "Loading..."){result in
            if(result == "fail")
            {
                self.addnewComplaint()
                return
            }
            let NewCompResp = AppUser(JSONString:result)
            
            if NewCompResp?.myAppResult?.Code == 0 {
                
                self.Toast.text = "Successfully complaint Added."
                self.Toast.duration = 2
                self.Toast.show()
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
               
                
            }else if NewCompResp?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(NewCompResp?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (NewCompResp?.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    
    func getUserTransactions(){
        
//        let userinfo = uc.getUserInfo()
         let authToken = uc.getAuthToken()
        
        let parms =  ["ID":(authToken?.userId)!,
                      "Token":(authToken?.authToken)!,"AppID":ApiUrls.AppID,
                      "Limit":"0",
                      "PaymentMethod":""]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetTransactionList, params:parms , message: "Loading..."){result in
            
            if(result == "fail")
            {
                self.getUserTransactions()
                return
            }
            
            self.transactionListResponse = TransactionList(JSONString:result)
            
            if self.transactionListResponse?.myAppResult?.Code == 0 {
                
                if(self.transactionListResponse.AceTransList != nil && (self.transactionListResponse.AceTransList?.count)! > 0){
                    
                    self.isTransactionAvailable = true
                    for i in 0..<(self.transactionListResponse.AceTransList?.count)!
                    {
                        
                        let text = "Ref#\(String(describing: self.transactionListResponse.AceTransList![i].PaymentNumber!)) - \(String(describing: self.transactionListResponse.AceTransList![i].ReceivingCurrency!)) - \(String(describing: self.transactionListResponse.AceTransList![i].PayOutAmount!)) - \(String(describing: self.transactionListResponse.AceTransList![i].BeneName!))"
                        
                        self.PaymentNumArray.append(text)
                        
                        
                    }
                    
                    self.PaymentIDDrop.dataSource = self.PaymentNumArray
                    self.PaymentIDDrop.reloadAllComponents()
                    
                  
                }else{
                    self.isTransactionAvailable = false
                }
                self.getDocumnetTypeList()
                
            }else if self.transactionListResponse?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else if self.transactionListResponse?.myAppResult?.Code == 102 {
                
                self.getDocumnetTypeList()
                
            }
            else
            {
                self.getDocumnetTypeList()
                if(self.transactionListResponse?.myAppResult?.Message == nil){
                    
                  //  self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                  //  self.uc.errorSuccessAler("Error", (self.transactionListResponse?.myAppResult?.Message)!, self)
                }
            }
        }
    }
    

}


extension NewComplaintVC
{
    
    func getDocumnetTypeList(){
        //
//        let userinfo = uc.getUserInfo()
        let authToken = uc.getAuthToken()
        
        let parms =  ["Token":(authToken?.authToken)!,"ID":(authToken?.userId)!,"AppID":ApiUrls.AppID,"ListType":"COMPLAINTTYPE"]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetLists, params:parms , message: "Loading..."){result in
            
            
            if(result == "fail")
            {
                self.getDocumnetTypeList()
                return
            }
            
            
            self.DocumentTypeList = ListingTypesresult(JSONString:result)
            
            if  self.DocumentTypeList?.myAppResult?.Code == 0 {
                
                for i in 0..<(self.DocumentTypeList.ListType?.count)!{
                    
                    if(self.isTransactionAvailable)
                    {
                        self.DocumentTypeName.append(self.DocumentTypeList.ListType![i].Text!)
                        self.DocumentType.append(self.DocumentTypeList.ListType![i].ID!)
                        print(self.DocumentTypeList.ListType![i].Text!)
                    }else{
                        if(self.DocumentTypeList.ListType![i].Text! == "Others" || self.DocumentTypeList.ListType![i].Text! == "Remarks")
                        {
                            self.DocumentTypeName.append(self.DocumentTypeList.ListType![i].Text!)
                            self.DocumentType.append(self.DocumentTypeList.ListType![i].ID!)
                            print(self.DocumentTypeList.ListType![i].Text!)
                        }
                    }
                }
                
                self.ComplaintTypeDrop.dataSource = self.DocumentTypeName
                self.ComplaintTypeDrop.reloadAllComponents()
                
                
            }else if  self.DocumentTypeList?.myAppResult?.Code == 101{
                
                self.uc.logout(self)
                
            }else{
                
                if( self.DocumentTypeList?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( self.DocumentTypeList?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
        
    }
}
