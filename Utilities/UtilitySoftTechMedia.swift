//
//  UtilitySoftTechMedia.swift
//  UtilitySofttechMedia
//
//  Created by Softech Media on 15/01/2018.
//  Copyright © 2018 Softtechmedia. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import MaterialControls

//public typealias SelectionClosure = (DataResponse<Any>) -> Void
class UtilitySoftTechMedia: NSObject {

  // public var webserviceResponse: SelectionClosure?
    
    func AspectRatio(view:UIView)->Void{
        
        for vi in view.subviews {
         
            
            var  x:CGFloat = 0.0,y:CGFloat = 0.0,w:CGFloat = 0.0, h:CGFloat = 0.0
            if vi.tag == 0 {
                x = getWidth(width: vi.frame.origin.x)
                y = getHeight(width: vi.frame.origin.y)
                w = getWidth(width: vi.frame.size.width)
                h = getHeight(width: vi.frame.size.height)
            }
                
            else if(vi.tag==1) //without height
                
            {
                x = getWidth(width: vi.frame.origin.x)
                y = getHeight(width: vi.frame.origin.y)
                w = getWidth(width: vi.frame.size.width)
                h = vi.frame.size.height
                
            }
                
            else if (vi.tag==2)//without width
            {
                x = getWidth(width: vi.frame.origin.x)
                y = getHeight(width: vi.frame.origin.y)
                w = vi.frame.size.width
                h = getHeight(width: vi.frame.size.height)
                
            }
                
            else if(vi.tag==3)//without y orgin move
            {
                
                x = getWidth(width: vi.frame.origin.x)
                y =  vi.frame.origin.y
                w = getWidth(width: vi.frame.size.width)
                h = getHeight(width: vi.frame.size.height)
            }
            else if(vi.tag==4)//without y orgin expandable height
            {
                
                x = getWidth(width: vi.frame.origin.x)
                y =  vi.frame.origin.y
                w = getWidth(width: vi.frame.size.width)
                h = getHeight(width:vi.frame.size.height)+getHeight(width:vi.frame.origin.y)
            }
                
            else if(vi.tag==5){
                
                x = getWidth(width: vi.frame.origin.x)
                y = view.frame.size.height-getHeight(width:vi.frame.size.height)
                w = getWidth(width:vi.frame.size.width)
                h = getHeight(width: vi.frame.size.height)
                
            }
            
            vi.frame = CGRect(x:x,y:y,width:w,height:h)
                
                
        
            
            if  let s = vi as? UIButton{
                s.titleLabel?.font = UIFont(name:(s.titleLabel?.font.fontName)!, size:self.SetFontSize(font: (s.titleLabel?.font.pointSize)!))

            }
                
            else if let s = vi as? UITextField{
                s.font = UIFont(name:(s.font?.fontName)!, size: self.SetFontSize(font:(s.font?.pointSize)!))

            }
                
            else if let s = vi as? UILabel{
                s.font = UIFont(name:s.font.fontName, size:self.SetFontSize(font:s.font.pointSize))
              
                
            }
                
            else if let s = vi as? UITextView {
              
                s.font = UIFont(name:(s.font?.fontName)!, size:self.SetFontSize(font:(s.font?.pointSize)!))
               
                
            }
            
        }
        
    }
    func getWidth(width:CGFloat)->CGFloat{
        
        
        if UI_USER_INTERFACE_IDIOM() ==  UIUserInterfaceIdiom.pad{
        
        
        return (width/768.0)*UIScreen.main.bounds.size.width
        }
        else{
        return (width/320.0)*UIScreen.main.bounds.size.width
        }
    }
    func getHeight(width:CGFloat)->CGFloat{
        
        
        if UI_USER_INTERFACE_IDIOM() ==  UIUserInterfaceIdiom.pad{
            
            
            return (width/1025.0)*UIScreen.main.bounds.size.height
        }
        else{
            return (width/568.0)*UIScreen.main.bounds.size.height
        }
    }
    func SetFontSize(font:CGFloat)->CGFloat{
        var font1: CGFloat = font
        if UI_USER_INTERFACE_IDIOM() ==  UIUserInterfaceIdiom.pad{
            
           font1 = font+((font/100)*1)+(UIScreen.main.bounds.size.width/100)*0.1;
          
            
        }
            
        else if(UIScreen.main.bounds.size.height>568){
            
            font1 = font+((font/100)*10)+(UIScreen.main.bounds.size.width/100)*0.5;
            
            
        }
        return font1;
    }
    
    func saveLogininfo(result:String){
        
        UserDefaults.standard.set(result, forKey: "UserLogin")
    }
    
    
    func saveAuthToken(result:String){
        
        UserDefaults.standard.set(result, forKey: "AuthToken")
        
    }
    
    //getting Auth Token info
    func getAuthToken()->AppToken?{
        
        if(UserDefaults.standard.value(forKey: "AuthToken") == nil){
            
            return nil
        }
        let result:String = UserDefaults.standard.value(forKey: "AuthToken") as! String
        
        let resultObject = LoginResult(JSONString:result)!
        let token = resultObject.AppToken
        return token
        
    }

  
    func makeAlert(title:String,message:String,button:Array<Any>, delegate:UIViewController){
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        for  b in button{
            let action = UIAlertAction.init(title: b as? String, style: UIAlertAction.Style.default, handler: { s in
                alertController.dismiss(animated: true, completion: nil)
            })
            alertController .addAction(action)
        }
        delegate.present(alertController, animated: true, completion: nil)

    }

    func makeToast(message:String)->Void{

        let s=MDToast()
        s.text = message as String
        s.backgroundColor = UIColor.blue
        s.frame = CGRect(x:0,y:0,width:300,height:40)
        s.layer.cornerRadius = 6.0
        s.backgroundColor = UIColor.black
        s.textColor = UIColor.white
        s.show()


    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //alert Message for error or Success
    func errorSuccessAler(_ title:String,_ message:String,_ sender:UIViewController) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
         alertController.addAction(action)
         sender.present(alertController, animated: true, completion: nil)
    
    }
    
    //getting user login info
    func getUserInfo()->AppUser?{

        if(UserDefaults.standard.value(forKey: "UserLogin") == nil){

            return nil
        }
        let result:String = UserDefaults.standard.value(forKey: "UserLogin") as! String

        return AppUser(JSONString:result)!

    }


    //getting current Date
    func getCurrentDate()->String{
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
        
    }
    
    func performSegue(_ viewController:UIViewController){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        _ = storyboard.instantiateViewController(withIdentifier: "Home")
        
        
    }
    
    func logout(_ viewController: UIViewController){
//        let transition = CATransition()
//        transition.duration = 0.5
//        transition.type = kCATransitionPush
//        transition.subtype = kCATransitionFromLeft
//        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
//        viewController.view.window!.layer.add(transition, forKey: kCATransition)
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "Start")
//        viewController.present(controller, animated: false, completion: nil)
        viewController.navigationController?.popToRootViewController(animated: true)
    }
    

    
    func webServicePosthttp(urlString:String, params:Parameters,message:String,completion:  @escaping (_ result: String) -> Void){

        var myurlString =  urlString
        var headers: HTTPHeaders? = nil
        headers = [
          "Content-Type": "application/x-www-form-urlencoded"
       ]
        if !(urlString.contains("http")){

            myurlString = ApiUrls.serverUrl + urlString
        }
        
        if(urlString == ApiUrls.AddDocumentList){
            
            headers = [
                "Content-Type": "multipart/form-data"
            ]
        }

        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.gradient)

        
        print(myurlString)
        print(params)
        Alamofire.request(myurlString, method: HTTPMethod.post, parameters: params,encoding: URLEncoding(destination: .httpBody), headers: headers).responseString {
            response in
            switch response.result {
            case .success:
                do {

                    let convertedString = String(data: response.data!, encoding: String.Encoding.utf8)
//                    print(response.result.value ?? "0")

                  print(response.value ?? "0")
                    
                    if((convertedString?.contains("Authorization has been denied for this request"))!){

                        let vc = UIApplication.shared.keyWindow?.rootViewController
                        self.logout(vc!)

                    }else{

                        completion(convertedString!)
                    }

                }catch{

                    completion("")
               }
                break
            case .failure(let error):

                self.makeToast(message: error.localizedDescription)
                print(error)
            }
            SVProgressHUD.dismiss()
        }

    }
    
    
    
    func webServicePosthttpUpload(urlString:String, params:Parameters,message:String,currentController:UIViewController,completion:  @escaping (_ result: String) -> Void){
        
        SVProgressHUD.show()
        let myurlString = ApiUrls.serverUrl + urlString
        
        
        //Header HERE
        let headers = [
            "Content-type": "multipart/form-data",
            "Content-Disposition" : "form-data"
        ]
        
        let imgData = params["ImageData"] as? Data
        let DocType = params["DocTypeName"] as? String
        
        var parms = params
        parms["ImageData"] = ""
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            //Parameter for Upload files
            multipartFormData.append(imgData!, withName: "DocBody",fileName: (params["DocBody"] as? String)! , mimeType: "image/png")
            
             if(DocType == "National ID Card" || DocType == "Driving Licence"){
//            if(DocType == "Resident Card" || DocType == "Licence"){
                
                let imgData2 = params["ImageData2"] as? Data
                multipartFormData.append(imgData2!, withName: "DocBodyBack",fileName: (params["DocBodyBack"] as? String)! , mimeType: "image/png")
                parms["ImageData2"] = ""
            }
            
           
            for (key, value) in parms
            {
                print("key = \(key) -----> value = \(value)")
                multipartFormData.append(((value as? String)?.data(using: String.Encoding.utf8)!)!, withName: key)
            }
            
        }, usingThreshold:UInt64.init(),
           to: myurlString, //URL Here
            method: .post,
            headers: headers, //pass header dictionary here
            encodingCompletion: { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    print("the status code is :")
                    
                    upload.uploadProgress(closure: { (progress) in
                        if #available(iOS 11.0, *) {
                            print(progress.fractionCompleted)
                            // SVProgressHUD.showProgress(Float(progress.fractionCompleted))
                            SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Uploading....")
                            
                            print("Completed \((progress.completedUnitCount))")
                            
                        } else {
                            // Fallback on earlier versions
                        }
                    })
                    
                    upload.responseJSON { response in
                        if (response.description.contains("response : nil") || response.description.contains("The request timed out.") || response.description.contains("The network connection was lost.") || response.description.contains("The Internet connection appears to be offline."))
                        {
                            SVProgressHUD.dismiss()
                            print(response.description)
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "", message: "The request time out due to slow internet connection, Please try again.", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                                    completion("fail")
                                }))
                                currentController.present(alert, animated: true, completion: nil)
                            }
                        }
                        else
                        {
                            print("the resopnse code is : \((response.response?.statusCode)!)")
                            print("the response is : \(response)")
                            let convertedString = String(data: response.data!, encoding: String.Encoding.utf8)
                            completion(convertedString!)
                            SVProgressHUD.dismiss()
                        }
                    }
                    break
                case .failure(let error):
                    SVProgressHUD.dismiss()
                    if(error.localizedDescription == "The Internet connection appears to be offline."){
                        // other error statement
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline. Please try again.", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                                completion("fail")
                            }))
                            currentController.present(alert, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "", message: "Internal server error occured. Please try again.", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                                completion("fail")
                            }))
                            currentController.present(alert, animated: true, completion: nil)
                        }
                    }
                    SVProgressHUD.dismiss()
                    print("the error is  : \(error.localizedDescription)")
                    break
                }
                SVProgressHUD.dismiss()
                
        })
        
    }
    
    
    func webServicePosthttpUploadPhoto(urlString:String, params:Parameters,message:String,currentController:UIViewController,completion:  @escaping (_ result: String) -> Void){
        
        SVProgressHUD.show()
        let myurlString = ApiUrls.serverUrl + urlString
        
        
        //Header HERE
        let headers = [
            "Content-type": "multipart/form-data",
            "Content-Disposition" : "form-data"
        ]
        
        let imgData = params["ImageData"] as? Data
        
        var parms = params
        parms["ImageData"] = ""
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            //Parameter for Upload files
            multipartFormData.append(imgData!, withName: "PhotoBody",fileName: (params["PhotoBody"] as? String)! , mimeType: "image/png")
            
            
            for (key, value) in parms
            {
                multipartFormData.append(((value as? String)?.data(using: String.Encoding.utf8)!)!, withName: key)
            }
            
        }, usingThreshold:UInt64.init(),
           to: myurlString, //URL Here
            method: .post,
            headers: headers, //pass header dictionary here
            encodingCompletion: { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    print("the status code is :")
                    
                    upload.uploadProgress(closure: { (progress) in
                        if #available(iOS 11.0, *) {
                            print(progress.fractionCompleted)
                            // SVProgressHUD.showProgress(Float(progress.fractionCompleted))
                            SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Uploading....")
                            
                            print("Completed \((progress.completedUnitCount))")
                            
                        } else {
                            // Fallback on earlier versions
                        }
                    })
                    
                    upload.responseJSON { response in
                        if (response.description.contains("response : nil") || response.description.contains("The request timed out.") || response.description.contains("The network connection was lost.") || response.description.contains("The Internet connection appears to be offline."))
                        {
                            SVProgressHUD.dismiss()
                            print(response.description)
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "", message: "The request time out due to slow internet connection, Please try again.", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                                    completion("fail")
                                }))
                                currentController.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        else
                        {
                            print("the resopnse code is : \((response.response?.statusCode)!)")
                            print("the response is : \(response)")
                            let convertedString = String(data: response.data!, encoding: String.Encoding.utf8)
                            completion(convertedString!)
                            SVProgressHUD.dismiss()
                        }
                    }
                    break
                case .failure(let error):
                    SVProgressHUD.dismiss()
                    if(error.localizedDescription == "The Internet connection appears to be offline."){
                        // other error statement
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline. Please try again.", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                                completion("fail")
                            }))
                            currentController.present(alert, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "", message: "Internal server error occured. Please try again.", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                                completion("fail")
                            }))
                            currentController.present(alert, animated: true, completion: nil)
                        }
                    }
                    SVProgressHUD.dismiss()
                    print("the error is  : \(error.localizedDescription)")
                    break
                }
                SVProgressHUD.dismiss()
                
        })
        
    }

    func validatePhone(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
   
    func ProfileCompleteCheck() -> Bool{

        let userInfo = self.getUserInfo()

        guard let _  = userInfo?.UserInfo?.address , userInfo?.UserInfo?.address != "" else {

            return false
        }

        guard let _ = userInfo?.UserInfo?.houseNo , userInfo?.UserInfo?.houseNo != "" else {

            return false
        }

        guard let _ = userInfo?.UserInfo?.city , userInfo?.UserInfo?.city != "" else {

            return false
        }

        guard let _ = userInfo?.UserInfo?.postalCode , userInfo?.UserInfo?.postalCode != "" else {

            return false
        }

        guard  let _ = userInfo?.UserInfo?.nationalityIsoCode , userInfo?.UserInfo?.nationalityIsoCode != "" else {

            return false

        }

        guard let _ = userInfo?.UserInfo?.countryOfBirthIsoCode , userInfo?.UserInfo?.countryOfBirthIsoCode != "" else {

            return false

        }

        guard let _ = userInfo?.UserInfo?.email , userInfo?.UserInfo?.email != "" else {

            return false
        }

        guard let _ = userInfo?.UserInfo?.phone , userInfo?.UserInfo?.phone != "" else {

            return false
        }

        guard let _ = userInfo?.UserInfo?.fullName , userInfo?.UserInfo?.fullName != "" else {

            return false
        }

        return true
    }


    
//    func primaryColor()->UIColor{
//
//        return UIColorHelper.color(withRGBA: "#b4a98d")
//    }
//
//    func primaryDarkColor() -> UIColor {
//
//        return UIColorHelper.color(withRGBA:"#aa9f83")
//    }
    
}
