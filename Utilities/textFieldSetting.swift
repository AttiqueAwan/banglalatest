//
//  textFieldSetting.swift
//  KitehenHealth
//
//  Created by Softtech Media on 11/05/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class textFieldSetting: NSObject {

    func settingtextfield(_ textfield:UITextField,_ string:String) -> UITextField {

        textfield.layer.borderColor = #colorLiteral(red: 0.7568627451, green: 0.7568627451, blue: 0.7568627451, alpha: 1)
        textfield.layer.borderWidth = 1.0
        textfield.attributedPlaceholder = NSAttributedString(string: string,
                                                             attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.09019607843, green: 0.09019607843, blue: 0.09019607843, alpha: 1)])
        return textfield
    }

    func settingtextView(_ textView:UITextView,_ string:String) -> UITextView {

        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        textView.textColor = #colorLiteral(red: 0.1346280556, green: 0, blue: 0, alpha: 1)
        textView.layer.masksToBounds = true

        return textView
    }


    func textFieldBaseLine(_ textField : JVFloatLabeledTextField ,_ color: CGColor) -> JVFloatLabeledTextField{

        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = color
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width: textField.frame.size.width, height: textField.frame.size.height)

        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true

        return textField
    }

    
}


//Setting TextField Maximum Length
import UIKit
private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
}

extension String
{
    func safelyLimitedTo(length n: Int)->String {
        if (self.count <= n) {
            return self
        }
        return String( Array(self).prefix(upTo: n) )
    }
}

