//
//  RoundedCorner.swift
//  KitehenHealth
//
//  Created by Softtech Media on 11/05/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class RoundedCorner: NSObject {
    
    func RoundCorner(_ sender:UIButton) -> UIButton {
        
        let btn = sender
        btn.layer.cornerRadius=5
        btn.layer.masksToBounds = true
        
        return sender
    }
    
    func RoundLabel(_ sender:UILabel) -> UILabel {
        
        let btn = sender
        btn.layer.cornerRadius=5
        btn.layer.masksToBounds = true
        
        return sender
    }
    
    func RoundCornerView(_ sender:UIView) -> UIView {
        
        let btn = sender
        btn.layer.shadowColor = UIColor.lightGray.cgColor
        btn.layer.shadowOpacity = 0.5
        btn.layer.shadowOffset = CGSize.zero
        btn.layer.shadowRadius = 5
        btn.layer.cornerRadius = 5.0
        return sender
    }
    
    
    func RoundViews(_ sender:UIView) -> UIView {
        
        
        sender.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        sender.layer.borderWidth = 1.0
        sender.layer.masksToBounds = true
        sender.layer.cornerRadius = 8.0
        return sender
    }
   
    func CircleLabel(_ sender:UILabel) -> UILabel {
        
        
        sender.layer.cornerRadius = sender.frame.width/2
        sender.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        sender.layer.borderWidth = 1.0
        sender.layer.masksToBounds = true
        return sender
    }
    
    func CircleButton(_ sender:UIButton) -> UIButton {
        
        
        sender.layer.cornerRadius = sender.frame.width/2
        sender.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        sender.layer.borderWidth = 1.0
        sender.layer.masksToBounds = true
        return sender
    }
    
    func CircleView(_ sender:UIView) -> UIView {
        
        
        sender.layer.cornerRadius = sender.frame.width/2
        sender.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        sender.layer.borderWidth = 1.0
        sender.layer.masksToBounds = true
        return sender
    }
    

}

