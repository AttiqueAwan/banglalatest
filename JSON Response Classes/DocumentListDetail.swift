//
//  DocumentListDetail.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 15/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import ObjectMapper

class DocumentListDetail: Mappable {
    
    var UserID                           : Int?
    var DocID                            : Int?
    var DocType                          : String?
    var DocNumber                        : String?
    var DocIssueDate                     : String?
    var DocExpireDate                    : String?
    var DocAddedDate                     : String?
    var DocUserType                      : String?
    var DocBody                          : String?
    var DocBodyURL                       : String?
    var DocBodyBack                      : String?
    var DocBodyBackURL                   : String?
    var FromApi                          : String?
    var DocTypeName                      : String?
    var IsVerified                       : Bool?
  
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {

        UserID                           <- map["UserID"]
        DocID                            <- map["DocID"]
        DocType                          <- map["DocType"]
        DocNumber                        <- map["DocNumber"]
        DocIssueDate                     <- map["DocIssueDate"]
        DocExpireDate                    <- map["DocExpireDate"]
        DocAddedDate                     <- map["DocAddedDate"]
        DocUserType                      <- map["DocUserType"]
        DocBody                          <- map["DocBody"]
        DocBodyURL                       <- map["DocBodyURL"]
        DocBodyBack                      <- map["DocBodyBack"]
        DocBodyBackURL                   <- map["DocBodyBackURL"]
        FromApi                          <- map["FromApi"]
        DocTypeName                      <- map ["DocTypeName"]
        IsVerified                       <- map ["IsVerified"]
        
    }
    
}


