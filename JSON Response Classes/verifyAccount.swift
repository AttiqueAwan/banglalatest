//
//  verifyAccount.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 02/09/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import Foundation
import ObjectMapper
class verifyAccount: Mappable {
    
    
    var myAppResult             :     AppResult?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                 <-      map["result"]
        
    }
}
