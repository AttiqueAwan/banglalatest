//
//  LoginResult.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 03/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//
import UIKit
import ObjectMapper
class LoginResult: Mappable {
    
    
    var myAppResult             :     AppResult?
    var AppToken                :     AppToken?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
      
        myAppResult                 <-      map["result"]
        AppToken                    <-      map["data"]
        
    }
}
