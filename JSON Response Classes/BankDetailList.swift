//
//  BankDetailList.swift
//  BanglaRemitt
//
//  Created by Apple on 06/05/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper

class BankDetailList: Mappable {
    
    var AccountTitle                        :     String?
    var AccountNumber                       :     String?
    var SortCode                             :     String?
    
     
      
      required init?(map: Map) {
          
      }
      
      // Mappable
      func mapping(map: Map) {
          
          
          AccountTitle                           <-      map["AccountTitle"]
          AccountNumber                          <-      map["AccountNumber"]
        
          SortCode                                <-      map["SortCode"]
          
              
      }
}


//import UIKit
//import ObjectMapper
//class PostCodeFinderDetail: Mappable {
//
//
//    var ResultId                        :     String?
//    var HouseNo                         :     Int?
//    var Address                         :     String?
//    var City                            :     String?
//    var PostCode                        :     String?
//
//
//    required init?(map: Map) {
//
//    }
//
//    // Mappable
//    func mapping(map: Map) {
//
//
//        ResultId                               <-      map["ResultId"]
//        HouseNo                                <-      map["HouseNo"]
//        Address                                <-      map["Text"]
//        City                                   <-      map["City"]
//        PostCode                               <-      map["Description"]
//
//    }
//}
