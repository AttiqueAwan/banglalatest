//
//  BankListDetail.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 12/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class BankListDetail: Mappable {
    
    
    var BankName                        :     String?
    var BankCode                        :     String?
    var Charges                         :     Double?
    var BranchCode                      :     Int?
    var BranchName                      :     String?
    var BranchAddress                   :     String?
    var BranchPayCode                   :     Int?
    var BranhCity                       :     String?
    var ReceivingCountryIsoCode         :     String?
    var ReceivingCurrencyIsoCode        :     String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        BankName                            <-      map["BankName"]
        BankCode                            <-      map["BankCode"]
        Charges                             <-      map["Charges"]
        BranchCode                          <-      map["BranchCode"]
        BranchName                          <-      map["BranchName"]
        BranchAddress                       <-      map["BranchAddress"]
        BranchPayCode                       <-      map["BranchPayCode"]
        BranhCity                           <-      map["BranhCity"]
        ReceivingCountryIsoCode            <-      map["ReceivingCountryIsoCode"]
        ReceivingCurrencyIsoCode            <-      map["ReceivingCurrencyIsoCode"]
        
    }
}


class BankListModel2: Mappable {
    
    
    var DistrictCode                        :     String?
    var DistrictName                        :     String?
    

    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        DistrictCode                            <-      map["DistrictCode"]
        DistrictName                            <-      map["DistrictName"]
        
    }
}

class BankListModel3: Mappable {
    
    
    var BranchCode                     :     String?
    var BranchName                       :     String?
    var BankNameSubtitle                 :     String?
    var RoutingNo                       : String?
   

    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        BranchCode                         <-      map["BranchCode"]
        BranchName                           <-      map["BranchName"]
        BankNameSubtitle                     <-      map["BankName"]
        RoutingNo                            <-     map["RoutingNo"]
    }
}

