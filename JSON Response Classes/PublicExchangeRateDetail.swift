//
//  PublicExchangeRateDetail.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 19/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import ObjectMapper

class PublicExchangeRateDetail: Mappable {
    
    var ExchangeRate                           : Double?
    var RecievingCountryName                   : String?
    var RecievingCurrencyyISOCode              : String?
    var PayerID                                : Int?
    var PayerName                              : String?
    var PaymentMethod                          : String?
    var SendingCountryName                     : String?
    var SendingCurrencyISOCode                 : String?
    var CreationDate                           : String?
    
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        ExchangeRate                              <- map["ExchangeRate"]
        RecievingCountryName                      <- map["RecievingCountryName"]
        RecievingCurrencyyISOCode                 <- map["RecievingCurrencyyISOCode"]
        PayerID                                   <- map["PayerID"]
        PayerName                                 <- map["PayerName"]
        PaymentMethod                             <- map["PaymentMethod"]
        SendingCountryName                        <- map["SendingCountryName"]
        SendingCurrencyISOCode                    <- map["SendingCurrencyISOCode"]
        CreationDate                              <- map["CreationDate"]
    }
    
}


