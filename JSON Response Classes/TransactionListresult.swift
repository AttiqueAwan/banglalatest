//
//  TransactionListresult.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 22/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class TransactionListresult: Mappable {
    
    
    var myAppResult                :     AppResult?
    var transactionListDetail      :     [TransactionListDetail]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                    <-      map["result"]
        transactionListDetail          <-      map["data"]
        
    }
}

