//
//  PostCodeFinderDetail.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 17/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class PostCodeFinderDetail: Mappable {
    
    
    var ResultId                        :     String?
    var HouseNo                         :     Int?
    var Address                         :     String?
    var City                            :     String?
    var PostCode                        :     String?
   
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        ResultId                               <-      map["ResultId"]
        HouseNo                                <-      map["HouseNo"]
        Address                                <-      map["Text"]
        City                                   <-      map["City"]
        PostCode                               <-      map["Description"]
            
    }
}

class PostCodeFinderDetail2: Mappable {
    
    var BuildingName                         :     String?
    var BuildingNumber                         :     String?
    var Line1                         :     String?
    var Line2                         :     String?
    var Line3                         :     String?
    var City                            :     String?
    var Street                            :     String?
    var Label                         :     String?
    var SubBuilding                         :     String?
    var PostalCode                         :     String?
    
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        BuildingName                       <-      map["BuildingName"]
        BuildingNumber                     <-     map["BuildingNumber"]
        Line1                              <-     map["Line1"]
        Line2                              <-     map["Line2"]
        Label                           <-           map["Label"]
        City                               <-     map["City"]
        Street                             <-     map["Street"]
        SubBuilding                             <-     map["SubBuilding"]
        PostalCode                             <-     map["PostalCode"]
        
            
    }
}

