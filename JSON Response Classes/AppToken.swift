//
//  AppToken.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 03/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class AppToken: Mappable {
    
    var userId : String!
    var issueOn : String!
    var expireOn : String!
    var authToken : String!
    var authKey : String!

    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        userId                     <-         map["user_id"]
        issueOn                    <-         map["issue_on"]
        expireOn                   <-         map["expire_on"]
        authToken                  <-         map["auth_token"]
        authKey                    <-         map["auth_key"]
        
    }
}
