//
//  Complanceresult.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 17/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class Complanceresult: Mappable {
    
    
    var myAppResult                        :     AppResult?
    var ComplanceDetail                    :     [ComplanceDetail]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                            <-      map["result"]
        ComplanceDetail                        <-      map["data"]
        
    }
}

