//
//  SupportModelResponse.swift
//  Bakhter Money
//
//  Created by Mac on 30/05/2019.
//  Copyright © 2019 BakhterMoney. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
class SupportModelResponse: Mappable {
    
    
    var myAppResult                            :     AppResult?
    var SupportModelList                       :     [SupportModel]?
    var SupportModelData                       :     SupportModel?
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                            <-      map["result"]
        SupportModelList                       <-      map["data"]
        SupportModelData                       <-      map["data"]
    }
}
