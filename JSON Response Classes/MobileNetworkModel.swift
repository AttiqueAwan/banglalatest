//
//  MobileNetworkModel.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 28/08/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
class NetworkModelResult: Mappable {
    
    
    var myAppResult                            :     AppResult?
    var networkModel                           :     [networkMapping]?
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                            <-      map["result"]
        networkModel                           <-      map["data"]
    }
}

