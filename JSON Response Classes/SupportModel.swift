//
//  SupportModel.swift
//  Bakhter Money
//
//  Created by Mac on 30/05/2019.
//  Copyright © 2019 BakhterMoney. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class SupportModel: Mappable {
    
    var SenderUserId                        : Int?
    var SenderUserName                      : String?
    var SenderUserType                      : Int?
    var Status                              : String?
    var isReadAdmin                         : String?
    var isReadSender                        : String?
    var AddedDate                           : String?
    var Body                                : String?
    var ComplaintDate                       : String?
    var ComplaintId                         : Int?
    var ComplaintType                       : String?
    var PaymentID                           : Int?
    var PaymentNumber                       : String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        SenderUserId                        <- map["SenderUserId"]
        SenderUserName                      <- map["SenderUserName"]
        SenderUserType                      <- map["SenderUserType"]
        Status                              <- map["Status"]
        isReadAdmin                         <- map["isReadAdmin"]
        isReadSender                        <- map["isReadSender"]
        AddedDate                           <- map["AddedDate"]
        Body                                <- map["Body"]
        ComplaintDate                       <- map["ComplaintDate"]
        ComplaintId                         <- map["ComplaintId"]
        ComplaintType                       <- map["ComplaintType"]
        PaymentID                           <- map["PaymentID"]
        PaymentNumber                       <- map["PaymentNumber"]
    }
    
}
