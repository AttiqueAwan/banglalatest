//
//  CountryListDetail.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 12/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper

class CountryListDetail: Mappable {
    
    var CountryName      : String?
    var Iso2Code         : String?
    var Iso3Code         : String?
    var IsoNumericCode   : Int?
    var CurrencyIsoCode  : String?
    var Nationality      : String?
    var DialingCode      : Int?
    var Status           : String?
    
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        CountryName                <- map["CountryName"]
        Iso2Code                   <- map["Iso2Code"]
        Iso3Code                   <- map["Iso3Code"]
        IsoNumericCode             <- map["IsoNumericCode"]
        CurrencyIsoCode            <- map["CurrencyIsoCode"]
        Nationality                <- map["Nationality"]
        DialingCode                <- map["DialingCode"]
        Status                     <- map["Status"]
        
    }
    
}
