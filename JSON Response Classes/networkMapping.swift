//
//  networkMapping.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 28/08/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import Foundation
import ObjectMapper
class networkMapping: Mappable {
    
    
    var MobilePaymentTypeId           :     String?
    var CountryISOCode                :     String?
    var PaymentMethod                 :     String?
    var MobileCompanyName             :     String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        MobilePaymentTypeId         <-      map["MobilePaymentTypeId"]
        CountryISOCode              <-      map["CountryISOCode"]
        PaymentMethod               <-      map["PaymentMethod"]
        MobileCompanyName           <-      map["MobileCompanyName"]
        
    }
}
