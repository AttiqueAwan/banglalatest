//
//  TransactinList.swift
//  MTGlobal
//
//  Created by Softtech Media on 06/08/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class TransactionList: Mappable {
    
    
    var myAppResult                        :     AppResult?
    var AceTransList                       :     [TransactionListDetail]?
    var AceTransaction                     :     TransactionListresult?
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                            <-      map["result"]
        AceTransList                           <-      map["data"]
        AceTransaction                         <-      map["data"]
    }
}
