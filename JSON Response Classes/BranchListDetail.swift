//
//  BranchListDetail.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 19/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class BranchListDetail: Mappable {
    
    var BranchCode                       :     Int?
    var BranchPayCode                    :     Int?
    var BranchName                       :     String?
    var BranchAddress                    :     String?
    var BranhCity                        :     String?
    var ReceivingCountryIsoCode          :     String?
    var ReceivingCurrencyIsoCode         :     String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        BranchCode                             <-      map["BranchCode"]
        BranchPayCode                          <-      map["BranchPayCode"]
        BranchName                             <-      map["BranchName"]
        BranchAddress                          <-      map["BranchAddress"]
        BranhCity                              <-      map["BranhCity"]
        ReceivingCountryIsoCode                <-      map["ReceivingCountryIsoCode"]
        ReceivingCurrencyIsoCode               <-      map["ReceivingCurrencyIsoCode"]
        
    }
}

