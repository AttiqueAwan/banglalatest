//
//  SendingMethodResult.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 03/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class SendingMethodResult: Mappable {
    
    
    var myAppResult                :     AppResult?
    var SendingMehodList           :     [SendingMethodDetails]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                    <-      map["result"]
        SendingMehodList               <-      map["data"]
        
    }
}
