//
//  RecipientListDetail.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 13/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import ObjectMapper

class RecipientListDetail: Mappable {
    
    var UserID                           : Int?
    var BeneID                           : Int?
    var BenePayMethod                    : Int?
    var PayerId                          : Int?
    var BeneName                         : String?
    var BeneFirstName                    : String?
    var BeneLastName                     : String?
    var BeneAddress                      : String?
    var BeneCity                         : String?
    var BenePostCode                     : String?
    var BeneCountryIsoCode               : String?
    var BenePhone                        : String?
    var BeneBankName                     : String?
    var BeneBranchName                   : String?
    var BeneAccountNumber                : String?
    var BeneIBAN                         : String?
    var BeneBankCode                     : String?
    var BeneBankBranchCode               : String?
    var BeneRelationWithSender           : String?
    var BeneMiddleName                   : String?
    var BenemobileCompany                : String?
    var BeneMobilePaymentTypeId          : String?
    var MobileAccountNumber              : String?
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
            UserID                                     <- map["UserID"]
            BeneID                                     <- map["BeneID"]
            BenePayMethod                              <- map["BenePayMethod"]
            BeneName                                   <- map["BeneName"]
            BeneFirstName                              <- map["BeneFirstName"]
            BeneLastName                               <- map["BeneLastName"]
            BeneAddress                                <- map["BeneAddress"]
            BeneCity                                   <- map["BeneCity"]
            BenePostCode                               <- map["BenePostCode"]
            BeneCountryIsoCode                         <- map["BeneCountryIsoCode"]
            BenePhone                                  <- map["BenePhone"]
            BeneBankName                               <- map["BeneBankName"]
            BeneBranchName                             <- map["BeneBranchName"]
            BeneAccountNumber                          <- map["BeneAccountNumber"]
            BeneIBAN                                   <- map["BeneIBAN"]
            BeneBankCode                               <- map["BeneBankCode"]
            BeneBankBranchCode                         <- map["BeneBankBranchCode"]
            BeneRelationWithSender                     <- map["BeneRelationWithSender"]
            BeneMiddleName                             <- map["BeneMiddleName"]
            BenemobileCompany                          <- map["MobileCompanyName"]
            BeneMobilePaymentTypeId                    <- map["MobilePaymentTypeId"]
            PayerId                                    <- map["PayerId"]
            MobileAccountNumber                        <- map["MobileAccountNumber"]
    }
    
}

