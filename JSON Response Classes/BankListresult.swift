//
//  BankListresult.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 12/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class BankListresult: Mappable {
    
    
    var myAppResult                        :     AppResult?
    var BankList                           :     [BankListDetail]?
    var BankListDistrict                   :     [BankListModel2]?
    var BankListBranch                     :     [BankListModel3]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                                   <-      map["result"]
        BankList                                      <-      map["data"]
        
    }
}

class BankListresult2: Mappable {
    
    
    var myAppResult                        :     AppResult?
   
    var BankListDistrict                   :     [BankListModel2]?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                                   <-      map["result"]
        BankListDistrict                               <-      map["data"]
        
    }
}

class BankListresult3: Mappable {
    
    
    var myAppResult                        :     AppResult?
    var BankListBranch                     :     [BankListModel3]?

    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                                   <-      map["result"]
        BankListBranch                                   <-      map["data"]
        
    }
}
