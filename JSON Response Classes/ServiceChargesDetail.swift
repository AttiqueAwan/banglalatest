//
//  ServiceChargesDetail.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 20/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class ServiceChargesDetail: Mappable {
    
    
    var ChargesType             :    String?
    var Charges                 :    Double?
    var DiscountAmount          :    Double?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
       ChargesType                    <-      map["ChargesType"]
       Charges                        <-      map["Charges"]
       DiscountAmount                 <-      map["DiscountAmount"]
    }
}
