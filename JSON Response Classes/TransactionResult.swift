//
//  TransactionResult.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 04/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class TransactionResult: Mappable {
    
    
    var myAppResult                :     AppResult?
    var transactionDetail      :     TransactionListDetail?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        myAppResult                    <-      map["result"]
        transactionDetail              <-      map["data"]
        
    }
}
