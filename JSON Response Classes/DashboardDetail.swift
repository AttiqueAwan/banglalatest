//
//  DashboardDetail.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 12/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import ObjectMapper

class DashboardDetail: Mappable {
    
    var    TotalTransections              :     Int!
    var    CompletedTransections          :     Int!
    var    InCompletedTransections        :     Int!
    var    TotalIDoc                      :     Int!
    var    TotalBene                      :     Int!
    var    TotalAmount                    :     Double!
    var    IncentiveMaximumAmount         :     Double?
    var    AvailableCreditLimit90Days     :     Int?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
            TotalTransections              <-   map["TotalTransections"]
            CompletedTransections          <-   map["CompletedTransections"]
            InCompletedTransections        <-   map["InCompletedTransections"]
            TotalIDoc                      <-   map["TotalIDoc"]
            TotalBene                      <-   map["TotalBene"]
            TotalAmount                    <-   map["TotalAmount"]
            IncentiveMaximumAmount         <-   map["IncentiveMaximumAmount"]
            AvailableCreditLimit90Days     <-   map["AvailableCreditLimit90Days"]
      
    }
}
