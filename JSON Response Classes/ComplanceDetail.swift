//
//  ComplanceDetail.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 17/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class ComplanceDetail: Mappable {
    
    
    var ComplaintId             :  Int?
    var MessageId               :  Int?
    var SenderUserId            :  Int?
    var SenderUserType          :  String?
    var AddedDate               :  String?
    var MessageBody             :  String?
    
    
    required init?(map: Map) {
        
    }
    
    init(ComplaintId : Int , MessageId : Int , SenderUserId : Int , SenderUserType : String , AddedDate : String , MessageBody : String) {
        self.AddedDate = AddedDate
        self.ComplaintId = ComplaintId
        self.MessageId = MessageId
        self.MessageBody = MessageBody
        self.SenderUserId = SenderUserId
        self.SenderUserType = SenderUserType
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        ComplaintId            <- map["ComplaintId"]
        MessageId              <- map["MessageId"]
        SenderUserId           <- map["SenderUserId"]
        SenderUserType         <- map["SenderUserType"]
        AddedDate              <- map["AddedDate"]
        MessageBody            <- map["MessageBody"]
        
    }
}












