//
//  AppUserInfo.swift
//  MTGlobal
//
//  Created by Softtech Media on 20/07/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class AppUser: Mappable {
    
    
    var myAppResult             :     AppResult?
    var UserInfo                :     UserloginInfo?
    var AuthToken               :     AuthToken?
    var Data                    :     String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
      
        myAppResult                 <-      map["result"]
        UserInfo                    <-      map["data"]
        AuthToken                   <-      map["authToken"]
        Data                        <-      map["data"]
        
    }
}

class OTPdata: Mappable {
    
    var MessageId                :     Int?
    var To                       :     String?
    var Status                   :     String?
    var OTPToken                 :     String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        MessageId                <-      map["MessageId"]
        To                       <-      map["To"]
        Status                   <-      map["Status"]
        OTPToken                 <-      map["OTPToken"]
       
    }
}

class OTPResponse: Mappable {
    
    
    var myAppResult             :     AppResult?
    var OTPResp                 :     OTPdata?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
      
        myAppResult                 <-      map["result"]
        OTPResp                     <-      map["data"]
        
    }
}
