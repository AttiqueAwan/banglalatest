//
//  CountryListresult.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 12/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class CountryListresult: Mappable {
    
    
    var myAppResult                   :     AppResult?
    var CountryList                   :     [CountryListDetail]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                 <-      map["result"]
        CountryList                 <-      map["data"]
        
    }
}
