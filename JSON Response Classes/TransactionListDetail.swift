//
//  TransactionListDetail.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 22/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import ObjectMapper

class TransactionListDetail: Mappable {
    
    var UserID                          :  Int?
    var PaymentID                       :  Int?
    var PaymentNumber                   :  String?
    var PaymentMethod                   :  String?
    var SendingPaymentMethod            :  String?
    var PaymentDate                     :  String?
    var SendingCountry                  :  String?
    var SendingCountryIso3Code          :  String?
    var ReceivingCountry                :  String?
    var ReceivingCountryIso3Code        :  String?
    var SendingCurrency                 :  String?
    var ReceivingCurrency               :  String?
    var PayInAmount                     :  String?
    var PayOutAmount                    :  String?
    var ServiceCharges                  :  String?
    var ExchangeRate                    :  String?
    var SenderName                      :  String?
    var BeneName                        :  String?
    var BenePhone                       :  String?
    var PaymentStatus                   :  String?
    var PayerID                         :  Int?
    var PayerName                       :  String?
    var PayerBranchCode                 :  String?
    var PayOutBranchName                :  String?
    var ReceiverBankAccountNumber       :  String?
    var ReceiverBankAccountTitle        :  String?
    var ReceiverBankIBAN                :  String?
    var ReceiverBankCode                :  String?
    var ReceiverBankName                :  String?
    var ReceiverBranchName              :  String?
    var ReceiverBankBranchCode          :  String?
    var MobileAppPayment                :  String?
    var SenderNationality               :  String?
    var PaymentPageURL                  :  String?
    var PayoutBranchAddress             :  String?
    var SendingReason                   :  String?
    var IncentiveAmount                 :  Int?
   
    
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        UserID                                  <- map["UserID"]
        PaymentID                               <- map["PaymentID"]
        PaymentNumber                           <- map["PaymentNumber"]
        PaymentMethod                           <- map["PaymentMethod"]
        SendingPaymentMethod                    <- map["SendingPaymentMethod"]
        PaymentDate                             <- map["PaymentDate"]
        SendingCountry                          <- map["SendingCountry"]
        SendingCountryIso3Code                  <- map["SendingCountryIso3Code"]
        ReceivingCountry                        <- map["ReceivingCountry"]
        ReceivingCountryIso3Code                <- map["ReceivingCountryIso3Code"]
        SendingCurrency                         <- map["SendingCurrency"]
        ReceivingCurrency                       <- map["ReceivingCurrency"]
        PayInAmount                             <- map["PayInAmount"]
        PayOutAmount                            <- map["PayOutAmount"]
        ServiceCharges                          <- map["ServiceCharges"]
        ExchangeRate                            <- map["ExchangeRate"]
        SenderName                              <- map["SenderName"]
        BeneName                                <- map["BeneName"]
        BenePhone                               <- map["BenePhone"]
        PaymentStatus                           <- map["PaymentStatus"]
        PayerID                                 <- map["PayerID"]
        PayerName                               <- map["PayerName"]
        PayerBranchCode                         <- map["PayerBranchCode"]
        PayOutBranchName                        <- map["PayoutBranchName"]
        ReceiverBankAccountNumber               <- map["ReceiverBankAccountNumber"]
        ReceiverBankAccountTitle                <- map["ReceiverBankAccountTitle"]
        ReceiverBankIBAN                        <- map["ReceiverBankIBAN"]
        ReceiverBankCode                        <- map["ReceiverBankCode"]
        ReceiverBankName                        <- map["ReceiverBankName"]
        ReceiverBranchName                      <- map["ReceiverBranchName"]
        ReceiverBankBranchCode                  <- map["ReceiverBankBranchCode"]
        MobileAppPayment                        <- map["MobileAppPayment"]
        SenderNationality                       <- map["SenderNationality"]
        PaymentPageURL                          <- map["PaymentPageURL"]
        PayoutBranchAddress                     <- map["PayoutBranchAddress"]
        SendingReason                           <- map["SendingReason"]
        IncentiveAmount                         <- map["IncentiveAmount"]
    }
}


