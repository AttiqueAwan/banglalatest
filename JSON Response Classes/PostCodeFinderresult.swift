//
//  PostCodeFinderresult.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 17/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class PostCodeFinderresult: Mappable {
    
    
    var myAppResult                        :     AppResult?
    var PostCodeDetail                     :     [PostCodeFinderDetail]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                            <-      map["result"]
        PostCodeDetail                      <-        map["data"]
        
    }
}

class PostCodeFinderresultDetail: Mappable {
    
    
    var myAppResult                        :     AppResult?
    var PostCodeDetail                     :     [PostCodeFinderDetail2]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                            <-      map["result"]
        PostCodeDetail                      <-        map["data"]
        
    }
}

