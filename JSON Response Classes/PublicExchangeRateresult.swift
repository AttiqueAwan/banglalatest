//
//  PublicExchangeRateresult.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 19/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class PublicExchangeRateresult: Mappable {
    
    
    var myAppResult             :     AppResult?
    var ExchangeRateDetail      :     [PublicExchangeRateDetail]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                 <-      map["result"]
        ExchangeRateDetail          <-      map["data"]
        
    }
}

