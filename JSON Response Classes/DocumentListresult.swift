//
//  DocumentListresult.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 15/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class DocumentListresult: Mappable {
    
    
    var myAppResult             :     AppResult?
    var DocumentList           :     [DocumentListDetail]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                 <-      map["result"]
        DocumentList               <-      map["data"]
        
    }
}

