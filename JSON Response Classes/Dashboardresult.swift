//
//  Dashboardresult.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 12/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class Dashboardresult: Mappable {
    
    
    var myAppResult             :     AppResult?
    var UserInfo                :     UserloginInfo?
    var dashboard               :     DashboardDetail?
   
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                 <-      map["result"]
        UserInfo                    <-      map["data"]
        dashboard                   <-      map["dashboard"]
        
    }
}
