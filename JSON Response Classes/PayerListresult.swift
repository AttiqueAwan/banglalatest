//
//  PayerListresult.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 12/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class PayerListresult: Mappable {
    
    
    var myAppResult             :     AppResult?
    var PayerList               :     [PayerListDetail]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                 <-      map["result"]
        PayerList                   <-      map["data"]
        
    }
}

