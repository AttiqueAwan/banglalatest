//
//  ListingTypesDetail.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 23/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class ListingTypesDetail: Mappable {
    
    
    var ID                :     String?
    var Text              :     String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        ID         <-      map["Id"]
        Text       <-      map["Text"]
        
    }
}



