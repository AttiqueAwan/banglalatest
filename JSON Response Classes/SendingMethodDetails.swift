//
//  SendingMethodResult.swift
//  BanglaRemitt
//
//  Created by Soft Tech Media on 03/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//
import UIKit
import ObjectMapper
class SendingMethodDetails: Mappable {
    
    
    var PaymentMethodID                :     Int?
    var PaymentMethodName              :     String?
    var PaymentMethodCode              :     String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        PaymentMethodID         <-      map["PaymentMethodID"]
        PaymentMethodName       <-      map["PaymentMethodName"]
        PaymentMethodCode       <-      map["PaymentMethodCode"]
        
    }
}
