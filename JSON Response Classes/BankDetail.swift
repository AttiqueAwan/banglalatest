//
//  BankDetail.swift
//  BanglaRemitt
//
//  Created by Apple on 06/05/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//


import UIKit
import ObjectMapper

class BankDetail: Mappable {
    
   
    var myAppResult                        :     AppResult?
    var bankDetailList                     :     [BankDetailList]?

   required init?(map: Map) {
          
      }
    
    // Mappable
       func mapping(map: Map) {
           
           
           myAppResult                            <-      map["result"]
           bankDetailList                      <-        map["data"]
           
       }

}

//
//import UIKit
//import ObjectMapper
//class PostCodeFinderresult: Mappable {
//
//
//    var myAppResult                        :     AppResult?
//    var PostCodeDetail                     :     [PostCodeFinderDetail]?
//
//    required init?(map: Map) {
//
//    }
//
//    // Mappable
//    func mapping(map: Map) {
//
//
//        myAppResult                            <-      map["result"]
//        PostCodeDetail                      <-        map["data"]
//
//    }
//}
