//
//  RecipientListresult.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 13/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class RecipientListresult: Mappable {
    
    
    var myAppResult             :     AppResult?
    var RecipientList           :     [RecipientListDetail]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                 <-      map["result"]
        RecipientList               <-      map["data"]
        
    }
}

