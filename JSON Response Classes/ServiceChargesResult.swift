//
//  ServiceChargesResult.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 20/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class ServiceChargesResult: Mappable {
    
    
    var myAppResult             :     AppResult?
    var ServiceCharges          :     ServiceChargesDetail?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                      <-      map["result"]
        ServiceCharges                   <-      map["data"]
        
    }
}


