//
//  UserLoginInfo.swift
//  MTGlobal
//
//  Created by Softtech Media on 20/07/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class UserloginInfo: Mappable {
    
    var aagentCurPrfxId : String!
    var address : String!
    var agentBaseCurrency : String!
    var agentLimitAmount : Int!
    var agentLimitType : String!
    var agentPromoCode : String!
    var agentSign : String!
    var agentStatus : String!
    var agentType : String!
    var agentUserName : String!
    var birthDay : Int!
    var birthMonth : Int!
    var birthYear : Int!
    var city : String!
    var countryIsoCode : String!
    var countryOfBirthIsoCode : String!
    var currencyIsoCode : String!
    var customerID : Int!
    var deviceType : String!
    var domainID : Int!
    var email : String!
    var employer : String!
    var firstName : String!
    var fullName : String!
    var gender : String!
    var houseNo : String!
    var iD : Int!
    var lastName : String!
    var nationalityIsoCode : String!
    var occupation : String!
    var password : String!
    var phone : String!
    var photo : String!
    var postalCode : String!
    var referralCode : String!
    var senderStatus : String!
    var timeZoneHours : String!
    var timeZoneMinunts : String!
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        aagentCurPrfxId                    <-         map["AagentCurPrfxId"]
        address                            <-         map["Address"]
        agentBaseCurrency                  <-         map["AgentBaseCurrency"]
        agentLimitAmount                   <-         map["AgentLimitAmount"]
        agentLimitType                     <-         map["AgentLimitType"]
        agentPromoCode                     <-         map["AgentPromoCode"]
        agentSign                          <-         map["AgentSign"]
        agentStatus                        <-         map["AgentStatus"]
        agentType                          <-         map["AgentType"]
        agentUserName                      <-         map["AgentUserName"]
        birthDay                           <-         map["BirthDay"]
        birthMonth                         <-         map["BirthMonth"]
        birthYear                          <-         map["BirthYear"]
        city                               <-         map["City"]
        countryIsoCode                     <-         map["CountryIsoCode"]
        countryOfBirthIsoCode              <-         map["CountryOfBirthIsoCode"]
        currencyIsoCode                    <-         map["CurrencyIsoCode"]
        customerID                         <-         map["CustomerID"]
        deviceType                         <-         map["DeviceType"]
        domainID                           <-         map["DomainID"]
        email                              <-         map["Email"]
        employer                           <-         map["Employer"]
        firstName                          <-         map["FirstName"]
        fullName                           <-         map["FullName"]
        gender                             <-         map["Gender"]
        houseNo                            <-         map["HouseNo"]
        iD                                 <-         map["ID"]
        lastName                           <-         map["LastName"]
        nationalityIsoCode                 <-         map["NationalityIsoCode"]
        occupation                         <-         map["Occupation"]
        password                           <-         map["Password"]
        phone                              <-         map["Phone"]
        photo                              <-         map["Photo"]
        postalCode                         <-         map["PostalCode"]
        referralCode                       <-         map["ReferralCode"]
        senderStatus                       <-         map["SenderStatus"]
        timeZoneHours                      <-         map["TimeZoneHours"]
        timeZoneMinunts                    <-         map["TimeZoneMinunts"]
    }
}
